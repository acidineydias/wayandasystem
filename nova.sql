-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 31-Jan-2017 às 05:25
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nova`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbbanco`
--

CREATE TABLE IF NOT EXISTS `tbbanco` (
  `nomebanco` varchar(50) NOT NULL,
  `contabanco` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`nomebanco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tbbanco`
--

INSERT INTO `tbbanco` (`nomebanco`, `contabanco`) VALUES
('BCI', '5560924410'),
('BIC', '9900985'),
('IBAM', 'A006.0051.0000.5560.9244.1011.3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbconfirmar`
--

CREATE TABLE IF NOT EXISTS `tbconfirmar` (
  `idaluno` int(11) NOT NULL AUTO_INCREMENT,
  `aluno` int(11) DEFAULT NULL,
  `curso` varchar(45) NOT NULL,
  `classe` int(11) NOT NULL,
  `sala` int(11) NOT NULL,
  `turno` varchar(45) NOT NULL,
  `ano` int(11) NOT NULL,
  `status` varchar(45) NOT NULL,
  `dataConf` date DEFAULT NULL,
  `saldo` int(11) DEFAULT '0',
  PRIMARY KEY (`idaluno`),
  KEY `fk` (`aluno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `tbconfirmar`
--

INSERT INTO `tbconfirmar` (`idaluno`, `aluno`, `curso`, `classe`, `sala`, `turno`, `ano`, `status`, `dataConf`, `saldo`) VALUES
(1, 1, 'Técnico de Informática', 12, 16, 'Tarde', 2016, 'Não Repetente', '2016-10-13', 120000),
(3, 1, 'Técnico de Informática', 10, 5, 'Manhã', 2014, 'Não Repetente', '2014-10-13', 4000),
(5, 8, 'Técnico de Informática', 12, 16, 'Tarde', 2016, 'Não Repetente', '2016-11-09', 16000),
(6, 3, 'Técnico de Informática', 12, 16, 'Tarde', 2016, 'Não Repetente', '2016-11-09', 0),
(7, 7, 'Técnico de Informática', 10, 5, 'Manhã', 2010, 'Não Repetente', '2016-11-17', 0),
(8, 10, 'Construção Civil', 10, 16, 'Tarde', 2016, 'Não Repetente', '2016-11-25', 0),
(9, 11, 'Técnico de Informática', 12, 5, 'Tarde', 2016, 'Não Repitente', '2016-11-30', 0),
(10, 12, 'Técnico de Informática', 10, 5, 'Manhã', 2016, 'Não Repitente', '2016-11-30', 4000);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbdeposito`
--

CREATE TABLE IF NOT EXISTS `tbdeposito` (
  `iddep` int(11) NOT NULL AUTO_INCREMENT,
  `alunoid` int(11) DEFAULT NULL,
  `talao` int(11) unsigned NOT NULL,
  `valordep` int(11) DEFAULT NULL,
  `datadep` date DEFAULT NULL,
  `banco` varchar(45) DEFAULT NULL,
  `dataact` date DEFAULT NULL,
  `anoal` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iddep`),
  UNIQUE KEY `talao_UNIQUE` (`talao`),
  KEY `fkban` (`banco`),
  KEY `fklk` (`alunoid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Extraindo dados da tabela `tbdeposito`
--

INSERT INTO `tbdeposito` (`iddep`, `alunoid`, `talao`, `valordep`, `datadep`, `banco`, `dataact`, `anoal`) VALUES
(10, 1, 23432525, 16000, '2016-11-16', 'BCI', '2016-11-16', '2016'),
(11, 1, 14114, 16000, '2016-11-16', 'IBAM', '2016-11-16', '2016'),
(12, 1, 325752523, 64000, '2016-11-21', 'BCI', '2016-11-21', '2016'),
(13, 3, 235235, 48000, '2016-11-21', 'BCI', '2016-11-21', '2016'),
(14, 1, 34553, 200000, '2016-11-25', 'BCI', '2016-11-25', '2016'),
(16, 1, 325252, 32000, '2016-11-16', 'BCI', '2016-11-16', '2014'),
(17, 8, 6071996, 16000, '2016-11-29', 'BCI', '2016-11-29', '2016'),
(18, 8, 454343466, 32000, '2016-11-29', 'BCI', '2016-11-29', '2016'),
(19, 12, 456636, 32000, '2016-11-30', 'BIC', '2016-11-30', '2016');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbmatri`
--

CREATE TABLE IF NOT EXISTS `tbmatri` (
  `processo` int(11) NOT NULL AUTO_INCREMENT,
  `bi` varchar(45) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `genero` varchar(45) DEFAULT NULL,
  `nomepai` varchar(45) DEFAULT NULL,
  `nomemae` varchar(45) DEFAULT NULL,
  `morada` varchar(45) NOT NULL,
  `data` date NOT NULL,
  `anoMat` int(11) NOT NULL,
  `dataMat` date NOT NULL,
  PRIMARY KEY (`processo`),
  UNIQUE KEY `bi_UNIQUE` (`bi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `tbmatri`
--

INSERT INTO `tbmatri` (`processo`, `bi`, `nome`, `genero`, `nomepai`, `nomemae`, `morada`, `data`, `anoMat`, `dataMat`) VALUES
(1, 'AZ3699L3FA8', 'Lourenço Daniel Sabastião Carlos', 'Femenino', 'Luyindula Daniel Carlos', 'Rita Sebastião Luzolo', 'Viana/Luanda Sul', '1998-05-29', 2016, '2016-10-25'),
(3, 'FL236464LOJ', 'Manuel João Afonso Bonefacio', 'Masculino', 'Nascimento Bonefacio', 'Teresa Samba', 'Viana/Jacinto Tchipa', '1995-12-28', 2016, '2016-10-13'),
(4, 'DFH79768LK', 'Amado Panzo', 'Masculino', '********************', 'Joana', 'Viana/Luanda Sul', '1996-03-09', 2016, '2016-10-25'),
(7, 'BVAJ7779779P', 'Joaquim Delegado', 'Masculino', '*******************', '***********************', 'Viana/Kilamba', '1997-06-02', 2015, '2015-10-13'),
(8, 'IKD7858JHGJGG', 'Benjamim Tiago Moço', 'Masculino', 'Antonio Moço', 'Delfina Tiago', 'Viana/Porto Seco', '1996-07-06', 2016, '2016-11-09'),
(9, 'NDH79797HH', 'Acidiney Dias', 'Masculino', 'Antonio Dias', 'Celeste Dias', 'Viana/Luanda Sul', '2000-02-11', 2016, '2016-11-09'),
(10, 'DTHDYFJM', 'Marcos', 'Masculino', 'Fabio', 'Marta', 'Viana', '1998-11-25', 2016, '2016-11-25'),
(11, '97555757', 'Hairton', 'Masculino', 'simao ntoni', 'isa palanca', 'anola', '2016-11-30', 2016, '2016-11-30'),
(12, '462646246', 'Hairton Vanda', 'Masculino', 'Simão Ntoni', 'Isabel Palanca', 'Zango-0', '1999-08-02', 2016, '2016-11-30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbpagamento`
--

CREATE TABLE IF NOT EXISTS `tbpagamento` (
  `idprop` int(11) NOT NULL AUTO_INCREMENT,
  `al` int(11) DEFAULT NULL,
  `nummes` int(11) NOT NULL,
  `descmes` varchar(45) NOT NULL DEFAULT 'Nenhum',
  `datapag` date NOT NULL,
  `user` varchar(45) DEFAULT NULL,
  `anopag` int(11) DEFAULT NULL,
  PRIMARY KEY (`idprop`),
  KEY `fgbvb` (`al`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Extraindo dados da tabela `tbpagamento`
--

INSERT INTO `tbpagamento` (`idprop`, `al`, `nummes`, `descmes`, `datapag`, `user`, `anopag`) VALUES
(3, 1, 1, 'Fevereiro', '2016-11-20', 'Sara', 2016),
(5, 1, 1, 'Março', '2016-11-20', 'Sara', 2016),
(6, 1, 1, 'Abril', '2016-11-20', 'l', 2016),
(7, 1, 1, 'Maio', '2016-11-20', 'l', 2016),
(8, 1, 1, 'Junho', '2016-11-20', 'l', 2016),
(9, 1, 1, 'Julho', '2016-11-21', 'l', 2016),
(11, 3, 1, 'Fevereiro', '2016-11-21', 'Sara', 2016),
(12, 1, 1, 'Fevereiro', '2016-11-29', 'l', 2014),
(13, 1, 1, 'Agosto', '2016-11-29', 'l', 2016),
(14, 1, 1, 'Março', '2016-11-29', 'l', 2014),
(15, 1, 1, 'Setembro', '2016-11-29', 'l', 2016),
(16, 1, 1, 'Outubro', '2016-11-29', 'Sara', 2016),
(17, 1, 1, 'Novembro', '2016-11-29', 'Sara', 2016),
(18, 8, 1, 'Fevereiro', '2016-11-29', 'Sara', 2016),
(19, 1, 1, 'Dezembro', '2016-11-29', 'Sara', 2016),
(20, 8, 1, 'Março', '2016-11-29', 'Sara', 2016);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbutilizador`
--

CREATE TABLE IF NOT EXISTS `tbutilizador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `passe` varchar(45) DEFAULT NULL,
  `nivel` varchar(45) NOT NULL DEFAULT 'Normal',
  `pergunta` varchar(45) DEFAULT NULL,
  `resposta` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`usuario`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `tbutilizador`
--

INSERT INTO `tbutilizador` (`id`, `usuario`, `passe`, `nivel`, `pergunta`, `resposta`) VALUES
(7, 'Hairton', '37Y4L2B5HJ0', 'Administrador', 'Nome da Namorada', 'Alice'),
(4, 'l', '1', 'Administrador', 'Primeiro Nome do pai?', 'lolo'),
(5, 'lolo', 'lolo', 'Administrador', 'Namorada', 'LY'),
(6, 'Maria', 'hada', 'Normal', 'Namorado', 'nando'),
(3, 'Mauricio', 'cacabgi12', 'Administrador', 'Primeiro Nome do pai?', 'mauro'),
(2, 'Sara', 'mariano', 'Normal', 'Primeiro Nome do pai?', 'gomes');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tbconfirmar`
--
ALTER TABLE `tbconfirmar`
  ADD CONSTRAINT `tbconfirmar_ibfk_1` FOREIGN KEY (`aluno`) REFERENCES `tbmatri` (`processo`);

--
-- Limitadores para a tabela `tbdeposito`
--
ALTER TABLE `tbdeposito`
  ADD CONSTRAINT `fklk` FOREIGN KEY (`alunoid`) REFERENCES `tbconfirmar` (`aluno`),
  ADD CONSTRAINT `tbdeposito_ibfk_1` FOREIGN KEY (`banco`) REFERENCES `tbbanco` (`nomebanco`);

--
-- Limitadores para a tabela `tbpagamento`
--
ALTER TABLE `tbpagamento`
  ADD CONSTRAINT `fgbvb` FOREIGN KEY (`al`) REFERENCES `tbconfirmar` (`idaluno`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
