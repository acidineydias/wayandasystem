﻿namespace PPA
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_nome = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.Tempo = new System.Windows.Forms.Timer(this.components);
            this.lbdate = new System.Windows.Forms.Label();
            this.uS_splash1 = new PPA.US_splash();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label4.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(40, 559);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(172, 18);
            this.label4.TabIndex = 17;
            this.label4.Text = "Este produto é licenciado para :";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(37, 473);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 18);
            this.label3.TabIndex = 16;
            this.label3.Text = "Todos os direitos reservados";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(37, 450);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(305, 18);
            this.label2.TabIndex = 15;
            this.label2.Text = "Este programa é protegido pelas leis de direitos autorais.";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(38, 495);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 18);
            this.label1.TabIndex = 14;
            this.label1.Text = "© 2016 Halobema Corporation";
            // 
            // lb_nome
            // 
            this.lb_nome.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lb_nome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.lb_nome.Font = new System.Drawing.Font("Malgun Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.lb_nome.ForeColor = System.Drawing.Color.White;
            this.lb_nome.Location = new System.Drawing.Point(215, 563);
            this.lb_nome.Name = "lb_nome";
            this.lb_nome.Size = new System.Drawing.Size(152, 17);
            this.lb_nome.TabIndex = 13;
            this.lb_nome.Text = "#####";
            this.lb_nome.Click += new System.EventHandler(this.lb_nome_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(0, 13);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(0, 0);
            this.progressBar1.TabIndex = 18;
            this.progressBar1.Visible = false;
            // 
            // Tempo
            // 
            this.Tempo.Enabled = true;
            this.Tempo.Tick += new System.EventHandler(this.Tempo_Tick);
            // 
            // lbdate
            // 
            this.lbdate.AutoSize = true;
            this.lbdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.lbdate.Location = new System.Drawing.Point(407, 576);
            this.lbdate.Name = "lbdate";
            this.lbdate.Size = new System.Drawing.Size(0, 13);
            this.lbdate.TabIndex = 19;
            this.lbdate.Visible = false;
            // 
            // uS_splash1
            // 
            this.uS_splash1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.uS_splash1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uS_splash1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uS_splash1.Location = new System.Drawing.Point(0, 0);
            this.uS_splash1.Name = "uS_splash1";
            this.uS_splash1.Size = new System.Drawing.Size(471, 598);
            this.uS_splash1.TabIndex = 0;
            this.uS_splash1.Load += new System.EventHandler(this.uS_splash1_Load);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(471, 598);
            this.Controls.Add(this.lbdate);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lb_nome);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.uS_splash1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private US_splash uS_splash1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_nome;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer Tempo;
        private System.Windows.Forms.Label lbdate;
    }
}

