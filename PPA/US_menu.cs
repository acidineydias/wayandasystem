﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace PPA
{
    public partial class US_menu : UserControl
    {
        Banco ban = new Banco();




        public US_menu()
        {
            InitializeComponent();
           
            //Propina
            ban.ListarVerProp(c, c1, c2, c3, c4, c5, c6, c7);


            ban.ListaalterDeletarCad(l1, l2, l3, l4, l5, l6, l7, l8, l9);


            ban.ListaalterDeletarConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10);

            ban.ListarDeposito(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal);

            ban.ListarPropina(a, a1, a2, a3, a4, a6, a5, ax1, ax2, ax3, ax4, ax5);

            lb_data.Text += " " + DateTime.Now.ToShortDateString();
            lbNomeUsuario.Text = Properties.Settings.Default.NomeUsuario;
            lbNivel.Text = Properties.Settings.Default.Nivel;

            //dg.DataSource = ban.ListarMatricula(); dg_alt_del_Mat.DataSource = ban.ListarMatricula();

            dgConf.DataSource = ban.ListarConfirmacao();

            dgCompleto.DataSource = ban.ListarTotal();
            dg.DataSource = ban.ListarMatricula();

        }

        private void US_menu_Load(object sender, EventArgs e)
        {
            ParentForm.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;
            tb_An.Items.Add(System.DateTime.Now.ToString("yyyy"));
            tb_d.Items.Add(System.DateTime.Now.ToString("yyyy"));
            tbAno_alt_del_Conf.Items.Add(System.DateTime.Now.ToString("yyyy"));
            if (lbNivel.Text == "Normal")
            {
                btnAbriAlt_del_Matr.Enabled = false;
                btnAbriralt_del_Conf.Enabled = false;
                button55.Enabled = false;
                button57.Enabled = false;

                lb_ferra.Visible = false;
                btn_ferra.Visible = false;

                btn_actDep.Enabled = false;
                btn_deletDep.Enabled = false;


            }
            else
            {

            }


        }



        private void btnEstast_MouseLeave(object sender, EventArgs e)
        {

        }

        private void btnPropina_Click(object sender, EventArgs e)
        {
            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.DodgerBlue;
            TB_CENTRAL.SelectedTab = tpagePROPRINA;
        }

        private void lb_data_Click(object sender, EventArgs e)
        {

        }

        private void lb_fech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MessageBox.Show("Desejas sair?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void lbMini_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ParentForm.WindowState = FormWindowState.Minimized;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void btnAluno_Click(object sender, EventArgs e)
        {
            ban.ListaalterDeletarCad(l1, l2, l3, l4, l5, l6, l7, l8, l9);
            ban.ListaalterDeletarConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10);
            btnAluno.ForeColor = System.Drawing.Color.DodgerBlue;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.Gray;
            TB_CENTRAL.SelectedTab = tpageALUNO;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_pub;

            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;





        }

        private void btnPag_Click(object sender, EventArgs e)
        {

            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.DodgerBlue;
            btnPropina.ForeColor = System.Drawing.Color.Gray;
            //TB_CENTRAL.SelectedTab = tpagePAGAMENT;
        }

        private void btnInicio_Click(object sender, EventArgs e)
        {
            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.DodgerBlue;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.Gray;
            TB_CENTRAL.SelectedTab = tpagINICIO;
        }

        private void btnDeposit_Click(object sender, EventArgs e)
        {
            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.DodgerBlue;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.Gray;
            TB_CENTRAL.SelectedTab = tpageDEPOSITO;
        }

        private void btnEstast_Click(object sender, EventArgs e)
        {
            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.DodgerBlue;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.Gray;
            TB_CENTRAL.SelectedTab = tpageEstatis;
        }

        //############# MATRICULA ##########################

        private void btn_cadMarti_Click(object sender, EventArgs e)
        {
            if (
                tb_nome.Text != "" &&
            tb_morada.Text != "" &&

            tb_bi.Text != "" &&
            tb_pai.Text != "" &&
            tb_mae.Text != ""
            )
            {
                Matricular();
                dg.DataSource = ban.ListarMatricula();

                tb_nome.Text = "";
                tb_morada.Text = "";
                tb_genero.Text = "Masculino";
                da.Value = DateTime.Now;
                tb_bi.Text = "";
                tb_pai.Text = "";
                tb_mae.Text = "";
            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void Matricular()
        {

            ArrayList arr = new ArrayList();
            try
            {
                arr.Add(tb_nome.Text);
                arr.Add(tb_morada.Text);
                arr.Add(tb_genero.Text);
                arr.Add(da.Value.ToString("yyyy-MM-dd"));
                arr.Add(tb_bi.Text);
                arr.Add(tb_pai.Text);
                arr.Add(tb_mae.Text);
                arr.Add(tb_An.Text);
                arr.Add(dataHoje.Value.ToString("yyyy-MM-dd"));

                if (ban.MATRICULAR(arr))
                {
                    MessageBox.Show("Cadastrado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Não Casdrastrou", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro ao inserir", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }


        private void btnTerminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desejas Terminar a Sessão?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                frm_control fg = new frm_control();
                fg.Show();
                this.Hide();
            }
        }


        private void btnPesqCad_Click(object sender, EventArgs e)
        {


        }

        //Confirmar Insert

        public void Confirmar()
        {
            Banco ban = new Banco();
            ArrayList arr = new ArrayList();
            try
            {
                arr.Add(tb_confMatri.Text);
                arr.Add(tb_curso.Text);
                arr.Add(tb_classe.Text);
                arr.Add(tb_sala.Text);
                arr.Add(tb_turno.Text);
                arr.Add(tb_anoConf.Text);
                arr.Add(tb_status.Text);
                arr.Add(dataHoje.Value.ToString("yyyy-MM-dd"));

                if (ban.CONFIRMAR(arr))
                {
                    MessageBox.Show("Cadastrado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Não Castrado", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro ao inserir", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Confir_Click(object sender, EventArgs e)
        {
            if (
                 tb_confMatri.Text != "" &&
            tb_curso.Text != "" &&
            tb_classe.Text != "" &&
            tb_sala.Text != "" &&
            tb_turno.Text != "" &&
            tb_anoConf.Text != "" &&
            tb_status.Text != ""
                )
            {
                Confirmar();
                dgConf.DataSource = ban.ListarConfirmacao();
                tb_confMatri.Text = "";
                tb_curso.Text = "";
                tb_classe.Text = "";
                tb_sala.Text = "";
                tb_turno.Text = "";
                tb_anoConf.Text = "";
                tb_status.Text = "";
                dataHoje.Value = DateTime.Now;
            }

            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CbAn_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btn_pesConf_Click(object sender, EventArgs e)
        {
            string sql;


            int num;
            try
            {
                sql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and  ano=@valor and nome like @valor2";
                if (tb_AnC.Text != "Todos Anos")
                {
                    num = int.Parse(tb_AnC.Text.ToString());


                    dgConf.DataSource = ban.PesqConfirmacaoComposta(sql, num, "%" + tb_pesqConf.Text + "%");
                }
                if (tb_AnC.Text == "Todos Anos")
                {
                    dgConf.DataSource = ban.PesqCormacao("%" + tb_pesqConf.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");

            }

        }

        private void tb_AnC_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and  ano=@valor and nome like @valor2";
                if (tb_AnC.Text != "Todos Anos")
                {
                    num = int.Parse(tb_AnC.Text.ToString());


                    dgConf.DataSource = ban.PesqConfirmacaoComposta(sql, num, "%" + tb_pesqConf.Text + "%");
                }
                if (tb_AnC.Text == "Todos Anos")
                {
                    dgConf.DataSource = ban.PesqCormacao("%" + tb_pesqConf.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void btn_pesqComleto_Click(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and ano=@valor and nome like @valor2";
                if (Cb_AnoCompleto.Text != "Todos Anos")
                {
                    num = int.Parse(Cb_AnoCompleto.Text.ToString());


                    dgCompleto.DataSource = ban.PesqTotalComposta(sql, num, "%" + tb_pesqCompleto.Text + "%");
                }
                if (Cb_AnoCompleto.Text == "Todos Anos")
                {
                    dgCompleto.DataSource = ban.PesqTotal("%" + tb_pesqCompleto.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }

        }

        private void Cb_AnoCompleto_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and ano=@valor and nome like @valor2";
                if (Cb_AnoCompleto.Text != "Todos Anos")
                {
                    num = int.Parse(Cb_AnoCompleto.Text.ToString());


                    dgCompleto.DataSource = ban.PesqTotalComposta(sql, num, "%" + tb_pesqCompleto.Text + "%");
                }
                if (Cb_AnoCompleto.Text == "Todos Anos")
                {
                    dgCompleto.DataSource = ban.PesqTotal("%" + tb_pesqCompleto.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void button54_Click(object sender, EventArgs e)
        {

            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.Silver;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;

            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Cadastrar;
        }

        private void button53_Click(object sender, EventArgs e)
        {
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.Silver;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Confirmar;
        }

        private void button56_Click(object sender, EventArgs e)
        {
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.Silver;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_PesMat_Conf;
        }

        private void button55_Click(object sender, EventArgs e)
        {
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.Silver;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Alt_det_Matr;
        }

        private void button57_Click(object sender, EventArgs e)
        {
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.Silver;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Alt_det_Conf;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpageALUNO;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Cadastrar;

            ban.ListaalterDeletarCad(l1, l2, l3, l4, l5, l6, l7, l8, l9);
            ban.ListaalterDeletarConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10);
            btnAluno.ForeColor = System.Drawing.Color.DodgerBlue;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.Gray;

            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.Silver;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpageALUNO;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Confirmar;

            ban.ListaalterDeletarCad(l1, l2, l3, l4, l5, l6, l7, l8, l9);
            ban.ListaalterDeletarConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10);
            btnAluno.ForeColor = System.Drawing.Color.DodgerBlue;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.Gray;

            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.Silver;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
        }

        private void chec_verConf_CheckedChanged(object sender)
        {
            if (chec_verConf.Checked)
            {
                lp.Visible = true;
                lpA.Visible = true;
                tb_pesqConf.Visible = true;
                btn_pesConf.Visible = true;
                tb_AnC.Visible = true;
                rb_nomeConf.Visible = true;
                dgConf.Visible = true;

            }
            else
            {
                lp.Visible = false;
                lpA.Visible = false;
                tb_pesqConf.Visible = false;
                btn_pesConf.Visible = false;
                tb_AnC.Visible = false;
                rb_nomeConf.Visible = false;
                dgConf.Visible = false;
            }
        }

        private void chec_verMat_CheckedChanged(object sender)
        {

            if (chec_verMat.Checked)
            {

                lpMat.Visible = true;
                lpMatAno.Visible = true;
                tb_pesqCad.Visible = true;
                btnPesqCad.Visible = true;
                CbAn.Visible = true;
                rb_nome2.Visible = true;
                dg.Visible = true;

            }
            else
            {
                lpMat.Visible = false;
                lpMatAno.Visible = false;
                tb_pesqCad.Visible = false;
                btnPesqCad.Visible = false;
                CbAn.Visible = false;
                rb_nome2.Visible = false;
                dg.Visible = false;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpageALUNO;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Cadastrar;
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.Silver;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
        }

        private void CbAn_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string sql;


            int num;
            try
            {

                sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula  from tbmatri where anoMat=@valor and nome like @valor2 order by processo Asc";
                if (CbAn.Text != "Todos Anos")
                {
                    num = int.Parse(CbAn.Text.ToString());


                    dg.DataSource = ban.PesqMatriculaComposta(sql, num, "%" + tb_pesqCad.Text + "%");
                }
                if (CbAn.Text == "Todos Anos")
                {
                    dg.DataSource = ban.PesqMatricula("%" + tb_pesqCad.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo!", "Erro Basico");
            }
        }

        private void label49_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desejas Terminar a Sessão?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                frm_control fg = new frm_control();
                fg.Show();
                this.Hide();
            }
        }

        private void btnAbrir_Cad_matr_Click(object sender, EventArgs e)
        {

            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.Silver;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;

            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Cadastrar;
        }

        private void btnAbrirConf_Click(object sender, EventArgs e)
        {
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.Silver;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Confirmar;
        }

        private void btnAbrirSele_alu_Click(object sender, EventArgs e)
        {

            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.Silver;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_PesMat_Conf;
        }

        private void btnAbriAlt_del_Matr_Click(object sender, EventArgs e)
        {
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.Silver;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Alt_det_Matr;

            ban.ListaalterDeletarCad(l1, l2, l3, l4, l5, l6, l7, l8, l9);


        }

        private void btnAbriralt_del_Conf_Click(object sender, EventArgs e)
        {
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.Silver;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Alt_det_Conf;

            ban.ListaalterDeletarConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpageALUNO;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Cadastrar;
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.Silver;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpageALUNO;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_CRUD;
            dentroAlu_CRUD.SelectedTab = tp_Cadastrar;
            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.Silver;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            btnAluno.ForeColor = System.Drawing.Color.DodgerBlue;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.Gray;
            TB_CENTRAL.SelectedTab = tpageALUNO;
            tabContestadentrodoAluno.SelectedTab = tpdentroAlu_pub;

            btnAbrir_Cad_matr.ForeColor = System.Drawing.Color.White;
            btnAbrirSele_alu.ForeColor = System.Drawing.Color.White;
            btnAbriralt_del_Conf.ForeColor = System.Drawing.Color.White;
            btnAbrirConf.ForeColor = System.Drawing.Color.White;
            btnAbriAlt_del_Matr.ForeColor = System.Drawing.Color.White;

        }


        private void btnPesqCad_Click_1(object sender, EventArgs e)
        {
            string sql;


            int num;
            try
            {
                sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula  from tbmatri where anoMat=@valor and nome like @valor2 order by processo Asc";
                if (CbAn.Text != "Todos Anos")
                {
                    num = int.Parse(CbAn.Text.ToString());


                    dg.DataSource = ban.PesqMatriculaComposta(sql, num, "%" + tb_pesqCad.Text + "%");
                }
                if (CbAn.Text == "Todos Anos")
                {
                    dg.DataSource = ban.PesqMatricula("%" + tb_pesqCad.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }

        }

        private void btnPesq_alt_del_Mat_Click(object sender, EventArgs e)
        {

            string sql;


            int num;

            try
            {

                sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula  from tbmatri where anoMat=@valor and nome like @valor2 order by processo Asc";
                if (cbAninho_alt_del_Mat.Text != "Todos Anos")
                {
                    num = int.Parse(cbAninho_alt_del_Mat.Text.ToString());


                    ban.DeleteAlterPesqMatriculaComposta(l1, l2, l3, l4, l5, l6, l7, l8, l9, sql, num, "%" + tbPesq_alt_del_Mat.Text + "%");
                }
                if (cbAninho_alt_del_Mat.Text == "Todos Anos")
                {
                    ban.DeleAlterPesqMatricula(l1, l2, l3, l4, l5, l6, l7, l8, l9, "%" + tbPesq_alt_del_Mat.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }



        private void dg_alt_del_Mat_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dg_alt_del_Mat_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void dg_alt_del_Mat_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tb_pesqCad_TextChanged(object sender, EventArgs e)
        {
            string sql;


            int num;
            try
            {
                sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula  from tbmatri where anoMat=@valor and nome like @valor2 order by processo Asc";
                if (CbAn.Text != "Todos Anos")
                {
                    num = int.Parse(CbAn.Text.ToString());


                    dg.DataSource = ban.PesqMatriculaComposta(sql, num, "%" + tb_pesqCad.Text + "%");
                }
                if (CbAn.Text == "Todos Anos")
                {
                    dg.DataSource = ban.PesqMatricula("%" + tb_pesqCad.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void tb_pesqConf_TextChanged(object sender, EventArgs e)
        {
            string sql;


            int num;
            try
            {
                sql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and  ano=@valor and nome like @valor2";
                if (tb_AnC.Text != "Todos Anos")
                {
                    num = int.Parse(tb_AnC.Text.ToString());


                    dgConf.DataSource = ban.PesqConfirmacaoComposta(sql, num, "%" + tb_pesqConf.Text + "%");
                }
                if (tb_AnC.Text == "Todos Anos")
                {
                    dgConf.DataSource = ban.PesqCormacao("%" + tb_pesqConf.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");

            }
        }

        private void tb_pesqCompleto_TextChanged(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and ano=@valor and nome like @valor2";
                if (Cb_AnoCompleto.Text != "Todos Anos")
                {
                    num = int.Parse(Cb_AnoCompleto.Text.ToString());


                    dgCompleto.DataSource = ban.PesqTotalComposta(sql, num, "%" + tb_pesqCompleto.Text + "%");
                }
                if (Cb_AnoCompleto.Text == "Todos Anos")
                {
                    dgCompleto.DataSource = ban.PesqTotal("%" + tb_pesqCompleto.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void tbPesq_alt_del_Mat_TextChanged(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula  from tbmatri where anoMat=@valor and nome like @valor2 order by processo Asc";
                if (cbAninho_alt_del_Mat.Text != "Todos Anos")
                {
                    num = int.Parse(cbAninho_alt_del_Mat.Text.ToString());


                    ban.DeleteAlterPesqMatriculaComposta(l1, l2, l3, l4, l5, l6, l7, l8, l9, sql, num, "%" + tbPesq_alt_del_Mat.Text + "%");
                }
                if (cbAninho_alt_del_Mat.Text == "Todos Anos")
                {
                    ban.DeleAlterPesqMatricula(l1, l2, l3, l4, l5, l6, l7, l8, l9, "%" + tbPesq_alt_del_Mat.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void cbAninho_alt_del_Mat_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql;


            int num;
            try
            {
                sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula  from tbmatri where anoMat=@valor and nome like @valor2 order by processo Asc";
                if (cbAninho_alt_del_Mat.Text != "Todos Anos")
                {
                    num = int.Parse(cbAninho_alt_del_Mat.Text.ToString());


                    ban.DeleteAlterPesqMatriculaComposta(l1, l2, l3, l4, l5, l6, l7, l8, l9, sql, num, "%" + tbPesq_alt_del_Mat.Text + "%");
                }
                if (cbAninho_alt_del_Mat.Text == "Todos Anos")
                {
                    ban.DeleAlterPesqMatricula(l1, l2, l3, l4, l5, l6, l7, l8, l9, "%" + tbPesq_alt_del_Mat.Text + "%");
                }
            }
            catch (Exception er)
            {

                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void tbId_alt_del_Mat_TextChanged(object sender, EventArgs e)
        {

        }

        private void l3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list.SelectedIndex != -1)
            {
                l1.SelectedIndex = list.SelectedIndex;
                l2.SelectedIndex = list.SelectedIndex;
                l3.SelectedIndex = list.SelectedIndex;
                l4.SelectedIndex = list.SelectedIndex;
                l5.SelectedIndex = list.SelectedIndex;
                l6.SelectedIndex = list.SelectedIndex;
                l7.SelectedIndex = list.SelectedIndex;
                l8.SelectedIndex = list.SelectedIndex;
                l9.SelectedIndex = list.SelectedIndex;


                tbId_alt_del_Mat.Text = l1.SelectedItem.ToString();
                tbBi_alt_del_Mat.Text = l2.SelectedItem.ToString();
                tbNome_alt_del_Mat.Text = l3.SelectedItem.ToString();
                tbGenero_alt_del_Mat.Text = l4.SelectedItem.ToString();
                tbMorada_alt_del_Mat.Text = l5.SelectedItem.ToString();
                da_alt_del_Mat.Text = l6.SelectedItem.ToString();
                tbPai_alt_del_Mat.Text = l7.SelectedItem.ToString();
                tbMae_alt_del_Mat.Text = l8.SelectedItem.ToString();
                tbAno_alt_del_Mat.Text = l9.SelectedItem.ToString();
            }
        }

        private void button58_Click(object sender, EventArgs e)
        {
            if (
                     tbId_alt_del_Mat.Text != "" &&
                tbBi_alt_del_Mat.Text != "" &&
                tbNome_alt_del_Mat.Text != "" &&
                tbGenero_alt_del_Mat.Text != "" &&
                tbMorada_alt_del_Mat.Text != "" &&

                tbPai_alt_del_Mat.Text != "" &&
                tbMae_alt_del_Mat.Text != "" &&
                tbAno_alt_del_Mat.Text != ""
                    )
            {
                if (MessageBox.Show("Desejas Deletar?, OBS.: As confirmações Também Serão Eliminadas!", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    try
                    {

                        int il;
                        il = int.Parse(l1.SelectedItem.ToString());
                        if (ban.DELETAR_CONFIRMACAO_Confirmacao(il))
                        {
                            if (ban.DELETAR_MATRICULAR(il))
                            {

                                MessageBox.Show("Deletado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);


                                ban.ListaalterDeletarCad(l1, l2, l3, l4, l5, l6, l7, l8, l9);

                                tbId_alt_del_Mat.Text = "";
                                tbBi_alt_del_Mat.Text = "";
                                tbNome_alt_del_Mat.Text = "";
                                tbGenero_alt_del_Mat.Text = "";
                                tbMorada_alt_del_Mat.Text = "";
                                da_alt_del_Mat.Value = DateTime.Now;
                                tbPai_alt_del_Mat.Text = "";
                                tbMae_alt_del_Mat.Text = "";
                                tbAno_alt_del_Mat.Text = "";

                            }
                        }
                        else
                        {
                            MessageBox.Show("Não Deletado", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    catch (MySqlException ex)
                    {
                        MessageBox.Show("Não Foi possivel Deletar :\n", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }


            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnAtualizarMAt_Click(object sender, EventArgs e)
        {
            if (
                    tbId_alt_del_Mat.Text != "" &&
               tbBi_alt_del_Mat.Text != "" &&
               tbNome_alt_del_Mat.Text != "" &&
               tbGenero_alt_del_Mat.Text != "" &&
               tbMorada_alt_del_Mat.Text != "" &&

               tbPai_alt_del_Mat.Text != "" &&
               tbMae_alt_del_Mat.Text != "" &&
               tbAno_alt_del_Mat.Text != ""
                   )
            {
                if (MessageBox.Show("Desejas Actualizar?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    ALTERARMatricular();

                    tbId_alt_del_Mat.Text = "";
                    tbBi_alt_del_Mat.Text = "";
                    tbNome_alt_del_Mat.Text = "";
                    tbGenero_alt_del_Mat.Text = "";
                    tbMorada_alt_del_Mat.Text = "";
                    da_alt_del_Mat.Value = DateTime.Now;
                    tbPai_alt_del_Mat.Text = "";
                    tbMae_alt_del_Mat.Text = "";
                    tbAno_alt_del_Mat.Text = "";




                    ban.ListaalterDeletarCad(l1, l2, l3, l4, l5, l6, l7, l8, l9);
                }


            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //########### ACTUALIZAR MAT

        public void ALTERARMatricular()
        {

            ArrayList arr = new ArrayList();
            try
            {
                arr.Add(tbNome_alt_del_Mat.Text);
                arr.Add(tbMorada_alt_del_Mat.Text);
                arr.Add(tbGenero_alt_del_Mat.Text);
                arr.Add(da_alt_del_Mat.Value.ToString("yyyy-MM-dd"));
                arr.Add(tbBi_alt_del_Mat.Text);
                arr.Add(tbPai_alt_del_Mat.Text);
                arr.Add(tbMae_alt_del_Mat.Text);
                arr.Add(tbAno_alt_del_Mat.Text);
                arr.Add(dataHoje.Value.ToString("yyyy-MM-dd"));
                int il;
                il = int.Parse(l1.SelectedItem.ToString());

                if (ban.ALTERAR_MATRICULAR(arr, il))
                {
                    MessageBox.Show("Actualizado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Não Actualizado", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro ao Actualizar :\n", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        //ActualizarConfirmar Insert

        public void ActualizarConfirmar()
        {
            Banco ban = new Banco();
            ArrayList arr = new ArrayList();
            try
            {
                arr.Add(tbId_alt_del_Conf.Text);
                arr.Add(tbCurso_alt_del_Conf.Text);
                arr.Add(tbClasse_alt_del_Conf.Text);
                arr.Add(tbSala_alt_del_Conf.Text);
                arr.Add(tbTurno_alt_del_Conf.Text);
                arr.Add(tbAno_alt_del_Conf.Text);
                arr.Add(tbStatus_alt_del_Conf.Text);
                arr.Add(dataHoje.Value.ToString("yyyy-MM-dd"));
                int il;
                il = int.Parse(lb1.SelectedItem.ToString());
                if (ban.ALTERARCONFIRMAR(arr, il))
                {
                    MessageBox.Show("Actualizado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Não Actualizou", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro ao Actualizar ", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lb1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list.SelectedIndex != -1)
            {
                lb1.SelectedIndex = list.SelectedIndex;
                lb2.SelectedIndex = list.SelectedIndex;
                lb3.SelectedIndex = list.SelectedIndex;
                lb4.SelectedIndex = list.SelectedIndex;
                lb5.SelectedIndex = list.SelectedIndex;
                lb6.SelectedIndex = list.SelectedIndex;
                lb7.SelectedIndex = list.SelectedIndex;
                lb8.SelectedIndex = list.SelectedIndex;
                lb9.SelectedIndex = list.SelectedIndex;
                lb10.SelectedIndex = list.SelectedIndex;


                tbId_alt_del_Conf.Text = lb2.SelectedItem.ToString();
                tbCurso_alt_del_Conf.Text = lb4.SelectedItem.ToString();
                tbClasse_alt_del_Conf.Text = lb5.SelectedItem.ToString();
                tbSala_alt_del_Conf.Text = lb6.SelectedItem.ToString();
                tbTurno_alt_del_Conf.Text = lb7.SelectedItem.ToString();
                tbAno_alt_del_Conf.Text = lb10.SelectedItem.ToString();
                tbStatus_alt_del_Conf.Text = lb8.SelectedItem.ToString();

            }
        }

        private void btnPesq__alt_del_Conf_Click(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select idaluno as ID,processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and Nome like @valor and Ano=@an";

                if (cbAno_alt_del_Conf.Text != "Todos Anos")
                {
                    num = int.Parse(cbAno_alt_del_Conf.Text.ToString());


                    ban.ListaalterDeletaCOMPOSTAConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, sql, num, "%" + tbPesq_alt_del_Conf.Text + "%");
                }
                if (cbAno_alt_del_Conf.Text == "Todos Anos")
                {
                    ban.ListaalterDeletarUmPARAmConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, "%" + tbPesq_alt_del_Conf.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void tbPesq_alt_del_Conf_TextChanged(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select idaluno as ID,processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and Nome like @valor and Ano=@an";

                if (cbAno_alt_del_Conf.Text != "Todos Anos")
                {
                    num = int.Parse(cbAno_alt_del_Conf.Text.ToString());


                    ban.ListaalterDeletaCOMPOSTAConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, sql, num, "%" + tbPesq_alt_del_Conf.Text + "%");
                }
                if (cbAno_alt_del_Conf.Text == "Todos Anos")
                {
                    ban.ListaalterDeletarUmPARAmConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, "%" + tbPesq_alt_del_Conf.Text + "%");
                }
            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void cbAno_alt_del_Conf_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select idaluno as ID,processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and Nome like @valor and Ano=@an";

                if (cbAno_alt_del_Conf.Text != "Todos Anos")
                {
                    num = int.Parse(cbAno_alt_del_Conf.Text.ToString());


                    ban.ListaalterDeletaCOMPOSTAConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, sql, num, "%" + tbPesq_alt_del_Conf.Text + "%");
                }
                if (cbAno_alt_del_Conf.Text == "Todos Anos")
                {
                    ban.ListaalterDeletarUmPARAmConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, "%" + tbPesq_alt_del_Conf.Text + "%");
                }
            }
            catch (Exception er)
            {

                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void tbnActualiza_alt_del_Conf_Click(object sender, EventArgs e)
        {
            if (
               tbId_alt_del_Conf.Text != "" &&
                tbCurso_alt_del_Conf.Text != "" &&
                tbClasse_alt_del_Conf.Text != "" &&
                tbSala_alt_del_Conf.Text != "" &&
                tbTurno_alt_del_Conf.Text != "" &&
                tbAno_alt_del_Conf.Text != "" &&
                tbStatus_alt_del_Conf.Text != ""
              )
            {
                if (MessageBox.Show("Desejas Actualizar?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ActualizarConfirmar();


                    tbId_alt_del_Conf.Text = "";
                    tbCurso_alt_del_Conf.Text = "";
                    tbClasse_alt_del_Conf.Text = "";
                    tbSala_alt_del_Conf.Text = "";
                    tbTurno_alt_del_Conf.Text = "";
                    tbAno_alt_del_Conf.Text = "";
                    tbStatus_alt_del_Conf.Text = "";

                    ban.ListaalterDeletarConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10);

                }
            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeletar__alt_del_Conf_Click(object sender, EventArgs e)
        {

            if (
               tbId_alt_del_Conf.Text != "" &&
                tbCurso_alt_del_Conf.Text != "" &&
                tbClasse_alt_del_Conf.Text != "" &&
                tbSala_alt_del_Conf.Text != "" &&
                tbTurno_alt_del_Conf.Text != "" &&
                tbAno_alt_del_Conf.Text != "" &&
                tbStatus_alt_del_Conf.Text != ""
              )
            {
                if (MessageBox.Show("Desejas Deletar?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {



                    try
                    {

                        int il;
                        il = int.Parse(lb1.SelectedItem.ToString());

                        if (ban.DELETAR_CONFIRMACAO(il))
                        {
                            MessageBox.Show("Deletado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            tbId_alt_del_Conf.Text = "";
                            tbCurso_alt_del_Conf.Text = "";
                            tbClasse_alt_del_Conf.Text = "";
                            tbSala_alt_del_Conf.Text = "";
                            tbTurno_alt_del_Conf.Text = "";
                            tbAno_alt_del_Conf.Text = "";
                            tbStatus_alt_del_Conf.Text = "";

                            ban.ListaalterDeletarConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10);
                        }
                        else
                        {
                            MessageBox.Show("Não Deletado", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    catch (MySqlException ex)
                    {
                        MessageBox.Show("Não Foi possivel Deletar :\n", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tp_Alt_det_Conf_Click(object sender, EventArgs e)
        {

        }

        private void CbAn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;

                string sql;


                int num;
                try
                {

                    sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula  from tbmatri where anoMat=@valor and nome like @valor2 order by processo Asc";
                    if (CbAn.Text != "Todos Anos")
                    {
                        num = int.Parse(CbAn.Text.ToString());


                        dg.DataSource = ban.PesqMatriculaComposta(sql, num, "%" + tb_pesqCad.Text + "%");
                    }
                    if (CbAn.Text == "Todos Anos")
                    {
                        dg.DataSource = ban.PesqMatricula("%" + tb_pesqCad.Text + "%");
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show("Erro de Campo!", "Erro Basico");
                }
            }
        }

        private void tb_AnC_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                string sql;


                int num;

                try
                {

                    sql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and  ano=@valor and nome like @valor2";
                    if (tb_AnC.Text != "Todos Anos")
                    {
                        num = int.Parse(tb_AnC.Text.ToString());


                        dgConf.DataSource = ban.PesqConfirmacaoComposta(sql, num, "%" + tb_pesqConf.Text + "%");
                    }
                    if (tb_AnC.Text == "Todos Anos")
                    {
                        dgConf.DataSource = ban.PesqCormacao("%" + tb_pesqConf.Text + "%");
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show("Erro de Campo", "Erro Basico");
                }
            }
        }

        private void Cb_AnoCompleto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;


                string sql;


                int num;

                try
                {

                    sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and ano=@valor and nome like @valor2";
                    if (Cb_AnoCompleto.Text != "Todos Anos")
                    {
                        num = int.Parse(Cb_AnoCompleto.Text.ToString());


                        dgCompleto.DataSource = ban.PesqTotalComposta(sql, num, "%" + tb_pesqCompleto.Text + "%");
                    }
                    if (Cb_AnoCompleto.Text == "Todos Anos")
                    {
                        dgCompleto.DataSource = ban.PesqTotal("%" + tb_pesqCompleto.Text + "%");
                    }
                }
                catch (Exception er)
                {
                    MessageBox.Show("Erro de Campo", "Erro Basico");
                }
            }
        }

        private void cbAninho_alt_del_Mat_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                e.SuppressKeyPress = true;

                string sql;


                int num;
                try
                {
                    sql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula  from tbmatri where anoMat=@valor and nome like @valor2 order by processo Asc";
                    if (cbAninho_alt_del_Mat.Text != "Todos Anos")
                    {
                        num = int.Parse(cbAninho_alt_del_Mat.Text.ToString());


                        ban.DeleteAlterPesqMatriculaComposta(l1, l2, l3, l4, l5, l6, l7, l8, l9, sql, num, "%" + tbPesq_alt_del_Mat.Text + "%");
                    }
                    if (cbAninho_alt_del_Mat.Text == "Todos Anos")
                    {
                        ban.DeleAlterPesqMatricula(l1, l2, l3, l4, l5, l6, l7, l8, l9, "%" + tbPesq_alt_del_Mat.Text + "%");
                    }
                }
                catch (Exception er)
                {

                    MessageBox.Show("Erro de Campo", "Erro Basico");
                }
            }
        }

        private void cbAno_alt_del_Conf_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                e.SuppressKeyPress = true;

                string sql;


                int num;

                try
                {

                    sql = "select idaluno as ID,processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and Nome like @valor and Ano=@an";

                    if (cbAno_alt_del_Conf.Text != "Todos Anos")
                    {
                        num = int.Parse(cbAno_alt_del_Conf.Text.ToString());


                        ban.ListaalterDeletaCOMPOSTAConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, sql, num, "%" + tbPesq_alt_del_Conf.Text + "%");
                    }
                    if (cbAno_alt_del_Conf.Text == "Todos Anos")
                    {
                        ban.ListaalterDeletarUmPARAmConf(lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, "%" + tbPesq_alt_del_Conf.Text + "%");
                    }
                }
                catch (Exception er)
                {

                    MessageBox.Show("Erro de Campo", "Erro Basico");
                }

            }
        }

        //DEPOSITO

        public void Depositar()
        {

            ArrayList arr = new ArrayList();
            try
            {

                arr.Add(tb_IDalunoDep.Text);
                arr.Add(tb_talão.Text);
                arr.Add(tb_valorDep.Text);
                arr.Add(tb_bancoDep.Text);
                arr.Add(data_dep.Value.ToString("yyyy-MM-dd"));
                arr.Add(data_actual.Value.ToString("yyyy-MM-dd"));
                arr.Add(tb_d.Text);



                if (ban.DEPOSITAR(arr))
                {
                    ban.ADDSALDO(int.Parse(tb_valorDep.Text), int.Parse(tb_IDalunoDep.Text), int.Parse(tb_d.Text));

                    MessageBox.Show("Cadastrado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Não Casdrastrou", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }



            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro ao inserir", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }


        private void btn_cadDep_Click(object sender, EventArgs e)
        {


            if (ban.testeANO(tb_IDalunoDep.Text, int.Parse(tb_d.Text)) &&
                    tb_IDalunoDep.Text != "" &&
                tb_talão.Text != "" &&
                tb_valorDep.Text != "" &&

                tb_bancoDep.Text != ""

                )
            {
                Depositar();
                ban.ListarDeposito(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal);

                tb_IDalunoDep.Text = "";
                tb_talão.Text = "";
                tb_valorDep.Text = "";

                tb_bancoDep.Text = "";

            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btn_pesqDep_Click(object sender, EventArgs e)
        {
            string sql;


            int num;
            try
            {
                sql = "select iddep,alunoid,nome as Aluno,talao as Talao,valordep as Valor,banco as Banco,datadep as DataDeposito,dataact as DataActualizacao,anoal,saldo from tbdeposito,tbconfirmar,tbmatri where aluno=alunoid and processo=aluno and ano=anoal and nome like @valor and ano=@classe order by alunoid Asc";

                if (cb_cla.Text != "Todos Anos")
                {
                    num = int.Parse(cb_cla.Text.ToString());


                    ban.ListarDepositoCondClass(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal, sql, num, "%" + tb_pesqDep.Text + "%");
                }
                if (cb_cla.Text == "Todos Anos")
                {
                    ban.ListarDepositoCond(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal, "%" + tb_pesqDep.Text + "%");
                }
            }
            catch (Exception er)
            {

                MessageBox.Show("Erro de Campo", "Erro Basico");
            }

        }

        private void tb_pesqDep_TextChanged(object sender, EventArgs e)
        {
            string sql;


            int num;
            try
            {
                sql = "select iddep,alunoid,nome as Aluno,talao as Talao,valordep as Valor,banco as Banco,datadep as DataDeposito,dataact as DataActualizacao,anoal,saldo from tbdeposito,tbconfirmar,tbmatri where aluno=alunoid and processo=aluno and ano=anoal and nome like @valor and classe=@classe order by alunoid Asc";

                if (cb_cla.Text != "Todas Classes")
                {
                    num = int.Parse(cb_cla.Text.ToString());


                    ban.ListarDepositoCondClass(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal, sql, num, "%" + tb_pesqDep.Text + "%");
                }
                if (cb_cla.Text == "Todas Classes")
                {
                    ban.ListarDepositoCond(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal, "%" + tb_pesqDep.Text + "%");
                }
            }
            catch (Exception er)
            {


            }


            //ban.ListarDepositoCond(dd, d, d1, d2, d3, d4, d5, d6, ddd,sal, "%" + tb_pesqDep.Text + "%");

        }

        private void button47_Click(object sender, EventArgs e)
        {
            try
            {
                ban.ListarDepositodata(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal, dataPesqDep.Value.ToString("yyyy-MM-dd"));


            }
            catch (Exception er)
            {
                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void button46_Click(object sender, EventArgs e)
        {
            ban.ListarDeposito(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal);
        }

        private void d1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox list = sender as ListBox;

            try
            {
                if (list.SelectedIndex != -1)
                {
                    dd.SelectedIndex = list.SelectedIndex;
                    d.SelectedIndex = list.SelectedIndex;
                    d1.SelectedIndex = list.SelectedIndex;
                    d2.SelectedIndex = list.SelectedIndex;
                    d3.SelectedIndex = list.SelectedIndex;
                    d4.SelectedIndex = list.SelectedIndex;
                    d5.SelectedIndex = list.SelectedIndex;
                    d6.SelectedIndex = list.SelectedIndex;

                    ddd.SelectedIndex = list.SelectedIndex;
                    sal.SelectedIndex = list.SelectedIndex;

                    tb_IDATODEP.Text = dd.SelectedItem.ToString();
                    tb_IDalunoDep.Text = d.SelectedItem.ToString();
                    tb_talão.Text = d2.SelectedItem.ToString();
                    tb_valorDep.Text = d3.SelectedItem.ToString();
                    tb_bancoDep.Text = d4.SelectedItem.ToString();
                    data_dep.Text = d5.SelectedItem.ToString();
                    data_actual.Text = d6.SelectedItem.ToString();
                    tb_d.Text = ddd.SelectedItem.ToString();


                }
            }
            catch (Exception r)
            { }

        }



        public void AlterarDeposito()
        {

            ArrayList arr = new ArrayList();
            try
            {
                arr.Add(tb_IDalunoDep.Text);
                arr.Add(tb_talão.Text);
                arr.Add(tb_valorDep.Text);
                arr.Add(tb_bancoDep.Text);
                arr.Add(data_dep.Value.ToString("yyyy-MM-dd"));
                arr.Add(data_actual.Value.ToString("yyyy-MM-dd"));
                arr.Add(tb_d.Text);
                int il;
                il = int.Parse(tb_IDATODEP.Text.ToString());

                if (ban.ALTERARDEPOSITO(arr, il))
                {
                    ban.ADDSALDO(int.Parse(tb_valorDep.Text), int.Parse(tb_IDalunoDep.Text), int.Parse(tb_d.Text));
                    MessageBox.Show("Alterado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Não Alterou", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro ao Alterar", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btn_actDep_Click(object sender, EventArgs e)
        {
            if (ban.testeANO(tb_IDalunoDep.Text, int.Parse(tb_d.Text)) &&
             tb_IDalunoDep.Text != "" &&
         tb_talão.Text != "" &&
         tb_valorDep.Text != "" &&

         tb_bancoDep.Text != ""

         )
            {
                AlterarDeposito();

                ban.ListarDeposito(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal);

                tb_IDalunoDep.Text = "";
                tb_talão.Text = "";
                tb_valorDep.Text = "";

                tb_bancoDep.Text = "";

            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_deletDep_Click(object sender, EventArgs e)
        {
            if (
             tb_IDalunoDep.Text != "" &&
         tb_talão.Text != "" &&
         tb_valorDep.Text != "" &&

         tb_bancoDep.Text != ""

         )
            {

                if (MessageBox.Show("Desejas Deletar?, OBS.: As confirmações Também Serão Eliminadas!", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    try
                    {

                        int il;
                        il = int.Parse(tb_IDATODEP.Text);


                        if (ban.DELETARDEPOSITO(il))
                        {

                            MessageBox.Show("Deletado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);


                            ban.ListarDeposito(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal);
                            tb_IDalunoDep.Text = "";
                            tb_talão.Text = "";
                            tb_valorDep.Text = "";

                            tb_bancoDep.Text = "";

                        }



                    }
                    catch (MySqlException ex)
                    {
                        MessageBox.Show("Não Foi possivel Deletar :\n", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button19_Click(object sender, EventArgs e)
        {
            TbDepositoDen.SelectedTab = TbDepositoDen_CRD;
        }

        private void button31_Click(object sender, EventArgs e)
        {
            TbDepositoDen.SelectedTab = TbDepositoDen_CRD;
        }

        private void button28_Click(object sender, EventArgs e)
        {
            TbDepositoDen.SelectedTab = TbDepositoDen_pub;
        }

        private void tb__SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            string sql;

            int num;

            try
            {
                sql = "select iddep,alunoid,nome as Aluno,talao as Talao,valordep as Valor,banco as Banco,datadep as DataDeposito,dataact as DataActualizacao,anoal from tbdeposito,tbconfirmar,tbmatri where aluno=alunoid and processo=aluno and ano=anoal and nome like @v and anoal=@an order by alunoid Asc";
             

                if (tb_.Text != "Todos Anos")
                {
                    num = int.Parse(tb_.Text.ToString());


                    ban.ListarDepositoCondAno(dd, d, d1, d2, d3, d4, d5, d6, ddd,sql, "%" + tb_pesqDep.Text + "%", num);
                }
                if (tb_.Text == "Todos Anos")
                {
                    ban.ListarDepositoCond(dd, d, d1, d2, d3, d4, d5, d6, ddd, "%" + tb_pesqDep.Text + "%");
                }
            }
            catch (Exception er)
            {

                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
             * */
        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (ban.testeANO(tb_IDalunoDep.Text, int.Parse(tb_d.Text)))
            {
                MessageBox.Show("Aluno Comfirmado!");

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql;


            int num;
            try
            {
                sql = "select iddep,alunoid,nome as Aluno,talao as Talao,valordep as Valor,banco as Banco,datadep as DataDeposito,dataact as DataActualizacao,anoal,saldo from tbdeposito,tbconfirmar,tbmatri where aluno=alunoid and processo=aluno and ano=anoal and nome like @valor and ano=@classe order by alunoid Asc";

                if (cb_cla.Text != "Todos Anos")
                {
                    num = int.Parse(cb_cla.Text.ToString());


                    ban.ListarDepositoCondClass(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal, sql, num, "%" + tb_pesqDep.Text + "%");
                }
                if (cb_cla.Text == "Todos Anos")
                {
                    ban.ListarDepositoCond(dd, d, d1, d2, d3, d4, d5, d6, ddd, sal, "%" + tb_pesqDep.Text + "%");
                }
            }
            catch (Exception er)
            {

                MessageBox.Show("Erro de Campo", "Erro Basico");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpagePROPRINA;
            TbPropinaDen.SelectedTab = TbPropinaDen_crud;

            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.DodgerBlue;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpageDEPOSITO;
            TbDepositoDen.SelectedTab = TbDepositoDen_CRD;

            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.DodgerBlue;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.Gray;
        }

        private void button61_Click(object sender, EventArgs e)
        {

        }

        private void btn_ferra_Click(object sender, EventArgs e)
        {
            try
            {

                frm_ADIM adm = new frm_ADIM();
                adm.ShowDialog();

            }
            catch (Exception h)
            { }
        }

        private void label74_Click(object sender, EventArgs e)
        {
            try
            {

                frm_ADIM adm = new frm_ADIM();
                adm.ShowDialog();

            }
            catch (Exception h)
            { }
        }

        private void TbPropinaDen_CRUD_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,saldo as Saldo,ano as Ano from tbmatri,tbconfirmar where Processo=aluno and Nome like @valor and Ano=@an";

                if (comboBox1.Text != "Todos Anos")
                {
                    num = int.Parse(comboBox1.Text.ToString());

                    ban.ListaalterVerPropComp(c, c1, c2, c3, c4, c5, c6, c7, sql, num, "%" + tb_pesqProp.Text + "%");

                }
                if (comboBox1.Text == "Todos Anos")
                {

                    ban.ListarVerPropCond(c, c1, c2, c3, c4, c5, c6, c7, "%" + tb_pesqProp.Text + "%");

                }
            }
            catch (Exception er)
            {

                MessageBox.Show("Erro de Campo", "Erro Basico");
            }


        }

        private void tb_pesqProp_TextChanged(object sender, EventArgs e)
        {
            string sql;


            int num;

            try
            {

                sql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,saldo as Saldo,ano as Ano from tbmatri,tbconfirmar where Processo=aluno and Nome like @valor and Ano=@an";

                if (comboBox1.Text != "Todos Anos")
                {
                    num = int.Parse(comboBox1.Text.ToString());

                    ban.ListaalterVerPropComp(c, c1, c2, c3, c4, c5, c6, c7, sql, num, "%" + tb_pesqProp.Text + "%");

                }
                if (comboBox1.Text == "Todos Anos")
                {

                    ban.ListarVerPropCond(c, c1, c2, c3, c4, c5, c6, c7, "%" + tb_pesqProp.Text + "%");

                }
            }
            catch (Exception er)
            {

                MessageBox.Show("Erro de Campo", "Erro Basico");
            }


        }

        private void c_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list.SelectedIndex != -1)
            {
                c1.SelectedIndex = list.SelectedIndex;
                c2.SelectedIndex = list.SelectedIndex;
                c3.SelectedIndex = list.SelectedIndex;
                c4.SelectedIndex = list.SelectedIndex;
                c5.SelectedIndex = list.SelectedIndex;
                c6.SelectedIndex = list.SelectedIndex;
                c7.SelectedIndex = list.SelectedIndex;
                c.SelectedIndex = list.SelectedIndex;



                Lid.Text = c.SelectedItem.ToString();
                Lnome.Text = c1.SelectedItem.ToString();
                Lcurso.Text = c2.SelectedItem.ToString();
                Lclasse.Text = c3.SelectedItem.ToString();
                Lturno.Text = c4.SelectedItem.ToString();
                Lsala.Text = c5.SelectedItem.ToString();
                Lsaldo.Text = c6.SelectedItem.ToString();
                Lano.Text = c7.SelectedItem.ToString();

                if (Lclasse.Text == "12")
                { Classepreco.Text = "16000"; }
                if (Lclasse.Text == "11")
                { Classepreco.Text = "15000"; }
                if (Lclasse.Text == "10")
                { Classepreco.Text = "14000"; }

                try
                {
                    ban.checarmes(Lid.Text, int.Parse(Lano.Text), Lmeses);
                }
                catch (Exception j) { }
                try
                {
                    precott.Text = "" + (int.Parse(tb_numMes.Text) * int.Parse(Classepreco.Text));
                }
                catch (Exception h)
                { MessageBox.Show("Verifica os campos", "Erro"); }
            }
        }

        private void panel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button26_Click(object sender, EventArgs e)
        {
            TbPropinaDen.SelectedTab = TbPropinaDen_crud;
        }

        private void button39_Click(object sender, EventArgs e)
        {
            TbPropinaDen.SelectedTab = TbPropinaDen_crud;
        }

        private void button37_Click(object sender, EventArgs e)
        {
            TbPropinaDen.SelectedTab = TbPropinaDen_propina;
        }

        private void button27_Click(object sender, EventArgs e)
        {
            TbPropinaDen.SelectedTab = TbPropinaDen_propina;
        }

        private void button34_Click(object sender, EventArgs e)
        {
            TbPropinaDen.SelectedTab = TbPropinaDen_pub;
        }

        private void label112_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                precott.Text = "" + (int.Parse(tb_numMes.Text) * int.Parse(Classepreco.Text));
            }
            catch (Exception h)
            { MessageBox.Show("Verifica os campos", "Erro"); }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                if (Lid.Text != "" && tb_numMes.Text != "" && tb_mesdesc.Text != "")
                {
                    if (ban.checarmesRepetido(Lid.Text, int.Parse(Lano.Text), tb_mesdesc.Text) == false)
                    {
                        if (int.Parse(Lsaldo.Text) >= int.Parse(precott.Text))
                        {
                            txtOBSmes.Text = tb_mesdesc.Text;
                            txtdatOBS.Text = datadoPAG.Text;





                            ban.PAGAR(int.Parse(Lid.Text), int.Parse(tb_numMes.Text), tb_mesdesc.Text, datadoPAG.Value.ToString("yyyy-MM-dd"), int.Parse(Lano.Text), lbNomeUsuario.Text);
                            ban.REDSALDO(int.Parse(precott.Text), int.Parse(Lid.Text), int.Parse(Lano.Text));

                            try
                            {
                                ban.checarmes(Lid.Text, int.Parse(Lano.Text), Lmeses);
                            }
                            catch (Exception j) { }



                            tb_numMes.Text = "1";
                            tb_mesdesc.Text = "Fevereiro";

                            Lsaldo.Text = (int.Parse(Lsaldo.Text) - int.Parse(precott.Text)).ToString();

                            ban.ListarVerProp(c, c1, c2, c3, c4, c5, c6, c7);

                            MessageBox.Show("Propina Actualizada!", "Mensagem");
                        }

                        else
                        {
                            MessageBox.Show("O seu SALDO não é Suficiente!", "Mensagem");
                        }
                    }

                    else
                    { MessageBox.Show("O mês referente já foi pago, Verifique os campos!", "Mensagem"); }
                }
                else
                {

                    MessageBox.Show("Por favor, Verifique os campos!", "Erro");
                }
            }
            catch (Exception f)
            { }
        }

        private void label101_Click(object sender, EventArgs e)
        {

        }

        private void btn_CadImprimir_Click(object sender, EventArgs e)
        {
             try
            {
                
                
                if (Lid.Text != "" && tb_numMes.Text != "" && tb_mesdesc.Text != "" && txtOBSmes.Text !="")
                {
                    
            Properties.Settings.Default.Nome = Lnome.Text.ToString();
            Properties.Settings.Default.Mes = txtOBSmes.Text.ToString();

            Properties.Settings.Default.ID = Lid.Text.ToString();
            Properties.Settings.Default.Us = lbNomeUsuario.Text;
            Properties.Settings.Default.Ano = Lano.Text.ToString();
            Properties.Settings.Default.Datap = datadoPAG.Value.ToString();
           

            Properties.Settings.Default.IDpag = "Auto";

            Properties.Settings.Default.Curso = Lcurso.Text;
            Properties.Settings.Default.Classe = Lclasse.Text;
            Properties.Settings.Default.Turno = Lturno.Text;
            Properties.Settings.Default.Sala = Lsala.Text;
            Properties.Settings.Default.Saldo = Lsaldo.Text;

            Properties.Settings.Default.Save();


            frm_pag lk = new frm_pag();
            lk.ShowDialog();

            txtOBSmes.Text = "";
                }
                else
                {

                    //MessageBox.Show("Por favor, Verifique os campos!", "Erro");
                }
            }
             catch (Exception f)
             { }

        }

        private void label198_Click(object sender, EventArgs e)
        {

        }

        private void tpagINICIO_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void ovalShape1_Click(object sender, EventArgs e)
        {

        }

        private void label113_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpagePROPRINA;
            TbPropinaDen.SelectedTab = TbPropinaDen_crud;

            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.DodgerBlue;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpagePROPRINA;
            TbPropinaDen.SelectedTab = TbPropinaDen_crud;

            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.DodgerBlue;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            TB_CENTRAL.SelectedTab = tpagePROPRINA;
            TbPropinaDen.SelectedTab = TbPropinaDen_crud;

            btnAluno.ForeColor = System.Drawing.Color.Gray;
            btnDeposit.ForeColor = System.Drawing.Color.Gray;
            btnInicio.ForeColor = System.Drawing.Color.Gray;
            btnEstast.ForeColor = System.Drawing.Color.Gray;
            //btnPag.ForeColor = System.Drawing.Color.Gray;
            btnPropina.ForeColor = System.Drawing.Color.DodgerBlue;
        }

        private void button20_Click(object sender, EventArgs e)
        {

          //  ban.ListarPropina(a1, a, a2, a3, a4, a6, a5);

        }

        private void a_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox list = sender as ListBox;

            try
            {
                if (list.SelectedIndex != -1)
                {
                    a.SelectedIndex = list.SelectedIndex;
                    a1.SelectedIndex = list.SelectedIndex;

                    a2.SelectedIndex = list.SelectedIndex;
                    a3.SelectedIndex = list.SelectedIndex;
                    a4.SelectedIndex = list.SelectedIndex;
                    a5.SelectedIndex = list.SelectedIndex;
                    a6.SelectedIndex = list.SelectedIndex;

                    ax1.SelectedIndex = list.SelectedIndex;
                    ax2.SelectedIndex = list.SelectedIndex;
                    ax3.SelectedIndex = list.SelectedIndex;
                    ax4.SelectedIndex = list.SelectedIndex;
                    ax5.SelectedIndex = list.SelectedIndex;
                    

                    tb_IDpropina.Text = a1.SelectedItem.ToString();

                    tb_Mesprop.Text = a3.SelectedItem.ToString();
                    data_prop.Text = a4.SelectedItem.ToString();
                    tb_Userprop.Text = a5.SelectedItem.ToString();
                    tb_Anoprop.Text = a6.SelectedItem.ToString();
                    tb_pagamentoID.Text = a.SelectedItem.ToString();

                    textNOme.Text = a2.SelectedItem.ToString();


                    textCurso.Text = ax1.SelectedItem.ToString();
                    textClasse.Text = ax2.SelectedItem.ToString();
                    textTurno.Text = ax3.SelectedItem.ToString();
                    textSala.Text = ax4.SelectedItem.ToString();
                    textSaldo.Text = ax5.SelectedItem.ToString();
                }

            }
            catch (Exception p)
            { }


        }

        private void btn_actProp_Click(object sender, EventArgs e)
        {
            try
            {
                if (tb_IDpropina.Text != "")
                {


                    if (ban.testeANO(tb_IDpropina.Text, int.Parse(tb_Anoprop.Text)))
                    {

                    ban.ACTUALIZARPAGAM(int.Parse(tb_pagamentoID.Text), int.Parse(tb_IDpropina.Text), tb_Mesprop.Text, data_prop.Value.ToString("yyyy-MM-dd"), int.Parse(tb_Anoprop.Text), tb_Userprop.Text);
                    ban.ListarPropina(a, a1, a2, a3, a4, a6, a5, ax1, ax2, ax3, ax4, ax5);
                    MessageBox.Show("Actualizado com Sucesso", "Relatorio");
                    }

                    
                }
                else
                {
                    MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch(Exception ef)
            {
                MessageBox.Show("(2)Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_delProp_Click(object sender, EventArgs e)
        {

            try
            {


                if (tb_IDpropina.Text != "")
                {
                    if (MessageBox.Show("Deseja deletar?", "Pergntar", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {

                        ban.DELETARPAG(int.Parse(tb_pagamentoID.Text));

                        ban.ListarPropina(a, a1, a2, a3, a4, a6, a5, ax1, ax2, ax3, ax4, ax5);

                        MessageBox.Show("Deletado com Sucesso", "Relatorio");
                    }
                }
                else
                {
                    MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch(Exception fd)
            {
                MessageBox.Show("(2)Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void tb_Peprop_TextChanged(object sender, EventArgs e)
        {

            //try
            //{
            //    ban.ListarPropinaCOND(a, a1, a2, a3, a4, a6, a5, "%" + tb_Peprop.Text + "%");
            //}
            //catch(Exception fd)
            //{
            //    MessageBox.Show("(2)Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            string sql;




            //sql = "select idprop,nome,anopag,al,descmes,datapag,user from tbpagamento,tbconfirmar,tbmatri where aluno=al and processo=aluno and nome like @nm and anopag=@ano";
            sql = "select idprop,nome,curso,turno,sala,classe,saldo,anopag,al,descmes,datapag,user from tbpagamento,tbconfirmar,tbmatri where aluno=al and processo=aluno and anopag=@ano and nome like @nm ";

            if (cbANOpag.Text != "Todos Anos")
            {

                ban.ListarPropinaCONDXXX(a, a1, a2, a3, a4, a6, a5,ax1,ax2,ax3,ax4,ax5, "%" + tb_Peprop.Text + "%", cbANOpag.Text,sql);

               // ban.ListarPropinaCONDano(a, a1, a2, a3, a4, a6, a5, "%" + tb_Peprop.Text + "%", sql, tb_Anoprop.Text);
            }
            if (cbANOpag.Text == "Todos Anos")
            {
                ban.ListarPropinaCOND(a, a1, a2, a3, a4, a6, a5, ax1, ax2, ax3, ax4, ax5, "%" + tb_Peprop.Text + "%");
            }

           // ban.ListarPropinaCONDXXX(a, a1, a2, a3, a4, a6, a5, "%" + tb_Peprop.Text + "%", tb_Peprop.Text);
      
        }

        private void cbANOpag_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    ban.ListarPropinaCOND(a, a1, a2, a3, a4, a6, a5, "%" + tb_Peprop.Text + "%");
            //}
            //catch(Exception fd)
            //{
            //    MessageBox.Show("(2)Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            string sql;




            //sql = "select idprop,nome,anopag,al,descmes,datapag,user from tbpagamento,tbconfirmar,tbmatri where aluno=al and processo=aluno and nome like @nm and anopag=@ano";
            sql = "select idprop,nome,curso,turno,sala,classe,saldo,anopag,al,descmes,datapag,user from tbpagamento,tbconfirmar,tbmatri where aluno=al and processo=aluno and anopag=@ano and nome like @nm ";

            if (cbANOpag.Text != "Todos Anos")
            {

                ban.ListarPropinaCONDXXX(a, a1, a2, a3, a4, a6, a5, ax1, ax2, ax3, ax4, ax5, "%" + tb_Peprop.Text + "%", cbANOpag.Text, sql);

                // ban.ListarPropinaCONDano(a, a1, a2, a3, a4, a6, a5, "%" + tb_Peprop.Text + "%", sql, tb_Anoprop.Text);
            }
            if (cbANOpag.Text == "Todos Anos")
            {
                ban.ListarPropinaCOND(a, a1, a2, a3, a4, a6, a5, ax1, ax2, ax3, ax4, ax5, "%" + tb_Peprop.Text + "%");
            }

            // ban.ListarPropinaCONDXXX(a, a1, a2, a3, a4, a6, a5, "%" + tb_Peprop.Text + "%", tb_Peprop.Text);
        }

        private void button14_Click_1(object sender, EventArgs e)
        {
            
            try
            {
                if (tb_IDpropina.Text != "" && textClasse.Text!="")
                {

            Properties.Settings.Default.Nome = textNOme.Text.ToString();
            Properties.Settings.Default.Mes = tb_Mesprop.Text.ToString();
            Properties.Settings.Default.ID = tb_IDpropina.Text.ToString();
            Properties.Settings.Default.Us = tb_Userprop.Text.ToString();
            Properties.Settings.Default.Ano = tb_Anoprop.Text.ToString();
            Properties.Settings.Default.Datap = data_prop.Text.ToString();

            Properties.Settings.Default.IDpag = tb_pagamentoID.Text;

            Properties.Settings.Default.Curso = textCurso.Text;
            Properties.Settings.Default.Classe = textClasse.Text;
            Properties.Settings.Default.Turno = textTurno.Text;
            Properties.Settings.Default.Sala = textSala.Text;
            Properties.Settings.Default.Saldo = textSaldo.Text;

            Properties.Settings.Default.Save(); 


            frm_pag lk = new frm_pag();
            lk.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ef)
            {
                MessageBox.Show("(2)Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void ax4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            
        }

        private void button12_Click_1(object sender, EventArgs e)
        {
            try
            {
                //Propina
                ban.ListarVerProp(c, c1, c2, c3, c4, c5, c6, c7);
            }
            catch(Exception j)
            {}
        }

        private void tbCurso_alt_del_Conf_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tbClasse_alt_del_Conf_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tbAno_alt_del_Conf_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
