﻿namespace PPA
{
    partial class US_esqueci
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLogo = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lb_fech = new System.Windows.Forms.LinkLabel();
            this.panelRodSuperio = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_confirmar = new System.Windows.Forms.Button();
            this.tb_usuario = new System.Windows.Forms.ComboBox();
            this.tb_senha = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_confsenha = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_respo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_testResp = new System.Windows.Forms.Button();
            this.lb_perg = new System.Windows.Forms.Label();
            this.btn_testUser = new System.Windows.Forms.Button();
            this.lnome = new System.Windows.Forms.Label();
            this.panelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panelLogo
            // 
            this.panelLogo.BackColor = System.Drawing.Color.White;
            this.panelLogo.Controls.Add(this.pictureBox1);
            this.panelLogo.Controls.Add(this.lb_fech);
            this.panelLogo.Controls.Add(this.panelRodSuperio);
            this.panelLogo.Controls.Add(this.pictureBox2);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(658, 118);
            this.panelLogo.TabIndex = 36;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PPA.Properties.Resources.jkiu_fw;
            this.pictureBox1.Location = new System.Drawing.Point(58, 86);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // lb_fech
            // 
            this.lb_fech.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_fech.AutoSize = true;
            this.lb_fech.DisabledLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_fech.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lb_fech.LinkColor = System.Drawing.Color.Gray;
            this.lb_fech.Location = new System.Drawing.Point(631, 18);
            this.lb_fech.Name = "lb_fech";
            this.lb_fech.Size = new System.Drawing.Size(21, 20);
            this.lb_fech.TabIndex = 16;
            this.lb_fech.TabStop = true;
            this.lb_fech.Text = "X";
            this.lb_fech.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_fech.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lb_fech_LinkClicked);
            // 
            // panelRodSuperio
            // 
            this.panelRodSuperio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.panelRodSuperio.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelRodSuperio.Location = new System.Drawing.Point(0, 0);
            this.panelRodSuperio.Name = "panelRodSuperio";
            this.panelRodSuperio.Size = new System.Drawing.Size(658, 15);
            this.panelRodSuperio.TabIndex = 13;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PPA.Properties.Resources._15824271_1216074758470968_68670057_o;
            this.pictureBox2.Location = new System.Drawing.Point(-1, 37);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(168, 58);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(45, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 38);
            this.label2.TabIndex = 37;
            this.label2.Text = "Bem-Vindo ->";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.label1.Location = new System.Drawing.Point(203, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 31);
            this.label1.TabIndex = 38;
            this.label1.Text = " Esqueci a Minha Palavra-Passe";
            // 
            // btn_confirmar
            // 
            this.btn_confirmar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_confirmar.BackColor = System.Drawing.Color.White;
            this.btn_confirmar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_confirmar.Enabled = false;
            this.btn_confirmar.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.btn_confirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_confirmar.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.btn_confirmar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.btn_confirmar.Location = new System.Drawing.Point(48, 432);
            this.btn_confirmar.Name = "btn_confirmar";
            this.btn_confirmar.Size = new System.Drawing.Size(95, 30);
            this.btn_confirmar.TabIndex = 45;
            this.btn_confirmar.Text = "Confirmar";
            this.btn_confirmar.UseVisualStyleBackColor = false;
            this.btn_confirmar.Click += new System.EventHandler(this.btn_confirmar_Click);
            // 
            // tb_usuario
            // 
            this.tb_usuario.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tb_usuario.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.tb_usuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.tb_usuario.FormattingEnabled = true;
            this.tb_usuario.Location = new System.Drawing.Point(51, 231);
            this.tb_usuario.Name = "tb_usuario";
            this.tb_usuario.Size = new System.Drawing.Size(200, 24);
            this.tb_usuario.TabIndex = 40;
            // 
            // tb_senha
            // 
            this.tb_senha.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tb_senha.Enabled = false;
            this.tb_senha.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.tb_senha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.tb_senha.Location = new System.Drawing.Point(48, 381);
            this.tb_senha.Name = "tb_senha";
            this.tb_senha.Size = new System.Drawing.Size(200, 24);
            this.tb_senha.TabIndex = 42;
            this.tb_senha.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label3.Location = new System.Drawing.Point(48, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 39;
            this.label3.Text = "Usuário";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label4.Location = new System.Drawing.Point(45, 357);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 20);
            this.label4.TabIndex = 41;
            this.label4.Text = "Nova Senha";
            // 
            // tb_confsenha
            // 
            this.tb_confsenha.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tb_confsenha.Enabled = false;
            this.tb_confsenha.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.tb_confsenha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.tb_confsenha.Location = new System.Drawing.Point(271, 381);
            this.tb_confsenha.Name = "tb_confsenha";
            this.tb_confsenha.Size = new System.Drawing.Size(200, 24);
            this.tb_confsenha.TabIndex = 47;
            this.tb_confsenha.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label5.Location = new System.Drawing.Point(268, 357);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 20);
            this.label5.TabIndex = 46;
            this.label5.Text = "Confirmar Nova Senha";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.label6.Location = new System.Drawing.Point(47, 273);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 20);
            this.label6.TabIndex = 48;
            this.label6.Text = "Pergunta";
            // 
            // tb_respo
            // 
            this.tb_respo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tb_respo.Enabled = false;
            this.tb_respo.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.tb_respo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.tb_respo.FormattingEnabled = true;
            this.tb_respo.Location = new System.Drawing.Point(269, 299);
            this.tb_respo.Name = "tb_respo";
            this.tb_respo.Size = new System.Drawing.Size(200, 24);
            this.tb_respo.TabIndex = 51;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.label7.Location = new System.Drawing.Point(268, 273);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 20);
            this.label7.TabIndex = 50;
            this.label7.Text = "Resposta";
            // 
            // btn_testResp
            // 
            this.btn_testResp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_testResp.BackColor = System.Drawing.Color.White;
            this.btn_testResp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_testResp.Enabled = false;
            this.btn_testResp.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.btn_testResp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_testResp.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.btn_testResp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.btn_testResp.Location = new System.Drawing.Point(489, 293);
            this.btn_testResp.Name = "btn_testResp";
            this.btn_testResp.Size = new System.Drawing.Size(95, 30);
            this.btn_testResp.TabIndex = 52;
            this.btn_testResp.Text = "Testar";
            this.btn_testResp.UseVisualStyleBackColor = false;
            this.btn_testResp.Click += new System.EventHandler(this.btn_testResp_Click);
            // 
            // lb_perg
            // 
            this.lb_perg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lb_perg.BackColor = System.Drawing.Color.Transparent;
            this.lb_perg.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lb_perg.ForeColor = System.Drawing.Color.Purple;
            this.lb_perg.Location = new System.Drawing.Point(48, 300);
            this.lb_perg.Name = "lb_perg";
            this.lb_perg.Size = new System.Drawing.Size(216, 58);
            this.lb_perg.TabIndex = 53;
            this.lb_perg.Text = "*";
            // 
            // btn_testUser
            // 
            this.btn_testUser.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_testUser.BackColor = System.Drawing.Color.White;
            this.btn_testUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_testUser.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.btn_testUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_testUser.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.btn_testUser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.btn_testUser.Location = new System.Drawing.Point(269, 228);
            this.btn_testUser.Name = "btn_testUser";
            this.btn_testUser.Size = new System.Drawing.Size(95, 30);
            this.btn_testUser.TabIndex = 54;
            this.btn_testUser.Text = "Testar";
            this.btn_testUser.UseVisualStyleBackColor = false;
            this.btn_testUser.Click += new System.EventHandler(this.btn_testUser_Click);
            // 
            // lnome
            // 
            this.lnome.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lnome.AutoSize = true;
            this.lnome.BackColor = System.Drawing.Color.Transparent;
            this.lnome.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lnome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.lnome.Location = new System.Drawing.Point(404, 228);
            this.lnome.Name = "lnome";
            this.lnome.Size = new System.Drawing.Size(45, 20);
            this.lnome.TabIndex = 55;
            this.lnome.Text = "******";
            this.lnome.Visible = false;
            // 
            // US_esqueci
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lnome);
            this.Controls.Add(this.btn_testUser);
            this.Controls.Add(this.lb_perg);
            this.Controls.Add(this.btn_testResp);
            this.Controls.Add(this.tb_respo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tb_confsenha);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_confirmar);
            this.Controls.Add(this.tb_usuario);
            this.Controls.Add(this.tb_senha);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panelLogo);
            this.Name = "US_esqueci";
            this.Size = new System.Drawing.Size(658, 524);
            this.Load += new System.EventHandler(this.US_esqueci_Load);
            this.panelLogo.ResumeLayout(false);
            this.panelLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel lb_fech;
        private System.Windows.Forms.Panel panelRodSuperio;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_confirmar;
        internal System.Windows.Forms.ComboBox tb_usuario;
        internal System.Windows.Forms.TextBox tb_senha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox tb_confsenha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        internal System.Windows.Forms.ComboBox tb_respo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_testResp;
        private System.Windows.Forms.Label lb_perg;
        private System.Windows.Forms.Button btn_testUser;
        private System.Windows.Forms.Label lnome;
    }
}
