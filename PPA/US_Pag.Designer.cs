﻿namespace PPA
{
    partial class US_Pag
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLogo = new System.Windows.Forms.Panel();
            this.lxdat = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lb_fech = new System.Windows.Forms.LinkLabel();
            this.label63 = new System.Windows.Forms.Label();
            this.panelRodSuperio = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.vUs = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.vCurso = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.vSaldo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.vMes = new System.Windows.Forms.Label();
            this.vPag = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.vAno = new System.Windows.Forms.Label();
            this.vSala = new System.Windows.Forms.Label();
            this.vTurno = new System.Windows.Forms.Label();
            this.vClasse = new System.Windows.Forms.Label();
            this.vNome = new System.Windows.Forms.Label();
            this.vID = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.vDat = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lxid = new System.Windows.Forms.Label();
            this.lxNome = new System.Windows.Forms.Label();
            this.lxCurso = new System.Windows.Forms.Label();
            this.lxClasse = new System.Windows.Forms.Label();
            this.lxTurno = new System.Windows.Forms.Label();
            this.lxSala = new System.Windows.Forms.Label();
            this.lxAno = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lxpag = new System.Windows.Forms.Label();
            this.lxMes = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.lxSaldo = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.lxus = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLogo
            // 
            this.panelLogo.BackColor = System.Drawing.Color.White;
            this.panelLogo.Controls.Add(this.lxdat);
            this.panelLogo.Controls.Add(this.pictureBox1);
            this.panelLogo.Controls.Add(this.lb_fech);
            this.panelLogo.Controls.Add(this.label63);
            this.panelLogo.Controls.Add(this.panelRodSuperio);
            this.panelLogo.Controls.Add(this.pictureBox2);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(597, 82);
            this.panelLogo.TabIndex = 39;
            this.panelLogo.Paint += new System.Windows.Forms.PaintEventHandler(this.panelLogo_Paint);
            // 
            // lxdat
            // 
            this.lxdat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxdat.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lxdat.Location = new System.Drawing.Point(462, 48);
            this.lxdat.Name = "lxdat";
            this.lxdat.Size = new System.Drawing.Size(112, 20);
            this.lxdat.TabIndex = 311;
            this.lxdat.Text = "*";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PPA.Properties.Resources.jkiu_fw;
            this.pictureBox1.Location = new System.Drawing.Point(58, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // lb_fech
            // 
            this.lb_fech.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_fech.AutoSize = true;
            this.lb_fech.DisabledLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_fech.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lb_fech.LinkColor = System.Drawing.Color.Gray;
            this.lb_fech.Location = new System.Drawing.Point(570, 18);
            this.lb_fech.Name = "lb_fech";
            this.lb_fech.Size = new System.Drawing.Size(21, 20);
            this.lb_fech.TabIndex = 16;
            this.lb_fech.TabStop = true;
            this.lb_fech.Text = "X";
            this.lb_fech.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_fech.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lb_fech_LinkClicked);
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label63.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label63.Location = new System.Drawing.Point(412, 50);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(52, 20);
            this.label63.TabIndex = 277;
            this.label63.Text = "* Data:";
            this.label63.Click += new System.EventHandler(this.label63_Click);
            // 
            // panelRodSuperio
            // 
            this.panelRodSuperio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.panelRodSuperio.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelRodSuperio.Location = new System.Drawing.Point(0, 0);
            this.panelRodSuperio.Name = "panelRodSuperio";
            this.panelRodSuperio.Size = new System.Drawing.Size(597, 5);
            this.panelRodSuperio.TabIndex = 13;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PPA.Properties.Resources._15824271_1216074758470968_68670057_o;
            this.pictureBox2.Location = new System.Drawing.Point(-1, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(168, 58);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.vUs);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.vCurso);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.vSaldo);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.vMes);
            this.panel4.Controls.Add(this.vPag);
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.vAno);
            this.panel4.Controls.Add(this.vSala);
            this.panel4.Controls.Add(this.vTurno);
            this.panel4.Controls.Add(this.vClasse);
            this.panel4.Controls.Add(this.vNome);
            this.panel4.Controls.Add(this.vID);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.panel10);
            this.panel4.Controls.Add(this.label60);
            this.panel4.Controls.Add(this.label61);
            this.panel4.Controls.Add(this.label62);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 446);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(597, 290);
            this.panel4.TabIndex = 251;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // vUs
            // 
            this.vUs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vUs.BackColor = System.Drawing.Color.Transparent;
            this.vUs.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F, System.Drawing.FontStyle.Underline);
            this.vUs.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vUs.Location = new System.Drawing.Point(421, 187);
            this.vUs.Name = "vUs";
            this.vUs.Size = new System.Drawing.Size(151, 20);
            this.vUs.TabIndex = 333;
            this.vUs.Text = "*************************";
            this.vUs.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(207, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 20);
            this.label4.TabIndex = 332;
            this.label4.Text = "*Nº";
            // 
            // vCurso
            // 
            this.vCurso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vCurso.BackColor = System.Drawing.Color.Transparent;
            this.vCurso.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vCurso.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vCurso.Location = new System.Drawing.Point(262, 72);
            this.vCurso.Name = "vCurso";
            this.vCurso.Size = new System.Drawing.Size(275, 20);
            this.vCurso.TabIndex = 321;
            this.vCurso.Text = "*********";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(207, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 20);
            this.label6.TabIndex = 314;
            this.label6.Text = "* Curso:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label7.Location = new System.Drawing.Point(446, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 20);
            this.label7.TabIndex = 331;
            this.label7.Text = "*Funcionário(a)";
            // 
            // vSaldo
            // 
            this.vSaldo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vSaldo.BackColor = System.Drawing.Color.Transparent;
            this.vSaldo.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vSaldo.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vSaldo.Location = new System.Drawing.Point(124, 184);
            this.vSaldo.Name = "vSaldo";
            this.vSaldo.Size = new System.Drawing.Size(126, 20);
            this.vSaldo.TabIndex = 330;
            this.vSaldo.Text = "*********";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label9.Location = new System.Drawing.Point(25, 184);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 20);
            this.label9.TabIndex = 329;
            this.label9.Text = "* Saldo Actual:";
            // 
            // vMes
            // 
            this.vMes.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vMes.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vMes.Location = new System.Drawing.Point(129, 131);
            this.vMes.Name = "vMes";
            this.vMes.Size = new System.Drawing.Size(443, 20);
            this.vMes.TabIndex = 328;
            this.vMes.Text = "*********";
            // 
            // vPag
            // 
            this.vPag.AutoSize = true;
            this.vPag.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vPag.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.vPag.Location = new System.Drawing.Point(238, 6);
            this.vPag.Name = "vPag";
            this.vPag.Size = new System.Drawing.Size(15, 20);
            this.vPag.TabIndex = 327;
            this.vPag.Text = "*";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label24.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label24.Location = new System.Drawing.Point(24, 131);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(104, 20);
            this.label24.TabIndex = 326;
            this.label24.Text = "* Mes(es) Pago:";
            // 
            // vAno
            // 
            this.vAno.BackColor = System.Drawing.Color.Transparent;
            this.vAno.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vAno.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vAno.Location = new System.Drawing.Point(74, 158);
            this.vAno.Name = "vAno";
            this.vAno.Size = new System.Drawing.Size(54, 20);
            this.vAno.TabIndex = 325;
            this.vAno.Text = "*********";
            // 
            // vSala
            // 
            this.vSala.BackColor = System.Drawing.Color.Transparent;
            this.vSala.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vSala.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vSala.Location = new System.Drawing.Point(255, 104);
            this.vSala.Name = "vSala";
            this.vSala.Size = new System.Drawing.Size(54, 20);
            this.vSala.TabIndex = 324;
            this.vSala.Text = "*********";
            // 
            // vTurno
            // 
            this.vTurno.BackColor = System.Drawing.Color.Transparent;
            this.vTurno.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vTurno.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vTurno.Location = new System.Drawing.Point(75, 104);
            this.vTurno.Name = "vTurno";
            this.vTurno.Size = new System.Drawing.Size(94, 20);
            this.vTurno.TabIndex = 323;
            this.vTurno.Text = "*********";
            // 
            // vClasse
            // 
            this.vClasse.BackColor = System.Drawing.Color.Transparent;
            this.vClasse.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vClasse.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vClasse.Location = new System.Drawing.Point(82, 72);
            this.vClasse.Name = "vClasse";
            this.vClasse.Size = new System.Drawing.Size(66, 20);
            this.vClasse.TabIndex = 322;
            this.vClasse.Text = "*********";
            // 
            // vNome
            // 
            this.vNome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vNome.BackColor = System.Drawing.Color.Transparent;
            this.vNome.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vNome.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vNome.Location = new System.Drawing.Point(266, 41);
            this.vNome.Name = "vNome";
            this.vNome.Size = new System.Drawing.Size(313, 20);
            this.vNome.TabIndex = 320;
            this.vNome.Text = "*********";
            // 
            // vID
            // 
            this.vID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vID.BackColor = System.Drawing.Color.Transparent;
            this.vID.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vID.ForeColor = System.Drawing.Color.DodgerBlue;
            this.vID.Location = new System.Drawing.Point(54, 41);
            this.vID.Name = "vID";
            this.vID.Size = new System.Drawing.Size(130, 20);
            this.vID.TabIndex = 319;
            this.vID.Text = "*********";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label31.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label31.Location = new System.Drawing.Point(24, 160);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(47, 20);
            this.label31.TabIndex = 318;
            this.label31.Text = "* Ano:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label32.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label32.Location = new System.Drawing.Point(207, 104);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 20);
            this.label32.TabIndex = 317;
            this.label32.Text = "* Sala:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label33.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label33.Location = new System.Drawing.Point(24, 104);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 20);
            this.label33.TabIndex = 316;
            this.label33.Text = "*Turno:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label34.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label34.Location = new System.Drawing.Point(19, 72);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(61, 20);
            this.label34.TabIndex = 315;
            this.label34.Text = "* Classe:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label35.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label35.Location = new System.Drawing.Point(18, 41);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(36, 20);
            this.label35.TabIndex = 313;
            this.label35.Text = "* ID:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label36.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label36.Location = new System.Drawing.Point(207, 41);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(58, 20);
            this.label36.TabIndex = 312;
            this.label36.Text = "* Nome:";
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F);
            this.label37.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label37.Location = new System.Drawing.Point(25, 2);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(171, 31);
            this.label37.TabIndex = 311;
            this.label37.Text = "Recibo de Pagamento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(199, 267);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(194, 18);
            this.label2.TabIndex = 309;
            this.label2.Text = "*OBS.: Não há devolção de valores!";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label21.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label21.Location = new System.Drawing.Point(9, 248);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(566, 18);
            this.label21.TabIndex = 308;
            this.label21.Text = "*Pague a sua Propina de 1 à 10 de cada mês, Caso contrario é acrescido uma multa," +
    " de acordo com o RFE.";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label22.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label22.Location = new System.Drawing.Point(419, 222);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(168, 18);
            this.label22.TabIndex = 306;
            this.label22.Text = "*Guilhermina Manuel Silvestre";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9.5F);
            this.label23.ForeColor = System.Drawing.Color.Purple;
            this.label23.Location = new System.Drawing.Point(9, 224);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 15);
            this.label23.TabIndex = 303;
            this.label23.Text = "*BIC: 5560924410";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel5.Location = new System.Drawing.Point(12, 243);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(569, 4);
            this.panel5.TabIndex = 302;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel10.Location = new System.Drawing.Point(12, 213);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(569, 4);
            this.panel10.TabIndex = 293;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9.5F);
            this.label60.ForeColor = System.Drawing.Color.Purple;
            this.label60.Location = new System.Drawing.Point(113, 224);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(77, 15);
            this.label60.TabIndex = 304;
            this.label60.Text = "*BCI: 9900985";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9.5F);
            this.label61.ForeColor = System.Drawing.Color.Purple;
            this.label61.Location = new System.Drawing.Point(195, 224);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(219, 15);
            this.label61.TabIndex = 305;
            this.label61.Text = "*IBAN: A006.0051.0000.5560.9244.1011.3";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label62.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label62.Location = new System.Drawing.Point(410, 221);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(18, 20);
            this.label62.TabIndex = 307;
            this.label62.Text = "|";
            this.label62.Visible = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1, 290);
            this.panel6.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.vDat);
            this.panel7.Controls.Add(this.label39);
            this.panel7.Controls.Add(this.pictureBox3);
            this.panel7.Controls.Add(this.linkLabel1);
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.pictureBox4);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 364);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(597, 82);
            this.panel7.TabIndex = 252;
            // 
            // vDat
            // 
            this.vDat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.vDat.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.vDat.Location = new System.Drawing.Point(468, 44);
            this.vDat.Name = "vDat";
            this.vDat.Size = new System.Drawing.Size(112, 20);
            this.vDat.TabIndex = 313;
            this.vDat.Text = "*";
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label39.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label39.Location = new System.Drawing.Point(418, 46);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(52, 20);
            this.label39.TabIndex = 312;
            this.label39.Text = "* Data:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::PPA.Properties.Resources.jkiu_fw;
            this.pictureBox3.Location = new System.Drawing.Point(58, 54);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(163, 26);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.DisabledLinkColor = System.Drawing.Color.DimGray;
            this.linkLabel1.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel1.LinkColor = System.Drawing.Color.Gray;
            this.linkLabel1.Location = new System.Drawing.Point(570, 18);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(21, 20);
            this.linkLabel1.TabIndex = 16;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "X";
            this.linkLabel1.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(597, 5);
            this.panel8.TabIndex = 13;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::PPA.Properties.Resources._15824271_1216074758470968_68670057_o;
            this.pictureBox4.Location = new System.Drawing.Point(-1, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(168, 58);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 25;
            this.pictureBox4.TabStop = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label51.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label51.Location = new System.Drawing.Point(424, 216);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(18, 20);
            this.label51.TabIndex = 274;
            this.label51.Text = "|";
            this.label51.Visible = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9.5F);
            this.label49.ForeColor = System.Drawing.Color.Purple;
            this.label49.Location = new System.Drawing.Point(196, 219);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(219, 15);
            this.label49.TabIndex = 262;
            this.label49.Text = "*IBAN: A006.0051.0000.5560.9244.1011.3";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9.5F);
            this.label48.ForeColor = System.Drawing.Color.Purple;
            this.label48.Location = new System.Drawing.Point(114, 219);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(77, 15);
            this.label48.TabIndex = 261;
            this.label48.Text = "*BCI: 9900985";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1, 282);
            this.panel3.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F);
            this.label20.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label20.Location = new System.Drawing.Point(27, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(171, 31);
            this.label20.TabIndex = 30;
            this.label20.Text = "Recibo de Pagamento";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label19.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label19.Location = new System.Drawing.Point(209, 39);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 20);
            this.label19.TabIndex = 171;
            this.label19.Text = "* Nome:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label18.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label18.Location = new System.Drawing.Point(20, 39);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 20);
            this.label18.TabIndex = 172;
            this.label18.Text = "* ID:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label17.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label17.Location = new System.Drawing.Point(209, 70);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 20);
            this.label17.TabIndex = 173;
            this.label17.Text = "* Curso:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label16.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label16.Location = new System.Drawing.Point(21, 70);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 20);
            this.label16.TabIndex = 174;
            this.label16.Text = "* Classe:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label15.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label15.Location = new System.Drawing.Point(26, 102);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 20);
            this.label15.TabIndex = 175;
            this.label15.Text = "*Turno:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label14.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label14.Location = new System.Drawing.Point(209, 102);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 20);
            this.label14.TabIndex = 176;
            this.label14.Text = "* Sala:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label12.Location = new System.Drawing.Point(26, 158);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 20);
            this.label12.TabIndex = 178;
            this.label12.Text = "* Ano:";
            // 
            // lxid
            // 
            this.lxid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lxid.BackColor = System.Drawing.Color.Transparent;
            this.lxid.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxid.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxid.Location = new System.Drawing.Point(56, 39);
            this.lxid.Name = "lxid";
            this.lxid.Size = new System.Drawing.Size(130, 20);
            this.lxid.TabIndex = 179;
            this.lxid.Text = "*********";
            // 
            // lxNome
            // 
            this.lxNome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lxNome.BackColor = System.Drawing.Color.Transparent;
            this.lxNome.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxNome.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxNome.Location = new System.Drawing.Point(268, 39);
            this.lxNome.Name = "lxNome";
            this.lxNome.Size = new System.Drawing.Size(313, 20);
            this.lxNome.TabIndex = 180;
            this.lxNome.Text = "*********";
            // 
            // lxCurso
            // 
            this.lxCurso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lxCurso.BackColor = System.Drawing.Color.Transparent;
            this.lxCurso.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxCurso.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxCurso.Location = new System.Drawing.Point(264, 70);
            this.lxCurso.Name = "lxCurso";
            this.lxCurso.Size = new System.Drawing.Size(275, 20);
            this.lxCurso.TabIndex = 181;
            this.lxCurso.Text = "*********";
            // 
            // lxClasse
            // 
            this.lxClasse.BackColor = System.Drawing.Color.Transparent;
            this.lxClasse.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxClasse.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxClasse.Location = new System.Drawing.Point(84, 70);
            this.lxClasse.Name = "lxClasse";
            this.lxClasse.Size = new System.Drawing.Size(66, 20);
            this.lxClasse.TabIndex = 182;
            this.lxClasse.Text = "*********";
            // 
            // lxTurno
            // 
            this.lxTurno.BackColor = System.Drawing.Color.Transparent;
            this.lxTurno.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxTurno.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxTurno.Location = new System.Drawing.Point(77, 102);
            this.lxTurno.Name = "lxTurno";
            this.lxTurno.Size = new System.Drawing.Size(94, 20);
            this.lxTurno.TabIndex = 183;
            this.lxTurno.Text = "*********";
            // 
            // lxSala
            // 
            this.lxSala.BackColor = System.Drawing.Color.Transparent;
            this.lxSala.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxSala.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxSala.Location = new System.Drawing.Point(257, 102);
            this.lxSala.Name = "lxSala";
            this.lxSala.Size = new System.Drawing.Size(54, 20);
            this.lxSala.TabIndex = 184;
            this.lxSala.Text = "*********";
            // 
            // lxAno
            // 
            this.lxAno.BackColor = System.Drawing.Color.Transparent;
            this.lxAno.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxAno.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxAno.Location = new System.Drawing.Point(76, 156);
            this.lxAno.Name = "lxAno";
            this.lxAno.Size = new System.Drawing.Size(54, 20);
            this.lxAno.TabIndex = 185;
            this.lxAno.Text = "*********";
            this.lxAno.Click += new System.EventHandler(this.label5_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.panel2.Location = new System.Drawing.Point(13, 208);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(569, 4);
            this.panel2.TabIndex = 187;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(26, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 20);
            this.label3.TabIndex = 249;
            this.label3.Text = "* Mes(es) Pago:";
            // 
            // lxpag
            // 
            this.lxpag.AutoSize = true;
            this.lxpag.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxpag.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lxpag.Location = new System.Drawing.Point(240, 4);
            this.lxpag.Name = "lxpag";
            this.lxpag.Size = new System.Drawing.Size(15, 20);
            this.lxpag.TabIndex = 251;
            this.lxpag.Text = "*";
            // 
            // lxMes
            // 
            this.lxMes.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxMes.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxMes.Location = new System.Drawing.Point(131, 129);
            this.lxMes.Name = "lxMes";
            this.lxMes.Size = new System.Drawing.Size(443, 20);
            this.lxMes.TabIndex = 252;
            this.lxMes.Text = "*********";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label44.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label44.Location = new System.Drawing.Point(27, 182);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(98, 20);
            this.label44.TabIndex = 254;
            this.label44.Text = "* Saldo Actual:";
            // 
            // lxSaldo
            // 
            this.lxSaldo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lxSaldo.BackColor = System.Drawing.Color.Transparent;
            this.lxSaldo.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lxSaldo.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxSaldo.Location = new System.Drawing.Point(126, 182);
            this.lxSaldo.Name = "lxSaldo";
            this.lxSaldo.Size = new System.Drawing.Size(126, 20);
            this.lxSaldo.TabIndex = 255;
            this.lxSaldo.Text = "*********";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label13.Location = new System.Drawing.Point(448, 156);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 20);
            this.label13.TabIndex = 256;
            this.label13.Text = "*Funcionário(a)";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.panel9.Location = new System.Drawing.Point(13, 238);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(569, 4);
            this.panel9.TabIndex = 258;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9.5F);
            this.label47.ForeColor = System.Drawing.Color.Purple;
            this.label47.Location = new System.Drawing.Point(10, 219);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(95, 15);
            this.label47.TabIndex = 260;
            this.label47.Text = "*BIC: 5560924410";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label50.ForeColor = System.Drawing.Color.Purple;
            this.label50.Location = new System.Drawing.Point(420, 217);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(168, 18);
            this.label50.TabIndex = 263;
            this.label50.Text = "*Guilhermina Manuel Silvestre";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label52.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label52.Location = new System.Drawing.Point(10, 243);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(566, 18);
            this.label52.TabIndex = 275;
            this.label52.Text = "*Pague a sua Propina de 1 à 10 de cada mês, Caso contrario é acrescido uma multa," +
    " de acordo com o RFE.";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.label53.ForeColor = System.Drawing.Color.Red;
            this.label53.Location = new System.Drawing.Point(200, 262);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(194, 18);
            this.label53.TabIndex = 276;
            this.label53.Text = "*OBS.: Não há devolção de valores!";
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label64.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label64.Location = new System.Drawing.Point(209, 4);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(31, 20);
            this.label64.TabIndex = 278;
            this.label64.Text = "*Nº";
            // 
            // lxus
            // 
            this.lxus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lxus.BackColor = System.Drawing.Color.Transparent;
            this.lxus.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F, System.Drawing.FontStyle.Underline);
            this.lxus.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lxus.Location = new System.Drawing.Point(423, 185);
            this.lxus.Name = "lxus";
            this.lxus.Size = new System.Drawing.Size(151, 20);
            this.lxus.TabIndex = 310;
            this.lxus.Text = "*************************";
            this.lxus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lxus);
            this.panel1.Controls.Add(this.label64);
            this.panel1.Controls.Add(this.label53);
            this.panel1.Controls.Add(this.lxCurso);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label52);
            this.panel1.Controls.Add(this.label50);
            this.panel1.Controls.Add(this.label47);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.lxSaldo);
            this.panel1.Controls.Add(this.label44);
            this.panel1.Controls.Add(this.lxMes);
            this.panel1.Controls.Add(this.lxpag);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.lxAno);
            this.panel1.Controls.Add(this.lxSala);
            this.panel1.Controls.Add(this.lxTurno);
            this.panel1.Controls.Add(this.lxClasse);
            this.panel1.Controls.Add(this.lxNome);
            this.panel1.Controls.Add(this.lxid);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label48);
            this.panel1.Controls.Add(this.label49);
            this.panel1.Controls.Add(this.label51);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 82);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(597, 282);
            this.panel1.TabIndex = 250;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // US_Pag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelLogo);
            this.Name = "US_Pag";
            this.Size = new System.Drawing.Size(597, 736);
            this.Load += new System.EventHandler(this.US_Pag_Load);
            this.panelLogo.ResumeLayout(false);
            this.panelLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel lb_fech;
        private System.Windows.Forms.Panel panelRodSuperio;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lxid;
        private System.Windows.Forms.Label lxNome;
        private System.Windows.Forms.Label lxCurso;
        private System.Windows.Forms.Label lxClasse;
        private System.Windows.Forms.Label lxTurno;
        private System.Windows.Forms.Label lxSala;
        private System.Windows.Forms.Label lxAno;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lxpag;
        private System.Windows.Forms.Label lxMes;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label lxSaldo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label lxus;
        private System.Windows.Forms.Label lxdat;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label vUs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label vCurso;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label vSaldo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label vMes;
        private System.Windows.Forms.Label vPag;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label vAno;
        private System.Windows.Forms.Label vSala;
        private System.Windows.Forms.Label vTurno;
        private System.Windows.Forms.Label vClasse;
        private System.Windows.Forms.Label vNome;
        private System.Windows.Forms.Label vID;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label vDat;
        private System.Windows.Forms.Label label39;
    }
}
