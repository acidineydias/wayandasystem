﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Animação
using AnimatorNS;

using System.IO;


namespace PPA
{
    public partial class US_splash : UserControl
    {
        Animator ad = new Animator();
        public US_splash()
        {
            InitializeComponent();
           
        }

        private void US_splash_Load(object sender, EventArgs e)
        {
            ParentForm.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;

            ad.AnimationType = AnimationType.Mosaic;
            ad.Interval = 30;

            ad.Show(pictureBox1);
        }
    }
}
