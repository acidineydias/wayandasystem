﻿namespace PPA
{
    partial class US_ADIM
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLogo = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lb_fech = new System.Windows.Forms.LinkLabel();
            this.panelRodSuperio = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.TC = new Heyech_Application_Development.HeyechTabControl();
            this.tpMEN = new System.Windows.Forms.TabPage();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.lb_esqueceu = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.tpCRD = new System.Windows.Forms.TabPage();
            this.tbAdmID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_pesqDep = new System.Windows.Forms.Button();
            this.tb_pesqDep = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.d5 = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.d4 = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.d3 = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.d2 = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.d1 = new System.Windows.Forms.ListBox();
            this.label92 = new System.Windows.Forms.Label();
            this.d = new System.Windows.Forms.ListBox();
            this.tbAdmPerg = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbAdmResp = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAdmNivel = new System.Windows.Forms.ComboBox();
            this.tbAdm = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.tbAdmPasse = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_AdmDelet = new System.Windows.Forms.Button();
            this.btn_AdmAct = new System.Windows.Forms.Button();
            this.btn_Admcad = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.panelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.TC.SuspendLayout();
            this.tpMEN.SuspendLayout();
            this.tpCRD.SuspendLayout();
            this.panel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLogo
            // 
            this.panelLogo.BackColor = System.Drawing.Color.White;
            this.panelLogo.Controls.Add(this.pictureBox1);
            this.panelLogo.Controls.Add(this.lb_fech);
            this.panelLogo.Controls.Add(this.panelRodSuperio);
            this.panelLogo.Controls.Add(this.pictureBox2);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(660, 118);
            this.panelLogo.TabIndex = 41;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PPA.Properties.Resources.jkiu_fw;
            this.pictureBox1.Location = new System.Drawing.Point(58, 86);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // lb_fech
            // 
            this.lb_fech.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_fech.AutoSize = true;
            this.lb_fech.DisabledLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_fech.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lb_fech.LinkColor = System.Drawing.Color.Gray;
            this.lb_fech.Location = new System.Drawing.Point(633, 18);
            this.lb_fech.Name = "lb_fech";
            this.lb_fech.Size = new System.Drawing.Size(21, 20);
            this.lb_fech.TabIndex = 16;
            this.lb_fech.TabStop = true;
            this.lb_fech.Text = "X";
            this.lb_fech.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_fech.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lb_fech_LinkClicked);
            // 
            // panelRodSuperio
            // 
            this.panelRodSuperio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.panelRodSuperio.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelRodSuperio.Location = new System.Drawing.Point(0, 0);
            this.panelRodSuperio.Name = "panelRodSuperio";
            this.panelRodSuperio.Size = new System.Drawing.Size(660, 15);
            this.panelRodSuperio.TabIndex = 13;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PPA.Properties.Resources._15824271_1216074758470968_68670057_o;
            this.pictureBox2.Location = new System.Drawing.Point(-1, 37);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(168, 58);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            // 
            // TC
            // 
            this.TC.Controls.Add(this.tpMEN);
            this.TC.Controls.Add(this.tpCRD);
            this.TC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TC.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.TC.ItemSize = new System.Drawing.Size(0, 1);
            this.TC.Location = new System.Drawing.Point(0, 118);
            this.TC.Name = "TC";
            this.TC.SelectedIndex = 0;
            this.TC.Size = new System.Drawing.Size(660, 364);
            this.TC.TabIndex = 42;
            // 
            // tpMEN
            // 
            this.tpMEN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tpMEN.Controls.Add(this.linkLabel1);
            this.tpMEN.Controls.Add(this.lb_esqueceu);
            this.tpMEN.Controls.Add(this.label2);
            this.tpMEN.Location = new System.Drawing.Point(4, 5);
            this.tpMEN.Name = "tpMEN";
            this.tpMEN.Padding = new System.Windows.Forms.Padding(3);
            this.tpMEN.Size = new System.Drawing.Size(652, 355);
            this.tpMEN.TabIndex = 0;
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.SystemColors.GrayText;
            this.linkLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkLabel1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.linkLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel1.LinkColor = System.Drawing.Color.Purple;
            this.linkLabel1.Location = new System.Drawing.Point(50, 92);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(165, 20);
            this.linkLabel1.TabIndex = 61;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "*Trocar Senha de Usuário";
            this.linkLabel1.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // lb_esqueceu
            // 
            this.lb_esqueceu.ActiveLinkColor = System.Drawing.SystemColors.GrayText;
            this.lb_esqueceu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lb_esqueceu.AutoSize = true;
            this.lb_esqueceu.BackColor = System.Drawing.Color.Transparent;
            this.lb_esqueceu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb_esqueceu.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lb_esqueceu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.lb_esqueceu.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lb_esqueceu.LinkColor = System.Drawing.Color.Purple;
            this.lb_esqueceu.Location = new System.Drawing.Point(50, 56);
            this.lb_esqueceu.Name = "lb_esqueceu";
            this.lb_esqueceu.Size = new System.Drawing.Size(99, 20);
            this.lb_esqueceu.TabIndex = 60;
            this.lb_esqueceu.TabStop = true;
            this.lb_esqueceu.Text = "*CRUD Usuário";
            this.lb_esqueceu.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_esqueceu.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lb_esqueceu_LinkClicked);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(45, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(245, 38);
            this.label2.TabIndex = 59;
            this.label2.Text = "Administrador";
            // 
            // tpCRD
            // 
            this.tpCRD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tpCRD.Controls.Add(this.tbAdmID);
            this.tpCRD.Controls.Add(this.label10);
            this.tpCRD.Controls.Add(this.btn_pesqDep);
            this.tpCRD.Controls.Add(this.tb_pesqDep);
            this.tpCRD.Controls.Add(this.label9);
            this.tpCRD.Controls.Add(this.d5);
            this.tpCRD.Controls.Add(this.label8);
            this.tpCRD.Controls.Add(this.d4);
            this.tpCRD.Controls.Add(this.label7);
            this.tpCRD.Controls.Add(this.d3);
            this.tpCRD.Controls.Add(this.label6);
            this.tpCRD.Controls.Add(this.d2);
            this.tpCRD.Controls.Add(this.label5);
            this.tpCRD.Controls.Add(this.d1);
            this.tpCRD.Controls.Add(this.label92);
            this.tpCRD.Controls.Add(this.d);
            this.tpCRD.Controls.Add(this.tbAdmPerg);
            this.tpCRD.Controls.Add(this.label3);
            this.tpCRD.Controls.Add(this.tbAdmResp);
            this.tpCRD.Controls.Add(this.label4);
            this.tpCRD.Controls.Add(this.tbAdmNivel);
            this.tpCRD.Controls.Add(this.tbAdm);
            this.tpCRD.Controls.Add(this.label33);
            this.tpCRD.Controls.Add(this.label34);
            this.tpCRD.Controls.Add(this.tbAdmPasse);
            this.tpCRD.Controls.Add(this.label38);
            this.tpCRD.Controls.Add(this.label1);
            this.tpCRD.Controls.Add(this.btn_AdmDelet);
            this.tpCRD.Controls.Add(this.btn_AdmAct);
            this.tpCRD.Controls.Add(this.btn_Admcad);
            this.tpCRD.Location = new System.Drawing.Point(4, 5);
            this.tpCRD.Name = "tpCRD";
            this.tpCRD.Padding = new System.Windows.Forms.Padding(3);
            this.tpCRD.Size = new System.Drawing.Size(652, 355);
            this.tpCRD.TabIndex = 1;
            this.tpCRD.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // tbAdmID
            // 
            this.tbAdmID.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbAdmID.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbAdmID.Location = new System.Drawing.Point(8, 93);
            this.tbAdmID.Name = "tbAdmID";
            this.tbAdmID.Size = new System.Drawing.Size(41, 25);
            this.tbAdmID.TabIndex = 249;
            this.tbAdmID.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label10.Location = new System.Drawing.Point(54, 183);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 20);
            this.label10.TabIndex = 248;
            this.label10.Text = "*Pesquisar";
            // 
            // btn_pesqDep
            // 
            this.btn_pesqDep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_pesqDep.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btn_pesqDep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesqDep.Font = new System.Drawing.Font("Rod", 12F);
            this.btn_pesqDep.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_pesqDep.Location = new System.Drawing.Point(224, 207);
            this.btn_pesqDep.Name = "btn_pesqDep";
            this.btn_pesqDep.Size = new System.Drawing.Size(21, 23);
            this.btn_pesqDep.TabIndex = 247;
            this.btn_pesqDep.UseVisualStyleBackColor = true;
            this.btn_pesqDep.Click += new System.EventHandler(this.btn_pesqDep_Click);
            // 
            // tb_pesqDep
            // 
            this.tb_pesqDep.Font = new System.Drawing.Font("Rod", 12F);
            this.tb_pesqDep.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_pesqDep.Location = new System.Drawing.Point(58, 207);
            this.tb_pesqDep.Name = "tb_pesqDep";
            this.tb_pesqDep.Size = new System.Drawing.Size(160, 23);
            this.tb_pesqDep.TabIndex = 246;
            this.tb_pesqDep.TextChanged += new System.EventHandler(this.tb_pesqDep_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label9.Location = new System.Drawing.Point(512, 233);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 20);
            this.label9.TabIndex = 245;
            this.label9.Text = "*Resposta";
            // 
            // d5
            // 
            this.d5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d5.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d5.ForeColor = System.Drawing.Color.Gray;
            this.d5.FormattingEnabled = true;
            this.d5.HorizontalScrollbar = true;
            this.d5.ItemHeight = 18;
            this.d5.Location = new System.Drawing.Point(517, 254);
            this.d5.Name = "d5";
            this.d5.Size = new System.Drawing.Size(116, 94);
            this.d5.TabIndex = 244;
            this.d5.SelectedIndexChanged += new System.EventHandler(this.d_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label8.Location = new System.Drawing.Point(390, 233);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 20);
            this.label8.TabIndex = 243;
            this.label8.Text = "*Pergunta";
            // 
            // d4
            // 
            this.d4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d4.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d4.ForeColor = System.Drawing.Color.Gray;
            this.d4.FormattingEnabled = true;
            this.d4.HorizontalScrollbar = true;
            this.d4.ItemHeight = 18;
            this.d4.Location = new System.Drawing.Point(395, 254);
            this.d4.Name = "d4";
            this.d4.Size = new System.Drawing.Size(116, 94);
            this.d4.TabIndex = 242;
            this.d4.SelectedIndexChanged += new System.EventHandler(this.d_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label7.Location = new System.Drawing.Point(296, 233);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 20);
            this.label7.TabIndex = 241;
            this.label7.Text = "*Nivel";
            // 
            // d3
            // 
            this.d3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d3.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d3.ForeColor = System.Drawing.Color.Gray;
            this.d3.FormattingEnabled = true;
            this.d3.ItemHeight = 18;
            this.d3.Location = new System.Drawing.Point(300, 254);
            this.d3.Name = "d3";
            this.d3.Size = new System.Drawing.Size(89, 94);
            this.d3.TabIndex = 240;
            this.d3.SelectedIndexChanged += new System.EventHandler(this.d_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(183, 233);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 20);
            this.label6.TabIndex = 239;
            this.label6.Text = "*Palavra-Passe";
            // 
            // d2
            // 
            this.d2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d2.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d2.ForeColor = System.Drawing.Color.Gray;
            this.d2.FormattingEnabled = true;
            this.d2.HorizontalScrollbar = true;
            this.d2.ItemHeight = 18;
            this.d2.Location = new System.Drawing.Point(188, 254);
            this.d2.Name = "d2";
            this.d2.Size = new System.Drawing.Size(107, 94);
            this.d2.TabIndex = 238;
            this.d2.SelectedIndexChanged += new System.EventHandler(this.d_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(82, 233);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 237;
            this.label5.Text = "*Usuário";
            // 
            // d1
            // 
            this.d1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d1.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d1.ForeColor = System.Drawing.Color.Gray;
            this.d1.FormattingEnabled = true;
            this.d1.HorizontalScrollbar = true;
            this.d1.ItemHeight = 18;
            this.d1.Location = new System.Drawing.Point(87, 254);
            this.d1.Name = "d1";
            this.d1.Size = new System.Drawing.Size(95, 94);
            this.d1.TabIndex = 236;
            this.d1.SelectedIndexChanged += new System.EventHandler(this.d_SelectedIndexChanged);
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label92.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label92.Location = new System.Drawing.Point(35, 233);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(28, 20);
            this.label92.TabIndex = 235;
            this.label92.Text = "*ID";
            // 
            // d
            // 
            this.d.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d.ForeColor = System.Drawing.Color.Gray;
            this.d.FormattingEnabled = true;
            this.d.ItemHeight = 18;
            this.d.Location = new System.Drawing.Point(40, 254);
            this.d.Name = "d";
            this.d.Size = new System.Drawing.Size(37, 94);
            this.d.TabIndex = 234;
            this.d.SelectedIndexChanged += new System.EventHandler(this.d_SelectedIndexChanged);
            // 
            // tbAdmPerg
            // 
            this.tbAdmPerg.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbAdmPerg.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbAdmPerg.Location = new System.Drawing.Point(58, 151);
            this.tbAdmPerg.Name = "tbAdmPerg";
            this.tbAdmPerg.Size = new System.Drawing.Size(160, 25);
            this.tbAdmPerg.TabIndex = 143;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(50, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 20);
            this.label3.TabIndex = 146;
            this.label3.Text = "*Pergunta";
            // 
            // tbAdmResp
            // 
            this.tbAdmResp.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbAdmResp.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbAdmResp.Location = new System.Drawing.Point(236, 150);
            this.tbAdmResp.Name = "tbAdmResp";
            this.tbAdmResp.Size = new System.Drawing.Size(160, 25);
            this.tbAdmResp.TabIndex = 144;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(230, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 20);
            this.label4.TabIndex = 145;
            this.label4.Text = "*Resposta";
            // 
            // tbAdmNivel
            // 
            this.tbAdmNivel.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbAdmNivel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbAdmNivel.FormattingEnabled = true;
            this.tbAdmNivel.Items.AddRange(new object[] {
            "Normal",
            "Administrador"});
            this.tbAdmNivel.Location = new System.Drawing.Point(412, 89);
            this.tbAdmNivel.Name = "tbAdmNivel";
            this.tbAdmNivel.Size = new System.Drawing.Size(160, 28);
            this.tbAdmNivel.TabIndex = 137;
            this.tbAdmNivel.Text = "Normal";
            // 
            // tbAdm
            // 
            this.tbAdm.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbAdm.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbAdm.Location = new System.Drawing.Point(55, 93);
            this.tbAdm.Name = "tbAdm";
            this.tbAdm.Size = new System.Drawing.Size(160, 25);
            this.tbAdm.TabIndex = 132;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label33.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label33.Location = new System.Drawing.Point(47, 65);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(62, 20);
            this.label33.TabIndex = 142;
            this.label33.Text = "*Usuário";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label34.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label34.Location = new System.Drawing.Point(409, 62);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(47, 20);
            this.label34.TabIndex = 141;
            this.label34.Text = "*Nivel";
            // 
            // tbAdmPasse
            // 
            this.tbAdmPasse.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbAdmPasse.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbAdmPasse.Location = new System.Drawing.Point(233, 92);
            this.tbAdmPasse.Name = "tbAdmPasse";
            this.tbAdmPasse.Size = new System.Drawing.Size(160, 25);
            this.tbAdmPasse.TabIndex = 133;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label38.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label38.Location = new System.Drawing.Point(227, 65);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(104, 20);
            this.label38.TabIndex = 138;
            this.label38.Text = "*Palavra-Passe";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(45, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(374, 38);
            this.label1.TabIndex = 60;
            this.label1.Text = "Administrador -  *CRUD Usuário";
            // 
            // btn_AdmDelet
            // 
            this.btn_AdmDelet.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_AdmDelet.BackgroundImage = global::PPA.Properties.Resources.save_delete;
            this.btn_AdmDelet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AdmDelet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AdmDelet.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_AdmDelet.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_AdmDelet.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_AdmDelet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AdmDelet.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_AdmDelet.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_AdmDelet.Location = new System.Drawing.Point(539, 148);
            this.btn_AdmDelet.Name = "btn_AdmDelet";
            this.btn_AdmDelet.Size = new System.Drawing.Size(30, 28);
            this.btn_AdmDelet.TabIndex = 222;
            this.btn_AdmDelet.UseVisualStyleBackColor = false;
            this.btn_AdmDelet.Click += new System.EventHandler(this.btn_AdmDelet_Click);
            // 
            // btn_AdmAct
            // 
            this.btn_AdmAct.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_AdmAct.BackgroundImage = global::PPA.Properties.Resources.save_41_;
            this.btn_AdmAct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AdmAct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_AdmAct.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_AdmAct.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_AdmAct.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_AdmAct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_AdmAct.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_AdmAct.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_AdmAct.Location = new System.Drawing.Point(475, 148);
            this.btn_AdmAct.Name = "btn_AdmAct";
            this.btn_AdmAct.Size = new System.Drawing.Size(30, 28);
            this.btn_AdmAct.TabIndex = 221;
            this.btn_AdmAct.UseVisualStyleBackColor = false;
            this.btn_AdmAct.Click += new System.EventHandler(this.btn_AdmAct_Click);
            // 
            // btn_Admcad
            // 
            this.btn_Admcad.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_Admcad.BackgroundImage = global::PPA.Properties.Resources.disk_blue_ok;
            this.btn_Admcad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Admcad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Admcad.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_Admcad.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_Admcad.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_Admcad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Admcad.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_Admcad.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_Admcad.Location = new System.Drawing.Point(414, 148);
            this.btn_Admcad.Name = "btn_Admcad";
            this.btn_Admcad.Size = new System.Drawing.Size(30, 28);
            this.btn_Admcad.TabIndex = 220;
            this.btn_Admcad.UseVisualStyleBackColor = false;
            this.btn_Admcad.Click += new System.EventHandler(this.btn_Admcad_Click);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.panel13.Controls.Add(this.button1);
            this.panel13.Controls.Add(this.button28);
            this.panel13.Controls.Add(this.button31);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel13.Location = new System.Drawing.Point(0, 482);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(660, 44);
            this.panel13.TabIndex = 42;
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Image = global::PPA.Properties.Resources.k4_fw;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(180, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 30);
            this.button1.TabIndex = 64;
            this.button1.Text = "      CRUD Usuário";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button28
            // 
            this.button28.AutoSize = true;
            this.button28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button28.FlatAppearance.BorderSize = 0;
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button28.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button28.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button28.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button28.Location = new System.Drawing.Point(1214, 7);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(59, 30);
            this.button28.TabIndex = 63;
            this.button28.Text = " (0_0)";
            this.button28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button28.UseVisualStyleBackColor = false;
            // 
            // button31
            // 
            this.button31.AutoSize = true;
            this.button31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button31.FlatAppearance.BorderSize = 0;
            this.button31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button31.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button31.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button31.Image = global::PPA.Properties.Resources.h2_fw;
            this.button31.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button31.Location = new System.Drawing.Point(16, 7);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(148, 30);
            this.button31.TabIndex = 58;
            this.button31.Text = "      Menu";
            this.button31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button31.UseVisualStyleBackColor = false;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // US_ADIM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.TC);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panelLogo);
            this.Name = "US_ADIM";
            this.Size = new System.Drawing.Size(660, 526);
            this.Load += new System.EventHandler(this.US_ADIM_Load);
            this.panelLogo.ResumeLayout(false);
            this.panelLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.TC.ResumeLayout(false);
            this.tpMEN.ResumeLayout(false);
            this.tpMEN.PerformLayout();
            this.tpCRD.ResumeLayout(false);
            this.tpCRD.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel lb_fech;
        private System.Windows.Forms.Panel panelRodSuperio;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Heyech_Application_Development.HeyechTabControl TC;
        private System.Windows.Forms.TabPage tpMEN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tpCRD;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel lb_esqueceu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox tbAdmNivel;
        private System.Windows.Forms.TextBox tbAdm;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tbAdmPasse;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbAdmPerg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbAdmResp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_AdmDelet;
        private System.Windows.Forms.Button btn_AdmAct;
        private System.Windows.Forms.Button btn_Admcad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListBox d5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox d4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox d3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox d2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox d1;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.ListBox d;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_pesqDep;
        private System.Windows.Forms.TextBox tb_pesqDep;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.TextBox tbAdmID;
    }
}
