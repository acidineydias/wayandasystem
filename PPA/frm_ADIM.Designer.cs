﻿namespace PPA
{
    partial class frm_ADIM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uS_ADIM1 = new PPA.US_ADIM();
            this.SuspendLayout();
            // 
            // uS_ADIM1
            // 
            this.uS_ADIM1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.uS_ADIM1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uS_ADIM1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uS_ADIM1.Location = new System.Drawing.Point(0, 0);
            this.uS_ADIM1.Name = "uS_ADIM1";
            this.uS_ADIM1.Size = new System.Drawing.Size(662, 528);
            this.uS_ADIM1.TabIndex = 0;
            // 
            // frm_ADIM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(662, 528);
            this.Controls.Add(this.uS_ADIM1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_ADIM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_ADIM";
            this.ResumeLayout(false);

        }

        #endregion

        private US_ADIM uS_ADIM1;
    }
}