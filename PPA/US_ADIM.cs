﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace PPA
{
    public partial class US_ADIM : UserControl
    {


        Banco ban = new Banco();

        public US_ADIM()
        {
            InitializeComponent();

        
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {

                frm_esqueci adm = new frm_esqueci();
                adm.ShowDialog();

            }
            catch (Exception h)
            { }
        }

        private void lb_fech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MessageBox.Show("Desejas sair?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                ParentForm.Close();
            }
        }

        private void US_ADIM_Load(object sender, EventArgs e)
        {
            ParentForm.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;

            ban.Listarusuaio(d, d1, d2, d3, d4, d5);


        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btn_Admcad_Click(object sender, EventArgs e)
        {
            if (
                           tbAdm.Text != "" &&
                       tbAdmPasse.Text != "" &&
                       tbAdmNivel.Text != "" &&

                       tbAdmPerg.Text != "" &&
                tbAdmResp.Text != ""

                       )
            {
                cadastrarUser();
                
                ban.Listarusuaio(d, d1, d2, d3, d4, d5);

                       tbAdm.Text = "";
                       tbAdmPasse.Text  = "";
                       tbAdmNivel.Text  = "";
                       tbAdmPerg.Text  = "";
                       tbAdmResp.Text = "";

            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }
        
        //CADASTRO
        public void cadastrarUser()
        {

            try
            {
                
                   
                    

                            if (ban.CADuser(tbAdm.Text,tbAdmPasse.Text,tbAdmNivel.Text,tbAdmPerg.Text,tbAdmResp.Text))
                            {
                               

                                MessageBox.Show("Cadastrado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Não Casdrastrou", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                
              

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro ao inserir", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void d_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox list = sender as ListBox;

            try
            {
                if (list.SelectedIndex != -1)
                {
                    
                    d.SelectedIndex = list.SelectedIndex;
                    d1.SelectedIndex = list.SelectedIndex;
                    d2.SelectedIndex = list.SelectedIndex;
                    d3.SelectedIndex = list.SelectedIndex;
                    d4.SelectedIndex = list.SelectedIndex;
                    d5.SelectedIndex = list.SelectedIndex;



                    tbAdmID.Text = d.SelectedItem.ToString();
                    tbAdm.Text = d1.SelectedItem.ToString();
                    tbAdmPasse.Text = d2.SelectedItem.ToString();
                    tbAdmNivel.Text = d3.SelectedItem.ToString();
                    tbAdmPerg.Text = d4.SelectedItem.ToString();
                    tbAdmResp.Text = d5.SelectedItem.ToString();
                    


                }
            }
            catch (Exception r)
            { }
        }

        private void tb_pesqDep_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ban.ListarusuaioCondicao(d, d1, d2, d3, d4, d5, "%" + tb_pesqDep.Text + "%");
            }
            catch (Exception er)
            {


            }

        }

        private void btn_pesqDep_Click(object sender, EventArgs e)
        {
            try
            {
                ban.ListarusuaioCondicao(d, d1, d2, d3, d4, d5, "%" + tb_pesqDep.Text + "%");
            }
            catch (Exception er)
            {


            }
        }

        private void lb_esqueceu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            TC.SelectedTab = tpCRD;
        }

        private void button31_Click(object sender, EventArgs e)
        {
            TC.SelectedTab = tpMEN;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TC.SelectedTab = tpCRD;
        }

        private void btn_AdmAct_Click(object sender, EventArgs e)
        {
            if (
                          tbAdm.Text != "" &&
                      tbAdmPasse.Text != "" &&
                      tbAdmNivel.Text != "" &&

                      tbAdmPerg.Text != "" &&
               tbAdmResp.Text != ""

                      )
            {
                AlterarUser();

                ban.Listarusuaio(d, d1, d2, d3, d4, d5);

                tbAdm.Text = "";
                tbAdmPasse.Text = "";
                tbAdmNivel.Text = "";
                tbAdmPerg.Text = "";
                tbAdmResp.Text = "";

            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }



        //ALTERAR
        public void AlterarUser()
        {

            try
            {




                if (ban.Updateuser(tbAdm.Text, tbAdmPasse.Text, tbAdmNivel.Text, tbAdmPerg.Text, tbAdmResp.Text,int.Parse(tbAdmID.Text)))
                {


                    MessageBox.Show("Alterado Com Sucesso!!", "Feito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Não Alterou", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }



            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro ao inserir", "Erro 325", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btn_AdmDelet_Click(object sender, EventArgs e)
        {
            if (
                        tbAdm.Text != "" &&
                    tbAdmPasse.Text != "" &&
                    tbAdmNivel.Text != "" &&

                    tbAdmPerg.Text != "" &&
             tbAdmResp.Text != ""

                    )
            {

                if (MessageBox.Show("Desejas Deletar?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ban.DELETARuser(int.Parse(tbAdmID.Text));

                    ban.Listarusuaio(d, d1, d2, d3, d4, d5);

                    tbAdm.Text = "";
                    tbAdmPasse.Text = "";
                    tbAdmNivel.Text = "";
                    tbAdmPerg.Text = "";
                    tbAdmResp.Text = "";
                }

            }
            else
            {
                MessageBox.Show("Verifica os campos!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }
        }
    }

