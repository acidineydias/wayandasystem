﻿namespace PPA
{
    partial class frm_deposito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uS_actual_dep2 = new PPA.US_actual_dep();
           
            this.SuspendLayout();
            // 
            // uS_actual_dep2
            // 
            this.uS_actual_dep2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.uS_actual_dep2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uS_actual_dep2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uS_actual_dep2.Location = new System.Drawing.Point(0, 0);
            this.uS_actual_dep2.Name = "uS_actual_dep2";
            this.uS_actual_dep2.Size = new System.Drawing.Size(662, 528);
            this.uS_actual_dep2.TabIndex = 0;
            this.uS_actual_dep2.Load += new System.EventHandler(this.uS_actual_dep2_Load);
            // 
  
            // frm_deposito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(662, 528);
            this.Controls.Add(this.uS_actual_dep2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_deposito";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_actualizar_dep";
            this.ResumeLayout(false);

        }

        #endregion


        private US_actual_dep uS_actual_dep2;
    }
}