﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPA
{
    public partial class licensa : Form
    {
        public licensa()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            if(mtbvalidate.Text == Properties.Settings.Default.validarlicensa)
            {
                Properties.Settings.Default.idlicensa = System.DateTime.Now.ToString("yyyy-MM");
            }
        }

        private void licensa_Load(object sender, EventArgs e)
        {

        }

        private void mtbvalidate_Validating(object sender, CancelEventArgs e)
        {
            if (mtbvalidate.Text == Properties.Settings.Default.validarlicensa)
            {
                Properties.Settings.Default.idlicensa = System.DateTime.Now.ToString("yyyy-MM");
                label2.Text = "Licensa Renovada";
                label2.ForeColor = Color.Green;
                Properties.Settings.Default.Save();
            }
            else
            {
                label2.Text = "Licensa Invalida";
                label2.ForeColor = Color.Red;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btnreinicar.Show();
            button2.Hide();
        }

        private void btnreinicar_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
    }
}
