﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPA
{
    public partial class US_actual_dep : UserControl
    {
        public US_actual_dep()
        {
            InitializeComponent();
        }

        private void US_actual_dep_Load(object sender, EventArgs e)
        {
            ParentForm.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;
        }

        private void lb_fech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MessageBox.Show("Desejas sair?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void lbMini_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ParentForm.WindowState = FormWindowState.Minimized;
        }
    }
}
