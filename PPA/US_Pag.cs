﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPA
{
    public partial class US_Pag : UserControl
    {
        public US_Pag()
        {
            InitializeComponent();


            lxNome.Text = Properties.Settings.Default.Nome;
            lxMes.Text = Properties.Settings.Default.Mes;
            lxid.Text= Properties.Settings.Default.ID;
            lxus.Text= Properties.Settings.Default.Us;
            lxAno.Text= Properties.Settings.Default.Ano;
            lxdat.Text= Properties.Settings.Default.Datap;

            lxpag.Text= Properties.Settings.Default.IDpag;


            lxCurso.Text= Properties.Settings.Default.Curso;
            lxClasse.Text =Properties.Settings.Default.Classe;
            lxTurno.Text =Properties.Settings.Default.Turno;
            lxSala.Text =Properties.Settings.Default.Sala;
            lxSaldo.Text =Properties.Settings.Default.Saldo;

            vID.Text = lxid.Text;
            vNome.Text = lxNome.Text;
            vCurso.Text = lxCurso.Text;
            vClasse.Text = lxClasse.Text;
            vSala.Text = lxSala.Text;
            vTurno.Text = lxTurno.Text;
            vSaldo.Text = lxSaldo.Text;
            vMes.Text = lxMes.Text;
            vPag.Text = lxpag.Text;
            vDat.Text = lxdat.Text;
            vAno.Text = lxAno.Text;
            vUs.Text = lxus.Text;


        }

        private void US_Pag_Load(object sender, EventArgs e)
        {

            ParentForm.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;



            
        }

        private void LvalorDesc_Paint(object sender, PaintEventArgs e)
        {
            

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MessageBox.Show("Desejas sair?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void lb_fech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MessageBox.Show("Desejas sair?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void label63_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            
           
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelLogo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
