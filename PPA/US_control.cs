﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace PPA
{
    public partial class US_control : UserControl
    {
        Banco ban = new Banco();

        public US_control()
        {
            InitializeComponent();
        }
        

        private void US_control_Load(object sender, EventArgs e)
        {
            ParentForm.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;


            MySqlConnection con = new MySqlConnection("server=localhost; userid=root; password=; database=nova");
               MySqlCommand cmd = null;
            //ban.desconectar();
            string Psql = "SELECT * FROM nova.tbutilizador";
            
                   try
                   {
                con.Open();
                       cmd = new MySqlCommand(Psql,con);
                       MySqlDataReader le = null;
                       le = cmd.ExecuteReader();
                       if (le.HasRows)
                       {

                           while (le.Read())
                           {
                               tb_usuario.Items.Add(le["usuario"].ToString());
                           }
                       }
                   }
                   catch (MySqlException ex)
                   {
                       MessageBox.Show("Erro, Servidor Desligado!!!", "Verificar");

                   }
                   finally
                   {
                       con.Close();
                   }
               
         
        }

        private void lbMini_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ParentForm.WindowState = FormWindowState.Minimized;
        }

        private void lb_fech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MessageBox.Show("Desejas sair?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btn_entrar_Click(object sender, EventArgs e)
        {

            if (ban.utilizador(tb_usuario.Text, tb_senha.Text))
            {
                //label.Text = Properties.Settings.Default.NomeUsuario;
                frm_menu fg = new frm_menu();
                fg.Show();
                this.Hide();
            }
        }

        private void btn_sair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desejas sair?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void lb_esqueceu_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frm_esqueci nu = new frm_esqueci();
            nu.ShowDialog();
        }

        private void tb_senha_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {
                e.SuppressKeyPress = true;


                if (ban.utilizador(tb_usuario.Text, tb_senha.Text))
                {
                    //label.Text = Properties.Settings.Default.NomeUsuario;
                    frm_menu fg = new frm_menu();
                    fg.Show();
                    this.Hide();
                }
            }
        }

        private void tb_usuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;


                if (ban.utilizador(tb_usuario.Text, tb_senha.Text))
                {
                    //label.Text = Properties.Settings.Default.NomeUsuario;
                    frm_menu fg = new frm_menu();
                    fg.Show();
                    this.Hide();
                }
            }
        }

        private void tb_usuario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
