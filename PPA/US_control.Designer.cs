﻿namespace PPA
{
    partial class US_control
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(US_control));
            this.panel2 = new System.Windows.Forms.Panel();
            this.lb_errsenha = new System.Windows.Forms.Label();
            this.btn_sair = new System.Windows.Forms.Button();
            this.tb_usuario = new System.Windows.Forms.ComboBox();
            this.tb_senha = new System.Windows.Forms.TextBox();
            this.lb_esqueceu = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lb_fech = new System.Windows.Forms.LinkLabel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbMini = new System.Windows.Forms.LinkLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_entrar = new System.Windows.Forms.Button();
            this.btnInicio = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 711);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1340, 57);
            this.panel2.TabIndex = 2;
            // 
            // lb_errsenha
            // 
            this.lb_errsenha.AutoSize = true;
            this.lb_errsenha.BackColor = System.Drawing.Color.Transparent;
            this.lb_errsenha.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.lb_errsenha.ForeColor = System.Drawing.Color.Red;
            this.lb_errsenha.Location = new System.Drawing.Point(571, 513);
            this.lb_errsenha.Name = "lb_errsenha";
            this.lb_errsenha.Size = new System.Drawing.Size(81, 18);
            this.lb_errsenha.TabIndex = 31;
            this.lb_errsenha.Text = "Senha errada";
            this.lb_errsenha.Visible = false;
            // 
            // btn_sair
            // 
            this.btn_sair.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_sair.BackColor = System.Drawing.Color.White;
            this.btn_sair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_sair.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.btn_sair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sair.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.btn_sair.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.btn_sair.Location = new System.Drawing.Point(677, 536);
            this.btn_sair.Name = "btn_sair";
            this.btn_sair.Size = new System.Drawing.Size(95, 30);
            this.btn_sair.TabIndex = 30;
            this.btn_sair.Text = "Sair";
            this.btn_sair.UseVisualStyleBackColor = false;
            this.btn_sair.Click += new System.EventHandler(this.btn_sair_Click);
            // 
            // tb_usuario
            // 
            this.tb_usuario.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tb_usuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tb_usuario.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.tb_usuario.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F);
            this.tb_usuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.tb_usuario.FormattingEnabled = true;
            this.tb_usuario.Location = new System.Drawing.Point(572, 423);
            this.tb_usuario.Name = "tb_usuario";
            this.tb_usuario.Size = new System.Drawing.Size(200, 24);
            this.tb_usuario.TabIndex = 26;
            this.tb_usuario.SelectedIndexChanged += new System.EventHandler(this.tb_usuario_SelectedIndexChanged);
            this.tb_usuario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_usuario_KeyDown);
            // 
            // tb_senha
            // 
            this.tb_senha.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tb_senha.Font = new System.Drawing.Font("MS Reference Sans Serif", 10F);
            this.tb_senha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.tb_senha.Location = new System.Drawing.Point(572, 485);
            this.tb_senha.Name = "tb_senha";
            this.tb_senha.Size = new System.Drawing.Size(200, 24);
            this.tb_senha.TabIndex = 28;
            this.tb_senha.UseSystemPasswordChar = true;
            this.tb_senha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_senha_KeyDown);
            // 
            // lb_esqueceu
            // 
            this.lb_esqueceu.ActiveLinkColor = System.Drawing.SystemColors.GrayText;
            this.lb_esqueceu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lb_esqueceu.AutoSize = true;
            this.lb_esqueceu.BackColor = System.Drawing.Color.Transparent;
            this.lb_esqueceu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb_esqueceu.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lb_esqueceu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.lb_esqueceu.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lb_esqueceu.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.lb_esqueceu.Location = new System.Drawing.Point(571, 590);
            this.lb_esqueceu.Name = "lb_esqueceu";
            this.lb_esqueceu.Size = new System.Drawing.Size(130, 20);
            this.lb_esqueceu.TabIndex = 29;
            this.lb_esqueceu.TabStop = true;
            this.lb_esqueceu.Text = "*Esqueceu a senha?";
            this.lb_esqueceu.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_esqueceu.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lb_esqueceu_LinkClicked);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label2.Location = new System.Drawing.Point(571, 400);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 25;
            this.label2.Text = "Usuário";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label1.Location = new System.Drawing.Point(569, 461);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 20);
            this.label1.TabIndex = 27;
            this.label1.Text = "Senha";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.lb_fech);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.lbMini);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1340, 147);
            this.panel3.TabIndex = 33;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PPA.Properties.Resources.jkiu_fw;
            this.pictureBox1.Location = new System.Drawing.Point(58, 86);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // lb_fech
            // 
            this.lb_fech.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_fech.AutoSize = true;
            this.lb_fech.DisabledLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_fech.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lb_fech.LinkColor = System.Drawing.Color.Gray;
            this.lb_fech.Location = new System.Drawing.Point(1313, 18);
            this.lb_fech.Name = "lb_fech";
            this.lb_fech.Size = new System.Drawing.Size(21, 20);
            this.lb_fech.TabIndex = 16;
            this.lb_fech.TabStop = true;
            this.lb_fech.Text = "X";
            this.lb_fech.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_fech.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lb_fech_LinkClicked);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1340, 15);
            this.panel5.TabIndex = 13;
            // 
            // lbMini
            // 
            this.lbMini.ActiveLinkColor = System.Drawing.SystemColors.GrayText;
            this.lbMini.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMini.BackColor = System.Drawing.Color.Transparent;
            this.lbMini.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbMini.Font = new System.Drawing.Font("Vrinda", 30F, System.Drawing.FontStyle.Bold);
            this.lbMini.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.lbMini.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lbMini.LinkColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbMini.Location = new System.Drawing.Point(1266, 0);
            this.lbMini.Name = "lbMini";
            this.lbMini.Size = new System.Drawing.Size(41, 33);
            this.lbMini.TabIndex = 24;
            this.lbMini.TabStop = true;
            this.lbMini.Text = "-";
            this.lbMini.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbMini.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbMini_LinkClicked);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PPA.Properties.Resources._15824271_1216074758470968_68670057_o;
            this.pictureBox2.Location = new System.Drawing.Point(-1, 37);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(168, 58);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGray;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel1.Location = new System.Drawing.Point(0, 178);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1340, 1);
            this.panel1.TabIndex = 36;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.DarkGray;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel6.Location = new System.Drawing.Point(0, 147);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1340, 1);
            this.panel6.TabIndex = 34;
            // 
            // btn_entrar
            // 
            this.btn_entrar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btn_entrar.BackColor = System.Drawing.Color.White;
            this.btn_entrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_entrar.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.btn_entrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_entrar.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F);
            this.btn_entrar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.btn_entrar.Location = new System.Drawing.Point(572, 536);
            this.btn_entrar.Name = "btn_entrar";
            this.btn_entrar.Size = new System.Drawing.Size(95, 30);
            this.btn_entrar.TabIndex = 37;
            this.btn_entrar.Text = "Entrar";
            this.btn_entrar.UseVisualStyleBackColor = false;
            this.btn_entrar.Click += new System.EventHandler(this.btn_entrar_Click);
            // 
            // btnInicio
            // 
            this.btnInicio.AutoSize = true;
            this.btnInicio.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnInicio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnInicio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInicio.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnInicio.FlatAppearance.BorderSize = 0;
            this.btnInicio.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnInicio.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInicio.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnInicio.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnInicio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInicio.Location = new System.Drawing.Point(1, -2);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Size = new System.Drawing.Size(198, 30);
            this.btnInicio.TabIndex = 0;
            this.btnInicio.Text = "     Controle de Acesso";
            this.btnInicio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInicio.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel4.Controls.Add(this.btnInicio);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 148);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1340, 30);
            this.panel4.TabIndex = 35;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(602, 245);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(128, 128);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 32;
            this.pictureBox3.TabStop = false;
            // 
            // US_control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.btn_entrar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lb_errsenha);
            this.Controls.Add(this.btn_sair);
            this.Controls.Add(this.tb_usuario);
            this.Controls.Add(this.tb_senha);
            this.Controls.Add(this.lb_esqueceu);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Name = "US_control";
            this.Size = new System.Drawing.Size(1340, 768);
            this.Load += new System.EventHandler(this.US_control_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        internal System.Windows.Forms.Label lb_errsenha;
        private System.Windows.Forms.Button btn_sair;
        internal System.Windows.Forms.ComboBox tb_usuario;
        internal System.Windows.Forms.TextBox tb_senha;
        private System.Windows.Forms.LinkLabel lb_esqueceu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.LinkLabel lbMini;
        private System.Windows.Forms.LinkLabel lb_fech;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn_entrar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnInicio;
        private System.Windows.Forms.Panel panel4;
    }
}
