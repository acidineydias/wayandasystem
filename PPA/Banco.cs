﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using System.Data;
using MySql.Data.MySqlClient;

namespace PPA
{
    class Banco
    {
        public string var = "server=localhost;userid=root;password=;database=nova";
        string Psql = "";

        MySqlConnection con = null;




        public bool conectar()
        {
            con = new MySqlConnection(var);
            try { con.Open(); return true; }
            catch { return false; }
        }

        public bool desconectar()
        {
            if (con.State != ConnectionState.Closed)
            { con.Close(); return true; }
            else { con.Dispose(); return false; }

        }

        //################UTILIZADOR#########

        public bool utilizador(string param1, string param2)
        {

            Psql = "select *from tbutilizador where usuario=@valor1 and passe=@valor2";
             MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor1", param1));
                    cmd.Parameters.Add(new MySqlParameter("@valor2", param2));

                    MySqlDataReader le=null;

                  le= cmd.ExecuteReader();

                  if (le.HasRows)
                  {

                      while (le.Read())
                      {
                          
                          Properties.Settings.Default.NomeUsuario = le["usuario"].ToString();
                          Properties.Settings.Default.Nivel = le["nivel"].ToString();
                          Properties.Settings.Default.Save(); 
                          
                      }
                      return true;
                  }
                  else { MessageBox.Show("Senha Errada,Verifica os Campos!!!", "Verificar",MessageBoxButtons.OK,MessageBoxIcon.Error); return false; }
                    
                    

                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }


        //#### INSERT User

        public bool CADuser(string usuario, string passe,string nivel, string perg, string resp)
        {

            Psql = "insert into tbutilizador (usuario,passe,nivel,pergunta,resposta) values(@n1,@n2,@n3,@n4,@n5)";

            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);

                    cmd.Parameters.Add(new MySqlParameter("@n1",usuario));
                    cmd.Parameters.Add(new MySqlParameter("@n2",passe));
                    cmd.Parameters.Add(new MySqlParameter("@n3",nivel));
                    cmd.Parameters.Add(new MySqlParameter("@n4",perg));
                    cmd.Parameters.Add(new MySqlParameter("@n5", resp));

                    cmd.ExecuteNonQuery();


                    return true;

                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro ao Cadastrar, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool Updateuser(string usuario, string passe, string nivel, string perg, string resp,int ii)
        {

            Psql = "update tbutilizador set usuario=@n1,passe=@n2,nivel=@n3,pergunta=@n4,resposta=@n5 where id=@i";

            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);

                    cmd.Parameters.Add(new MySqlParameter("@n1", usuario));
                    cmd.Parameters.Add(new MySqlParameter("@n2", passe));
                    cmd.Parameters.Add(new MySqlParameter("@n3", nivel));
                    cmd.Parameters.Add(new MySqlParameter("@n4", perg));
                    cmd.Parameters.Add(new MySqlParameter("@n5", resp));
                    cmd.Parameters.Add(new MySqlParameter("@i", ii));

                    cmd.ExecuteNonQuery();


                    return true;

                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro ao Cadastrar, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }



        public bool Listarusuaio(ListBox d, ListBox d1, ListBox d2, ListBox d3, ListBox d4, ListBox d5)
        {

            Psql = "SELECT * FROM nova.tbutilizador";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
           


                    MySqlDataReader le = null;

                    

                    d.Items.Clear();
                    d1.Items.Clear();
                    d2.Items.Clear();
                    d3.Items.Clear();
                    d4.Items.Clear();
                    d5.Items.Clear();

                    le = cmd.ExecuteReader();
                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            d.Items.Add(le["id"].ToString());
                            d1.Items.Add(le["usuario"].ToString());
                            d2.Items.Add(le["passe"].ToString());
                            d3.Items.Add(le["nivel"].ToString());
                            d4.Items.Add(le["pergunta"].ToString());
                            d5.Items.Add(le["resposta"].ToString());
                            


                        }
                        return true;
                    }
                    else { MessageBox.Show("Verifica os Campos!!!", "Verificar"); return false; }



                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }



        public bool ListarusuaioCondicao(ListBox d, ListBox d1, ListBox d2, ListBox d3, ListBox d4, ListBox d5,string pesq)
        {

            Psql = "SELECT id,usuario,passe,nivel,pergunta,resposta FROM nova.tbutilizador where usuario like @valor";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", pesq));



                    MySqlDataReader le = null;



                    d.Items.Clear();
                    d1.Items.Clear();
                    d2.Items.Clear();
                    d3.Items.Clear();
                    d4.Items.Clear();
                    d5.Items.Clear();

                    le = cmd.ExecuteReader();
                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            d.Items.Add(le["id"].ToString());
                            d1.Items.Add(le["usuario"].ToString());
                            d2.Items.Add(le["passe"].ToString());
                            d3.Items.Add(le["nivel"].ToString());
                            d4.Items.Add(le["pergunta"].ToString());
                            d5.Items.Add(le["resposta"].ToString());



                        }
                        return true;
                    }
                    else { MessageBox.Show("Verifica os Campos!!!", "Verificar"); return false; }



                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }


        //0000

        public bool DELETARuser(int condicao)
        {
            Psql = "delete from tbutilizador where id=@c";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@c", condicao));



                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }


        //#

        public bool testeusuaio(string param1,Label lnome,Label lperg)
        {

            Psql = "select *from tbutilizador where usuario=@valor1";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor1", param1));
                

                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {

                            lnome.Text = le["usuario"].ToString();
                            lperg.Text = le["pergunta"].ToString();
                        

                        }
                        return true;
                    }
                    else { MessageBox.Show("Verifica os Campos!!!", "Verificar"); return false; }



                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool testeperg(string usuario, string perg, string resp)
        {
            string lk, lo;
            Psql = "select *from tbutilizador where usuario=@valor1 and pergunta=@valor2 and resposta=@valor3";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor1", usuario));
                    cmd.Parameters.Add(new MySqlParameter("@valor2", perg));
                    cmd.Parameters.Add(new MySqlParameter("@valor3", resp));

                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();


                    if (le.HasRows)
                    {

                        while (le.Read())
                        {

                            lk = le["usuario"].ToString();
                            lo = le["pergunta"].ToString();


                        }
                        return true;
                    }
                    else { MessageBox.Show("Verifica os Campos!!!", "Verificar"); return false; }
                    



                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool alterarpasse(string usuario, string perg, string resp,string passe)
        {
           
            Psql = "update tbutilizador set passe=@n1 where usuario=@valor1 and pergunta=@valor2 and resposta=@valor3";
            
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);

                    cmd.Parameters.Add(new MySqlParameter("@n1", passe));
                    cmd.Parameters.Add(new MySqlParameter("@valor1", usuario));
                    cmd.Parameters.Add(new MySqlParameter("@valor2", perg));
                    cmd.Parameters.Add(new MySqlParameter("@valor3", resp));

                    cmd.ExecuteNonQuery();

                   
                        return true;
                  
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }
        //############MATRICULAR############
        public bool MATRICULAR(ArrayList p_arrInsert)
        {
            Psql = "insert into tbmatri (nome,morada,genero,data,bi,nomepai,nomemae,anoMat,dataMat) values (@n1,@n2,@n3,@n4,@n5,@n6,@n7,@n8,@n9)";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@n1", p_arrInsert[0]));
                    cmd.Parameters.Add(new MySqlParameter("@n2", p_arrInsert[1]));
                    cmd.Parameters.Add(new MySqlParameter("@n3", p_arrInsert[2]));
                    cmd.Parameters.Add(new MySqlParameter("@n4", p_arrInsert[3]));
                    cmd.Parameters.Add(new MySqlParameter("@n5", p_arrInsert[4]));
                    cmd.Parameters.Add(new MySqlParameter("@n6", p_arrInsert[5]));
                    cmd.Parameters.Add(new MySqlParameter("@n7", p_arrInsert[6]));
                    cmd.Parameters.Add(new MySqlParameter("@n8", p_arrInsert[7]));
                    cmd.Parameters.Add(new MySqlParameter("@n9", p_arrInsert[8]));




                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }


        //#############LISTAR MATRICULA#############
        public DataTable ListarMatricula()
        {
            Psql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula from tbmatri order by processo Asc";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    
                    MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }

                catch (MySqlException e)
                {
                    throw e;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return null;
            }
        }

        public DataTable PesqMatricula(string lk)
        {
            Psql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula from tbmatri where nome like @valor order by processo Asc";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", lk));
                    MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }

                catch (MySqlException e)
                {
                    throw e;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return null;
            }
        }

        public DataTable PesqMatriculaComposta(string sql,int ano,string oj)
        {
           
            this.Psql = sql;
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", ano));
                    cmd.Parameters.Add(new MySqlParameter("@valor2", oj));
                    MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }

                catch (MySqlException e)
                {
                    throw e;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return null;
            }
        }




















        //############CONFIRMAR############
        public bool CONFIRMAR(ArrayList p_arrInsert)
        {
            Psql = "insert into tbconfirmar (aluno,curso,classe,sala,turno,ano,status,dataConf) values (@n1,@n2,@n3,@n4,@n5,@n6,@n7,@n8) ";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@n1", p_arrInsert[0]));
                    cmd.Parameters.Add(new MySqlParameter("@n2", p_arrInsert[1]));
                    cmd.Parameters.Add(new MySqlParameter("@n3", p_arrInsert[2]));
                    cmd.Parameters.Add(new MySqlParameter("@n4", p_arrInsert[3]));
                    cmd.Parameters.Add(new MySqlParameter("@n5", p_arrInsert[4]));
                    cmd.Parameters.Add(new MySqlParameter("@n6", p_arrInsert[5]));
                    cmd.Parameters.Add(new MySqlParameter("@n7", p_arrInsert[6]));
                    cmd.Parameters.Add(new MySqlParameter("@n8", p_arrInsert[7]));
                  




                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }
        //#############LISTAR CONFIRMAÇAO#############
        public DataTable ListarConfirmacao()
        {
            Psql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);

                    MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }

                catch (MySqlException e)
                {
                    throw e;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return null;
            }
        }

        public DataTable PesqCormacao(string lk)
        {

            Psql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and nome like @valor";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", lk));
                    MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }

                catch (MySqlException e)
                {
                    throw e;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return null;
            }
        }

        public DataTable PesqConfirmacaoComposta(string sql, int ano, string oj)
        {

            this.Psql = sql;
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", ano));
                    cmd.Parameters.Add(new MySqlParameter("@valor2", oj));
                    MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }

                catch (MySqlException e)
                {
                    throw e;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return null;
            }
        }

        //############ALTERAR CONFIRMAR############
        public bool ALTERARCONFIRMAR(ArrayList p_arrInsert,int c)
        {
            Psql = "update tbconfirmar set aluno=@n1,curso=@n2,classe=@n3,sala=@n4,turno=@n5,ano=@n6,status=@n7,dataConf=@n8 where idaluno=@val";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@n1", p_arrInsert[0]));
                    cmd.Parameters.Add(new MySqlParameter("@n2", p_arrInsert[1]));
                    cmd.Parameters.Add(new MySqlParameter("@n3", p_arrInsert[2]));
                    cmd.Parameters.Add(new MySqlParameter("@n4", p_arrInsert[3]));
                    cmd.Parameters.Add(new MySqlParameter("@n5", p_arrInsert[4]));
                    cmd.Parameters.Add(new MySqlParameter("@n6", p_arrInsert[5]));
                    cmd.Parameters.Add(new MySqlParameter("@n7", p_arrInsert[6]));
                    cmd.Parameters.Add(new MySqlParameter("@n8", p_arrInsert[7]));
                    cmd.Parameters.Add(new MySqlParameter("@val", c));





                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }

        public bool DELETAR_CONFIRMACAO(int condicao)
        {
            Psql = "delete from tbconfirmar where idaluno=@c";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@c", condicao));



                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }



        public bool ListaalterDeletarConf(ListBox lb1, ListBox lb2, ListBox lb3, ListBox lb4, ListBox lb5, ListBox lb6, ListBox lb7, ListBox lb8, ListBox lb9,ListBox lb10)
        {
            Psql = "select idaluno as ID,processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);

                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();

                    lb1.Items.Clear();
                    lb2.Items.Clear();
                    lb3.Items.Clear();
                    lb4.Items.Clear();
                    lb5.Items.Clear();
                    lb6.Items.Clear();
                    lb7.Items.Clear();
                    lb8.Items.Clear();
                    lb9.Items.Clear();
                    lb10.Items.Clear();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/
                       

                            lb1.Items.Add(le["ID"].ToString());
                            lb2.Items.Add(le["Processo"].ToString());
                            lb3.Items.Add(le["Nome"].ToString());
                            lb4.Items.Add(le["Curso"].ToString());
                            lb5.Items.Add(le["Classe"].ToString());
                            lb6.Items.Add(le["Sala"].ToString());
                            lb7.Items.Add(le["Turno"].ToString());
                            lb8.Items.Add(le["Status"].ToString());
                            lb9.Items.Add(le["DataConfirmacao"].ToString());
                            lb10.Items.Add(le["Ano"].ToString());

                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }




        public bool ListaalterDeletarUmPARAmConf(ListBox lb1, ListBox lb2, ListBox lb3, ListBox lb4, ListBox lb5, ListBox lb6, ListBox lb7, ListBox lb8, ListBox lb9, ListBox lb10, string nc)
        {
            Psql = "select idaluno as ID,processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and Nome like @valor";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", nc));
                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();

                    lb1.Items.Clear();
                    lb2.Items.Clear();
                    lb3.Items.Clear();
                    lb4.Items.Clear();
                    lb5.Items.Clear();
                    lb6.Items.Clear();
                    lb7.Items.Clear();
                    lb8.Items.Clear();
                    lb9.Items.Clear();
                    lb10.Items.Clear();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/


                            lb1.Items.Add(le["ID"].ToString());
                            lb2.Items.Add(le["Processo"].ToString());
                            lb3.Items.Add(le["Nome"].ToString());
                            lb4.Items.Add(le["Curso"].ToString());
                            lb5.Items.Add(le["Classe"].ToString());
                            lb6.Items.Add(le["Sala"].ToString());
                            lb7.Items.Add(le["Turno"].ToString());
                            lb8.Items.Add(le["Status"].ToString());
                            lb9.Items.Add(le["DataConfirmacao"].ToString());
                            lb10.Items.Add(le["Ano"].ToString());

                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }



        public bool ListaalterDeletaCOMPOSTAConf(ListBox lb1, ListBox lb2, ListBox lb3, ListBox lb4, ListBox lb5, ListBox lb6, ListBox lb7, ListBox lb8, ListBox lb9, ListBox lb10,string sql, int ano, string nc)
        {
            this.Psql = sql;
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", nc));
                    cmd.Parameters.Add(new MySqlParameter("@an", ano));
                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();

                    lb1.Items.Clear();
                    lb2.Items.Clear();
                    lb3.Items.Clear();
                    lb4.Items.Clear();
                    lb5.Items.Clear();
                    lb6.Items.Clear();
                    lb7.Items.Clear();
                    lb8.Items.Clear();
                    lb9.Items.Clear();
                    lb10.Items.Clear();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/


                            lb1.Items.Add(le["ID"].ToString());
                            lb2.Items.Add(le["Processo"].ToString());
                            lb3.Items.Add(le["Nome"].ToString());
                            lb4.Items.Add(le["Curso"].ToString());
                            lb5.Items.Add(le["Classe"].ToString());
                            lb6.Items.Add(le["Sala"].ToString());
                            lb7.Items.Add(le["Turno"].ToString());
                            lb8.Items.Add(le["Status"].ToString());
                            lb9.Items.Add(le["DataConfirmacao"].ToString());
                            lb10.Items.Add(le["Ano"].ToString());

                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }
        //#############LISTAR TOTAL#############
        public DataTable ListarTotal()
        {
            Psql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);

                    MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }

                catch (MySqlException e)
                {
                    throw e;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return null;
            }
        }

        public DataTable PesqTotal(string lk)
        {

            Psql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula,curso as Curso,classe as Classe,sala as Sala,turno as Turno,status as Status,dataConf as DataConfirmacao,ano as Ano from tbmatri,tbconfirmar where processo=aluno and nome like @valor";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", lk));
                    MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }

                catch (MySqlException e)
                {
                    throw e;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return null;
            }
        }

        public DataTable PesqTotalComposta(string sql, int ano, string oj)
        {

            this.Psql = sql;
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", ano));
                    cmd.Parameters.Add(new MySqlParameter("@valor2", oj));
                    MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }

                catch (MySqlException e)
                {
                    throw e;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return null;
            }
        }























        //############ ALTERAR E DELETAR MATRICULA############
        public bool ALTERAR_MATRICULAR(ArrayList p_arrInsert, int condicao)
        {
            Psql = "update tbmatri set nome=@n1 ,morada=@n2, genero=@n3, data=@n4, bi=@n5, nomepai=@n6, nomemae=@n7, anoMat=@n8, dataMat=@n9 where processo=@c";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@n1", p_arrInsert[0]));
                    cmd.Parameters.Add(new MySqlParameter("@n2", p_arrInsert[1]));
                    cmd.Parameters.Add(new MySqlParameter("@n3", p_arrInsert[2]));
                    cmd.Parameters.Add(new MySqlParameter("@n4", p_arrInsert[3]));
                    cmd.Parameters.Add(new MySqlParameter("@n5", p_arrInsert[4]));
                    cmd.Parameters.Add(new MySqlParameter("@n6", p_arrInsert[5]));
                    cmd.Parameters.Add(new MySqlParameter("@n7", p_arrInsert[6]));
                    cmd.Parameters.Add(new MySqlParameter("@n8", p_arrInsert[7]));
                    cmd.Parameters.Add(new MySqlParameter("@n9", p_arrInsert[8]));
                    cmd.Parameters.Add(new MySqlParameter("@c", condicao));



                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }

        public bool DELETAR_MATRICULAR(int condicao)
        {
            Psql = "delete from tbmatri where processo=@c";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@c", condicao));



                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }
        public bool DELETAR_CONFIRMACAO_Confirmacao(int condicao)
        {
            Psql = "delete from tbconfirmar where aluno=@c";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@c", condicao));



                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }

       
        public bool ListaalterDeletarCad(ListBox l1,ListBox l2,ListBox l3,ListBox l4,ListBox l5,ListBox l6,ListBox l7,ListBox l8,ListBox l9)
        {
            Psql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula from tbmatri order by processo Asc";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);

                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();

                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();
                    l8.Items.Clear();
                    l9.Items.Clear();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/
                            l1.Items.Add(le["Processo"].ToString());
                            l2.Items.Add(le["Bilhete"].ToString());
                            l3.Items.Add(le["Nome"].ToString());
                            l4.Items.Add(le["Genero"].ToString());
                            l5.Items.Add(le["Morada"].ToString());
                            l6.Items.Add(le["Data"].ToString());
                            l7.Items.Add(le["Pai"].ToString());
                            l8.Items.Add(le["Mae"].ToString());
                            l9.Items.Add(le["anoMAtricula"].ToString());



                        }
                        
                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }




        //Listar ALTER DELETE

        public bool DeleAlterPesqMatricula(ListBox l1,ListBox l2,ListBox l3,ListBox l4,ListBox l5,ListBox l6,ListBox l7,ListBox l8,ListBox l9,string lk)
        {
            Psql = "select processo as Processo,bi as Bilhete,nome as Nome,genero as Genero,morada as Morada,data as Data,nomepai as Pai,nomemae as Mae,anoMat as AnoMatricula from tbmatri where nome like @valor order by processo Asc";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", lk));
                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();


                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();
                    l8.Items.Clear();
                    l9.Items.Clear();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/
                            l1.Items.Add(le["Processo"].ToString());
                            l2.Items.Add(le["Bilhete"].ToString());
                            l3.Items.Add(le["Nome"].ToString());
                            l4.Items.Add(le["Genero"].ToString());
                            l5.Items.Add(le["Morada"].ToString());
                            l6.Items.Add(le["Data"].ToString());
                            l7.Items.Add(le["Pai"].ToString());
                            l8.Items.Add(le["Mae"].ToString());
                            l9.Items.Add(le["anoMAtricula"].ToString());



                        }

                    }

                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool DeleteAlterPesqMatriculaComposta(ListBox l1,ListBox l2,ListBox l3,ListBox l4,ListBox l5,ListBox l6,ListBox l7,ListBox l8,ListBox l9,string sql, int ano, string oj)
        {

            this.Psql = sql;
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", ano));
                    cmd.Parameters.Add(new MySqlParameter("@valor2", oj));
                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();


                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();
                    l8.Items.Clear();
                    l9.Items.Clear();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/
                            l1.Items.Add(le["Processo"].ToString());
                            l2.Items.Add(le["Bilhete"].ToString());
                            l3.Items.Add(le["Nome"].ToString());
                            l4.Items.Add(le["Genero"].ToString());
                            l5.Items.Add(le["Morada"].ToString());
                            l6.Items.Add(le["Data"].ToString());
                            l7.Items.Add(le["Pai"].ToString());
                            l8.Items.Add(le["Mae"].ToString());
                            l9.Items.Add(le["anoMAtricula"].ToString());



                        }

                    }

                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }











//DEPOSITO

        //CADASTRAR DEposito


        public bool DEPOSITAR(ArrayList p_arrInsert)
        {
            Psql = "insert into tbdeposito (alunoid,talao,valordep,banco,datadep,dataact,anoal) values (@n1,@n2,@n3,@n4,@n5,@n6,@n7)";

            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@n1", p_arrInsert[0]));
                    cmd.Parameters.Add(new MySqlParameter("@n2", p_arrInsert[1]));
                    cmd.Parameters.Add(new MySqlParameter("@n3", p_arrInsert[2]));
                    cmd.Parameters.Add(new MySqlParameter("@n4", p_arrInsert[3]));
                    cmd.Parameters.Add(new MySqlParameter("@n5", p_arrInsert[4]));
                    cmd.Parameters.Add(new MySqlParameter("@n6", p_arrInsert[5]));
                    cmd.Parameters.Add(new MySqlParameter("@n7", p_arrInsert[6]));
                




                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }

        public bool ListarDeposito(ListBox ll, ListBox l, ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox l7, ListBox l8)
        {
            Psql = "select iddep,alunoid,nome as Aluno,talao as Talao,valordep as Valor,banco as Banco,datadep as DataDeposito,dataact as DataActualizacao, anoal,saldo from tbdeposito,tbconfirmar,tbmatri where aluno=alunoid and processo=aluno and ano=anoal order by alunoid Asc";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);



                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();
                    ll.Items.Clear();
                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();
                    l8.Items.Clear();
                    

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/

                            ll.Items.Add(le["iddep"].ToString());
                            l.Items.Add(le["alunoid"].ToString());
                            l1.Items.Add(le["Aluno"].ToString());
                            l2.Items.Add(le["Talao"].ToString());
                            l3.Items.Add(le["Valor"].ToString());
                            l4.Items.Add(le["Banco"].ToString());
                            l5.Items.Add(le["DataDeposito"].ToString());
                            l6.Items.Add(le["DataActualizacao"].ToString());
                            l7.Items.Add(le["anoal"].ToString());
                            l8.Items.Add(le["saldo"].ToString());
                            



                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool ListarDepositoCond(ListBox ll, ListBox l, ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox l7, ListBox l8, string condicao)
        {
            Psql = "select iddep,alunoid,nome as Aluno,talao as Talao,valordep as Valor,banco as Banco,datadep as DataDeposito,dataact as DataActualizacao,anoal,saldo from tbdeposito,tbconfirmar,tbmatri where aluno=alunoid and processo=aluno and ano=anoal and nome like @v order by alunoid Asc";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@v",condicao));
                  

                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();
                    ll.Items.Clear();
                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();
                    l8.Items.Clear();


                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/
                            ll.Items.Add(le["iddep"].ToString());
                            l.Items.Add(le["alunoid"].ToString());
                            l1.Items.Add(le["Aluno"].ToString());
                            l2.Items.Add(le["Talao"].ToString());
                            l3.Items.Add(le["Valor"].ToString());
                            l4.Items.Add(le["Banco"].ToString());
                            l5.Items.Add(le["DataDeposito"].ToString());
                            l6.Items.Add(le["DataActualizacao"].ToString());
                            l7.Items.Add(le["anoal"].ToString());
                            l8.Items.Add(le["saldo"].ToString());


                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }



        public bool ListarDepositoCondClass(ListBox ll, ListBox l, ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox l7, ListBox l8, string sql, int classe, string nc)
        {
            this.Psql = sql;
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", nc));
                    cmd.Parameters.Add(new MySqlParameter("@classe", classe));
                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();

                    ll.Items.Clear();
                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();
                    l8.Items.Clear();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/

                            ll.Items.Add(le["iddep"].ToString());
                            l.Items.Add(le["alunoid"].ToString());
                            l1.Items.Add(le["Aluno"].ToString());
                            l2.Items.Add(le["Talao"].ToString());
                            l3.Items.Add(le["Valor"].ToString());
                            l4.Items.Add(le["Banco"].ToString());
                            l5.Items.Add(le["DataDeposito"].ToString());
                            l6.Items.Add(le["DataActualizacao"].ToString());
                            l7.Items.Add(le["anoal"].ToString());
                            l8.Items.Add(le["saldo"].ToString());

                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }



        public bool ListarDepositodata(ListBox ll, ListBox l, ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox l7, ListBox l8, string data)
        {
            Psql = "select iddep,alunoid,nome as Aluno,talao as Talao,valordep as Valor,banco as Banco,datadep as DataDeposito,dataact as DataActualizacao,anoal, saldo from tbdeposito,tbconfirmar,tbmatri where aluno=alunoid and processo=aluno and ano=anoal and datadep=@v order by alunoid Asc";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@v", data));
                  

                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();
                    ll.Items.Clear();
                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();
                    l8.Items.Clear();


                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            /*
                            ListViewItem items = new ListViewItem(new string[] { le["Processo"].ToString(), le["Bilhete"].ToString(), 
                            le["Nome"].ToString(), le["Genero"].ToString(), le["Morada"].ToString(), le["Data"].ToString(), le["Pai"].ToString(),
                            le["Mae"].ToString(), le["AnoMatricula"].ToString()});
                            lv.Items.Add(items);*/
                            ll.Items.Add(le["iddep"].ToString());
                            l.Items.Add(le["alunoid"].ToString());
                            l1.Items.Add(le["Aluno"].ToString());
                            l2.Items.Add(le["Talao"].ToString());
                            l3.Items.Add(le["Valor"].ToString());
                            l4.Items.Add(le["Banco"].ToString());
                            l5.Items.Add(le["DataDeposito"].ToString());
                            l6.Items.Add(le["DataActualizacao"].ToString());
                            l7.Items.Add(le["anoal"].ToString());
                            l8.Items.Add(le["saldo"].ToString());
                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool ALTERARDEPOSITO(ArrayList p_arrInsert, int c)
        {
            Psql = "update tbdeposito set alunoid=@n1,talao=@n2,valordep=@n3,banco=@n4,datadep=@n5,dataact=@n6,anoal=@n7 where iddep=@val";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@n1", p_arrInsert[0]));
                    cmd.Parameters.Add(new MySqlParameter("@n2", p_arrInsert[1]));
                    cmd.Parameters.Add(new MySqlParameter("@n3", p_arrInsert[2]));
                    cmd.Parameters.Add(new MySqlParameter("@n4", p_arrInsert[3]));
                    cmd.Parameters.Add(new MySqlParameter("@n5", p_arrInsert[4]));
                    cmd.Parameters.Add(new MySqlParameter("@n6", p_arrInsert[5]));
                    cmd.Parameters.Add(new MySqlParameter("@n7", p_arrInsert[6]));
                   
                    cmd.Parameters.Add(new MySqlParameter("@val", c));





                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }


        public bool DELETARDEPOSITO(int condicao)
        {
            Psql = "delete from tbdeposito where iddep=@c";
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@c", condicao));



                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }

        //######## text ######                                                            

        public bool testeANO(string aluno, int ano)
        {
            string lk, lo;

            Psql = "SELECT * FROM nova.tbconfirmar where aluno=@al and ano=@a";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@al", aluno));
                    cmd.Parameters.Add(new MySqlParameter("@a", ano));


                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {

                            lk = le["aluno"].ToString();
                            lo = le["ano"].ToString();


                        }
                        return true;
                    }
                    else { MessageBox.Show("Preenche pelo menos o campo *ID Aluno e verifique o Ano, Aluno não Confirmado", "Verificar"); return false; }



                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }


        //######## ADD SalDO ######


        public bool ADDSALDO(int valor,int id, int an)
        {
            Psql = "Update tbconfirmar set saldo=saldo+@valor where aluno=@id and ano=@an";

            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", valor));
                    cmd.Parameters.Add(new MySqlParameter("@id", id));
                    cmd.Parameters.Add(new MySqlParameter("@an", an));
                   





                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }

  //######### PROPINA

        public bool ListarVerProp(ListBox l,ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox l7)
        {
            Psql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,saldo as Saldo,ano as Ano from tbmatri,tbconfirmar where processo=aluno";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();
                   
                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();
                   


                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                          
                           
                            l.Items.Add(le["Processo"].ToString());
                            l1.Items.Add(le["Nome"].ToString());
                            l2.Items.Add(le["Curso"].ToString());
                            l3.Items.Add(le["Classe"].ToString());
                            l4.Items.Add(le["Turno"].ToString());
                            l5.Items.Add(le["Sala"].ToString());
                            l6.Items.Add(le["Saldo"].ToString());
                            l7.Items.Add(le["Ano"].ToString());
                           
                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool ListarVerPropCond(ListBox l, ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox l7, string condicao)
        {
            Psql = "select processo as Processo,nome as Nome,curso as Curso,classe as Classe,sala as Sala,turno as Turno,saldo as Saldo,ano as Ano from tbmatri,tbconfirmar where Processo=aluno and Nome like @v";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@v", condicao));
                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();

                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();



                    if (le.HasRows)
                    {

                        while (le.Read())
                        {


                            l.Items.Add(le["Processo"].ToString());
                            l1.Items.Add(le["Nome"].ToString());
                            l2.Items.Add(le["Curso"].ToString());
                            l3.Items.Add(le["Classe"].ToString());
                            l4.Items.Add(le["Turno"].ToString());
                            l5.Items.Add(le["Sala"].ToString());
                            l6.Items.Add(le["Saldo"].ToString());
                            l7.Items.Add(le["Ano"].ToString());

                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }


        public bool ListaalterVerPropComp(ListBox l, ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox l7, string sql, int ano, string nc)
        {
            this.Psql = sql;
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", nc));
                    cmd.Parameters.Add(new MySqlParameter("@an", ano));
                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();
                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();
                    l7.Items.Clear();

                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            l.Items.Add(le["Processo"].ToString());
                            l1.Items.Add(le["Nome"].ToString());
                            l2.Items.Add(le["Curso"].ToString());
                            l3.Items.Add(le["Classe"].ToString());
                            l4.Items.Add(le["Turno"].ToString());
                            l5.Items.Add(le["Sala"].ToString());
                            l6.Items.Add(le["Saldo"].ToString());
                            l7.Items.Add(le["Ano"].ToString());
                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }


        public bool checarmes(string id,int ano,ListBox mes)
        {

            Psql = "select descmes from tbpagamento where al=@valor and anopag=@an";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", id));
                    cmd.Parameters.Add(new MySqlParameter("@an", ano));
                   

                    MySqlDataReader le = null;
                    mes.Items.Clear();
                    le = cmd.ExecuteReader();

                    if (le.HasRows)
                    {
                        while (le.Read())
                        {
                            mes.Items.Add(le["descmes"].ToString()); 
                        }
                      
                        return true;
                    }
                    else { mes.Items.Add("Nenhum mes pago!"); return false; }



                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool checarmesRepetido(string id, int ano, string mes)
        {
            string m;

            Psql = "select descmes from tbpagamento where al=@valor and anopag=@an and descmes like @me";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", id));
                    cmd.Parameters.Add(new MySqlParameter("@an", ano));
                    cmd.Parameters.Add(new MySqlParameter("@me", mes));


                    MySqlDataReader le = null;
                  
                    le = cmd.ExecuteReader();

                    if (le.HasRows)
                    {
                        while (le.Read())
                        {
                            m=le["descmes"].ToString();
                        }

                        return true;
                    }
                    else { return false; }



                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool PAGAR(int id,int nummes,string descmes,string data,int ano, string user)
        {
            Psql = "insert into tbpagamento (al,nummes,descmes,datapag,anopag,user) values (@n1,@n2,@n3,@n4,@n5,@n6)";

            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@n1", id));
                    cmd.Parameters.Add(new MySqlParameter("@n2", nummes));
                    cmd.Parameters.Add(new MySqlParameter("@n3", descmes));
                    cmd.Parameters.Add(new MySqlParameter("@n4", data));
                    cmd.Parameters.Add(new MySqlParameter("@n5", ano));
                    cmd.Parameters.Add(new MySqlParameter("@n6", user));
                  





                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }


        public bool ListarPropinaCOND(ListBox l, ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox lx1, ListBox lx2, ListBox lx3, ListBox lx4, ListBox lx5, string nome)
        {
            Psql = "select idprop,nome,curso,turno,sala,classe,saldo,anopag,al,descmes,datapag,user from tbpagamento,tbconfirmar,tbmatri where aluno=al and processo=aluno and nome like @nm ";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@nm", nome));

                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();
                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();

                    lx1.Items.Clear();
                    lx2.Items.Clear();
                    lx3.Items.Clear();
                    lx4.Items.Clear();
                    lx5.Items.Clear();


                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            l.Items.Add(le["idprop"].ToString());
                            l1.Items.Add(le["al"].ToString());
                            l2.Items.Add(le["nome"].ToString());
                            l3.Items.Add(le["descmes"].ToString());
                            l4.Items.Add(le["datapag"].ToString());
                            l5.Items.Add(le["anopag"].ToString());
                            l6.Items.Add(le["user"].ToString());

                          
                            lx1.Items.Add(le["curso"].ToString());
                            lx2.Items.Add(le["classe"].ToString());
                            lx3.Items.Add(le["turno"].ToString());
                            lx4.Items.Add(le["sala"].ToString());
                            lx5.Items.Add(le["saldo"].ToString());

                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;

                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool ListarPropinaCONDXXX(ListBox l, ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox lx1, ListBox lx2, ListBox lx3, ListBox lx4, ListBox lx5, string nome, string ano, string sql)
        {
           

            this.Psql = sql;
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@nm", nome));
                    cmd.Parameters.Add(new MySqlParameter("@ano", ano));

                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();
                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();

                    lx1.Items.Clear();
                    lx2.Items.Clear();
                    lx3.Items.Clear();
                    lx4.Items.Clear();
                    lx5.Items.Clear();


                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            l.Items.Add(le["idprop"].ToString());
                            l1.Items.Add(le["al"].ToString());
                            l2.Items.Add(le["nome"].ToString());
                            l3.Items.Add(le["descmes"].ToString());
                            l4.Items.Add(le["datapag"].ToString());
                            l5.Items.Add(le["anopag"].ToString());
                            l6.Items.Add(le["user"].ToString());


                            lx1.Items.Add(le["curso"].ToString());
                            lx2.Items.Add(le["classe"].ToString());
                            lx3.Items.Add(le["turno"].ToString());
                            lx4.Items.Add(le["sala"].ToString());
                            lx5.Items.Add(le["saldo"].ToString());

                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;

                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }

       
        public bool ACTUALIZARPAGAM(int cod, int id, string descmes, string data, int ano, string user)
        {
            Psql = "update tbpagamento set al=@n1,descmes=@n3,datapag=@n4,anopag=@n5,user=@n6 where idprop=@cod";

            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@n1", id));
                 
                    cmd.Parameters.Add(new MySqlParameter("@n3", descmes));
                    cmd.Parameters.Add(new MySqlParameter("@n4", data));
                    cmd.Parameters.Add(new MySqlParameter("@n5", ano));
                    cmd.Parameters.Add(new MySqlParameter("@n6", user));

                    cmd.Parameters.Add(new MySqlParameter("@cod", cod));






                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;

                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }



        public bool DELETARPAG(int cod)
        {
            Psql = "delete from tbpagamento where idprop=@c";

       
            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@c", cod));



                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }
        //,tbconfirmar,tbmatri where aluno=al and processo=aluno
        public bool ListarPropina(ListBox l, ListBox l1, ListBox l2, ListBox l3, ListBox l4, ListBox l5, ListBox l6, ListBox lx1, ListBox lx2, ListBox lx3, ListBox lx4, ListBox lx5)
        {
            Psql = "select idprop,nome,curso,turno,sala,classe,saldo,anopag,al,descmes,datapag,user from tbpagamento,tbconfirmar,tbmatri where aluno=al and processo=aluno and anopag=ano";
            MySqlCommand cmd = null;

            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                 
                    MySqlDataReader le = null;

                    le = cmd.ExecuteReader();
                    l.Items.Clear();
                    l1.Items.Clear();
                    l2.Items.Clear();
                    l3.Items.Clear();
                    l4.Items.Clear();
                    l5.Items.Clear();
                    l6.Items.Clear();

                    lx1.Items.Clear();
                    lx2.Items.Clear();
                    lx3.Items.Clear();
                    lx4.Items.Clear();
                    lx5.Items.Clear();


                    if (le.HasRows)
                    {

                        while (le.Read())
                        {
                            l.Items.Add(le["idprop"].ToString());
                            l1.Items.Add(le["al"].ToString());
                            l2.Items.Add(le["nome"].ToString());
                            l3.Items.Add(le["descmes"].ToString());
                            l4.Items.Add(le["datapag"].ToString());
                            l5.Items.Add(le["anopag"].ToString());
                            l6.Items.Add(le["user"].ToString());


                            lx1.Items.Add(le["curso"].ToString());
                            lx2.Items.Add(le["classe"].ToString());
                            lx3.Items.Add(le["turno"].ToString());
                            lx4.Items.Add(le["sala"].ToString());
                            lx5.Items.Add(le["saldo"].ToString());
                        }

                    }
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }
        }



        //######## REDUZIR SalDO ######


        public bool REDSALDO(int valor, int id, int an)
        {
            Psql = "Update tbconfirmar set saldo=saldo-@valor where aluno=@id and ano=@an";

            MySqlCommand cmd = null;



            if (this.conectar())
            {
                try
                {
                    cmd = new MySqlCommand(Psql, con);
                    cmd.Parameters.Add(new MySqlParameter("@valor", valor));
                    cmd.Parameters.Add(new MySqlParameter("@id", id));
                    cmd.Parameters.Add(new MySqlParameter("@an", an));






                    cmd.ExecuteNonQuery();
                    return true;
                }

                catch (MySqlException e)
                {
                    MessageBox.Show("Erro, Verifica os Campos!!!", "Verificar");
                    return false;
                }

                finally
                {
                    this.desconectar();
                }
            }
            else
            {
                return false;
            }

        }

       

    }
}
