﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace PPA
{
    public partial class US_esqueci : UserControl
    {
        Banco ban = new Banco();

        public US_esqueci()
        {
            InitializeComponent();
        }

        private void US_esqueci_Load(object sender, EventArgs e)
        {
            ParentForm.FormBorderStyle = FormBorderStyle.None;
            this.Dock = DockStyle.Fill;



            MySqlConnection con = new MySqlConnection("server=localhost; userid=root; password=; database=nova");
            MySqlCommand cmd = null;
            //ban.desconectar();
            string Psql = "SELECT * FROM nova.tbutilizador";

            try
            {
                con.Open();
                cmd = new MySqlCommand(Psql, con);
                MySqlDataReader le = null;
                le = cmd.ExecuteReader();
                if (le.HasRows)
                {

                    while (le.Read())
                    {
                        tb_usuario.Items.Add(le["usuario"].ToString());
                    }
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Erro, Verifica os Campos!!!" + ex.Message, "Verificar");

            }
            finally
            {
                con.Close();
            }

        }

        private void lb_fech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MessageBox.Show("Desejas sair?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                ParentForm.Close();
            }
        }

        private void btn_testUser_Click(object sender, EventArgs e)
        {
            if (ban.testeusuaio(tb_usuario.Text, lnome, lb_perg))
            {
                btn_testUser.ForeColor = System.Drawing.Color.Green;

               
                btn_testResp.Enabled = true;
               
                tb_respo.Enabled = true;
                
            }
            else 
            {
             
                btn_testResp.Enabled = false;
          
                tb_respo.Enabled = false;
             
                MessageBox.Show("Usuário não encontrado", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
              
            }
        }

        private void btn_testResp_Click(object sender, EventArgs e)
        {
            if (ban.testeperg(tb_usuario.Text, lb_perg.Text, tb_respo.Text))
            {
                btn_testResp.ForeColor = System.Drawing.Color.Green;


                btn_confirmar.Enabled = true;

                tb_confsenha.Enabled = true;

                tb_senha.Enabled = true;
            }
            else
            {
                btn_confirmar.Enabled = false;

                tb_confsenha.Enabled = false;

                tb_senha.Enabled = false;
                MessageBox.Show("Resposta Errada!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btn_confirmar_Click(object sender, EventArgs e)
        {
            if(tb_senha.Text==tb_confsenha.Text)
            {


                                if (ban.alterarpasse(tb_usuario.Text, lb_perg.Text, tb_respo.Text,tb_confsenha.Text))
                                {
                                    btn_confirmar.ForeColor = System.Drawing.Color.Green;

                                    MessageBox.Show("Confirmado Com Sucesso!", "Certo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    
                                    tb_usuario.Text = "";
                                    tb_respo.Text = "";

                                   

                                    tb_confsenha.Text = "";

                                    tb_senha.Text = "";

                                    ParentForm.Close();
                                
                                }
                                else
                                {

                                    MessageBox.Show("Não Confirmado!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                                }
            }
            else
            {
            MessageBox.Show("Verifica a Nova Palavra-Passe e a Confirmação da Passe!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
