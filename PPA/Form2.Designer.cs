﻿namespace PPA
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.Processo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Bilhete = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Nome = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Genero = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Morada = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Data = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Pai = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Mãe = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AnodeMatricula = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Processo,
            this.Bilhete,
            this.Nome,
            this.Genero,
            this.Morada,
            this.Data,
            this.Pai,
            this.Mãe,
            this.AnodeMatricula});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1237, 146);
            this.listView1.TabIndex = 35;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // Processo
            // 
            this.Processo.Tag = "0";
            this.Processo.Text = "Processo";
            this.Processo.Width = 72;
            // 
            // Bilhete
            // 
            this.Bilhete.Tag = "1";
            this.Bilhete.Text = "Bilhete";
            this.Bilhete.Width = 100;
            // 
            // Nome
            // 
            this.Nome.Tag = "2";
            this.Nome.Text = "Nome";
            this.Nome.Width = 100;
            // 
            // Genero
            // 
            this.Genero.Text = "Genero";
            this.Genero.Width = 99;
            // 
            // Morada
            // 
            this.Morada.Text = "Morada";
            this.Morada.Width = 137;
            // 
            // Data
            // 
            this.Data.Text = "Data";
            this.Data.Width = 105;
            // 
            // Pai
            // 
            this.Pai.Text = "Pai";
            this.Pai.Width = 111;
            // 
            // Mãe
            // 
            this.Mãe.Text = "Mãe";
            this.Mãe.Width = 110;
            // 
            // AnodeMatricula
            // 
            this.AnodeMatricula.Text = "Ano de Matricula";
            this.AnodeMatricula.Width = 146;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1237, 418);
            this.Controls.Add(this.listView1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Processo;
        private System.Windows.Forms.ColumnHeader Bilhete;
        private System.Windows.Forms.ColumnHeader Nome;
        private System.Windows.Forms.ColumnHeader Genero;
        private System.Windows.Forms.ColumnHeader Morada;
        private System.Windows.Forms.ColumnHeader Data;
        private System.Windows.Forms.ColumnHeader Pai;
        private System.Windows.Forms.ColumnHeader Mãe;
        private System.Windows.Forms.ColumnHeader AnodeMatricula;

    }
}