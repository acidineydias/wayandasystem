﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lb_nome.Text = Environment.MachineName;


            lbdate.Text = System.DateTime.Now.ToString("yyyy-MM");
            if (Properties.Settings.Default.idlicensa == "")
                Properties.Settings.Default.idlicensa = lbdate.Text;
            else
                Properties.Settings.Default.idlicensa = Properties.Settings.Default.idlicensa;
        }

        private void Tempo_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(2);
            if (progressBar1.Value == 100)
            {
                Tempo.Enabled = false;
                if (Properties.Settings.Default.idlicensa != lbdate.Text)
                {
                    licensa frmlicensa = new licensa();
                    frmlicensa.Show();
                    this.Hide();
                }
                else
                {
                    frm_control fr = new frm_control();
                    fr.Show();
                    this.Hide();
                }
            }
        }

        private void lb_nome_Click(object sender, EventArgs e)
        {

        }

        private void uS_splash1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}
