﻿namespace PPA
{
    partial class US_menu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.painelROdape = new System.Windows.Forms.Panel();
            this.lb_ferra = new System.Windows.Forms.Label();
            this.btn_ferra = new System.Windows.Forms.Button();
            this.label49 = new System.Windows.Forms.Label();
            this.btnTerminar = new System.Windows.Forms.Button();
            this.lb_data = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbNivel = new System.Windows.Forms.Label();
            this.TB_CENTRAL = new System.Windows.Forms.TabControl();
            this.tpagINICIO = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.panelLAT = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.panelL1 = new System.Windows.Forms.Panel();
            this.panelL0 = new System.Windows.Forms.Panel();
            this.painelL2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panelVertical = new System.Windows.Forms.Panel();
            this.tb_estaNOinicio = new Heyech_Application_Development.HeyechTabControl();
            this.pagehome = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tpageALUNO = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.btnAbriralt_del_Conf = new System.Windows.Forms.Button();
            this.btnAbriAlt_del_Matr = new System.Windows.Forms.Button();
            this.btnAbrir_Cad_matr = new System.Windows.Forms.Button();
            this.btnAbrirSele_alu = new System.Windows.Forms.Button();
            this.btnAbrirConf = new System.Windows.Forms.Button();
            this.tabContestadentrodoAluno = new Heyech_Application_Development.HeyechTabControl();
            this.tpdentroAlu_pub = new System.Windows.Forms.TabPage();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.label138 = new System.Windows.Forms.Label();
            this.button57 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.button53 = new System.Windows.Forms.Button();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.button54 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.tpdentroAlu_CRUD = new System.Windows.Forms.TabPage();
            this.dentroAlu_CRUD = new Heyech_Application_Development.HeyechTabControl();
            this.tp_Cadastrar = new System.Windows.Forms.TabPage();
            this.label48 = new System.Windows.Forms.Label();
            this.lpMatAno = new System.Windows.Forms.Label();
            this.CbAn = new System.Windows.Forms.ComboBox();
            this.rb_nome2 = new Heyech_Application_Development.HeyechRadioButton();
            this.btnPesqCad = new System.Windows.Forms.Button();
            this.tb_pesqCad = new System.Windows.Forms.TextBox();
            this.lpMat = new System.Windows.Forms.Label();
            this.dg = new System.Windows.Forms.DataGridView();
            this.chec_verMat = new Heyech_Application_Development.HeyechCheckBox();
            this.dataHoje = new System.Windows.Forms.DateTimePicker();
            this.label50 = new System.Windows.Forms.Label();
            this.tb_An = new System.Windows.Forms.ComboBox();
            this.tbId = new System.Windows.Forms.TextBox();
            this.tb_bi = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btn_cadMarti = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.tb_genero = new System.Windows.Forms.ComboBox();
            this.tb_pai = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.tb_mae = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tb_nome = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.da = new System.Windows.Forms.DateTimePicker();
            this.tb_morada = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.tp_Confirmar = new System.Windows.Forms.TabPage();
            this.label41 = new System.Windows.Forms.Label();
            this.chec_verConf = new Heyech_Application_Development.HeyechCheckBox();
            this.lpA = new System.Windows.Forms.Label();
            this.tb_AnC = new System.Windows.Forms.ComboBox();
            this.btn_Confir = new System.Windows.Forms.Button();
            this.rb_nomeConf = new Heyech_Application_Development.HeyechRadioButton();
            this.btn_pesConf = new System.Windows.Forms.Button();
            this.tb_pesqConf = new System.Windows.Forms.TextBox();
            this.lp = new System.Windows.Forms.Label();
            this.dgConf = new System.Windows.Forms.DataGridView();
            this.label140 = new System.Windows.Forms.Label();
            this.tb_status = new System.Windows.Forms.ComboBox();
            this.button22 = new System.Windows.Forms.Button();
            this.tb_confMatri = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.tb_anoConf = new System.Windows.Forms.ComboBox();
            this.label139 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.tb_sala = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tb_classe = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tb_curso = new System.Windows.Forms.ComboBox();
            this.tb_turno = new System.Windows.Forms.ComboBox();
            this.tp_PesMat_Conf = new System.Windows.Forms.TabPage();
            this.label51 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.Cb_AnoCompleto = new System.Windows.Forms.ComboBox();
            this.heyechRadioButton70 = new Heyech_Application_Development.HeyechRadioButton();
            this.btn_pesqCompleto = new System.Windows.Forms.Button();
            this.tb_pesqCompleto = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.dgCompleto = new System.Windows.Forms.DataGridView();
            this.tp_Alt_det_Matr = new System.Windows.Forms.TabPage();
            this.label166 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.btnDeletarMAt = new System.Windows.Forms.Button();
            this.btnAtualizarMAt = new System.Windows.Forms.Button();
            this.l9 = new System.Windows.Forms.ListBox();
            this.l8 = new System.Windows.Forms.ListBox();
            this.l7 = new System.Windows.Forms.ListBox();
            this.l6 = new System.Windows.Forms.ListBox();
            this.l5 = new System.Windows.Forms.ListBox();
            this.l4 = new System.Windows.Forms.ListBox();
            this.l3 = new System.Windows.Forms.ListBox();
            this.l2 = new System.Windows.Forms.ListBox();
            this.l1 = new System.Windows.Forms.ListBox();
            this.label64 = new System.Windows.Forms.Label();
            this.cbAninho_alt_del_Mat = new System.Windows.Forms.ComboBox();
            this.btnPesq_alt_del_Mat = new System.Windows.Forms.Button();
            this.tbPesq_alt_del_Mat = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.tbAno_alt_del_Mat = new System.Windows.Forms.ComboBox();
            this.tbId_alt_del_Mat = new System.Windows.Forms.TextBox();
            this.tbBi_alt_del_Mat = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.tbGenero_alt_del_Mat = new System.Windows.Forms.ComboBox();
            this.tbPai_alt_del_Mat = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.tbMae_alt_del_Mat = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.tbNome_alt_del_Mat = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.da_alt_del_Mat = new System.Windows.Forms.DateTimePicker();
            this.tbMorada_alt_del_Mat = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.tp_Alt_det_Conf = new System.Windows.Forms.TabPage();
            this.tbnActualiza_alt_del_Conf = new System.Windows.Forms.Button();
            this.btnDeletar__alt_del_Conf = new System.Windows.Forms.Button();
            this.label157 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.lb10 = new System.Windows.Forms.ListBox();
            this.lb9 = new System.Windows.Forms.ListBox();
            this.lb8 = new System.Windows.Forms.ListBox();
            this.lb7 = new System.Windows.Forms.ListBox();
            this.lb6 = new System.Windows.Forms.ListBox();
            this.lb5 = new System.Windows.Forms.ListBox();
            this.lb4 = new System.Windows.Forms.ListBox();
            this.lb3 = new System.Windows.Forms.ListBox();
            this.lb2 = new System.Windows.Forms.ListBox();
            this.lb1 = new System.Windows.Forms.ListBox();
            this.label146 = new System.Windows.Forms.Label();
            this.cbAno_alt_del_Conf = new System.Windows.Forms.ComboBox();
            this.btnPesq__alt_del_Conf = new System.Windows.Forms.Button();
            this.tbPesq_alt_del_Conf = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.tbStatus_alt_del_Conf = new System.Windows.Forms.ComboBox();
            this.button13 = new System.Windows.Forms.Button();
            this.tbId_alt_del_Conf = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.tbAno_alt_del_Conf = new System.Windows.Forms.ComboBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.tbSala_alt_del_Conf = new System.Windows.Forms.ComboBox();
            this.label144 = new System.Windows.Forms.Label();
            this.tbClasse_alt_del_Conf = new System.Windows.Forms.ComboBox();
            this.label145 = new System.Windows.Forms.Label();
            this.tbCurso_alt_del_Conf = new System.Windows.Forms.ComboBox();
            this.tbTurno_alt_del_Conf = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tpagePROPRINA = new System.Windows.Forms.TabPage();
            this.panel14 = new System.Windows.Forms.Panel();
            this.button34 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.TbPropinaDen = new Heyech_Application_Development.HeyechTabControl();
            this.TbPropinaDen_pub = new System.Windows.Forms.TabPage();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.button26 = new System.Windows.Forms.Button();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.button27 = new System.Windows.Forms.Button();
            this.label68 = new System.Windows.Forms.Label();
            this.TbPropinaDen_crud = new System.Windows.Forms.TabPage();
            this.btn_CadImprimir = new System.Windows.Forms.Button();
            this.datadoPAG = new System.Windows.Forms.DateTimePicker();
            this.btnCadPAg = new System.Windows.Forms.Button();
            this.precott = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.LvalorDesc = new System.Windows.Forms.Panel();
            this.txtdatOBS = new System.Windows.Forms.Label();
            this.txtOBSmes = new System.Windows.Forms.Label();
            this.Lmeses = new System.Windows.Forms.ListBox();
            this.Classepreco = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.Lsaldo = new System.Windows.Forms.Label();
            this.Lano = new System.Windows.Forms.Label();
            this.Lsala = new System.Windows.Forms.Label();
            this.Lturno = new System.Windows.Forms.Label();
            this.Lclasse = new System.Windows.Forms.Label();
            this.Lcurso = new System.Windows.Forms.Label();
            this.Lnome = new System.Windows.Forms.Label();
            this.Lid = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.btn_cadPAg = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.tb_mesdesc = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tb_numMes = new System.Windows.Forms.ComboBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.c7 = new System.Windows.Forms.ListBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.c = new System.Windows.Forms.ListBox();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.c6 = new System.Windows.Forms.ListBox();
            this.c5 = new System.Windows.Forms.ListBox();
            this.c4 = new System.Windows.Forms.ListBox();
            this.c3 = new System.Windows.Forms.ListBox();
            this.c2 = new System.Windows.Forms.ListBox();
            this.c1 = new System.Windows.Forms.ListBox();
            this.button12 = new System.Windows.Forms.Button();
            this.prop_nome = new Heyech_Application_Development.HeyechRadioButton();
            this.tb_pesqProp = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this.TbPropinaDen_propina = new System.Windows.Forms.TabPage();
            this.textClasse = new System.Windows.Forms.TextBox();
            this.textSaldo = new System.Windows.Forms.TextBox();
            this.textTurno = new System.Windows.Forms.TextBox();
            this.textSala = new System.Windows.Forms.TextBox();
            this.textCurso = new System.Windows.Forms.TextBox();
            this.label168 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.ax4 = new System.Windows.Forms.ListBox();
            this.ax5 = new System.Windows.Forms.ListBox();
            this.ax3 = new System.Windows.Forms.ListBox();
            this.ax2 = new System.Windows.Forms.ListBox();
            this.ax1 = new System.Windows.Forms.ListBox();
            this.textNOme = new System.Windows.Forms.TextBox();
            this.button14 = new System.Windows.Forms.Button();
            this.cbANOpag = new System.Windows.Forms.ComboBox();
            this.tb_pagamentoID = new System.Windows.Forms.TextBox();
            this.tb_Userprop = new System.Windows.Forms.ComboBox();
            this.a = new System.Windows.Forms.ListBox();
            this.tb_Anoprop = new System.Windows.Forms.ComboBox();
            this.label117 = new System.Windows.Forms.Label();
            this.a1 = new System.Windows.Forms.ListBox();
            this.btn_delProp = new System.Windows.Forms.Button();
            this.btn_actProp = new System.Windows.Forms.Button();
            this.label119 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.a6 = new System.Windows.Forms.ListBox();
            this.a5 = new System.Windows.Forms.ListBox();
            this.a4 = new System.Windows.Forms.ListBox();
            this.a3 = new System.Windows.Forms.ListBox();
            this.a2 = new System.Windows.Forms.ListBox();
            this.label125 = new System.Windows.Forms.Label();
            this.heyechRadioButton2 = new Heyech_Application_Development.HeyechRadioButton();
            this.button20 = new System.Windows.Forms.Button();
            this.tb_Peprop = new System.Windows.Forms.TextBox();
            this.label127 = new System.Windows.Forms.Label();
            this.tb_IDpropina = new System.Windows.Forms.TextBox();
            this.tb_Mesprop = new System.Windows.Forms.TextBox();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.data_prop = new System.Windows.Forms.DateTimePicker();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tpageDEPOSITO = new System.Windows.Forms.TabPage();
            this.panel13 = new System.Windows.Forms.Panel();
            this.button28 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.TbDepositoDen = new Heyech_Application_Development.HeyechTabControl();
            this.TbDepositoDen_pub = new System.Windows.Forms.TabPage();
            this.label71 = new System.Windows.Forms.Label();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.button19 = new System.Windows.Forms.Button();
            this.label70 = new System.Windows.Forms.Label();
            this.TbDepositoDen_CRD = new System.Windows.Forms.TabPage();
            this.DepositoDen = new Heyech_Application_Development.HeyechTabControl();
            this.DepositoDen_cadasDep = new System.Windows.Forms.TabPage();
            this.cb_cla = new System.Windows.Forms.ComboBox();
            this.label73 = new System.Windows.Forms.Label();
            this.sal = new System.Windows.Forms.ListBox();
            this.label72 = new System.Windows.Forms.Label();
            this.ddd = new System.Windows.Forms.ListBox();
            this.button18 = new System.Windows.Forms.Button();
            this.tb_d = new System.Windows.Forms.ComboBox();
            this.label69 = new System.Windows.Forms.Label();
            this.tb_bancoDep = new System.Windows.Forms.ComboBox();
            this.tb_IDATODEP = new System.Windows.Forms.TextBox();
            this.dd = new System.Windows.Forms.ListBox();
            this.label92 = new System.Windows.Forms.Label();
            this.d = new System.Windows.Forms.ListBox();
            this.btn_deletDep = new System.Windows.Forms.Button();
            this.btn_actDep = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.dataPesqDep = new System.Windows.Forms.DateTimePicker();
            this.label81 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.d6 = new System.Windows.Forms.ListBox();
            this.d5 = new System.Windows.Forms.ListBox();
            this.d4 = new System.Windows.Forms.ListBox();
            this.d3 = new System.Windows.Forms.ListBox();
            this.d2 = new System.Windows.Forms.ListBox();
            this.d1 = new System.Windows.Forms.ListBox();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.heyechRadioButton1 = new Heyech_Application_Development.HeyechRadioButton();
            this.btn_pesqDep = new System.Windows.Forms.Button();
            this.tb_pesqDep = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.data_actual = new System.Windows.Forms.DateTimePicker();
            this.tb_IDalunoDep = new System.Windows.Forms.TextBox();
            this.tb_talão = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.btn_cadDep = new System.Windows.Forms.Button();
            this.label86 = new System.Windows.Forms.Label();
            this.tb_valorDep = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.data_dep = new System.Windows.Forms.DateTimePicker();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tpageEstatis = new System.Windows.Forms.TabPage();
            this.panel15 = new System.Windows.Forms.Panel();
            this.button40 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.TbEstatisticaDen = new Heyech_Application_Development.HeyechTabControl();
            this.TbEstatisticaDen_pub = new System.Windows.Forms.TabPage();
            this.label133 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.TbEstatisticaDen_CRUD = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lbMini = new System.Windows.Forms.LinkLabel();
            this.panelRodSuperio = new System.Windows.Forms.Panel();
            this.lb_fech = new System.Windows.Forms.LinkLabel();
            this.CbAnProp = new System.Windows.Forms.Panel();
            this.lbNomeUsuario = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panellinhacima = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.btnDeposit = new System.Windows.Forms.Button();
            this.btnEstast = new System.Windows.Forms.Button();
            this.btnPropina = new System.Windows.Forms.Button();
            this.btnAluno = new System.Windows.Forms.Button();
            this.btnInicio = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.painelROdape.SuspendLayout();
            this.TB_CENTRAL.SuspendLayout();
            this.tpagINICIO.SuspendLayout();
            this.panelLAT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.tb_estaNOinicio.SuspendLayout();
            this.pagehome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tpageALUNO.SuspendLayout();
            this.panel11.SuspendLayout();
            this.tabContestadentrodoAluno.SuspendLayout();
            this.tpdentroAlu_pub.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.tpdentroAlu_CRUD.SuspendLayout();
            this.dentroAlu_CRUD.SuspendLayout();
            this.tp_Cadastrar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg)).BeginInit();
            this.tp_Confirmar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgConf)).BeginInit();
            this.tp_PesMat_Conf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCompleto)).BeginInit();
            this.tp_Alt_det_Matr.SuspendLayout();
            this.tp_Alt_det_Conf.SuspendLayout();
            this.tpagePROPRINA.SuspendLayout();
            this.panel14.SuspendLayout();
            this.TbPropinaDen.SuspendLayout();
            this.TbPropinaDen_pub.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            this.TbPropinaDen_crud.SuspendLayout();
            this.LvalorDesc.SuspendLayout();
            this.panel5.SuspendLayout();
            this.TbPropinaDen_propina.SuspendLayout();
            this.tpageDEPOSITO.SuspendLayout();
            this.panel13.SuspendLayout();
            this.TbDepositoDen.SuspendLayout();
            this.TbDepositoDen_pub.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.TbDepositoDen_CRD.SuspendLayout();
            this.DepositoDen.SuspendLayout();
            this.DepositoDen_cadasDep.SuspendLayout();
            this.tpageEstatis.SuspendLayout();
            this.panel15.SuspendLayout();
            this.TbEstatisticaDen.SuspendLayout();
            this.TbEstatisticaDen_pub.SuspendLayout();
            this.CbAnProp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // painelROdape
            // 
            this.painelROdape.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.painelROdape.Controls.Add(this.lb_ferra);
            this.painelROdape.Controls.Add(this.btn_ferra);
            this.painelROdape.Controls.Add(this.label49);
            this.painelROdape.Controls.Add(this.btnTerminar);
            this.painelROdape.Controls.Add(this.lb_data);
            this.painelROdape.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.painelROdape.Location = new System.Drawing.Point(0, 711);
            this.painelROdape.Name = "painelROdape";
            this.painelROdape.Size = new System.Drawing.Size(1340, 57);
            this.painelROdape.TabIndex = 38;
            // 
            // lb_ferra
            // 
            this.lb_ferra.BackColor = System.Drawing.Color.Transparent;
            this.lb_ferra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb_ferra.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lb_ferra.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lb_ferra.Location = new System.Drawing.Point(221, 18);
            this.lb_ferra.Name = "lb_ferra";
            this.lb_ferra.Size = new System.Drawing.Size(155, 20);
            this.lb_ferra.TabIndex = 95;
            this.lb_ferra.Text = "Ferramenta Admim";
            this.lb_ferra.Click += new System.EventHandler(this.label74_Click);
            // 
            // btn_ferra
            // 
            this.btn_ferra.BackColor = System.Drawing.Color.Transparent;
            this.btn_ferra.BackgroundImage = global::PPA.Properties.Resources.redhat_system_tools__2_;
            this.btn_ferra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_ferra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_ferra.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.btn_ferra.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_ferra.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_ferra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ferra.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_ferra.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_ferra.Location = new System.Drawing.Point(185, 14);
            this.btn_ferra.Name = "btn_ferra";
            this.btn_ferra.Size = new System.Drawing.Size(30, 28);
            this.btn_ferra.TabIndex = 94;
            this.btn_ferra.UseVisualStyleBackColor = false;
            this.btn_ferra.Click += new System.EventHandler(this.btn_ferra_Click);
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label49.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label49.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label49.Location = new System.Drawing.Point(40, 17);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(155, 20);
            this.label49.TabIndex = 93;
            this.label49.Text = "Terminar a Sessão";
            this.label49.Click += new System.EventHandler(this.label49_Click);
            // 
            // btnTerminar
            // 
            this.btnTerminar.BackColor = System.Drawing.Color.Transparent;
            this.btnTerminar.BackgroundImage = global::PPA.Properties.Resources.basic2_047_display_screensaver_sleep_128;
            this.btnTerminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTerminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTerminar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.btnTerminar.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnTerminar.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnTerminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTerminar.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnTerminar.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnTerminar.Location = new System.Drawing.Point(4, 13);
            this.btnTerminar.Name = "btnTerminar";
            this.btnTerminar.Size = new System.Drawing.Size(30, 28);
            this.btnTerminar.TabIndex = 92;
            this.btnTerminar.UseVisualStyleBackColor = false;
            this.btnTerminar.Click += new System.EventHandler(this.btnTerminar_Click);
            // 
            // lb_data
            // 
            this.lb_data.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_data.AutoSize = true;
            this.lb_data.BackColor = System.Drawing.Color.Transparent;
            this.lb_data.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lb_data.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_data.Location = new System.Drawing.Point(1176, 21);
            this.lb_data.Name = "lb_data";
            this.lb_data.Size = new System.Drawing.Size(46, 20);
            this.lb_data.TabIndex = 42;
            this.lb_data.Text = "Data :";
            this.lb_data.Click += new System.EventHandler(this.lb_data_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(1107, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 39;
            this.label1.Text = "Usuário :";
            // 
            // lbNivel
            // 
            this.lbNivel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbNivel.BackColor = System.Drawing.Color.Transparent;
            this.lbNivel.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lbNivel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbNivel.Location = new System.Drawing.Point(1175, 120);
            this.lbNivel.Name = "lbNivel";
            this.lbNivel.Size = new System.Drawing.Size(145, 20);
            this.lbNivel.TabIndex = 41;
            this.lbNivel.Text = "Lourenço Carlos";
            // 
            // TB_CENTRAL
            // 
            this.TB_CENTRAL.Controls.Add(this.tpagINICIO);
            this.TB_CENTRAL.Controls.Add(this.tpageALUNO);
            this.TB_CENTRAL.Controls.Add(this.tpagePROPRINA);
            this.TB_CENTRAL.Controls.Add(this.tpageDEPOSITO);
            this.TB_CENTRAL.Controls.Add(this.tpageEstatis);
            this.TB_CENTRAL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_CENTRAL.ItemSize = new System.Drawing.Size(0, 1);
            this.TB_CENTRAL.Location = new System.Drawing.Point(0, 178);
            this.TB_CENTRAL.Name = "TB_CENTRAL";
            this.TB_CENTRAL.SelectedIndex = 0;
            this.TB_CENTRAL.Size = new System.Drawing.Size(1340, 533);
            this.TB_CENTRAL.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.TB_CENTRAL.TabIndex = 39;
            // 
            // tpagINICIO
            // 
            this.tpagINICIO.BackColor = System.Drawing.Color.White;
            this.tpagINICIO.Controls.Add(this.label2);
            this.tpagINICIO.Controls.Add(this.panelLAT);
            this.tpagINICIO.Controls.Add(this.tb_estaNOinicio);
            this.tpagINICIO.Location = new System.Drawing.Point(4, 5);
            this.tpagINICIO.Name = "tpagINICIO";
            this.tpagINICIO.Padding = new System.Windows.Forms.Padding(3);
            this.tpagINICIO.Size = new System.Drawing.Size(1332, 524);
            this.tpagINICIO.TabIndex = 0;
            this.tpagINICIO.Click += new System.EventHandler(this.tpagINICIO_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(30, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 54);
            this.label2.TabIndex = 26;
            this.label2.Text = "Bem-Vindo";
            // 
            // panelLAT
            // 
            this.panelLAT.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelLAT.Controls.Add(this.button10);
            this.panelLAT.Controls.Add(this.button9);
            this.panelLAT.Controls.Add(this.button8);
            this.panelLAT.Controls.Add(this.button7);
            this.panelLAT.Controls.Add(this.button6);
            this.panelLAT.Controls.Add(this.button5);
            this.panelLAT.Controls.Add(this.label29);
            this.panelLAT.Controls.Add(this.label30);
            this.panelLAT.Controls.Add(this.label31);
            this.panelLAT.Controls.Add(this.label26);
            this.panelLAT.Controls.Add(this.label27);
            this.panelLAT.Controls.Add(this.label28);
            this.panelLAT.Controls.Add(this.label23);
            this.panelLAT.Controls.Add(this.label24);
            this.panelLAT.Controls.Add(this.label25);
            this.panelLAT.Controls.Add(this.label20);
            this.panelLAT.Controls.Add(this.label21);
            this.panelLAT.Controls.Add(this.label22);
            this.panelLAT.Controls.Add(this.label17);
            this.panelLAT.Controls.Add(this.label18);
            this.panelLAT.Controls.Add(this.label19);
            this.panelLAT.Controls.Add(this.label16);
            this.panelLAT.Controls.Add(this.label15);
            this.panelLAT.Controls.Add(this.label14);
            this.panelLAT.Controls.Add(this.pictureBox10);
            this.panelLAT.Controls.Add(this.pictureBox11);
            this.panelLAT.Controls.Add(this.pictureBox12);
            this.panelLAT.Controls.Add(this.pictureBox9);
            this.panelLAT.Controls.Add(this.pictureBox8);
            this.panelLAT.Controls.Add(this.pictureBox7);
            this.panelLAT.Controls.Add(this.panelL1);
            this.panelLAT.Controls.Add(this.panelL0);
            this.panelLAT.Controls.Add(this.painelL2);
            this.panelLAT.Controls.Add(this.label7);
            this.panelLAT.Controls.Add(this.label6);
            this.panelLAT.Controls.Add(this.panelVertical);
            this.panelLAT.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelLAT.Location = new System.Drawing.Point(1012, 3);
            this.panelLAT.Name = "panelLAT";
            this.panelLAT.Size = new System.Drawing.Size(317, 518);
            this.panelLAT.TabIndex = 0;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 8F);
            this.button10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Location = new System.Drawing.Point(252, 457);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(39, 22);
            this.button10.TabIndex = 75;
            this.button10.Text = "(0_0)";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 8F);
            this.button9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button9.Location = new System.Drawing.Point(249, 389);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(39, 22);
            this.button9.TabIndex = 74;
            this.button9.Text = "(0_0)";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 8F);
            this.button8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button8.Location = new System.Drawing.Point(249, 319);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(39, 22);
            this.button8.TabIndex = 73;
            this.button8.Text = "(0_0)";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 8F);
            this.button7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Location = new System.Drawing.Point(252, 209);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(39, 22);
            this.button7.TabIndex = 72;
            this.button7.Text = "(0_0)";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 8F);
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Location = new System.Drawing.Point(252, 141);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(39, 22);
            this.button6.TabIndex = 71;
            this.button6.Text = "(0_0)";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 8F);
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(252, 69);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(39, 22);
            this.button5.TabIndex = 45;
            this.button5.Text = "(0_0)";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label29.ForeColor = System.Drawing.Color.Gray;
            this.label29.Location = new System.Drawing.Point(75, 475);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(62, 14);
            this.label29.TabIndex = 70;
            this.label29.Text = "*Classe: 10º";
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label30.ForeColor = System.Drawing.Color.Gray;
            this.label30.Location = new System.Drawing.Point(74, 461);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(142, 14);
            this.label30.TabIndex = 69;
            this.label30.Text = "*Curso: Técnico de Informática";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label31.ForeColor = System.Drawing.Color.Gray;
            this.label31.Location = new System.Drawing.Point(70, 442);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(121, 20);
            this.label31.TabIndex = 68;
            this.label31.Text = "*Joaquim Delgado";
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label26.ForeColor = System.Drawing.Color.Gray;
            this.label26.Location = new System.Drawing.Point(75, 407);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(62, 14);
            this.label26.TabIndex = 67;
            this.label26.Text = "*Classe: 12º";
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label27.ForeColor = System.Drawing.Color.Gray;
            this.label27.Location = new System.Drawing.Point(74, 393);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(105, 14);
            this.label27.TabIndex = 66;
            this.label27.Text = "*Curso: Contabilidade";
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label28.ForeColor = System.Drawing.Color.Gray;
            this.label28.Location = new System.Drawing.Point(70, 374);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(127, 20);
            this.label28.TabIndex = 65;
            this.label28.Text = "*Amado Alexandre";
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label23.ForeColor = System.Drawing.Color.Gray;
            this.label23.Location = new System.Drawing.Point(75, 337);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(62, 14);
            this.label23.TabIndex = 64;
            this.label23.Text = "*Classe: 11º";
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label24.ForeColor = System.Drawing.Color.Gray;
            this.label24.Location = new System.Drawing.Point(74, 323);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(135, 14);
            this.label24.TabIndex = 63;
            this.label24.Text = "*Curso: Informática e Gestão";
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label25.ForeColor = System.Drawing.Color.Gray;
            this.label25.Location = new System.Drawing.Point(70, 304);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(173, 20);
            this.label25.TabIndex = 62;
            this.label25.Text = "*Manuel João A. Bonefacio";
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label20.ForeColor = System.Drawing.Color.Gray;
            this.label20.Location = new System.Drawing.Point(75, 227);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 14);
            this.label20.TabIndex = 61;
            this.label20.Text = "*Classe: 10º";
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label21.ForeColor = System.Drawing.Color.Gray;
            this.label21.Location = new System.Drawing.Point(74, 213);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(131, 14);
            this.label21.TabIndex = 60;
            this.label21.Text = "*Curso: Gestão Empresarial";
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label22.ForeColor = System.Drawing.Color.Gray;
            this.label22.Location = new System.Drawing.Point(70, 194);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(148, 20);
            this.label22.TabIndex = 59;
            this.label22.Text = "*Benjamim Tiago Moço";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label17.ForeColor = System.Drawing.Color.Gray;
            this.label17.Location = new System.Drawing.Point(75, 159);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 14);
            this.label17.TabIndex = 58;
            this.label17.Text = "*Classe: 10º";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label18.ForeColor = System.Drawing.Color.Gray;
            this.label18.Location = new System.Drawing.Point(74, 145);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(119, 14);
            this.label18.TabIndex = 57;
            this.label18.Text = "*Curso: Técnico de Obras";
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label19.ForeColor = System.Drawing.Color.Gray;
            this.label19.Location = new System.Drawing.Point(70, 126);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(162, 20);
            this.label19.TabIndex = 56;
            this.label19.Text = "*Hairton Silvestre Vanda";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label16.ForeColor = System.Drawing.Color.Gray;
            this.label16.Location = new System.Drawing.Point(75, 87);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 14);
            this.label16.TabIndex = 55;
            this.label16.Text = "*Classe: 10º";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F);
            this.label15.ForeColor = System.Drawing.Color.Gray;
            this.label15.Location = new System.Drawing.Point(74, 73);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(142, 14);
            this.label15.TabIndex = 54;
            this.label15.Text = "*Curso: Técnico de Informática";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label14.ForeColor = System.Drawing.Color.Gray;
            this.label14.Location = new System.Drawing.Point(70, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(154, 20);
            this.label14.TabIndex = 53;
            this.label14.Text = "*Lourenço Daniel Carlos";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::PPA.Properties.Resources.jay_fw;
            this.pictureBox10.Location = new System.Drawing.Point(14, 442);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(43, 44);
            this.pictureBox10.TabIndex = 52;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::PPA.Properties.Resources.alex_fw;
            this.pictureBox11.Location = new System.Drawing.Point(14, 374);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(43, 44);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 51;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::PPA.Properties.Resources.boic_fw;
            this.pictureBox12.Location = new System.Drawing.Point(14, 304);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(43, 44);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 50;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::PPA.Properties.Resources.moco_fw;
            this.pictureBox9.Location = new System.Drawing.Point(14, 194);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(43, 44);
            this.pictureBox9.TabIndex = 49;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::PPA.Properties.Resources.hair_fw;
            this.pictureBox8.Location = new System.Drawing.Point(14, 126);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(43, 44);
            this.pictureBox8.TabIndex = 48;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::PPA.Properties.Resources.lou_fw;
            this.pictureBox7.Location = new System.Drawing.Point(14, 56);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(43, 44);
            this.pictureBox7.TabIndex = 47;
            this.pictureBox7.TabStop = false;
            // 
            // panelL1
            // 
            this.panelL1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.panelL1.Location = new System.Drawing.Point(5, 294);
            this.panelL1.Name = "panelL1";
            this.panelL1.Size = new System.Drawing.Size(309, 1);
            this.panelL1.TabIndex = 31;
            // 
            // panelL0
            // 
            this.panelL0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.panelL0.Location = new System.Drawing.Point(3, 245);
            this.panelL0.Name = "panelL0";
            this.panelL0.Size = new System.Drawing.Size(309, 1);
            this.panelL0.TabIndex = 30;
            // 
            // painelL2
            // 
            this.painelL2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.painelL2.Location = new System.Drawing.Point(5, 49);
            this.painelL2.Name = "painelL2";
            this.painelL2.Size = new System.Drawing.Size(309, 1);
            this.painelL2.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label7.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label7.Location = new System.Drawing.Point(42, 261);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(232, 20);
            this.label7.TabIndex = 46;
            this.label7.Text = "Propinas Actualizadas Recentemente";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(46, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(217, 20);
            this.label6.TabIndex = 45;
            this.label6.Text = "Alunos Cadastrados Recentemente";
            // 
            // panelVertical
            // 
            this.panelVertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.panelVertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelVertical.Location = new System.Drawing.Point(0, 0);
            this.panelVertical.Name = "panelVertical";
            this.panelVertical.Size = new System.Drawing.Size(1, 518);
            this.panelVertical.TabIndex = 0;
            // 
            // tb_estaNOinicio
            // 
            this.tb_estaNOinicio.Controls.Add(this.pagehome);
            this.tb_estaNOinicio.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.tb_estaNOinicio.ItemSize = new System.Drawing.Size(0, 30);
            this.tb_estaNOinicio.Location = new System.Drawing.Point(36, 42);
            this.tb_estaNOinicio.Name = "tb_estaNOinicio";
            this.tb_estaNOinicio.SelectedIndex = 0;
            this.tb_estaNOinicio.Size = new System.Drawing.Size(941, 372);
            this.tb_estaNOinicio.TabIndex = 27;
            // 
            // pagehome
            // 
            this.pagehome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pagehome.Controls.Add(this.label13);
            this.pagehome.Controls.Add(this.label12);
            this.pagehome.Controls.Add(this.label11);
            this.pagehome.Controls.Add(this.label10);
            this.pagehome.Controls.Add(this.pictureBox6);
            this.pagehome.Controls.Add(this.button4);
            this.pagehome.Controls.Add(this.pictureBox5);
            this.pagehome.Controls.Add(this.button3);
            this.pagehome.Controls.Add(this.pictureBox4);
            this.pagehome.Controls.Add(this.button2);
            this.pagehome.Controls.Add(this.pictureBox3);
            this.pagehome.Controls.Add(this.label9);
            this.pagehome.Controls.Add(this.button1);
            this.pagehome.Location = new System.Drawing.Point(4, 34);
            this.pagehome.Name = "pagehome";
            this.pagehome.Padding = new System.Windows.Forms.Padding(3);
            this.pagehome.Size = new System.Drawing.Size(933, 334);
            this.pagehome.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label13.Location = new System.Drawing.Point(619, 176);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(159, 61);
            this.label13.TabIndex = 44;
            this.label13.Text = "Faça o castramento dos depositos efectuados!";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label12.Location = new System.Drawing.Point(416, 176);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(152, 46);
            this.label12.TabIndex = 43;
            this.label12.Text = "Faça a actualização de propinas de Alunos!";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label11.Location = new System.Drawing.Point(212, 176);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(152, 46);
            this.label11.TabIndex = 42;
            this.label11.Text = "Faça a confirmação de alunos já cadastrados!";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label10.Location = new System.Drawing.Point(8, 176);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(152, 46);
            this.label10.TabIndex = 41;
            this.label10.Text = "Faça cadastarmento de novos Estudantes!";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::PPA.Properties.Resources.cad;
            this.pictureBox6.Location = new System.Drawing.Point(246, 45);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(74, 78);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 40;
            this.pictureBox6.TabStop = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Location = new System.Drawing.Point(212, 134);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(148, 27);
            this.button4.TabIndex = 39;
            this.button4.Text = "Confirmar Aluno";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::PPA.Properties.Resources.dfg;
            this.pictureBox5.Location = new System.Drawing.Point(655, 45);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(74, 78);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 38;
            this.pictureBox5.TabStop = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(621, 134);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(148, 27);
            this.button3.TabIndex = 37;
            this.button3.Text = "Cadastrar Depositos";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::PPA.Properties.Resources.lpo;
            this.pictureBox4.Location = new System.Drawing.Point(449, 45);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(74, 78);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 36;
            this.pictureBox4.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(415, 134);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(148, 27);
            this.button2.TabIndex = 35;
            this.button2.Text = "Actualizar Propina";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::PPA.Properties.Resources.images_9;
            this.pictureBox3.Location = new System.Drawing.Point(39, 45);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(74, 78);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 34;
            this.pictureBox3.TabStop = false;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label9.ForeColor = System.Drawing.Color.Gray;
            this.label9.Location = new System.Drawing.Point(-2, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(155, 20);
            this.label9.TabIndex = 33;
            this.label9.Text = "*Operações frequentes :";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(5, 134);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 27);
            this.button1.TabIndex = 32;
            this.button1.Text = "Cadastrar Aluno";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tpageALUNO
            // 
            this.tpageALUNO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tpageALUNO.Controls.Add(this.panel11);
            this.tpageALUNO.Controls.Add(this.tabContestadentrodoAluno);
            this.tpageALUNO.Controls.Add(this.panel1);
            this.tpageALUNO.Controls.Add(this.panel2);
            this.tpageALUNO.Location = new System.Drawing.Point(4, 5);
            this.tpageALUNO.Name = "tpageALUNO";
            this.tpageALUNO.Padding = new System.Windows.Forms.Padding(3);
            this.tpageALUNO.Size = new System.Drawing.Size(1332, 524);
            this.tpageALUNO.TabIndex = 1;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.panel11.Controls.Add(this.button11);
            this.panel11.Controls.Add(this.btnAbriralt_del_Conf);
            this.panel11.Controls.Add(this.btnAbriAlt_del_Matr);
            this.panel11.Controls.Add(this.btnAbrir_Cad_matr);
            this.panel11.Controls.Add(this.btnAbrirSele_alu);
            this.panel11.Controls.Add(this.btnAbrirConf);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel11.Location = new System.Drawing.Point(38, 477);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1256, 44);
            this.panel11.TabIndex = 30;
            // 
            // button11
            // 
            this.button11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button11.AutoSize = true;
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(1151, 7);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(59, 30);
            this.button11.TabIndex = 63;
            this.button11.Text = " (0_0)";
            this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // btnAbriralt_del_Conf
            // 
            this.btnAbriralt_del_Conf.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAbriralt_del_Conf.AutoSize = true;
            this.btnAbriralt_del_Conf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.btnAbriralt_del_Conf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAbriralt_del_Conf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAbriralt_del_Conf.FlatAppearance.BorderSize = 0;
            this.btnAbriralt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbriralt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnAbriralt_del_Conf.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAbriralt_del_Conf.Image = global::PPA.Properties.Resources.k3_fw;
            this.btnAbriralt_del_Conf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbriralt_del_Conf.Location = new System.Drawing.Point(732, 7);
            this.btnAbriralt_del_Conf.Name = "btnAbriralt_del_Conf";
            this.btnAbriralt_del_Conf.Size = new System.Drawing.Size(171, 30);
            this.btnAbriralt_del_Conf.TabIndex = 62;
            this.btnAbriralt_del_Conf.Text = "     Alterar/Deletar Conf.";
            this.btnAbriralt_del_Conf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbriralt_del_Conf.UseVisualStyleBackColor = false;
            this.btnAbriralt_del_Conf.Click += new System.EventHandler(this.btnAbriralt_del_Conf_Click);
            // 
            // btnAbriAlt_del_Matr
            // 
            this.btnAbriAlt_del_Matr.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAbriAlt_del_Matr.AutoSize = true;
            this.btnAbriAlt_del_Matr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.btnAbriAlt_del_Matr.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAbriAlt_del_Matr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAbriAlt_del_Matr.FlatAppearance.BorderSize = 0;
            this.btnAbriAlt_del_Matr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbriAlt_del_Matr.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnAbriAlt_del_Matr.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAbriAlt_del_Matr.Image = global::PPA.Properties.Resources.k3_fw;
            this.btnAbriAlt_del_Matr.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbriAlt_del_Matr.Location = new System.Drawing.Point(529, 7);
            this.btnAbriAlt_del_Matr.Name = "btnAbriAlt_del_Matr";
            this.btnAbriAlt_del_Matr.Size = new System.Drawing.Size(178, 30);
            this.btnAbriAlt_del_Matr.TabIndex = 61;
            this.btnAbriAlt_del_Matr.Text = "      Alterar/Deletar Matr.";
            this.btnAbriAlt_del_Matr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbriAlt_del_Matr.UseVisualStyleBackColor = false;
            this.btnAbriAlt_del_Matr.Click += new System.EventHandler(this.btnAbriAlt_del_Matr_Click);
            // 
            // btnAbrir_Cad_matr
            // 
            this.btnAbrir_Cad_matr.AutoSize = true;
            this.btnAbrir_Cad_matr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.btnAbrir_Cad_matr.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAbrir_Cad_matr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAbrir_Cad_matr.FlatAppearance.BorderSize = 0;
            this.btnAbrir_Cad_matr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbrir_Cad_matr.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnAbrir_Cad_matr.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAbrir_Cad_matr.Image = global::PPA.Properties.Resources.k4_fw;
            this.btnAbrir_Cad_matr.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbrir_Cad_matr.Location = new System.Drawing.Point(16, 7);
            this.btnAbrir_Cad_matr.Name = "btnAbrir_Cad_matr";
            this.btnAbrir_Cad_matr.Size = new System.Drawing.Size(148, 30);
            this.btnAbrir_Cad_matr.TabIndex = 58;
            this.btnAbrir_Cad_matr.Text = "      Cadatrar Aluno";
            this.btnAbrir_Cad_matr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbrir_Cad_matr.UseVisualStyleBackColor = false;
            this.btnAbrir_Cad_matr.Click += new System.EventHandler(this.btnAbrir_Cad_matr_Click);
            // 
            // btnAbrirSele_alu
            // 
            this.btnAbrirSele_alu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAbrirSele_alu.AutoSize = true;
            this.btnAbrirSele_alu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.btnAbrirSele_alu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAbrirSele_alu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAbrirSele_alu.FlatAppearance.BorderSize = 0;
            this.btnAbrirSele_alu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbrirSele_alu.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnAbrirSele_alu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAbrirSele_alu.Image = global::PPA.Properties.Resources.k2_fw;
            this.btnAbrirSele_alu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbrirSele_alu.Location = new System.Drawing.Point(360, 7);
            this.btnAbrirSele_alu.Name = "btnAbrirSele_alu";
            this.btnAbrirSele_alu.Size = new System.Drawing.Size(148, 30);
            this.btnAbrirSele_alu.TabIndex = 60;
            this.btnAbrirSele_alu.Text = "     Seleccionar Aluno";
            this.btnAbrirSele_alu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbrirSele_alu.UseVisualStyleBackColor = false;
            this.btnAbrirSele_alu.Click += new System.EventHandler(this.btnAbrirSele_alu_Click);
            // 
            // btnAbrirConf
            // 
            this.btnAbrirConf.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAbrirConf.AutoSize = true;
            this.btnAbrirConf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.btnAbrirConf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAbrirConf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAbrirConf.FlatAppearance.BorderSize = 0;
            this.btnAbrirConf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbrirConf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnAbrirConf.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAbrirConf.Image = global::PPA.Properties.Resources.k1_fw;
            this.btnAbrirConf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbrirConf.Location = new System.Drawing.Point(185, 7);
            this.btnAbrirConf.Name = "btnAbrirConf";
            this.btnAbrirConf.Size = new System.Drawing.Size(148, 30);
            this.btnAbrirConf.TabIndex = 59;
            this.btnAbrirConf.Text = "      Confirmar Aluno";
            this.btnAbrirConf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbrirConf.UseVisualStyleBackColor = false;
            this.btnAbrirConf.Click += new System.EventHandler(this.btnAbrirConf_Click);
            // 
            // tabContestadentrodoAluno
            // 
            this.tabContestadentrodoAluno.Controls.Add(this.tpdentroAlu_pub);
            this.tabContestadentrodoAluno.Controls.Add(this.tpdentroAlu_CRUD);
            this.tabContestadentrodoAluno.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tabContestadentrodoAluno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContestadentrodoAluno.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.tabContestadentrodoAluno.ItemSize = new System.Drawing.Size(0, 1);
            this.tabContestadentrodoAluno.Location = new System.Drawing.Point(38, 3);
            this.tabContestadentrodoAluno.Name = "tabContestadentrodoAluno";
            this.tabContestadentrodoAluno.SelectedIndex = 0;
            this.tabContestadentrodoAluno.Size = new System.Drawing.Size(1256, 518);
            this.tabContestadentrodoAluno.TabIndex = 0;
            // 
            // tpdentroAlu_pub
            // 
            this.tpdentroAlu_pub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tpdentroAlu_pub.Controls.Add(this.pictureBox17);
            this.tpdentroAlu_pub.Controls.Add(this.pictureBox15);
            this.tpdentroAlu_pub.Controls.Add(this.pictureBox16);
            this.tpdentroAlu_pub.Controls.Add(this.label138);
            this.tpdentroAlu_pub.Controls.Add(this.button57);
            this.tpdentroAlu_pub.Controls.Add(this.label8);
            this.tpdentroAlu_pub.Controls.Add(this.label137);
            this.tpdentroAlu_pub.Controls.Add(this.button55);
            this.tpdentroAlu_pub.Controls.Add(this.button56);
            this.tpdentroAlu_pub.Controls.Add(this.label4);
            this.tpdentroAlu_pub.Controls.Add(this.label5);
            this.tpdentroAlu_pub.Controls.Add(this.pictureBox13);
            this.tpdentroAlu_pub.Controls.Add(this.button53);
            this.tpdentroAlu_pub.Controls.Add(this.pictureBox14);
            this.tpdentroAlu_pub.Controls.Add(this.button54);
            this.tpdentroAlu_pub.Controls.Add(this.label32);
            this.tpdentroAlu_pub.Location = new System.Drawing.Point(4, 5);
            this.tpdentroAlu_pub.Name = "tpdentroAlu_pub";
            this.tpdentroAlu_pub.Padding = new System.Windows.Forms.Padding(3);
            this.tpdentroAlu_pub.Size = new System.Drawing.Size(1248, 509);
            this.tpdentroAlu_pub.TabIndex = 0;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::PPA.Properties.Resources.Desktop_10_;
            this.pictureBox17.Location = new System.Drawing.Point(1074, 95);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(74, 78);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 57;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::PPA.Properties.Resources.Desktop_29_;
            this.pictureBox15.Location = new System.Drawing.Point(816, 95);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(74, 78);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 56;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::PPA.Properties.Resources.search_red_icon;
            this.pictureBox16.Location = new System.Drawing.Point(558, 95);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(74, 78);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 55;
            this.pictureBox16.TabStop = false;
            // 
            // label138
            // 
            this.label138.BackColor = System.Drawing.Color.Transparent;
            this.label138.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label138.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label138.Location = new System.Drawing.Point(1033, 222);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(165, 46);
            this.label138.TabIndex = 54;
            this.label138.Text = "Faça Alter/Delete da Confirmação dos alunos!";
            // 
            // button57
            // 
            this.button57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button57.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button57.FlatAppearance.BorderSize = 0;
            this.button57.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button57.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button57.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button57.Location = new System.Drawing.Point(1033, 184);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(148, 27);
            this.button57.TabIndex = 53;
            this.button57.Text = "Aterar/Deletar Conf.";
            this.button57.UseVisualStyleBackColor = false;
            this.button57.Click += new System.EventHandler(this.button57_Click);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label8.Location = new System.Drawing.Point(777, 222);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 46);
            this.label8.TabIndex = 52;
            this.label8.Text = "Faça Alter/Delete da Matricula dos alunos!";
            // 
            // label137
            // 
            this.label137.BackColor = System.Drawing.Color.Transparent;
            this.label137.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label137.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label137.Location = new System.Drawing.Point(527, 222);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(152, 46);
            this.label137.TabIndex = 51;
            this.label137.Text = "Faça a pesquisa dos  Alunos!";
            // 
            // button55
            // 
            this.button55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button55.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button55.FlatAppearance.BorderSize = 0;
            this.button55.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button55.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button55.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button55.Location = new System.Drawing.Point(781, 184);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(148, 27);
            this.button55.TabIndex = 50;
            this.button55.Text = "Aterar/Deletar Matr.";
            this.button55.UseVisualStyleBackColor = false;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // button56
            // 
            this.button56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button56.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button56.FlatAppearance.BorderSize = 0;
            this.button56.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button56.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button56.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button56.Location = new System.Drawing.Point(523, 184);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(148, 27);
            this.button56.TabIndex = 49;
            this.button56.Text = "Seleccionar Aluno";
            this.button56.UseVisualStyleBackColor = false;
            this.button56.Click += new System.EventHandler(this.button56_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label4.Location = new System.Drawing.Point(281, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 46);
            this.label4.TabIndex = 48;
            this.label4.Text = "Faça a confirmação de alunos já cadastrados!";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label5.Location = new System.Drawing.Point(13, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 46);
            this.label5.TabIndex = 47;
            this.label5.Text = "Faça o cadastarmento de novos alunos!";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::PPA.Properties.Resources.cad;
            this.pictureBox13.Location = new System.Drawing.Point(308, 95);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(74, 78);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 46;
            this.pictureBox13.TabStop = false;
            // 
            // button53
            // 
            this.button53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button53.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button53.FlatAppearance.BorderSize = 0;
            this.button53.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button53.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button53.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button53.Location = new System.Drawing.Point(274, 184);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(148, 27);
            this.button53.TabIndex = 45;
            this.button53.Text = "Confirmar Aluno";
            this.button53.UseVisualStyleBackColor = false;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::PPA.Properties.Resources.images_9;
            this.pictureBox14.Location = new System.Drawing.Point(47, 95);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(74, 78);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 44;
            this.pictureBox14.TabStop = false;
            // 
            // button54
            // 
            this.button54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button54.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button54.FlatAppearance.BorderSize = 0;
            this.button54.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button54.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button54.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button54.Location = new System.Drawing.Point(13, 184);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(148, 27);
            this.button54.TabIndex = 43;
            this.button54.Text = "Cadastrar Aluno";
            this.button54.UseVisualStyleBackColor = false;
            this.button54.Click += new System.EventHandler(this.button54_Click);
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label32.Location = new System.Drawing.Point(0, 3);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(178, 54);
            this.label32.TabIndex = 28;
            this.label32.Text = "Aluno";
            // 
            // tpdentroAlu_CRUD
            // 
            this.tpdentroAlu_CRUD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tpdentroAlu_CRUD.Controls.Add(this.dentroAlu_CRUD);
            this.tpdentroAlu_CRUD.Location = new System.Drawing.Point(4, 5);
            this.tpdentroAlu_CRUD.Name = "tpdentroAlu_CRUD";
            this.tpdentroAlu_CRUD.Padding = new System.Windows.Forms.Padding(3);
            this.tpdentroAlu_CRUD.Size = new System.Drawing.Size(1248, 509);
            this.tpdentroAlu_CRUD.TabIndex = 1;
            // 
            // dentroAlu_CRUD
            // 
            this.dentroAlu_CRUD.Controls.Add(this.tp_Cadastrar);
            this.dentroAlu_CRUD.Controls.Add(this.tp_Confirmar);
            this.dentroAlu_CRUD.Controls.Add(this.tp_PesMat_Conf);
            this.dentroAlu_CRUD.Controls.Add(this.tp_Alt_det_Matr);
            this.dentroAlu_CRUD.Controls.Add(this.tp_Alt_det_Conf);
            this.dentroAlu_CRUD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dentroAlu_CRUD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dentroAlu_CRUD.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.dentroAlu_CRUD.ItemSize = new System.Drawing.Size(0, 30);
            this.dentroAlu_CRUD.Location = new System.Drawing.Point(3, 3);
            this.dentroAlu_CRUD.Name = "dentroAlu_CRUD";
            this.dentroAlu_CRUD.SelectedIndex = 0;
            this.dentroAlu_CRUD.Size = new System.Drawing.Size(1242, 503);
            this.dentroAlu_CRUD.TabIndex = 0;
            // 
            // tp_Cadastrar
            // 
            this.tp_Cadastrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tp_Cadastrar.Controls.Add(this.label48);
            this.tp_Cadastrar.Controls.Add(this.lpMatAno);
            this.tp_Cadastrar.Controls.Add(this.CbAn);
            this.tp_Cadastrar.Controls.Add(this.rb_nome2);
            this.tp_Cadastrar.Controls.Add(this.btnPesqCad);
            this.tp_Cadastrar.Controls.Add(this.tb_pesqCad);
            this.tp_Cadastrar.Controls.Add(this.lpMat);
            this.tp_Cadastrar.Controls.Add(this.dg);
            this.tp_Cadastrar.Controls.Add(this.chec_verMat);
            this.tp_Cadastrar.Controls.Add(this.dataHoje);
            this.tp_Cadastrar.Controls.Add(this.label50);
            this.tp_Cadastrar.Controls.Add(this.tb_An);
            this.tp_Cadastrar.Controls.Add(this.tbId);
            this.tp_Cadastrar.Controls.Add(this.tb_bi);
            this.tp_Cadastrar.Controls.Add(this.label33);
            this.tp_Cadastrar.Controls.Add(this.btn_cadMarti);
            this.tp_Cadastrar.Controls.Add(this.label34);
            this.tp_Cadastrar.Controls.Add(this.tb_genero);
            this.tp_Cadastrar.Controls.Add(this.tb_pai);
            this.tp_Cadastrar.Controls.Add(this.label35);
            this.tp_Cadastrar.Controls.Add(this.tb_mae);
            this.tp_Cadastrar.Controls.Add(this.label36);
            this.tp_Cadastrar.Controls.Add(this.label37);
            this.tp_Cadastrar.Controls.Add(this.tb_nome);
            this.tp_Cadastrar.Controls.Add(this.label38);
            this.tp_Cadastrar.Controls.Add(this.da);
            this.tp_Cadastrar.Controls.Add(this.tb_morada);
            this.tp_Cadastrar.Controls.Add(this.label40);
            this.tp_Cadastrar.Controls.Add(this.label39);
            this.tp_Cadastrar.Location = new System.Drawing.Point(4, 34);
            this.tp_Cadastrar.Name = "tp_Cadastrar";
            this.tp_Cadastrar.Padding = new System.Windows.Forms.Padding(3);
            this.tp_Cadastrar.Size = new System.Drawing.Size(1234, 465);
            this.tp_Cadastrar.TabIndex = 0;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label48.Location = new System.Drawing.Point(5, 16);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(178, 32);
            this.label48.TabIndex = 146;
            this.label48.Text = "Cadastrar Aluno";
            // 
            // lpMatAno
            // 
            this.lpMatAno.AutoSize = true;
            this.lpMatAno.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lpMatAno.ForeColor = System.Drawing.Color.Gray;
            this.lpMatAno.Location = new System.Drawing.Point(771, 244);
            this.lpMatAno.Name = "lpMatAno";
            this.lpMatAno.Size = new System.Drawing.Size(111, 20);
            this.lpMatAno.TabIndex = 142;
            this.lpMatAno.Text = "*Alunos do Ano :";
            this.lpMatAno.Visible = false;
            // 
            // CbAn
            // 
            this.CbAn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbAn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CbAn.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.CbAn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.CbAn.FormattingEnabled = true;
            this.CbAn.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014",
            "Todos Anos"});
            this.CbAn.Location = new System.Drawing.Point(895, 240);
            this.CbAn.Name = "CbAn";
            this.CbAn.Size = new System.Drawing.Size(146, 28);
            this.CbAn.TabIndex = 13;
            this.CbAn.Visible = false;
            this.CbAn.SelectedIndexChanged += new System.EventHandler(this.CbAn_SelectedIndexChanged_1);
            this.CbAn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CbAn_KeyDown);
            // 
            // rb_nome2
            // 
            this.rb_nome2.BaseColour = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.rb_nome2.BorderColour = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.rb_nome2.Checked = true;
            this.rb_nome2.CheckedColour = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            this.rb_nome2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rb_nome2.FontColour = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.rb_nome2.HighlightColour = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.rb_nome2.Location = new System.Drawing.Point(211, 246);
            this.rb_nome2.Name = "rb_nome2";
            this.rb_nome2.Size = new System.Drawing.Size(100, 22);
            this.rb_nome2.TabIndex = 140;
            this.rb_nome2.Text = "Por Nome";
            this.rb_nome2.Visible = false;
            // 
            // btnPesqCad
            // 
            this.btnPesqCad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesqCad.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnPesqCad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesqCad.Font = new System.Drawing.Font("Rod", 12F);
            this.btnPesqCad.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnPesqCad.Location = new System.Drawing.Point(181, 245);
            this.btnPesqCad.Name = "btnPesqCad";
            this.btnPesqCad.Size = new System.Drawing.Size(21, 23);
            this.btnPesqCad.TabIndex = 12;
            this.btnPesqCad.UseVisualStyleBackColor = true;
            this.btnPesqCad.Visible = false;
            this.btnPesqCad.Click += new System.EventHandler(this.btnPesqCad_Click_1);
            // 
            // tb_pesqCad
            // 
            this.tb_pesqCad.Font = new System.Drawing.Font("Rod", 12F);
            this.tb_pesqCad.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_pesqCad.Location = new System.Drawing.Point(4, 245);
            this.tb_pesqCad.Name = "tb_pesqCad";
            this.tb_pesqCad.Size = new System.Drawing.Size(174, 23);
            this.tb_pesqCad.TabIndex = 11;
            this.tb_pesqCad.Visible = false;
            this.tb_pesqCad.TextChanged += new System.EventHandler(this.tb_pesqCad_TextChanged);
            // 
            // lpMat
            // 
            this.lpMat.AutoSize = true;
            this.lpMat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lpMat.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lpMat.Location = new System.Drawing.Point(8, 220);
            this.lpMat.Name = "lpMat";
            this.lpMat.Size = new System.Drawing.Size(75, 20);
            this.lpMat.TabIndex = 137;
            this.lpMat.Text = "*Pesquisar";
            this.lpMat.Visible = false;
            // 
            // dg
            // 
            this.dg.AllowUserToAddRows = false;
            this.dg.AllowUserToDeleteRows = false;
            this.dg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dg.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dg.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dg.ColumnHeadersHeight = 35;
            this.dg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dg.Location = new System.Drawing.Point(5, 283);
            this.dg.Name = "dg";
            this.dg.ReadOnly = true;
            this.dg.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg.Size = new System.Drawing.Size(1201, 133);
            this.dg.TabIndex = 136;
            this.dg.Visible = false;
            // 
            // chec_verMat
            // 
            this.chec_verMat.BaseColour = System.Drawing.Color.White;
            this.chec_verMat.BorderColour = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.chec_verMat.Checked = false;
            this.chec_verMat.CheckedColour = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            this.chec_verMat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chec_verMat.FontColour = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.chec_verMat.Location = new System.Drawing.Point(942, 170);
            this.chec_verMat.Name = "chec_verMat";
            this.chec_verMat.Size = new System.Drawing.Size(100, 22);
            this.chec_verMat.TabIndex = 10;
            this.chec_verMat.Text = "Ver Matr.";
            this.chec_verMat.CheckedChanged += new Heyech_Application_Development.HeyechCheckBox.CheckedChangedEventHandler(this.chec_verMat_CheckedChanged);
            // 
            // dataHoje
            // 
            this.dataHoje.CalendarForeColor = System.Drawing.SystemColors.ControlDark;
            this.dataHoje.CalendarTitleBackColor = System.Drawing.SystemColors.ControlDark;
            this.dataHoje.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDark;
            this.dataHoje.CalendarTrailingForeColor = System.Drawing.Color.Silver;
            this.dataHoje.CustomFormat = "dd-MM-yyyy";
            this.dataHoje.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.dataHoje.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataHoje.Location = new System.Drawing.Point(896, 127);
            this.dataHoje.Name = "dataHoje";
            this.dataHoje.Size = new System.Drawing.Size(146, 25);
            this.dataHoje.TabIndex = 133;
            this.dataHoje.Visible = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label50.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label50.Location = new System.Drawing.Point(676, 141);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(101, 20);
            this.label50.TabIndex = 131;
            this.label50.Text = "*Ano Matricula";
            // 
            // tb_An
            // 
            this.tb_An.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tb_An.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tb_An.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_An.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_An.FormattingEnabled = true;
            this.tb_An.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014"});
            this.tb_An.Location = new System.Drawing.Point(675, 167);
            this.tb_An.Name = "tb_An";
            this.tb_An.Size = new System.Drawing.Size(174, 28);
            this.tb_An.TabIndex = 8;
            // 
            // tbId
            // 
            this.tbId.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbId.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbId.Location = new System.Drawing.Point(6, 97);
            this.tbId.Name = "tbId";
            this.tbId.ReadOnly = true;
            this.tbId.Size = new System.Drawing.Size(174, 25);
            this.tbId.TabIndex = 0;
            // 
            // tb_bi
            // 
            this.tb_bi.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_bi.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_bi.Location = new System.Drawing.Point(224, 96);
            this.tb_bi.Name = "tb_bi";
            this.tb_bi.Size = new System.Drawing.Size(174, 25);
            this.tb_bi.TabIndex = 1;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label33.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label33.Location = new System.Drawing.Point(214, 75);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(27, 20);
            this.label33.TabIndex = 92;
            this.label33.Text = "*BI";
            // 
            // btn_cadMarti
            // 
            this.btn_cadMarti.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_cadMarti.BackgroundImage = global::PPA.Properties.Resources.disk_blue_ok;
            this.btn_cadMarti.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cadMarti.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_cadMarti.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_cadMarti.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_cadMarti.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_cadMarti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cadMarti.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_cadMarti.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_cadMarti.Location = new System.Drawing.Point(896, 165);
            this.btn_cadMarti.Name = "btn_cadMarti";
            this.btn_cadMarti.Size = new System.Drawing.Size(30, 28);
            this.btn_cadMarti.TabIndex = 9;
            this.btn_cadMarti.UseVisualStyleBackColor = false;
            this.btn_cadMarti.Click += new System.EventHandler(this.btn_cadMarti_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label34.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label34.Location = new System.Drawing.Point(451, 141);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 20);
            this.label34.TabIndex = 90;
            this.label34.Text = "*Genero";
            // 
            // tb_genero
            // 
            this.tb_genero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tb_genero.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tb_genero.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_genero.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_genero.FormattingEnabled = true;
            this.tb_genero.Items.AddRange(new object[] {
            "Masculino",
            "Femenino"});
            this.tb_genero.Location = new System.Drawing.Point(452, 167);
            this.tb_genero.Name = "tb_genero";
            this.tb_genero.Size = new System.Drawing.Size(174, 28);
            this.tb_genero.TabIndex = 7;
            // 
            // tb_pai
            // 
            this.tb_pai.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_pai.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_pai.Location = new System.Drawing.Point(6, 168);
            this.tb_pai.Name = "tb_pai";
            this.tb_pai.Size = new System.Drawing.Size(174, 25);
            this.tb_pai.TabIndex = 5;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label35.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label35.Location = new System.Drawing.Point(0, 141);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(93, 20);
            this.label35.TabIndex = 58;
            this.label35.Text = "*Nome do Pai";
            // 
            // tb_mae
            // 
            this.tb_mae.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_mae.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_mae.Location = new System.Drawing.Point(224, 168);
            this.tb_mae.Name = "tb_mae";
            this.tb_mae.Size = new System.Drawing.Size(174, 25);
            this.tb_mae.TabIndex = 6;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label36.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label36.Location = new System.Drawing.Point(218, 141);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(101, 20);
            this.label36.TabIndex = 61;
            this.label36.Text = "*Nome da Mãe";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label37.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label37.Location = new System.Drawing.Point(2, 71);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(90, 20);
            this.label37.TabIndex = 39;
            this.label37.Text = "*ID Matricula";
            // 
            // tb_nome
            // 
            this.tb_nome.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_nome.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_nome.Location = new System.Drawing.Point(452, 97);
            this.tb_nome.Name = "tb_nome";
            this.tb_nome.Size = new System.Drawing.Size(174, 25);
            this.tb_nome.TabIndex = 2;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label38.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label38.Location = new System.Drawing.Point(446, 70);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(109, 20);
            this.label38.TabIndex = 40;
            this.label38.Text = "*Nome Completo";
            // 
            // da
            // 
            this.da.CalendarForeColor = System.Drawing.SystemColors.ControlDark;
            this.da.CalendarTitleBackColor = System.Drawing.SystemColors.ControlDark;
            this.da.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDark;
            this.da.CalendarTrailingForeColor = System.Drawing.Color.Silver;
            this.da.CustomFormat = "dd-MM-yyyy";
            this.da.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.da.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.da.Location = new System.Drawing.Point(896, 96);
            this.da.Name = "da";
            this.da.Size = new System.Drawing.Size(146, 25);
            this.da.TabIndex = 4;
            // 
            // tb_morada
            // 
            this.tb_morada.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_morada.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_morada.Location = new System.Drawing.Point(677, 97);
            this.tb_morada.Name = "tb_morada";
            this.tb_morada.Size = new System.Drawing.Size(174, 25);
            this.tb_morada.TabIndex = 3;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label40.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label40.Location = new System.Drawing.Point(888, 69);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(131, 20);
            this.label40.TabIndex = 55;
            this.label40.Text = "*Data de Nacimento";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label39.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label39.Location = new System.Drawing.Point(671, 70);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(64, 20);
            this.label39.TabIndex = 54;
            this.label39.Text = "*Morada";
            // 
            // tp_Confirmar
            // 
            this.tp_Confirmar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tp_Confirmar.Controls.Add(this.label41);
            this.tp_Confirmar.Controls.Add(this.chec_verConf);
            this.tp_Confirmar.Controls.Add(this.lpA);
            this.tp_Confirmar.Controls.Add(this.tb_AnC);
            this.tp_Confirmar.Controls.Add(this.btn_Confir);
            this.tp_Confirmar.Controls.Add(this.rb_nomeConf);
            this.tp_Confirmar.Controls.Add(this.btn_pesConf);
            this.tp_Confirmar.Controls.Add(this.tb_pesqConf);
            this.tp_Confirmar.Controls.Add(this.lp);
            this.tp_Confirmar.Controls.Add(this.dgConf);
            this.tp_Confirmar.Controls.Add(this.label140);
            this.tp_Confirmar.Controls.Add(this.tb_status);
            this.tp_Confirmar.Controls.Add(this.button22);
            this.tp_Confirmar.Controls.Add(this.tb_confMatri);
            this.tp_Confirmar.Controls.Add(this.label46);
            this.tp_Confirmar.Controls.Add(this.tb_anoConf);
            this.tp_Confirmar.Controls.Add(this.label139);
            this.tp_Confirmar.Controls.Add(this.label42);
            this.tp_Confirmar.Controls.Add(this.label47);
            this.tp_Confirmar.Controls.Add(this.tb_sala);
            this.tp_Confirmar.Controls.Add(this.label43);
            this.tp_Confirmar.Controls.Add(this.tb_classe);
            this.tp_Confirmar.Controls.Add(this.label45);
            this.tp_Confirmar.Controls.Add(this.tb_curso);
            this.tp_Confirmar.Controls.Add(this.tb_turno);
            this.tp_Confirmar.Location = new System.Drawing.Point(4, 34);
            this.tp_Confirmar.Name = "tp_Confirmar";
            this.tp_Confirmar.Padding = new System.Windows.Forms.Padding(3);
            this.tp_Confirmar.Size = new System.Drawing.Size(1234, 465);
            this.tp_Confirmar.TabIndex = 1;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label41.Location = new System.Drawing.Point(5, 16);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(181, 32);
            this.label41.TabIndex = 145;
            this.label41.Text = "Confirmar Aluno";
            // 
            // chec_verConf
            // 
            this.chec_verConf.BaseColour = System.Drawing.Color.White;
            this.chec_verConf.BorderColour = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.chec_verConf.Checked = false;
            this.chec_verConf.CheckedColour = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            this.chec_verConf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chec_verConf.FontColour = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.chec_verConf.Location = new System.Drawing.Point(513, 156);
            this.chec_verConf.Name = "chec_verConf";
            this.chec_verConf.Size = new System.Drawing.Size(100, 22);
            this.chec_verConf.TabIndex = 9;
            this.chec_verConf.Text = "Ver Conf.";
            this.chec_verConf.CheckedChanged += new Heyech_Application_Development.HeyechCheckBox.CheckedChangedEventHandler(this.chec_verConf_CheckedChanged);
            // 
            // lpA
            // 
            this.lpA.AutoSize = true;
            this.lpA.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lpA.ForeColor = System.Drawing.Color.Gray;
            this.lpA.Location = new System.Drawing.Point(775, 228);
            this.lpA.Name = "lpA";
            this.lpA.Size = new System.Drawing.Size(111, 20);
            this.lpA.TabIndex = 143;
            this.lpA.Text = "*Alunos do Ano :";
            this.lpA.Visible = false;
            // 
            // tb_AnC
            // 
            this.tb_AnC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tb_AnC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tb_AnC.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_AnC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.tb_AnC.FormattingEnabled = true;
            this.tb_AnC.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014",
            "Todos Anos"});
            this.tb_AnC.Location = new System.Drawing.Point(899, 224);
            this.tb_AnC.Name = "tb_AnC";
            this.tb_AnC.Size = new System.Drawing.Size(174, 28);
            this.tb_AnC.TabIndex = 12;
            this.tb_AnC.Visible = false;
            this.tb_AnC.SelectedIndexChanged += new System.EventHandler(this.tb_AnC_SelectedIndexChanged);
            this.tb_AnC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_AnC_KeyDown);
            // 
            // btn_Confir
            // 
            this.btn_Confir.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_Confir.BackgroundImage = global::PPA.Properties.Resources.disk_blue_ok;
            this.btn_Confir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Confir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Confir.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_Confir.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_Confir.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_Confir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Confir.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_Confir.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_Confir.Location = new System.Drawing.Point(465, 154);
            this.btn_Confir.Name = "btn_Confir";
            this.btn_Confir.Size = new System.Drawing.Size(30, 28);
            this.btn_Confir.TabIndex = 8;
            this.btn_Confir.UseVisualStyleBackColor = false;
            this.btn_Confir.Click += new System.EventHandler(this.btn_Confir_Click);
            // 
            // rb_nomeConf
            // 
            this.rb_nomeConf.BaseColour = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.rb_nomeConf.BorderColour = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.rb_nomeConf.Checked = true;
            this.rb_nomeConf.CheckedColour = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            this.rb_nomeConf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rb_nomeConf.FontColour = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.rb_nomeConf.HighlightColour = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.rb_nomeConf.Location = new System.Drawing.Point(214, 239);
            this.rb_nomeConf.Name = "rb_nomeConf";
            this.rb_nomeConf.Size = new System.Drawing.Size(100, 22);
            this.rb_nomeConf.TabIndex = 134;
            this.rb_nomeConf.Text = "Por Nome";
            this.rb_nomeConf.Visible = false;
            // 
            // btn_pesConf
            // 
            this.btn_pesConf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_pesConf.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btn_pesConf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesConf.Font = new System.Drawing.Font("Rod", 12F);
            this.btn_pesConf.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_pesConf.Location = new System.Drawing.Point(184, 238);
            this.btn_pesConf.Name = "btn_pesConf";
            this.btn_pesConf.Size = new System.Drawing.Size(21, 23);
            this.btn_pesConf.TabIndex = 11;
            this.btn_pesConf.UseVisualStyleBackColor = true;
            this.btn_pesConf.Visible = false;
            this.btn_pesConf.Click += new System.EventHandler(this.btn_pesConf_Click);
            // 
            // tb_pesqConf
            // 
            this.tb_pesqConf.Font = new System.Drawing.Font("Rod", 12F);
            this.tb_pesqConf.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_pesqConf.Location = new System.Drawing.Point(7, 238);
            this.tb_pesqConf.Name = "tb_pesqConf";
            this.tb_pesqConf.Size = new System.Drawing.Size(174, 23);
            this.tb_pesqConf.TabIndex = 10;
            this.tb_pesqConf.Visible = false;
            this.tb_pesqConf.TextChanged += new System.EventHandler(this.tb_pesqConf_TextChanged);
            // 
            // lp
            // 
            this.lp.AutoSize = true;
            this.lp.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lp.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lp.Location = new System.Drawing.Point(11, 213);
            this.lp.Name = "lp";
            this.lp.Size = new System.Drawing.Size(75, 20);
            this.lp.TabIndex = 131;
            this.lp.Text = "*Pesquisar";
            this.lp.Visible = false;
            // 
            // dgConf
            // 
            this.dgConf.AllowUserToAddRows = false;
            this.dgConf.AllowUserToDeleteRows = false;
            this.dgConf.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgConf.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgConf.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgConf.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgConf.ColumnHeadersHeight = 35;
            this.dgConf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgConf.Location = new System.Drawing.Point(8, 276);
            this.dgConf.Name = "dgConf";
            this.dgConf.ReadOnly = true;
            this.dgConf.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgConf.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgConf.Size = new System.Drawing.Size(1201, 130);
            this.dgConf.TabIndex = 130;
            this.dgConf.Visible = false;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label140.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label140.Location = new System.Drawing.Point(4, 133);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(57, 20);
            this.label140.TabIndex = 129;
            this.label140.Text = "*Estatus";
            // 
            // tb_status
            // 
            this.tb_status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tb_status.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tb_status.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_status.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_status.FormattingEnabled = true;
            this.tb_status.Items.AddRange(new object[] {
            "Repitente",
            "Não Repitente"});
            this.tb_status.Location = new System.Drawing.Point(8, 156);
            this.tb_status.Name = "tb_status";
            this.tb_status.Size = new System.Drawing.Size(174, 28);
            this.tb_status.TabIndex = 6;
            // 
            // button22
            // 
            this.button22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button22.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button22.Font = new System.Drawing.Font("Rod", 12F);
            this.button22.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button22.Location = new System.Drawing.Point(186, 97);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(21, 25);
            this.button22.TabIndex = 1;
            this.button22.UseVisualStyleBackColor = true;
            // 
            // tb_confMatri
            // 
            this.tb_confMatri.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_confMatri.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_confMatri.Location = new System.Drawing.Point(6, 97);
            this.tb_confMatri.Name = "tb_confMatri";
            this.tb_confMatri.Size = new System.Drawing.Size(174, 25);
            this.tb_confMatri.TabIndex = 0;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label46.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label46.Location = new System.Drawing.Point(244, 133);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(82, 20);
            this.label46.TabIndex = 71;
            this.label46.Text = "*Ano Leitivo";
            // 
            // tb_anoConf
            // 
            this.tb_anoConf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tb_anoConf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tb_anoConf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_anoConf.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_anoConf.FormattingEnabled = true;
            this.tb_anoConf.Location = new System.Drawing.Point(250, 155);
            this.tb_anoConf.Name = "tb_anoConf";
            this.tb_anoConf.Size = new System.Drawing.Size(174, 28);
            this.tb_anoConf.TabIndex = 7;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label139.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label139.Location = new System.Drawing.Point(2, 71);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(90, 20);
            this.label139.TabIndex = 126;
            this.label139.Text = "*ID Matricula";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label42.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label42.Location = new System.Drawing.Point(678, 70);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(49, 20);
            this.label42.TabIndex = 72;
            this.label42.Text = "*Turno";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label47.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label47.Location = new System.Drawing.Point(895, 70);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(42, 20);
            this.label47.TabIndex = 68;
            this.label47.Text = "*Sala";
            // 
            // tb_sala
            // 
            this.tb_sala.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tb_sala.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tb_sala.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_sala.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_sala.FormattingEnabled = true;
            this.tb_sala.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.tb_sala.Location = new System.Drawing.Point(899, 93);
            this.tb_sala.Name = "tb_sala";
            this.tb_sala.Size = new System.Drawing.Size(174, 28);
            this.tb_sala.TabIndex = 5;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label43.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label43.Location = new System.Drawing.Point(244, 70);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(48, 20);
            this.label43.TabIndex = 75;
            this.label43.Text = "*Curso";
            // 
            // tb_classe
            // 
            this.tb_classe.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_classe.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_classe.FormattingEnabled = true;
            this.tb_classe.Items.AddRange(new object[] {
            "10",
            "11",
            "12"});
            this.tb_classe.Location = new System.Drawing.Point(465, 94);
            this.tb_classe.Name = "tb_classe";
            this.tb_classe.Size = new System.Drawing.Size(174, 28);
            this.tb_classe.TabIndex = 3;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label45.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label45.Location = new System.Drawing.Point(460, 70);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(53, 20);
            this.label45.TabIndex = 63;
            this.label45.Text = "*Classe";
            // 
            // tb_curso
            // 
            this.tb_curso.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_curso.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_curso.FormattingEnabled = true;
            this.tb_curso.Items.AddRange(new object[] {
            "Técnico de Informática",
            "Desenhador Projectista",
            "Construção Civil",
            "Gestão Empresarial"});
            this.tb_curso.Location = new System.Drawing.Point(250, 94);
            this.tb_curso.Name = "tb_curso";
            this.tb_curso.Size = new System.Drawing.Size(174, 28);
            this.tb_curso.TabIndex = 2;
            // 
            // tb_turno
            // 
            this.tb_turno.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_turno.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_turno.FormattingEnabled = true;
            this.tb_turno.Items.AddRange(new object[] {
            "Tarde",
            "Manhã",
            "Noite"});
            this.tb_turno.Location = new System.Drawing.Point(685, 93);
            this.tb_turno.Name = "tb_turno";
            this.tb_turno.Size = new System.Drawing.Size(174, 28);
            this.tb_turno.TabIndex = 4;
            // 
            // tp_PesMat_Conf
            // 
            this.tp_PesMat_Conf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tp_PesMat_Conf.Controls.Add(this.label51);
            this.tp_PesMat_Conf.Controls.Add(this.label142);
            this.tp_PesMat_Conf.Controls.Add(this.Cb_AnoCompleto);
            this.tp_PesMat_Conf.Controls.Add(this.heyechRadioButton70);
            this.tp_PesMat_Conf.Controls.Add(this.btn_pesqCompleto);
            this.tp_PesMat_Conf.Controls.Add(this.tb_pesqCompleto);
            this.tp_PesMat_Conf.Controls.Add(this.label44);
            this.tp_PesMat_Conf.Controls.Add(this.dgCompleto);
            this.tp_PesMat_Conf.Location = new System.Drawing.Point(4, 34);
            this.tp_PesMat_Conf.Name = "tp_PesMat_Conf";
            this.tp_PesMat_Conf.Padding = new System.Windows.Forms.Padding(3);
            this.tp_PesMat_Conf.Size = new System.Drawing.Size(1234, 465);
            this.tp_PesMat_Conf.TabIndex = 2;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label51.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label51.Location = new System.Drawing.Point(5, 16);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(333, 32);
            this.label51.TabIndex = 146;
            this.label51.Text = "Pesquisa Completa Matr./Conf.";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label142.ForeColor = System.Drawing.Color.Gray;
            this.label142.Location = new System.Drawing.Point(362, 97);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(111, 20);
            this.label142.TabIndex = 145;
            this.label142.Text = "*Alunos do Ano :";
            // 
            // Cb_AnoCompleto
            // 
            this.Cb_AnoCompleto.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Cb_AnoCompleto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.Cb_AnoCompleto.FormattingEnabled = true;
            this.Cb_AnoCompleto.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014",
            "Todos Anos"});
            this.Cb_AnoCompleto.Location = new System.Drawing.Point(486, 93);
            this.Cb_AnoCompleto.Name = "Cb_AnoCompleto";
            this.Cb_AnoCompleto.Size = new System.Drawing.Size(174, 28);
            this.Cb_AnoCompleto.TabIndex = 2;
            this.Cb_AnoCompleto.Text = "Todos Anos";
            this.Cb_AnoCompleto.SelectedIndexChanged += new System.EventHandler(this.Cb_AnoCompleto_SelectedIndexChanged);
            this.Cb_AnoCompleto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cb_AnoCompleto_KeyDown);
            // 
            // heyechRadioButton70
            // 
            this.heyechRadioButton70.BaseColour = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.heyechRadioButton70.BorderColour = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.heyechRadioButton70.Checked = true;
            this.heyechRadioButton70.CheckedColour = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            this.heyechRadioButton70.Cursor = System.Windows.Forms.Cursors.Hand;
            this.heyechRadioButton70.FontColour = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.heyechRadioButton70.HighlightColour = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.heyechRadioButton70.Location = new System.Drawing.Point(223, 98);
            this.heyechRadioButton70.Name = "heyechRadioButton70";
            this.heyechRadioButton70.Size = new System.Drawing.Size(100, 22);
            this.heyechRadioButton70.TabIndex = 128;
            this.heyechRadioButton70.Text = "Por Nome";
            // 
            // btn_pesqCompleto
            // 
            this.btn_pesqCompleto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_pesqCompleto.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btn_pesqCompleto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesqCompleto.Font = new System.Drawing.Font("Rod", 12F);
            this.btn_pesqCompleto.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_pesqCompleto.Location = new System.Drawing.Point(185, 97);
            this.btn_pesqCompleto.Name = "btn_pesqCompleto";
            this.btn_pesqCompleto.Size = new System.Drawing.Size(21, 23);
            this.btn_pesqCompleto.TabIndex = 1;
            this.btn_pesqCompleto.UseVisualStyleBackColor = true;
            this.btn_pesqCompleto.Click += new System.EventHandler(this.btn_pesqComleto_Click);
            // 
            // tb_pesqCompleto
            // 
            this.tb_pesqCompleto.Font = new System.Drawing.Font("Rod", 12F);
            this.tb_pesqCompleto.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_pesqCompleto.Location = new System.Drawing.Point(6, 97);
            this.tb_pesqCompleto.Name = "tb_pesqCompleto";
            this.tb_pesqCompleto.Size = new System.Drawing.Size(174, 23);
            this.tb_pesqCompleto.TabIndex = 0;
            this.tb_pesqCompleto.TextChanged += new System.EventHandler(this.tb_pesqCompleto_TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label44.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label44.Location = new System.Drawing.Point(0, 72);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(75, 20);
            this.label44.TabIndex = 125;
            this.label44.Text = "*Pesquisar";
            // 
            // dgCompleto
            // 
            this.dgCompleto.AllowUserToAddRows = false;
            this.dgCompleto.AllowUserToDeleteRows = false;
            this.dgCompleto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgCompleto.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgCompleto.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgCompleto.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgCompleto.ColumnHeadersHeight = 35;
            this.dgCompleto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgCompleto.Location = new System.Drawing.Point(9, 150);
            this.dgCompleto.Name = "dgCompleto";
            this.dgCompleto.ReadOnly = true;
            this.dgCompleto.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgCompleto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCompleto.Size = new System.Drawing.Size(1201, 241);
            this.dgCompleto.TabIndex = 124;
            // 
            // tp_Alt_det_Matr
            // 
            this.tp_Alt_det_Matr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tp_Alt_det_Matr.Controls.Add(this.label166);
            this.tp_Alt_det_Matr.Controls.Add(this.label165);
            this.tp_Alt_det_Matr.Controls.Add(this.label164);
            this.tp_Alt_det_Matr.Controls.Add(this.label163);
            this.tp_Alt_det_Matr.Controls.Add(this.label162);
            this.tp_Alt_det_Matr.Controls.Add(this.label161);
            this.tp_Alt_det_Matr.Controls.Add(this.label160);
            this.tp_Alt_det_Matr.Controls.Add(this.label159);
            this.tp_Alt_det_Matr.Controls.Add(this.label158);
            this.tp_Alt_det_Matr.Controls.Add(this.btnDeletarMAt);
            this.tp_Alt_det_Matr.Controls.Add(this.btnAtualizarMAt);
            this.tp_Alt_det_Matr.Controls.Add(this.l9);
            this.tp_Alt_det_Matr.Controls.Add(this.l8);
            this.tp_Alt_det_Matr.Controls.Add(this.l7);
            this.tp_Alt_det_Matr.Controls.Add(this.l6);
            this.tp_Alt_det_Matr.Controls.Add(this.l5);
            this.tp_Alt_det_Matr.Controls.Add(this.l4);
            this.tp_Alt_det_Matr.Controls.Add(this.l3);
            this.tp_Alt_det_Matr.Controls.Add(this.l2);
            this.tp_Alt_det_Matr.Controls.Add(this.l1);
            this.tp_Alt_det_Matr.Controls.Add(this.label64);
            this.tp_Alt_det_Matr.Controls.Add(this.cbAninho_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.btnPesq_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.tbPesq_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.label63);
            this.tp_Alt_det_Matr.Controls.Add(this.label54);
            this.tp_Alt_det_Matr.Controls.Add(this.tbAno_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.tbId_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.tbBi_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.label55);
            this.tp_Alt_det_Matr.Controls.Add(this.label56);
            this.tp_Alt_det_Matr.Controls.Add(this.tbGenero_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.tbPai_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.label57);
            this.tp_Alt_det_Matr.Controls.Add(this.tbMae_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.label58);
            this.tp_Alt_det_Matr.Controls.Add(this.label59);
            this.tp_Alt_det_Matr.Controls.Add(this.tbNome_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.label60);
            this.tp_Alt_det_Matr.Controls.Add(this.da_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.tbMorada_alt_del_Mat);
            this.tp_Alt_det_Matr.Controls.Add(this.label61);
            this.tp_Alt_det_Matr.Controls.Add(this.label62);
            this.tp_Alt_det_Matr.Controls.Add(this.label52);
            this.tp_Alt_det_Matr.Location = new System.Drawing.Point(4, 34);
            this.tp_Alt_det_Matr.Name = "tp_Alt_det_Matr";
            this.tp_Alt_det_Matr.Padding = new System.Windows.Forms.Padding(3);
            this.tp_Alt_det_Matr.Size = new System.Drawing.Size(1234, 465);
            this.tp_Alt_det_Matr.TabIndex = 3;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label166.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label166.Location = new System.Drawing.Point(1133, 267);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(39, 20);
            this.label166.TabIndex = 205;
            this.label166.Text = "*Ano";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label165.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label165.Location = new System.Drawing.Point(920, 267);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(42, 20);
            this.label165.TabIndex = 204;
            this.label165.Text = "*Mãe";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label164.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label164.Location = new System.Drawing.Point(709, 267);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(35, 20);
            this.label164.TabIndex = 203;
            this.label164.Text = "*Pai";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label163.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label163.Location = new System.Drawing.Point(600, 267);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(44, 20);
            this.label163.TabIndex = 202;
            this.label163.Text = "*Data";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label162.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label162.Location = new System.Drawing.Point(460, 267);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(64, 20);
            this.label162.TabIndex = 201;
            this.label162.Text = "*Morada";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label161.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label161.Location = new System.Drawing.Point(378, 267);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(59, 20);
            this.label161.TabIndex = 200;
            this.label161.Text = "*Genero";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label160.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label160.Location = new System.Drawing.Point(166, 267);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(50, 20);
            this.label160.TabIndex = 199;
            this.label160.Text = "*Nome";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label159.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label159.Location = new System.Drawing.Point(62, 267);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(27, 20);
            this.label159.TabIndex = 198;
            this.label159.Text = "*BI";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label158.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label158.Location = new System.Drawing.Point(-1, 267);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(67, 20);
            this.label158.TabIndex = 197;
            this.label158.Text = "*Processo";
            // 
            // btnDeletarMAt
            // 
            this.btnDeletarMAt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeletarMAt.BackgroundImage = global::PPA.Properties.Resources.save_delete;
            this.btnDeletarMAt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDeletarMAt.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeletarMAt.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeletarMAt.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnDeletarMAt.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnDeletarMAt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeletarMAt.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnDeletarMAt.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnDeletarMAt.Location = new System.Drawing.Point(949, 168);
            this.btnDeletarMAt.Name = "btnDeletarMAt";
            this.btnDeletarMAt.Size = new System.Drawing.Size(30, 28);
            this.btnDeletarMAt.TabIndex = 10;
            this.btnDeletarMAt.UseVisualStyleBackColor = false;
            this.btnDeletarMAt.Click += new System.EventHandler(this.button58_Click);
            // 
            // btnAtualizarMAt
            // 
            this.btnAtualizarMAt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAtualizarMAt.BackgroundImage = global::PPA.Properties.Resources.save_41_;
            this.btnAtualizarMAt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAtualizarMAt.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAtualizarMAt.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAtualizarMAt.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAtualizarMAt.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnAtualizarMAt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtualizarMAt.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnAtualizarMAt.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnAtualizarMAt.Location = new System.Drawing.Point(896, 168);
            this.btnAtualizarMAt.Name = "btnAtualizarMAt";
            this.btnAtualizarMAt.Size = new System.Drawing.Size(30, 28);
            this.btnAtualizarMAt.TabIndex = 9;
            this.btnAtualizarMAt.UseVisualStyleBackColor = false;
            this.btnAtualizarMAt.Click += new System.EventHandler(this.btnAtualizarMAt_Click);
            // 
            // l9
            // 
            this.l9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.l9.Font = new System.Drawing.Font("Utsaah", 12F);
            this.l9.ForeColor = System.Drawing.Color.Black;
            this.l9.FormattingEnabled = true;
            this.l9.ItemHeight = 18;
            this.l9.Location = new System.Drawing.Point(1133, 289);
            this.l9.Name = "l9";
            this.l9.Size = new System.Drawing.Size(58, 126);
            this.l9.TabIndex = 185;
            this.l9.SelectedIndexChanged += new System.EventHandler(this.l3_SelectedIndexChanged);
            // 
            // l8
            // 
            this.l8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.l8.Font = new System.Drawing.Font("Utsaah", 12F);
            this.l8.ForeColor = System.Drawing.Color.Black;
            this.l8.FormattingEnabled = true;
            this.l8.HorizontalScrollbar = true;
            this.l8.ItemHeight = 18;
            this.l8.Location = new System.Drawing.Point(923, 289);
            this.l8.Name = "l8";
            this.l8.Size = new System.Drawing.Size(209, 126);
            this.l8.TabIndex = 184;
            this.l8.SelectedIndexChanged += new System.EventHandler(this.l3_SelectedIndexChanged);
            // 
            // l7
            // 
            this.l7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.l7.Font = new System.Drawing.Font("Utsaah", 12F);
            this.l7.ForeColor = System.Drawing.Color.Black;
            this.l7.FormattingEnabled = true;
            this.l7.HorizontalScrollbar = true;
            this.l7.ItemHeight = 18;
            this.l7.Location = new System.Drawing.Point(713, 289);
            this.l7.Name = "l7";
            this.l7.Size = new System.Drawing.Size(209, 126);
            this.l7.TabIndex = 183;
            this.l7.SelectedIndexChanged += new System.EventHandler(this.l3_SelectedIndexChanged);
            // 
            // l6
            // 
            this.l6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.l6.Font = new System.Drawing.Font("Utsaah", 12F);
            this.l6.ForeColor = System.Drawing.Color.Black;
            this.l6.FormattingEnabled = true;
            this.l6.ItemHeight = 18;
            this.l6.Location = new System.Drawing.Point(606, 289);
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(106, 126);
            this.l6.TabIndex = 182;
            this.l6.SelectedIndexChanged += new System.EventHandler(this.l3_SelectedIndexChanged);
            // 
            // l5
            // 
            this.l5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.l5.Font = new System.Drawing.Font("Utsaah", 12F);
            this.l5.ForeColor = System.Drawing.Color.Black;
            this.l5.FormattingEnabled = true;
            this.l5.HorizontalScrollbar = true;
            this.l5.ItemHeight = 18;
            this.l5.Location = new System.Drawing.Point(466, 289);
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(139, 126);
            this.l5.TabIndex = 181;
            this.l5.SelectedIndexChanged += new System.EventHandler(this.l3_SelectedIndexChanged);
            // 
            // l4
            // 
            this.l4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.l4.Font = new System.Drawing.Font("Utsaah", 12F);
            this.l4.ForeColor = System.Drawing.Color.Black;
            this.l4.FormattingEnabled = true;
            this.l4.ItemHeight = 18;
            this.l4.Location = new System.Drawing.Point(382, 289);
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(83, 126);
            this.l4.TabIndex = 180;
            this.l4.SelectedIndexChanged += new System.EventHandler(this.l3_SelectedIndexChanged);
            // 
            // l3
            // 
            this.l3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.l3.Font = new System.Drawing.Font("Utsaah", 12F);
            this.l3.ForeColor = System.Drawing.Color.Black;
            this.l3.FormattingEnabled = true;
            this.l3.HorizontalScrollbar = true;
            this.l3.ItemHeight = 18;
            this.l3.Location = new System.Drawing.Point(172, 289);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(209, 126);
            this.l3.TabIndex = 179;
            this.l3.SelectedIndexChanged += new System.EventHandler(this.l3_SelectedIndexChanged);
            // 
            // l2
            // 
            this.l2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.l2.Font = new System.Drawing.Font("Utsaah", 12F);
            this.l2.ForeColor = System.Drawing.Color.Black;
            this.l2.FormattingEnabled = true;
            this.l2.ItemHeight = 18;
            this.l2.Location = new System.Drawing.Point(69, 289);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(102, 126);
            this.l2.TabIndex = 178;
            this.l2.SelectedIndexChanged += new System.EventHandler(this.l3_SelectedIndexChanged);
            // 
            // l1
            // 
            this.l1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.l1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.l1.Font = new System.Drawing.Font("Utsaah", 12F);
            this.l1.ForeColor = System.Drawing.Color.Black;
            this.l1.FormattingEnabled = true;
            this.l1.ItemHeight = 18;
            this.l1.Location = new System.Drawing.Point(5, 290);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(63, 126);
            this.l1.TabIndex = 177;
            this.l1.SelectedIndexChanged += new System.EventHandler(this.l3_SelectedIndexChanged);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label64.ForeColor = System.Drawing.Color.Gray;
            this.label64.Location = new System.Drawing.Point(772, 235);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(111, 20);
            this.label64.TabIndex = 175;
            this.label64.Text = "*Alunos do Ano :";
            // 
            // cbAninho_alt_del_Mat
            // 
            this.cbAninho_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.cbAninho_alt_del_Mat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.cbAninho_alt_del_Mat.FormattingEnabled = true;
            this.cbAninho_alt_del_Mat.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014",
            "Todos Anos"});
            this.cbAninho_alt_del_Mat.Location = new System.Drawing.Point(896, 231);
            this.cbAninho_alt_del_Mat.Name = "cbAninho_alt_del_Mat";
            this.cbAninho_alt_del_Mat.Size = new System.Drawing.Size(146, 28);
            this.cbAninho_alt_del_Mat.TabIndex = 13;
            this.cbAninho_alt_del_Mat.Text = "Todos Anos";
            this.cbAninho_alt_del_Mat.SelectedIndexChanged += new System.EventHandler(this.cbAninho_alt_del_Mat_SelectedIndexChanged);
            this.cbAninho_alt_del_Mat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbAninho_alt_del_Mat_KeyDown);
            // 
            // btnPesq_alt_del_Mat
            // 
            this.btnPesq_alt_del_Mat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesq_alt_del_Mat.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnPesq_alt_del_Mat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesq_alt_del_Mat.Font = new System.Drawing.Font("Rod", 12F);
            this.btnPesq_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnPesq_alt_del_Mat.Location = new System.Drawing.Point(183, 234);
            this.btnPesq_alt_del_Mat.Name = "btnPesq_alt_del_Mat";
            this.btnPesq_alt_del_Mat.Size = new System.Drawing.Size(21, 23);
            this.btnPesq_alt_del_Mat.TabIndex = 12;
            this.btnPesq_alt_del_Mat.UseVisualStyleBackColor = true;
            this.btnPesq_alt_del_Mat.Click += new System.EventHandler(this.btnPesq_alt_del_Mat_Click);
            // 
            // tbPesq_alt_del_Mat
            // 
            this.tbPesq_alt_del_Mat.Font = new System.Drawing.Font("Rod", 12F);
            this.tbPesq_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tbPesq_alt_del_Mat.Location = new System.Drawing.Point(6, 234);
            this.tbPesq_alt_del_Mat.Name = "tbPesq_alt_del_Mat";
            this.tbPesq_alt_del_Mat.Size = new System.Drawing.Size(174, 23);
            this.tbPesq_alt_del_Mat.TabIndex = 11;
            this.tbPesq_alt_del_Mat.TextChanged += new System.EventHandler(this.tbPesq_alt_del_Mat_TextChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label63.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label63.Location = new System.Drawing.Point(0, 209);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(75, 20);
            this.label63.TabIndex = 167;
            this.label63.Text = "*Pesquisar";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label54.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label54.Location = new System.Drawing.Point(676, 141);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(101, 20);
            this.label54.TabIndex = 165;
            this.label54.Text = "*Ano Matricula";
            // 
            // tbAno_alt_del_Mat
            // 
            this.tbAno_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbAno_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbAno_alt_del_Mat.FormattingEnabled = true;
            this.tbAno_alt_del_Mat.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014"});
            this.tbAno_alt_del_Mat.Location = new System.Drawing.Point(675, 167);
            this.tbAno_alt_del_Mat.Name = "tbAno_alt_del_Mat";
            this.tbAno_alt_del_Mat.Size = new System.Drawing.Size(174, 28);
            this.tbAno_alt_del_Mat.TabIndex = 8;
            this.tbAno_alt_del_Mat.Text = "2016";
            // 
            // tbId_alt_del_Mat
            // 
            this.tbId_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbId_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbId_alt_del_Mat.Location = new System.Drawing.Point(6, 97);
            this.tbId_alt_del_Mat.Name = "tbId_alt_del_Mat";
            this.tbId_alt_del_Mat.ReadOnly = true;
            this.tbId_alt_del_Mat.Size = new System.Drawing.Size(174, 25);
            this.tbId_alt_del_Mat.TabIndex = 0;
            this.tbId_alt_del_Mat.TextChanged += new System.EventHandler(this.tbId_alt_del_Mat_TextChanged);
            // 
            // tbBi_alt_del_Mat
            // 
            this.tbBi_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbBi_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbBi_alt_del_Mat.Location = new System.Drawing.Point(224, 96);
            this.tbBi_alt_del_Mat.Name = "tbBi_alt_del_Mat";
            this.tbBi_alt_del_Mat.Size = new System.Drawing.Size(174, 25);
            this.tbBi_alt_del_Mat.TabIndex = 1;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label55.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label55.Location = new System.Drawing.Point(214, 75);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(27, 20);
            this.label55.TabIndex = 161;
            this.label55.Text = "*BI";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label56.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label56.Location = new System.Drawing.Point(451, 141);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(59, 20);
            this.label56.TabIndex = 160;
            this.label56.Text = "*Genero";
            // 
            // tbGenero_alt_del_Mat
            // 
            this.tbGenero_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbGenero_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbGenero_alt_del_Mat.FormattingEnabled = true;
            this.tbGenero_alt_del_Mat.Items.AddRange(new object[] {
            "Masculino",
            "Femenino"});
            this.tbGenero_alt_del_Mat.Location = new System.Drawing.Point(452, 167);
            this.tbGenero_alt_del_Mat.Name = "tbGenero_alt_del_Mat";
            this.tbGenero_alt_del_Mat.Size = new System.Drawing.Size(174, 28);
            this.tbGenero_alt_del_Mat.TabIndex = 7;
            this.tbGenero_alt_del_Mat.Text = "Masculino";
            // 
            // tbPai_alt_del_Mat
            // 
            this.tbPai_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbPai_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbPai_alt_del_Mat.Location = new System.Drawing.Point(6, 168);
            this.tbPai_alt_del_Mat.Name = "tbPai_alt_del_Mat";
            this.tbPai_alt_del_Mat.Size = new System.Drawing.Size(174, 25);
            this.tbPai_alt_del_Mat.TabIndex = 5;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label57.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label57.Location = new System.Drawing.Point(0, 141);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(93, 20);
            this.label57.TabIndex = 155;
            this.label57.Text = "*Nome do Pai";
            // 
            // tbMae_alt_del_Mat
            // 
            this.tbMae_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbMae_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbMae_alt_del_Mat.Location = new System.Drawing.Point(224, 168);
            this.tbMae_alt_del_Mat.Name = "tbMae_alt_del_Mat";
            this.tbMae_alt_del_Mat.Size = new System.Drawing.Size(174, 25);
            this.tbMae_alt_del_Mat.TabIndex = 6;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label58.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label58.Location = new System.Drawing.Point(218, 141);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(101, 20);
            this.label58.TabIndex = 158;
            this.label58.Text = "*Nome da Mãe";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label59.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label59.Location = new System.Drawing.Point(2, 71);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(90, 20);
            this.label59.TabIndex = 148;
            this.label59.Text = "*ID Matricula";
            // 
            // tbNome_alt_del_Mat
            // 
            this.tbNome_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbNome_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbNome_alt_del_Mat.Location = new System.Drawing.Point(452, 97);
            this.tbNome_alt_del_Mat.Name = "tbNome_alt_del_Mat";
            this.tbNome_alt_del_Mat.Size = new System.Drawing.Size(174, 25);
            this.tbNome_alt_del_Mat.TabIndex = 2;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label60.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label60.Location = new System.Drawing.Point(446, 70);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(109, 20);
            this.label60.TabIndex = 149;
            this.label60.Text = "*Nome Completo";
            // 
            // da_alt_del_Mat
            // 
            this.da_alt_del_Mat.CalendarForeColor = System.Drawing.SystemColors.ControlDark;
            this.da_alt_del_Mat.CalendarTitleBackColor = System.Drawing.SystemColors.ControlDark;
            this.da_alt_del_Mat.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDark;
            this.da_alt_del_Mat.CalendarTrailingForeColor = System.Drawing.Color.Silver;
            this.da_alt_del_Mat.CustomFormat = "dd-MM-yyyy";
            this.da_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.da_alt_del_Mat.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.da_alt_del_Mat.Location = new System.Drawing.Point(896, 96);
            this.da_alt_del_Mat.Name = "da_alt_del_Mat";
            this.da_alt_del_Mat.Size = new System.Drawing.Size(146, 25);
            this.da_alt_del_Mat.TabIndex = 4;
            // 
            // tbMorada_alt_del_Mat
            // 
            this.tbMorada_alt_del_Mat.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbMorada_alt_del_Mat.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbMorada_alt_del_Mat.Location = new System.Drawing.Point(677, 97);
            this.tbMorada_alt_del_Mat.Name = "tbMorada_alt_del_Mat";
            this.tbMorada_alt_del_Mat.Size = new System.Drawing.Size(174, 25);
            this.tbMorada_alt_del_Mat.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label61.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label61.Location = new System.Drawing.Point(888, 69);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(131, 20);
            this.label61.TabIndex = 154;
            this.label61.Text = "*Data de Nacimento";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label62.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label62.Location = new System.Drawing.Point(671, 70);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(64, 20);
            this.label62.TabIndex = 153;
            this.label62.Text = "*Morada";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label52.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label52.Location = new System.Drawing.Point(5, 16);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(281, 32);
            this.label52.TabIndex = 147;
            this.label52.Text = "Alterar/Deletar Matriculas";
            // 
            // tp_Alt_det_Conf
            // 
            this.tp_Alt_det_Conf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.tp_Alt_det_Conf.Controls.Add(this.tbnActualiza_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.btnDeletar__alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.label157);
            this.tp_Alt_det_Conf.Controls.Add(this.label156);
            this.tp_Alt_det_Conf.Controls.Add(this.label155);
            this.tp_Alt_det_Conf.Controls.Add(this.label154);
            this.tp_Alt_det_Conf.Controls.Add(this.label153);
            this.tp_Alt_det_Conf.Controls.Add(this.label152);
            this.tp_Alt_det_Conf.Controls.Add(this.label151);
            this.tp_Alt_det_Conf.Controls.Add(this.label150);
            this.tp_Alt_det_Conf.Controls.Add(this.label149);
            this.tp_Alt_det_Conf.Controls.Add(this.label148);
            this.tp_Alt_det_Conf.Controls.Add(this.lb10);
            this.tp_Alt_det_Conf.Controls.Add(this.lb9);
            this.tp_Alt_det_Conf.Controls.Add(this.lb8);
            this.tp_Alt_det_Conf.Controls.Add(this.lb7);
            this.tp_Alt_det_Conf.Controls.Add(this.lb6);
            this.tp_Alt_det_Conf.Controls.Add(this.lb5);
            this.tp_Alt_det_Conf.Controls.Add(this.lb4);
            this.tp_Alt_det_Conf.Controls.Add(this.lb3);
            this.tp_Alt_det_Conf.Controls.Add(this.lb2);
            this.tp_Alt_det_Conf.Controls.Add(this.lb1);
            this.tp_Alt_det_Conf.Controls.Add(this.label146);
            this.tp_Alt_det_Conf.Controls.Add(this.cbAno_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.btnPesq__alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.tbPesq_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.label147);
            this.tp_Alt_det_Conf.Controls.Add(this.label65);
            this.tp_Alt_det_Conf.Controls.Add(this.tbStatus_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.button13);
            this.tp_Alt_det_Conf.Controls.Add(this.tbId_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.label66);
            this.tp_Alt_det_Conf.Controls.Add(this.tbAno_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.label67);
            this.tp_Alt_det_Conf.Controls.Add(this.label141);
            this.tp_Alt_det_Conf.Controls.Add(this.label143);
            this.tp_Alt_det_Conf.Controls.Add(this.tbSala_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.label144);
            this.tp_Alt_det_Conf.Controls.Add(this.tbClasse_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.label145);
            this.tp_Alt_det_Conf.Controls.Add(this.tbCurso_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.tbTurno_alt_del_Conf);
            this.tp_Alt_det_Conf.Controls.Add(this.label53);
            this.tp_Alt_det_Conf.Location = new System.Drawing.Point(4, 34);
            this.tp_Alt_det_Conf.Name = "tp_Alt_det_Conf";
            this.tp_Alt_det_Conf.Padding = new System.Windows.Forms.Padding(3);
            this.tp_Alt_det_Conf.Size = new System.Drawing.Size(1234, 465);
            this.tp_Alt_det_Conf.TabIndex = 4;
            this.tp_Alt_det_Conf.Click += new System.EventHandler(this.tp_Alt_det_Conf_Click);
            // 
            // tbnActualiza_alt_del_Conf
            // 
            this.tbnActualiza_alt_del_Conf.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbnActualiza_alt_del_Conf.BackgroundImage = global::PPA.Properties.Resources.save_41_;
            this.tbnActualiza_alt_del_Conf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbnActualiza_alt_del_Conf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tbnActualiza_alt_del_Conf.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbnActualiza_alt_del_Conf.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.tbnActualiza_alt_del_Conf.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.tbnActualiza_alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tbnActualiza_alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbnActualiza_alt_del_Conf.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tbnActualiza_alt_del_Conf.Location = new System.Drawing.Point(466, 156);
            this.tbnActualiza_alt_del_Conf.Name = "tbnActualiza_alt_del_Conf";
            this.tbnActualiza_alt_del_Conf.Size = new System.Drawing.Size(30, 28);
            this.tbnActualiza_alt_del_Conf.TabIndex = 207;
            this.tbnActualiza_alt_del_Conf.UseVisualStyleBackColor = false;
            this.tbnActualiza_alt_del_Conf.Click += new System.EventHandler(this.tbnActualiza_alt_del_Conf_Click);
            // 
            // btnDeletar__alt_del_Conf
            // 
            this.btnDeletar__alt_del_Conf.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeletar__alt_del_Conf.BackgroundImage = global::PPA.Properties.Resources.save_delete;
            this.btnDeletar__alt_del_Conf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDeletar__alt_del_Conf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeletar__alt_del_Conf.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeletar__alt_del_Conf.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnDeletar__alt_del_Conf.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnDeletar__alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeletar__alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnDeletar__alt_del_Conf.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnDeletar__alt_del_Conf.Location = new System.Drawing.Point(502, 156);
            this.btnDeletar__alt_del_Conf.Name = "btnDeletar__alt_del_Conf";
            this.btnDeletar__alt_del_Conf.Size = new System.Drawing.Size(30, 28);
            this.btnDeletar__alt_del_Conf.TabIndex = 206;
            this.btnDeletar__alt_del_Conf.UseVisualStyleBackColor = false;
            this.btnDeletar__alt_del_Conf.Click += new System.EventHandler(this.btnDeletar__alt_del_Conf_Click);
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label157.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label157.Location = new System.Drawing.Point(890, 265);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(74, 20);
            this.label157.TabIndex = 205;
            this.label157.Text = "*Ano Conf.";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label156.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label156.Location = new System.Drawing.Point(831, 265);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(66, 20);
            this.label156.TabIndex = 204;
            this.label156.Text = "*Data Co.";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label155.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label155.Location = new System.Drawing.Point(729, 265);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(51, 20);
            this.label155.TabIndex = 203;
            this.label155.Text = "*Status";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label154.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label154.Location = new System.Drawing.Point(643, 265);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(49, 20);
            this.label154.TabIndex = 202;
            this.label154.Text = "*Turno";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label153.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label153.Location = new System.Drawing.Point(583, 265);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(42, 20);
            this.label153.TabIndex = 201;
            this.label153.Text = "*Sala";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label152.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label152.Location = new System.Drawing.Point(524, 265);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(53, 20);
            this.label152.TabIndex = 200;
            this.label152.Text = "*Classe";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label151.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label151.Location = new System.Drawing.Point(318, 265);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(48, 20);
            this.label151.TabIndex = 199;
            this.label151.Text = "*Curso";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label150.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label150.Location = new System.Drawing.Point(122, 267);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(50, 20);
            this.label150.TabIndex = 198;
            this.label150.Text = "*Nome";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label149.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label149.Location = new System.Drawing.Point(59, 267);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(67, 20);
            this.label149.TabIndex = 197;
            this.label149.Text = "*Processo";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label148.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label148.Location = new System.Drawing.Point(3, 267);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(28, 20);
            this.label148.TabIndex = 196;
            this.label148.Text = "*ID";
            // 
            // lb10
            // 
            this.lb10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb10.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb10.ForeColor = System.Drawing.Color.Black;
            this.lb10.FormattingEnabled = true;
            this.lb10.HorizontalScrollbar = true;
            this.lb10.ItemHeight = 18;
            this.lb10.Location = new System.Drawing.Point(897, 288);
            this.lb10.Name = "lb10";
            this.lb10.Size = new System.Drawing.Size(75, 126);
            this.lb10.TabIndex = 195;
            this.lb10.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // lb9
            // 
            this.lb9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb9.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb9.ForeColor = System.Drawing.Color.Black;
            this.lb9.FormattingEnabled = true;
            this.lb9.ItemHeight = 18;
            this.lb9.Location = new System.Drawing.Point(839, 288);
            this.lb9.Name = "lb9";
            this.lb9.Size = new System.Drawing.Size(57, 126);
            this.lb9.TabIndex = 194;
            this.lb9.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // lb8
            // 
            this.lb8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb8.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb8.ForeColor = System.Drawing.Color.Black;
            this.lb8.FormattingEnabled = true;
            this.lb8.HorizontalScrollbar = true;
            this.lb8.ItemHeight = 18;
            this.lb8.Location = new System.Drawing.Point(737, 288);
            this.lb8.Name = "lb8";
            this.lb8.Size = new System.Drawing.Size(101, 126);
            this.lb8.TabIndex = 193;
            this.lb8.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // lb7
            // 
            this.lb7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb7.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb7.ForeColor = System.Drawing.Color.Black;
            this.lb7.FormattingEnabled = true;
            this.lb7.HorizontalScrollbar = true;
            this.lb7.ItemHeight = 18;
            this.lb7.Location = new System.Drawing.Point(650, 288);
            this.lb7.Name = "lb7";
            this.lb7.Size = new System.Drawing.Size(86, 126);
            this.lb7.TabIndex = 192;
            this.lb7.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // lb6
            // 
            this.lb6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb6.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb6.ForeColor = System.Drawing.Color.Black;
            this.lb6.FormattingEnabled = true;
            this.lb6.ItemHeight = 18;
            this.lb6.Location = new System.Drawing.Point(590, 288);
            this.lb6.Name = "lb6";
            this.lb6.Size = new System.Drawing.Size(59, 126);
            this.lb6.TabIndex = 191;
            this.lb6.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // lb5
            // 
            this.lb5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb5.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb5.ForeColor = System.Drawing.Color.Black;
            this.lb5.FormattingEnabled = true;
            this.lb5.HorizontalScrollbar = true;
            this.lb5.ItemHeight = 18;
            this.lb5.Location = new System.Drawing.Point(530, 288);
            this.lb5.Name = "lb5";
            this.lb5.Size = new System.Drawing.Size(59, 126);
            this.lb5.TabIndex = 190;
            this.lb5.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // lb4
            // 
            this.lb4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb4.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb4.ForeColor = System.Drawing.Color.Black;
            this.lb4.FormattingEnabled = true;
            this.lb4.HorizontalScrollbar = true;
            this.lb4.ItemHeight = 18;
            this.lb4.Location = new System.Drawing.Point(325, 288);
            this.lb4.Name = "lb4";
            this.lb4.Size = new System.Drawing.Size(204, 126);
            this.lb4.TabIndex = 189;
            this.lb4.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // lb3
            // 
            this.lb3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb3.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb3.ForeColor = System.Drawing.Color.Black;
            this.lb3.FormattingEnabled = true;
            this.lb3.HorizontalScrollbar = true;
            this.lb3.ItemHeight = 18;
            this.lb3.Location = new System.Drawing.Point(127, 288);
            this.lb3.Name = "lb3";
            this.lb3.Size = new System.Drawing.Size(197, 126);
            this.lb3.TabIndex = 188;
            this.lb3.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // lb2
            // 
            this.lb2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb2.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb2.ForeColor = System.Drawing.Color.Black;
            this.lb2.FormattingEnabled = true;
            this.lb2.HorizontalScrollbar = true;
            this.lb2.ItemHeight = 18;
            this.lb2.Location = new System.Drawing.Point(67, 288);
            this.lb2.Name = "lb2";
            this.lb2.Size = new System.Drawing.Size(59, 126);
            this.lb2.TabIndex = 187;
            this.lb2.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // lb1
            // 
            this.lb1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lb1.Font = new System.Drawing.Font("Utsaah", 12F);
            this.lb1.ForeColor = System.Drawing.Color.Black;
            this.lb1.FormattingEnabled = true;
            this.lb1.HorizontalScrollbar = true;
            this.lb1.ItemHeight = 18;
            this.lb1.Location = new System.Drawing.Point(7, 288);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(59, 126);
            this.lb1.TabIndex = 186;
            this.lb1.SelectedIndexChanged += new System.EventHandler(this.lb1_SelectedIndexChanged);
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label146.ForeColor = System.Drawing.Color.Gray;
            this.label146.Location = new System.Drawing.Point(775, 233);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(111, 20);
            this.label146.TabIndex = 184;
            this.label146.Text = "*Alunos do Ano :";
            // 
            // cbAno_alt_del_Conf
            // 
            this.cbAno_alt_del_Conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAno_alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbAno_alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.cbAno_alt_del_Conf.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.cbAno_alt_del_Conf.FormattingEnabled = true;
            this.cbAno_alt_del_Conf.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014",
            "Todos Anos"});
            this.cbAno_alt_del_Conf.Location = new System.Drawing.Point(899, 229);
            this.cbAno_alt_del_Conf.Name = "cbAno_alt_del_Conf";
            this.cbAno_alt_del_Conf.Size = new System.Drawing.Size(146, 28);
            this.cbAno_alt_del_Conf.TabIndex = 8;
            this.cbAno_alt_del_Conf.SelectedIndexChanged += new System.EventHandler(this.cbAno_alt_del_Conf_SelectedIndexChanged);
            this.cbAno_alt_del_Conf.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbAno_alt_del_Conf_KeyDown);
            // 
            // btnPesq__alt_del_Conf
            // 
            this.btnPesq__alt_del_Conf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPesq__alt_del_Conf.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnPesq__alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesq__alt_del_Conf.Font = new System.Drawing.Font("Rod", 12F);
            this.btnPesq__alt_del_Conf.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnPesq__alt_del_Conf.Location = new System.Drawing.Point(186, 232);
            this.btnPesq__alt_del_Conf.Name = "btnPesq__alt_del_Conf";
            this.btnPesq__alt_del_Conf.Size = new System.Drawing.Size(21, 23);
            this.btnPesq__alt_del_Conf.TabIndex = 9;
            this.btnPesq__alt_del_Conf.UseVisualStyleBackColor = true;
            this.btnPesq__alt_del_Conf.Click += new System.EventHandler(this.btnPesq__alt_del_Conf_Click);
            // 
            // tbPesq_alt_del_Conf
            // 
            this.tbPesq_alt_del_Conf.Font = new System.Drawing.Font("Rod", 12F);
            this.tbPesq_alt_del_Conf.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tbPesq_alt_del_Conf.Location = new System.Drawing.Point(9, 232);
            this.tbPesq_alt_del_Conf.Name = "tbPesq_alt_del_Conf";
            this.tbPesq_alt_del_Conf.Size = new System.Drawing.Size(174, 23);
            this.tbPesq_alt_del_Conf.TabIndex = 10;
            this.tbPesq_alt_del_Conf.TextChanged += new System.EventHandler(this.tbPesq_alt_del_Conf_TextChanged);
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label147.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label147.Location = new System.Drawing.Point(3, 207);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(75, 20);
            this.label147.TabIndex = 176;
            this.label147.Text = "*Pesquisar";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label65.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label65.Location = new System.Drawing.Point(5, 133);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(57, 20);
            this.label65.TabIndex = 163;
            this.label65.Text = "*Estatus";
            // 
            // tbStatus_alt_del_Conf
            // 
            this.tbStatus_alt_del_Conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbStatus_alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tbStatus_alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbStatus_alt_del_Conf.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbStatus_alt_del_Conf.FormattingEnabled = true;
            this.tbStatus_alt_del_Conf.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.tbStatus_alt_del_Conf.Location = new System.Drawing.Point(9, 156);
            this.tbStatus_alt_del_Conf.Name = "tbStatus_alt_del_Conf";
            this.tbStatus_alt_del_Conf.Size = new System.Drawing.Size(174, 28);
            this.tbStatus_alt_del_Conf.TabIndex = 5;
            // 
            // button13
            // 
            this.button13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button13.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Rod", 12F);
            this.button13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button13.Location = new System.Drawing.Point(187, 97);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(21, 25);
            this.button13.TabIndex = 159;
            this.button13.UseVisualStyleBackColor = true;
            // 
            // tbId_alt_del_Conf
            // 
            this.tbId_alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbId_alt_del_Conf.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbId_alt_del_Conf.Location = new System.Drawing.Point(7, 97);
            this.tbId_alt_del_Conf.Name = "tbId_alt_del_Conf";
            this.tbId_alt_del_Conf.Size = new System.Drawing.Size(174, 25);
            this.tbId_alt_del_Conf.TabIndex = 0;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label66.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label66.Location = new System.Drawing.Point(245, 133);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(82, 20);
            this.label66.TabIndex = 156;
            this.label66.Text = "*Ano Leitivo";
            // 
            // tbAno_alt_del_Conf
            // 
            this.tbAno_alt_del_Conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbAno_alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tbAno_alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbAno_alt_del_Conf.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbAno_alt_del_Conf.FormattingEnabled = true;
            this.tbAno_alt_del_Conf.Location = new System.Drawing.Point(251, 155);
            this.tbAno_alt_del_Conf.Name = "tbAno_alt_del_Conf";
            this.tbAno_alt_del_Conf.Size = new System.Drawing.Size(174, 28);
            this.tbAno_alt_del_Conf.TabIndex = 6;
            this.tbAno_alt_del_Conf.SelectedIndexChanged += new System.EventHandler(this.tbAno_alt_del_Conf_SelectedIndexChanged);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label67.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label67.Location = new System.Drawing.Point(3, 71);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(90, 20);
            this.label67.TabIndex = 160;
            this.label67.Text = "*ID Matricula";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label141.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label141.Location = new System.Drawing.Point(679, 70);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(49, 20);
            this.label141.TabIndex = 157;
            this.label141.Text = "*Turno";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label143.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label143.Location = new System.Drawing.Point(896, 70);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(42, 20);
            this.label143.TabIndex = 155;
            this.label143.Text = "*Sala";
            // 
            // tbSala_alt_del_Conf
            // 
            this.tbSala_alt_del_Conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbSala_alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tbSala_alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbSala_alt_del_Conf.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbSala_alt_del_Conf.FormattingEnabled = true;
            this.tbSala_alt_del_Conf.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.tbSala_alt_del_Conf.Location = new System.Drawing.Point(900, 93);
            this.tbSala_alt_del_Conf.Name = "tbSala_alt_del_Conf";
            this.tbSala_alt_del_Conf.Size = new System.Drawing.Size(174, 28);
            this.tbSala_alt_del_Conf.TabIndex = 4;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label144.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label144.Location = new System.Drawing.Point(245, 70);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(48, 20);
            this.label144.TabIndex = 158;
            this.label144.Text = "*Curso";
            // 
            // tbClasse_alt_del_Conf
            // 
            this.tbClasse_alt_del_Conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbClasse_alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tbClasse_alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbClasse_alt_del_Conf.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbClasse_alt_del_Conf.FormattingEnabled = true;
            this.tbClasse_alt_del_Conf.Items.AddRange(new object[] {
            "10",
            "11",
            "12"});
            this.tbClasse_alt_del_Conf.Location = new System.Drawing.Point(466, 94);
            this.tbClasse_alt_del_Conf.Name = "tbClasse_alt_del_Conf";
            this.tbClasse_alt_del_Conf.Size = new System.Drawing.Size(174, 28);
            this.tbClasse_alt_del_Conf.TabIndex = 2;
            this.tbClasse_alt_del_Conf.SelectedIndexChanged += new System.EventHandler(this.tbClasse_alt_del_Conf_SelectedIndexChanged);
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label145.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label145.Location = new System.Drawing.Point(461, 70);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(53, 20);
            this.label145.TabIndex = 154;
            this.label145.Text = "*Classe";
            // 
            // tbCurso_alt_del_Conf
            // 
            this.tbCurso_alt_del_Conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbCurso_alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tbCurso_alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbCurso_alt_del_Conf.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbCurso_alt_del_Conf.FormattingEnabled = true;
            this.tbCurso_alt_del_Conf.Items.AddRange(new object[] {
            "Técnico de Informática",
            "Desenhador Projectista",
            "Construção Civil",
            "Gestão Empresarial"});
            this.tbCurso_alt_del_Conf.Location = new System.Drawing.Point(251, 94);
            this.tbCurso_alt_del_Conf.Name = "tbCurso_alt_del_Conf";
            this.tbCurso_alt_del_Conf.Size = new System.Drawing.Size(174, 28);
            this.tbCurso_alt_del_Conf.TabIndex = 1;
            this.tbCurso_alt_del_Conf.SelectedIndexChanged += new System.EventHandler(this.tbCurso_alt_del_Conf_SelectedIndexChanged);
            // 
            // tbTurno_alt_del_Conf
            // 
            this.tbTurno_alt_del_Conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tbTurno_alt_del_Conf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tbTurno_alt_del_Conf.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tbTurno_alt_del_Conf.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tbTurno_alt_del_Conf.FormattingEnabled = true;
            this.tbTurno_alt_del_Conf.Items.AddRange(new object[] {
            "Tarde",
            "Manhã",
            "Noite"});
            this.tbTurno_alt_del_Conf.Location = new System.Drawing.Point(686, 93);
            this.tbTurno_alt_del_Conf.Name = "tbTurno_alt_del_Conf";
            this.tbTurno_alt_del_Conf.Size = new System.Drawing.Size(174, 28);
            this.tbTurno_alt_del_Conf.TabIndex = 3;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label53.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label53.Location = new System.Drawing.Point(5, 16);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(311, 32);
            this.label53.TabIndex = 148;
            this.label53.Text = "Alterar/Deletar Confirmações";
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(35, 518);
            this.panel1.TabIndex = 28;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1294, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(35, 518);
            this.panel2.TabIndex = 29;
            // 
            // tpagePROPRINA
            // 
            this.tpagePROPRINA.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tpagePROPRINA.Controls.Add(this.panel14);
            this.tpagePROPRINA.Controls.Add(this.TbPropinaDen);
            this.tpagePROPRINA.Controls.Add(this.panel4);
            this.tpagePROPRINA.Controls.Add(this.panel3);
            this.tpagePROPRINA.Location = new System.Drawing.Point(4, 5);
            this.tpagePROPRINA.Name = "tpagePROPRINA";
            this.tpagePROPRINA.Padding = new System.Windows.Forms.Padding(3);
            this.tpagePROPRINA.Size = new System.Drawing.Size(1332, 524);
            this.tpagePROPRINA.TabIndex = 2;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.panel14.Controls.Add(this.button34);
            this.panel14.Controls.Add(this.button37);
            this.panel14.Controls.Add(this.button39);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel14.Location = new System.Drawing.Point(38, 477);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1256, 44);
            this.panel14.TabIndex = 32;
            // 
            // button34
            // 
            this.button34.AutoSize = true;
            this.button34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button34.FlatAppearance.BorderSize = 0;
            this.button34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button34.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button34.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button34.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button34.Location = new System.Drawing.Point(1122, 7);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(59, 30);
            this.button34.TabIndex = 63;
            this.button34.Text = " (0_0)";
            this.button34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button34.UseVisualStyleBackColor = false;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button37
            // 
            this.button37.AutoSize = true;
            this.button37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button37.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button37.FlatAppearance.BorderSize = 0;
            this.button37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button37.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button37.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button37.Image = global::PPA.Properties.Resources.k4_fw;
            this.button37.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button37.Location = new System.Drawing.Point(16, 7);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(148, 30);
            this.button37.TabIndex = 58;
            this.button37.Text = "      Actualizar CRUD";
            this.button37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button37.UseVisualStyleBackColor = false;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button39
            // 
            this.button39.AutoSize = true;
            this.button39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button39.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button39.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button39.FlatAppearance.BorderSize = 0;
            this.button39.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button39.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button39.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button39.Image = global::PPA.Properties.Resources.h5_fw;
            this.button39.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button39.Location = new System.Drawing.Point(266, 7);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(157, 30);
            this.button39.TabIndex = 59;
            this.button39.Text = "      Actualizar Propina";
            this.button39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button39.UseVisualStyleBackColor = false;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // TbPropinaDen
            // 
            this.TbPropinaDen.Controls.Add(this.TbPropinaDen_pub);
            this.TbPropinaDen.Controls.Add(this.TbPropinaDen_crud);
            this.TbPropinaDen.Controls.Add(this.TbPropinaDen_propina);
            this.TbPropinaDen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbPropinaDen.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.TbPropinaDen.ItemSize = new System.Drawing.Size(0, 30);
            this.TbPropinaDen.Location = new System.Drawing.Point(38, 3);
            this.TbPropinaDen.Name = "TbPropinaDen";
            this.TbPropinaDen.SelectedIndex = 0;
            this.TbPropinaDen.Size = new System.Drawing.Size(1256, 518);
            this.TbPropinaDen.TabIndex = 30;
            // 
            // TbPropinaDen_pub
            // 
            this.TbPropinaDen_pub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TbPropinaDen_pub.Controls.Add(this.label76);
            this.TbPropinaDen_pub.Controls.Add(this.label77);
            this.TbPropinaDen_pub.Controls.Add(this.pictureBox26);
            this.TbPropinaDen_pub.Controls.Add(this.button26);
            this.TbPropinaDen_pub.Controls.Add(this.pictureBox27);
            this.TbPropinaDen_pub.Controls.Add(this.button27);
            this.TbPropinaDen_pub.Controls.Add(this.label68);
            this.TbPropinaDen_pub.Location = new System.Drawing.Point(4, 34);
            this.TbPropinaDen_pub.Name = "TbPropinaDen_pub";
            this.TbPropinaDen_pub.Padding = new System.Windows.Forms.Padding(3);
            this.TbPropinaDen_pub.Size = new System.Drawing.Size(1248, 480);
            this.TbPropinaDen_pub.TabIndex = 0;
            // 
            // label76
            // 
            this.label76.BackColor = System.Drawing.Color.Transparent;
            this.label76.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label76.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label76.Location = new System.Drawing.Point(284, 215);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(152, 46);
            this.label76.TabIndex = 83;
            this.label76.Text = "Faça a Actualização de Propina do aluno!";
            // 
            // label77
            // 
            this.label77.BackColor = System.Drawing.Color.Transparent;
            this.label77.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label77.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label77.Location = new System.Drawing.Point(9, 215);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(162, 46);
            this.label77.TabIndex = 82;
            this.label77.Text = "Faça a CRUD de Propina do aluno!";
            // 
            // pictureBox26
            // 
            this.pictureBox26.Image = global::PPA.Properties.Resources.lpo;
            this.pictureBox26.Location = new System.Drawing.Point(321, 95);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(74, 78);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox26.TabIndex = 81;
            this.pictureBox26.TabStop = false;
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button26.FlatAppearance.BorderSize = 0;
            this.button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button26.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button26.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button26.Location = new System.Drawing.Point(284, 184);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(148, 27);
            this.button26.TabIndex = 80;
            this.button26.Text = "Actualizar Propina";
            this.button26.UseVisualStyleBackColor = false;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // pictureBox27
            // 
            this.pictureBox27.Image = global::PPA.Properties.Resources.images_9;
            this.pictureBox27.Location = new System.Drawing.Point(48, 95);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(74, 78);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox27.TabIndex = 79;
            this.pictureBox27.TabStop = false;
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button27.FlatAppearance.BorderSize = 0;
            this.button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button27.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button27.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button27.Location = new System.Drawing.Point(14, 184);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(148, 27);
            this.button27.TabIndex = 78;
            this.button27.Text = "Actualizar CRUD";
            this.button27.UseVisualStyleBackColor = false;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // label68
            // 
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label68.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label68.Location = new System.Drawing.Point(0, 3);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(178, 54);
            this.label68.TabIndex = 28;
            this.label68.Text = "Propina";
            // 
            // TbPropinaDen_crud
            // 
            this.TbPropinaDen_crud.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TbPropinaDen_crud.Controls.Add(this.btn_CadImprimir);
            this.TbPropinaDen_crud.Controls.Add(this.datadoPAG);
            this.TbPropinaDen_crud.Controls.Add(this.btnCadPAg);
            this.TbPropinaDen_crud.Controls.Add(this.precott);
            this.TbPropinaDen_crud.Controls.Add(this.label114);
            this.TbPropinaDen_crud.Controls.Add(this.LvalorDesc);
            this.TbPropinaDen_crud.Controls.Add(this.panel5);
            this.TbPropinaDen_crud.Controls.Add(this.tb_mesdesc);
            this.TbPropinaDen_crud.Controls.Add(this.comboBox1);
            this.TbPropinaDen_crud.Controls.Add(this.tb_numMes);
            this.TbPropinaDen_crud.Controls.Add(this.label75);
            this.TbPropinaDen_crud.Controls.Add(this.label111);
            this.TbPropinaDen_crud.Controls.Add(this.c7);
            this.TbPropinaDen_crud.Controls.Add(this.label110);
            this.TbPropinaDen_crud.Controls.Add(this.label94);
            this.TbPropinaDen_crud.Controls.Add(this.c);
            this.TbPropinaDen_crud.Controls.Add(this.label95);
            this.TbPropinaDen_crud.Controls.Add(this.label96);
            this.TbPropinaDen_crud.Controls.Add(this.label97);
            this.TbPropinaDen_crud.Controls.Add(this.label98);
            this.TbPropinaDen_crud.Controls.Add(this.label99);
            this.TbPropinaDen_crud.Controls.Add(this.label100);
            this.TbPropinaDen_crud.Controls.Add(this.c6);
            this.TbPropinaDen_crud.Controls.Add(this.c5);
            this.TbPropinaDen_crud.Controls.Add(this.c4);
            this.TbPropinaDen_crud.Controls.Add(this.c3);
            this.TbPropinaDen_crud.Controls.Add(this.c2);
            this.TbPropinaDen_crud.Controls.Add(this.c1);
            this.TbPropinaDen_crud.Controls.Add(this.button12);
            this.TbPropinaDen_crud.Controls.Add(this.prop_nome);
            this.TbPropinaDen_crud.Controls.Add(this.tb_pesqProp);
            this.TbPropinaDen_crud.Controls.Add(this.label93);
            this.TbPropinaDen_crud.Controls.Add(this.label74);
            this.TbPropinaDen_crud.Controls.Add(this.label101);
            this.TbPropinaDen_crud.Controls.Add(this.label198);
            this.TbPropinaDen_crud.Location = new System.Drawing.Point(4, 34);
            this.TbPropinaDen_crud.Name = "TbPropinaDen_crud";
            this.TbPropinaDen_crud.Padding = new System.Windows.Forms.Padding(3);
            this.TbPropinaDen_crud.Size = new System.Drawing.Size(1248, 480);
            this.TbPropinaDen_crud.TabIndex = 2;
            this.TbPropinaDen_crud.Click += new System.EventHandler(this.TbPropinaDen_CRUD_Click);
            // 
            // btn_CadImprimir
            // 
            this.btn_CadImprimir.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_CadImprimir.BackgroundImage = global::PPA.Properties.Resources.document_print;
            this.btn_CadImprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_CadImprimir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_CadImprimir.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_CadImprimir.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_CadImprimir.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_CadImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_CadImprimir.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_CadImprimir.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_CadImprimir.Location = new System.Drawing.Point(396, 339);
            this.btn_CadImprimir.Name = "btn_CadImprimir";
            this.btn_CadImprimir.Size = new System.Drawing.Size(30, 28);
            this.btn_CadImprimir.TabIndex = 253;
            this.btn_CadImprimir.UseVisualStyleBackColor = false;
            this.btn_CadImprimir.Click += new System.EventHandler(this.btn_CadImprimir_Click);
            // 
            // datadoPAG
            // 
            this.datadoPAG.CalendarForeColor = System.Drawing.SystemColors.ControlDark;
            this.datadoPAG.CalendarTitleBackColor = System.Drawing.SystemColors.ControlDark;
            this.datadoPAG.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDark;
            this.datadoPAG.CalendarTrailingForeColor = System.Drawing.Color.Silver;
            this.datadoPAG.CustomFormat = "dd-MM-yyyy";
            this.datadoPAG.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.datadoPAG.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datadoPAG.Location = new System.Drawing.Point(12, 289);
            this.datadoPAG.Name = "datadoPAG";
            this.datadoPAG.Size = new System.Drawing.Size(155, 25);
            this.datadoPAG.TabIndex = 252;
            this.datadoPAG.Visible = false;
            // 
            // btnCadPAg
            // 
            this.btnCadPAg.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCadPAg.BackgroundImage = global::PPA.Properties.Resources.disk_blue_ok;
            this.btnCadPAg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCadPAg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCadPAg.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCadPAg.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCadPAg.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnCadPAg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadPAg.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnCadPAg.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnCadPAg.Location = new System.Drawing.Point(290, 342);
            this.btnCadPAg.Name = "btnCadPAg";
            this.btnCadPAg.Size = new System.Drawing.Size(30, 28);
            this.btnCadPAg.TabIndex = 251;
            this.btnCadPAg.UseVisualStyleBackColor = false;
            this.btnCadPAg.Click += new System.EventHandler(this.button14_Click);
            // 
            // precott
            // 
            this.precott.BackColor = System.Drawing.Color.Transparent;
            this.precott.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.precott.ForeColor = System.Drawing.Color.Purple;
            this.precott.Location = new System.Drawing.Point(18, 405);
            this.precott.Name = "precott";
            this.precott.Size = new System.Drawing.Size(199, 20);
            this.precott.TabIndex = 250;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label114.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label114.Location = new System.Drawing.Point(8, 382);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(146, 20);
            this.label114.TabIndex = 249;
            this.label114.Text = "* Total a Pagar em Kz:";
            // 
            // LvalorDesc
            // 
            this.LvalorDesc.Controls.Add(this.txtdatOBS);
            this.LvalorDesc.Controls.Add(this.txtOBSmes);
            this.LvalorDesc.Controls.Add(this.Lmeses);
            this.LvalorDesc.Controls.Add(this.Classepreco);
            this.LvalorDesc.Controls.Add(this.label112);
            this.LvalorDesc.Controls.Add(this.panel17);
            this.LvalorDesc.Controls.Add(this.Lsaldo);
            this.LvalorDesc.Controls.Add(this.Lano);
            this.LvalorDesc.Controls.Add(this.Lsala);
            this.LvalorDesc.Controls.Add(this.Lturno);
            this.LvalorDesc.Controls.Add(this.Lclasse);
            this.LvalorDesc.Controls.Add(this.Lcurso);
            this.LvalorDesc.Controls.Add(this.Lnome);
            this.LvalorDesc.Controls.Add(this.Lid);
            this.LvalorDesc.Controls.Add(this.label109);
            this.LvalorDesc.Controls.Add(this.label108);
            this.LvalorDesc.Controls.Add(this.label107);
            this.LvalorDesc.Controls.Add(this.label106);
            this.LvalorDesc.Controls.Add(this.label105);
            this.LvalorDesc.Controls.Add(this.label104);
            this.LvalorDesc.Controls.Add(this.label103);
            this.LvalorDesc.Controls.Add(this.label102);
            this.LvalorDesc.Controls.Add(this.btn_cadPAg);
            this.LvalorDesc.Controls.Add(this.panel16);
            this.LvalorDesc.Dock = System.Windows.Forms.DockStyle.Right;
            this.LvalorDesc.Location = new System.Drawing.Point(707, 3);
            this.LvalorDesc.Name = "LvalorDesc";
            this.LvalorDesc.Size = new System.Drawing.Size(538, 474);
            this.LvalorDesc.TabIndex = 248;
            this.LvalorDesc.Paint += new System.Windows.Forms.PaintEventHandler(this.panel6_Paint);
            // 
            // txtdatOBS
            // 
            this.txtdatOBS.AutoSize = true;
            this.txtdatOBS.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.txtdatOBS.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.txtdatOBS.Location = new System.Drawing.Point(42, 46);
            this.txtdatOBS.Name = "txtdatOBS";
            this.txtdatOBS.Size = new System.Drawing.Size(15, 20);
            this.txtdatOBS.TabIndex = 254;
            this.txtdatOBS.Text = "*";
            this.txtdatOBS.Visible = false;
            // 
            // txtOBSmes
            // 
            this.txtOBSmes.AutoSize = true;
            this.txtOBSmes.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.txtOBSmes.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.txtOBSmes.Location = new System.Drawing.Point(36, 46);
            this.txtOBSmes.Name = "txtOBSmes";
            this.txtOBSmes.Size = new System.Drawing.Size(0, 20);
            this.txtOBSmes.TabIndex = 253;
            // 
            // Lmeses
            // 
            this.Lmeses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Lmeses.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Lmeses.ForeColor = System.Drawing.Color.Purple;
            this.Lmeses.FormattingEnabled = true;
            this.Lmeses.ItemHeight = 20;
            this.Lmeses.Location = new System.Drawing.Point(28, 222);
            this.Lmeses.MultiColumn = true;
            this.Lmeses.Name = "Lmeses";
            this.Lmeses.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Lmeses.ScrollAlwaysVisible = true;
            this.Lmeses.Size = new System.Drawing.Size(482, 40);
            this.Lmeses.TabIndex = 252;
            // 
            // Classepreco
            // 
            this.Classepreco.AutoSize = true;
            this.Classepreco.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Classepreco.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Classepreco.Location = new System.Drawing.Point(343, 10);
            this.Classepreco.Name = "Classepreco";
            this.Classepreco.Size = new System.Drawing.Size(19, 20);
            this.Classepreco.TabIndex = 251;
            this.Classepreco.Text = "* ";
            this.Classepreco.Visible = false;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label112.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label112.Location = new System.Drawing.Point(24, 199);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(96, 20);
            this.label112.TabIndex = 249;
            this.label112.Text = "* Meses Pago:";
            this.label112.Click += new System.EventHandler(this.label112_Click);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel17.Location = new System.Drawing.Point(26, 269);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(491, 6);
            this.panel17.TabIndex = 187;
            // 
            // Lsaldo
            // 
            this.Lsaldo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lsaldo.BackColor = System.Drawing.Color.Transparent;
            this.Lsaldo.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Lsaldo.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Lsaldo.Location = new System.Drawing.Point(82, 177);
            this.Lsaldo.Name = "Lsaldo";
            this.Lsaldo.Size = new System.Drawing.Size(161, 20);
            this.Lsaldo.TabIndex = 186;
            // 
            // Lano
            // 
            this.Lano.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lano.BackColor = System.Drawing.Color.Transparent;
            this.Lano.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Lano.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Lano.Location = new System.Drawing.Point(473, 146);
            this.Lano.Name = "Lano";
            this.Lano.Size = new System.Drawing.Size(54, 20);
            this.Lano.TabIndex = 185;
            // 
            // Lsala
            // 
            this.Lsala.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lsala.BackColor = System.Drawing.Color.Transparent;
            this.Lsala.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Lsala.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Lsala.Location = new System.Drawing.Point(366, 148);
            this.Lsala.Name = "Lsala";
            this.Lsala.Size = new System.Drawing.Size(54, 20);
            this.Lsala.TabIndex = 184;
            // 
            // Lturno
            // 
            this.Lturno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lturno.BackColor = System.Drawing.Color.Transparent;
            this.Lturno.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Lturno.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Lturno.Location = new System.Drawing.Point(209, 148);
            this.Lturno.Name = "Lturno";
            this.Lturno.Size = new System.Drawing.Size(94, 20);
            this.Lturno.TabIndex = 183;
            // 
            // Lclasse
            // 
            this.Lclasse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lclasse.BackColor = System.Drawing.Color.Transparent;
            this.Lclasse.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Lclasse.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Lclasse.Location = new System.Drawing.Point(84, 148);
            this.Lclasse.Name = "Lclasse";
            this.Lclasse.Size = new System.Drawing.Size(66, 20);
            this.Lclasse.TabIndex = 182;
            // 
            // Lcurso
            // 
            this.Lcurso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lcurso.BackColor = System.Drawing.Color.Transparent;
            this.Lcurso.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Lcurso.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Lcurso.Location = new System.Drawing.Point(84, 123);
            this.Lcurso.Name = "Lcurso";
            this.Lcurso.Size = new System.Drawing.Size(347, 20);
            this.Lcurso.TabIndex = 181;
            // 
            // Lnome
            // 
            this.Lnome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lnome.BackColor = System.Drawing.Color.Transparent;
            this.Lnome.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Lnome.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Lnome.Location = new System.Drawing.Point(84, 95);
            this.Lnome.Name = "Lnome";
            this.Lnome.Size = new System.Drawing.Size(399, 20);
            this.Lnome.TabIndex = 180;
            // 
            // Lid
            // 
            this.Lid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lid.BackColor = System.Drawing.Color.Transparent;
            this.Lid.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.Lid.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Lid.Location = new System.Drawing.Point(84, 70);
            this.Lid.Name = "Lid";
            this.Lid.Size = new System.Drawing.Size(145, 20);
            this.Lid.TabIndex = 179;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label109.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label109.Location = new System.Drawing.Point(420, 148);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(47, 20);
            this.label109.TabIndex = 178;
            this.label109.Text = "* Ano:";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label108.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label108.Location = new System.Drawing.Point(19, 177);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(57, 20);
            this.label108.TabIndex = 177;
            this.label108.Text = "* Saldo:";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label107.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label107.Location = new System.Drawing.Point(318, 148);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(50, 20);
            this.label107.TabIndex = 176;
            this.label107.Text = "* Sala:";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label106.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label106.Location = new System.Drawing.Point(159, 148);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(53, 20);
            this.label106.TabIndex = 175;
            this.label106.Text = "*Turno:";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label105.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label105.Location = new System.Drawing.Point(20, 148);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(61, 20);
            this.label105.TabIndex = 174;
            this.label105.Text = "* Classe:";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label104.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label104.Location = new System.Drawing.Point(20, 123);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(56, 20);
            this.label104.TabIndex = 173;
            this.label104.Text = "* Curso:";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label103.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label103.Location = new System.Drawing.Point(20, 70);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(36, 20);
            this.label103.TabIndex = 172;
            this.label103.Text = "* ID:";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label102.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label102.Location = new System.Drawing.Point(20, 95);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(58, 20);
            this.label102.TabIndex = 171;
            this.label102.Text = "* Nome:";
            // 
            // btn_cadPAg
            // 
            this.btn_cadPAg.BackColor = System.Drawing.Color.Transparent;
            this.btn_cadPAg.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.btn_cadPAg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.btn_cadPAg.Location = new System.Drawing.Point(5, 0);
            this.btn_cadPAg.Name = "btn_cadPAg";
            this.btn_cadPAg.Size = new System.Drawing.Size(313, 54);
            this.btn_cadPAg.TabIndex = 30;
            this.btn_cadPAg.Text = "Informações do Aluno";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1, 474);
            this.panel16.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel5.Controls.Add(this.panel12);
            this.panel5.Location = new System.Drawing.Point(10, 272);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(691, 6);
            this.panel5.TabIndex = 247;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Gray;
            this.panel12.Location = new System.Drawing.Point(0, 21);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(691, 88);
            this.panel12.TabIndex = 248;
            // 
            // tb_mesdesc
            // 
            this.tb_mesdesc.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_mesdesc.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_mesdesc.FormattingEnabled = true;
            this.tb_mesdesc.Items.AddRange(new object[] {
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"});
            this.tb_mesdesc.Location = new System.Drawing.Point(111, 342);
            this.tb_mesdesc.Name = "tb_mesdesc";
            this.tb_mesdesc.Size = new System.Drawing.Size(156, 28);
            this.tb_mesdesc.TabIndex = 248;
            this.tb_mesdesc.Text = "Fevereiro";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.comboBox1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Todos Anos",
            "2016",
            "2015",
            "2014"});
            this.comboBox1.Location = new System.Drawing.Point(320, 89);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(112, 28);
            this.comboBox1.TabIndex = 246;
            this.comboBox1.Text = "Todos Anos";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_1);
            // 
            // tb_numMes
            // 
            this.tb_numMes.Enabled = false;
            this.tb_numMes.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_numMes.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_numMes.FormattingEnabled = true;
            this.tb_numMes.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.tb_numMes.Location = new System.Drawing.Point(10, 342);
            this.tb_numMes.Name = "tb_numMes";
            this.tb_numMes.Size = new System.Drawing.Size(87, 28);
            this.tb_numMes.TabIndex = 247;
            this.tb_numMes.Text = "1";
            this.tb_numMes.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label75.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label75.Location = new System.Drawing.Point(604, 134);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(39, 20);
            this.label75.TabIndex = 245;
            this.label75.Text = "*Ano";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label111.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label111.Location = new System.Drawing.Point(108, 317);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(143, 20);
            this.label111.TabIndex = 191;
            this.label111.Text = "* Descrição dos Meses";
            // 
            // c7
            // 
            this.c7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c7.Font = new System.Drawing.Font("Utsaah", 12F);
            this.c7.ForeColor = System.Drawing.Color.Black;
            this.c7.FormattingEnabled = true;
            this.c7.ItemHeight = 18;
            this.c7.Location = new System.Drawing.Point(612, 156);
            this.c7.Name = "c7";
            this.c7.Size = new System.Drawing.Size(49, 108);
            this.c7.TabIndex = 244;
            this.c7.SelectedIndexChanged += new System.EventHandler(this.c_SelectedIndexChanged);
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label110.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label110.Location = new System.Drawing.Point(6, 317);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(91, 20);
            this.label110.TabIndex = 189;
            this.label110.Text = "*Nº de Meses";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label94.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label94.Location = new System.Drawing.Point(6, 133);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(28, 20);
            this.label94.TabIndex = 243;
            this.label94.Text = "*ID";
            // 
            // c
            // 
            this.c.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c.Font = new System.Drawing.Font("Utsaah", 12F);
            this.c.ForeColor = System.Drawing.Color.Black;
            this.c.FormattingEnabled = true;
            this.c.ItemHeight = 18;
            this.c.Location = new System.Drawing.Point(12, 156);
            this.c.Name = "c";
            this.c.Size = new System.Drawing.Size(61, 108);
            this.c.TabIndex = 242;
            this.c.SelectedIndexChanged += new System.EventHandler(this.c_SelectedIndexChanged);
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label95.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label95.Location = new System.Drawing.Point(544, 133);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(49, 20);
            this.label95.TabIndex = 241;
            this.label95.Text = "*Saldo";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label96.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label96.Location = new System.Drawing.Point(504, 133);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(42, 20);
            this.label96.TabIndex = 240;
            this.label96.Text = "*Sala";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label97.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label97.Location = new System.Drawing.Point(449, 133);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(49, 20);
            this.label97.TabIndex = 239;
            this.label97.Text = "*Turno";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label98.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label98.Location = new System.Drawing.Point(396, 133);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(53, 20);
            this.label98.TabIndex = 238;
            this.label98.Text = "*Classe";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label99.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label99.Location = new System.Drawing.Point(307, 133);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(48, 20);
            this.label99.TabIndex = 237;
            this.label99.Text = "*Curso";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label100.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label100.Location = new System.Drawing.Point(81, 133);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(50, 20);
            this.label100.TabIndex = 236;
            this.label100.Text = "*Aluno";
            // 
            // c6
            // 
            this.c6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c6.Font = new System.Drawing.Font("Utsaah", 12F);
            this.c6.ForeColor = System.Drawing.Color.Black;
            this.c6.FormattingEnabled = true;
            this.c6.ItemHeight = 18;
            this.c6.Location = new System.Drawing.Point(546, 156);
            this.c6.Name = "c6";
            this.c6.Size = new System.Drawing.Size(63, 108);
            this.c6.TabIndex = 235;
            this.c6.SelectedIndexChanged += new System.EventHandler(this.c_SelectedIndexChanged);
            // 
            // c5
            // 
            this.c5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c5.Font = new System.Drawing.Font("Utsaah", 12F);
            this.c5.ForeColor = System.Drawing.Color.Black;
            this.c5.FormattingEnabled = true;
            this.c5.ItemHeight = 18;
            this.c5.Location = new System.Drawing.Point(509, 156);
            this.c5.Name = "c5";
            this.c5.Size = new System.Drawing.Size(36, 108);
            this.c5.TabIndex = 234;
            this.c5.SelectedIndexChanged += new System.EventHandler(this.c_SelectedIndexChanged);
            // 
            // c4
            // 
            this.c4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c4.Font = new System.Drawing.Font("Utsaah", 12F);
            this.c4.ForeColor = System.Drawing.Color.Black;
            this.c4.FormattingEnabled = true;
            this.c4.ItemHeight = 18;
            this.c4.Location = new System.Drawing.Point(448, 156);
            this.c4.Name = "c4";
            this.c4.Size = new System.Drawing.Size(60, 108);
            this.c4.TabIndex = 233;
            this.c4.SelectedIndexChanged += new System.EventHandler(this.c_SelectedIndexChanged);
            // 
            // c3
            // 
            this.c3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c3.Font = new System.Drawing.Font("Utsaah", 12F);
            this.c3.ForeColor = System.Drawing.Color.Black;
            this.c3.FormattingEnabled = true;
            this.c3.HorizontalScrollbar = true;
            this.c3.ItemHeight = 18;
            this.c3.Location = new System.Drawing.Point(403, 156);
            this.c3.Name = "c3";
            this.c3.Size = new System.Drawing.Size(44, 108);
            this.c3.TabIndex = 232;
            this.c3.SelectedIndexChanged += new System.EventHandler(this.c_SelectedIndexChanged);
            // 
            // c2
            // 
            this.c2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c2.Font = new System.Drawing.Font("Utsaah", 12F);
            this.c2.ForeColor = System.Drawing.Color.Black;
            this.c2.FormattingEnabled = true;
            this.c2.ItemHeight = 18;
            this.c2.Location = new System.Drawing.Point(277, 156);
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(126, 108);
            this.c2.TabIndex = 231;
            this.c2.SelectedIndexChanged += new System.EventHandler(this.c_SelectedIndexChanged);
            // 
            // c1
            // 
            this.c1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.c1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.c1.Font = new System.Drawing.Font("Utsaah", 12F);
            this.c1.ForeColor = System.Drawing.Color.Black;
            this.c1.FormattingEnabled = true;
            this.c1.HorizontalScrollbar = true;
            this.c1.ItemHeight = 18;
            this.c1.Location = new System.Drawing.Point(74, 156);
            this.c1.Name = "c1";
            this.c1.Size = new System.Drawing.Size(199, 108);
            this.c1.TabIndex = 230;
            this.c1.SelectedIndexChanged += new System.EventHandler(this.c_SelectedIndexChanged);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button12.Font = new System.Drawing.Font("Rod", 12F);
            this.button12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button12.Location = new System.Drawing.Point(184, 90);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(21, 25);
            this.button12.TabIndex = 228;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click_1);
            // 
            // prop_nome
            // 
            this.prop_nome.BaseColour = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.prop_nome.BorderColour = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.prop_nome.Checked = true;
            this.prop_nome.CheckedColour = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            this.prop_nome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.prop_nome.FontColour = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.prop_nome.HighlightColour = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.prop_nome.Location = new System.Drawing.Point(216, 91);
            this.prop_nome.Name = "prop_nome";
            this.prop_nome.Size = new System.Drawing.Size(100, 22);
            this.prop_nome.TabIndex = 171;
            this.prop_nome.Text = "Por Nome";
            // 
            // tb_pesqProp
            // 
            this.tb_pesqProp.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_pesqProp.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_pesqProp.Location = new System.Drawing.Point(5, 90);
            this.tb_pesqProp.Name = "tb_pesqProp";
            this.tb_pesqProp.Size = new System.Drawing.Size(174, 25);
            this.tb_pesqProp.TabIndex = 168;
            this.tb_pesqProp.TextChanged += new System.EventHandler(this.tb_pesqProp_TextChanged);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label93.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label93.Location = new System.Drawing.Point(1, 64);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(54, 20);
            this.label93.TabIndex = 170;
            this.label93.Text = "* Aluno";
            // 
            // label74
            // 
            this.label74.BackColor = System.Drawing.Color.Transparent;
            this.label74.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label74.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label74.Location = new System.Drawing.Point(1, 3);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(313, 54);
            this.label74.TabIndex = 29;
            this.label74.Text = "Propina -> Pagamento";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label101.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label101.Location = new System.Drawing.Point(322, 346);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(67, 20);
            this.label101.TabIndex = 254;
            this.label101.Text = "*Guardar";
            this.label101.Click += new System.EventHandler(this.label101_Click);
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label198.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label198.Location = new System.Drawing.Point(429, 346);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(69, 20);
            this.label198.TabIndex = 255;
            this.label198.Text = "*Imprimir";
            this.label198.Click += new System.EventHandler(this.label198_Click);
            // 
            // TbPropinaDen_propina
            // 
            this.TbPropinaDen_propina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TbPropinaDen_propina.Controls.Add(this.textClasse);
            this.TbPropinaDen_propina.Controls.Add(this.textSaldo);
            this.TbPropinaDen_propina.Controls.Add(this.textTurno);
            this.TbPropinaDen_propina.Controls.Add(this.textSala);
            this.TbPropinaDen_propina.Controls.Add(this.textCurso);
            this.TbPropinaDen_propina.Controls.Add(this.label168);
            this.TbPropinaDen_propina.Controls.Add(this.label167);
            this.TbPropinaDen_propina.Controls.Add(this.label136);
            this.TbPropinaDen_propina.Controls.Add(this.label135);
            this.TbPropinaDen_propina.Controls.Add(this.label134);
            this.TbPropinaDen_propina.Controls.Add(this.ax4);
            this.TbPropinaDen_propina.Controls.Add(this.ax5);
            this.TbPropinaDen_propina.Controls.Add(this.ax3);
            this.TbPropinaDen_propina.Controls.Add(this.ax2);
            this.TbPropinaDen_propina.Controls.Add(this.ax1);
            this.TbPropinaDen_propina.Controls.Add(this.textNOme);
            this.TbPropinaDen_propina.Controls.Add(this.button14);
            this.TbPropinaDen_propina.Controls.Add(this.cbANOpag);
            this.TbPropinaDen_propina.Controls.Add(this.tb_pagamentoID);
            this.TbPropinaDen_propina.Controls.Add(this.tb_Userprop);
            this.TbPropinaDen_propina.Controls.Add(this.a);
            this.TbPropinaDen_propina.Controls.Add(this.tb_Anoprop);
            this.TbPropinaDen_propina.Controls.Add(this.label117);
            this.TbPropinaDen_propina.Controls.Add(this.a1);
            this.TbPropinaDen_propina.Controls.Add(this.btn_delProp);
            this.TbPropinaDen_propina.Controls.Add(this.btn_actProp);
            this.TbPropinaDen_propina.Controls.Add(this.label119);
            this.TbPropinaDen_propina.Controls.Add(this.label121);
            this.TbPropinaDen_propina.Controls.Add(this.label122);
            this.TbPropinaDen_propina.Controls.Add(this.label123);
            this.TbPropinaDen_propina.Controls.Add(this.label124);
            this.TbPropinaDen_propina.Controls.Add(this.a6);
            this.TbPropinaDen_propina.Controls.Add(this.a5);
            this.TbPropinaDen_propina.Controls.Add(this.a4);
            this.TbPropinaDen_propina.Controls.Add(this.a3);
            this.TbPropinaDen_propina.Controls.Add(this.a2);
            this.TbPropinaDen_propina.Controls.Add(this.label125);
            this.TbPropinaDen_propina.Controls.Add(this.heyechRadioButton2);
            this.TbPropinaDen_propina.Controls.Add(this.button20);
            this.TbPropinaDen_propina.Controls.Add(this.tb_Peprop);
            this.TbPropinaDen_propina.Controls.Add(this.label127);
            this.TbPropinaDen_propina.Controls.Add(this.tb_IDpropina);
            this.TbPropinaDen_propina.Controls.Add(this.tb_Mesprop);
            this.TbPropinaDen_propina.Controls.Add(this.label128);
            this.TbPropinaDen_propina.Controls.Add(this.label129);
            this.TbPropinaDen_propina.Controls.Add(this.label130);
            this.TbPropinaDen_propina.Controls.Add(this.data_prop);
            this.TbPropinaDen_propina.Controls.Add(this.label131);
            this.TbPropinaDen_propina.Controls.Add(this.label132);
            this.TbPropinaDen_propina.Location = new System.Drawing.Point(4, 34);
            this.TbPropinaDen_propina.Name = "TbPropinaDen_propina";
            this.TbPropinaDen_propina.Padding = new System.Windows.Forms.Padding(3);
            this.TbPropinaDen_propina.Size = new System.Drawing.Size(1248, 480);
            this.TbPropinaDen_propina.TabIndex = 3;
            this.TbPropinaDen_propina.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // textClasse
            // 
            this.textClasse.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.textClasse.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.textClasse.Location = new System.Drawing.Point(435, 175);
            this.textClasse.Name = "textClasse";
            this.textClasse.Size = new System.Drawing.Size(174, 25);
            this.textClasse.TabIndex = 293;
            this.textClasse.Visible = false;
            // 
            // textSaldo
            // 
            this.textSaldo.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.textSaldo.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.textSaldo.Location = new System.Drawing.Point(616, 145);
            this.textSaldo.Name = "textSaldo";
            this.textSaldo.Size = new System.Drawing.Size(174, 25);
            this.textSaldo.TabIndex = 292;
            this.textSaldo.Visible = false;
            // 
            // textTurno
            // 
            this.textTurno.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.textTurno.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.textTurno.Location = new System.Drawing.Point(616, 114);
            this.textTurno.Name = "textTurno";
            this.textTurno.Size = new System.Drawing.Size(174, 25);
            this.textTurno.TabIndex = 291;
            this.textTurno.Visible = false;
            // 
            // textSala
            // 
            this.textSala.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.textSala.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.textSala.Location = new System.Drawing.Point(434, 144);
            this.textSala.Name = "textSala";
            this.textSala.Size = new System.Drawing.Size(174, 25);
            this.textSala.TabIndex = 290;
            this.textSala.Visible = false;
            // 
            // textCurso
            // 
            this.textCurso.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.textCurso.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.textCurso.Location = new System.Drawing.Point(434, 115);
            this.textCurso.Name = "textCurso";
            this.textCurso.Size = new System.Drawing.Size(174, 25);
            this.textCurso.TabIndex = 289;
            this.textCurso.Visible = false;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label168.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label168.Location = new System.Drawing.Point(1132, 220);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(49, 20);
            this.label168.TabIndex = 288;
            this.label168.Text = "*Saldo";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label167.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label167.Location = new System.Drawing.Point(1071, 220);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(42, 20);
            this.label167.TabIndex = 287;
            this.label167.Text = "*Sala";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label136.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label136.Location = new System.Drawing.Point(989, 220);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(49, 20);
            this.label136.TabIndex = 286;
            this.label136.Text = "*Turno";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label135.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label135.Location = new System.Drawing.Point(927, 220);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(53, 20);
            this.label135.TabIndex = 285;
            this.label135.Text = "*Classe";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label134.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label134.Location = new System.Drawing.Point(712, 220);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(48, 20);
            this.label134.TabIndex = 284;
            this.label134.Text = "*Curso";
            // 
            // ax4
            // 
            this.ax4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ax4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ax4.Font = new System.Drawing.Font("Utsaah", 12F);
            this.ax4.ForeColor = System.Drawing.Color.Black;
            this.ax4.FormattingEnabled = true;
            this.ax4.ItemHeight = 18;
            this.ax4.Location = new System.Drawing.Point(1075, 243);
            this.ax4.Name = "ax4";
            this.ax4.Size = new System.Drawing.Size(56, 162);
            this.ax4.TabIndex = 283;
            this.ax4.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // ax5
            // 
            this.ax5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ax5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ax5.Font = new System.Drawing.Font("Utsaah", 12F);
            this.ax5.ForeColor = System.Drawing.Color.Black;
            this.ax5.FormattingEnabled = true;
            this.ax5.ItemHeight = 18;
            this.ax5.Location = new System.Drawing.Point(1136, 243);
            this.ax5.Name = "ax5";
            this.ax5.Size = new System.Drawing.Size(102, 162);
            this.ax5.TabIndex = 282;
            this.ax5.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // ax3
            // 
            this.ax3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ax3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ax3.Font = new System.Drawing.Font("Utsaah", 12F);
            this.ax3.ForeColor = System.Drawing.Color.Black;
            this.ax3.FormattingEnabled = true;
            this.ax3.ItemHeight = 18;
            this.ax3.Location = new System.Drawing.Point(993, 243);
            this.ax3.Name = "ax3";
            this.ax3.Size = new System.Drawing.Size(78, 162);
            this.ax3.TabIndex = 281;
            this.ax3.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // ax2
            // 
            this.ax2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ax2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ax2.Font = new System.Drawing.Font("Utsaah", 12F);
            this.ax2.ForeColor = System.Drawing.Color.Black;
            this.ax2.FormattingEnabled = true;
            this.ax2.ItemHeight = 18;
            this.ax2.Location = new System.Drawing.Point(931, 243);
            this.ax2.Name = "ax2";
            this.ax2.Size = new System.Drawing.Size(56, 162);
            this.ax2.TabIndex = 280;
            this.ax2.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // ax1
            // 
            this.ax1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ax1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ax1.Font = new System.Drawing.Font("Utsaah", 12F);
            this.ax1.ForeColor = System.Drawing.Color.Black;
            this.ax1.FormattingEnabled = true;
            this.ax1.HorizontalScrollbar = true;
            this.ax1.ItemHeight = 18;
            this.ax1.Location = new System.Drawing.Point(716, 243);
            this.ax1.Name = "ax1";
            this.ax1.Size = new System.Drawing.Size(209, 162);
            this.ax1.TabIndex = 279;
            this.ax1.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // textNOme
            // 
            this.textNOme.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.textNOme.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.textNOme.Location = new System.Drawing.Point(225, 115);
            this.textNOme.Name = "textNOme";
            this.textNOme.Size = new System.Drawing.Size(174, 25);
            this.textNOme.TabIndex = 278;
            this.textNOme.Visible = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button14.BackgroundImage = global::PPA.Properties.Resources.document_print;
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button14.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button14.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button14.Location = new System.Drawing.Point(905, 122);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(30, 28);
            this.button14.TabIndex = 277;
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click_1);
            // 
            // cbANOpag
            // 
            this.cbANOpag.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.cbANOpag.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.cbANOpag.FormattingEnabled = true;
            this.cbANOpag.Items.AddRange(new object[] {
            "Todos Anos",
            "2016",
            "2015",
            "2014"});
            this.cbANOpag.Location = new System.Drawing.Point(880, 166);
            this.cbANOpag.Name = "cbANOpag";
            this.cbANOpag.Size = new System.Drawing.Size(174, 28);
            this.cbANOpag.TabIndex = 276;
            this.cbANOpag.Text = "Todos Anos";
            this.cbANOpag.SelectedIndexChanged += new System.EventHandler(this.cbANOpag_SelectedIndexChanged);
            // 
            // tb_pagamentoID
            // 
            this.tb_pagamentoID.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_pagamentoID.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_pagamentoID.Location = new System.Drawing.Point(7, 115);
            this.tb_pagamentoID.Name = "tb_pagamentoID";
            this.tb_pagamentoID.Size = new System.Drawing.Size(174, 25);
            this.tb_pagamentoID.TabIndex = 275;
            // 
            // tb_Userprop
            // 
            this.tb_Userprop.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_Userprop.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_Userprop.FormattingEnabled = true;
            this.tb_Userprop.Location = new System.Drawing.Point(652, 81);
            this.tb_Userprop.Name = "tb_Userprop";
            this.tb_Userprop.Size = new System.Drawing.Size(174, 28);
            this.tb_Userprop.TabIndex = 274;
            // 
            // a
            // 
            this.a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.a.Cursor = System.Windows.Forms.Cursors.Hand;
            this.a.Font = new System.Drawing.Font("Utsaah", 12F);
            this.a.ForeColor = System.Drawing.Color.Gray;
            this.a.FormattingEnabled = true;
            this.a.ItemHeight = 18;
            this.a.Location = new System.Drawing.Point(7, 146);
            this.a.Name = "a";
            this.a.Size = new System.Drawing.Size(44, 20);
            this.a.TabIndex = 273;
            this.a.Visible = false;
            this.a.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // tb_Anoprop
            // 
            this.tb_Anoprop.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_Anoprop.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_Anoprop.FormattingEnabled = true;
            this.tb_Anoprop.Items.AddRange(new object[] {
            "2016",
            "2015",
            "2014"});
            this.tb_Anoprop.Location = new System.Drawing.Point(880, 81);
            this.tb_Anoprop.Name = "tb_Anoprop";
            this.tb_Anoprop.Size = new System.Drawing.Size(174, 28);
            this.tb_Anoprop.TabIndex = 269;
            this.tb_Anoprop.Text = "2016";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label117.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label117.Location = new System.Drawing.Point(7, 220);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(28, 20);
            this.label117.TabIndex = 267;
            this.label117.Text = "*ID";
            // 
            // a1
            // 
            this.a1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.a1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.a1.Font = new System.Drawing.Font("Utsaah", 12F);
            this.a1.ForeColor = System.Drawing.Color.Black;
            this.a1.FormattingEnabled = true;
            this.a1.ItemHeight = 18;
            this.a1.Location = new System.Drawing.Point(13, 243);
            this.a1.Name = "a1";
            this.a1.Size = new System.Drawing.Size(61, 162);
            this.a1.TabIndex = 266;
            this.a1.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // btn_delProp
            // 
            this.btn_delProp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_delProp.BackgroundImage = global::PPA.Properties.Resources.save_delete;
            this.btn_delProp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_delProp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_delProp.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_delProp.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_delProp.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_delProp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_delProp.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_delProp.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_delProp.Location = new System.Drawing.Point(1013, 122);
            this.btn_delProp.Name = "btn_delProp";
            this.btn_delProp.Size = new System.Drawing.Size(30, 28);
            this.btn_delProp.TabIndex = 265;
            this.btn_delProp.UseVisualStyleBackColor = false;
            this.btn_delProp.Click += new System.EventHandler(this.btn_delProp_Click);
            // 
            // btn_actProp
            // 
            this.btn_actProp.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_actProp.BackgroundImage = global::PPA.Properties.Resources.save_41_;
            this.btn_actProp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_actProp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_actProp.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_actProp.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_actProp.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_actProp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_actProp.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_actProp.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_actProp.Location = new System.Drawing.Point(956, 122);
            this.btn_actProp.Name = "btn_actProp";
            this.btn_actProp.Size = new System.Drawing.Size(30, 28);
            this.btn_actProp.TabIndex = 264;
            this.btn_actProp.UseVisualStyleBackColor = false;
            this.btn_actProp.Click += new System.EventHandler(this.btn_actProp_Click);
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label119.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label119.Location = new System.Drawing.Point(646, 220);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(39, 20);
            this.label119.TabIndex = 260;
            this.label119.Text = "*Ano";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label121.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label121.Location = new System.Drawing.Point(465, 220);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(62, 20);
            this.label121.TabIndex = 259;
            this.label121.Text = "*Usuário";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label122.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label122.Location = new System.Drawing.Point(383, 220);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(60, 20);
            this.label122.TabIndex = 258;
            this.label122.Text = "*DataPg";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label123.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label123.Location = new System.Drawing.Point(278, 220);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(40, 20);
            this.label123.TabIndex = 257;
            this.label123.Text = "*Mês";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label124.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label124.Location = new System.Drawing.Point(73, 220);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(50, 20);
            this.label124.TabIndex = 256;
            this.label124.Text = "*Aluno";
            // 
            // a6
            // 
            this.a6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.a6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.a6.Font = new System.Drawing.Font("Utsaah", 12F);
            this.a6.ForeColor = System.Drawing.Color.Black;
            this.a6.FormattingEnabled = true;
            this.a6.ItemHeight = 18;
            this.a6.Location = new System.Drawing.Point(654, 243);
            this.a6.Name = "a6";
            this.a6.Size = new System.Drawing.Size(56, 162);
            this.a6.TabIndex = 254;
            this.a6.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // a5
            // 
            this.a5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.a5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.a5.Font = new System.Drawing.Font("Utsaah", 12F);
            this.a5.ForeColor = System.Drawing.Color.Black;
            this.a5.FormattingEnabled = true;
            this.a5.ItemHeight = 18;
            this.a5.Location = new System.Drawing.Point(473, 243);
            this.a5.Name = "a5";
            this.a5.Size = new System.Drawing.Size(180, 162);
            this.a5.TabIndex = 253;
            this.a5.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // a4
            // 
            this.a4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.a4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.a4.Font = new System.Drawing.Font("Utsaah", 12F);
            this.a4.ForeColor = System.Drawing.Color.Black;
            this.a4.FormattingEnabled = true;
            this.a4.ItemHeight = 18;
            this.a4.Location = new System.Drawing.Point(391, 243);
            this.a4.Name = "a4";
            this.a4.Size = new System.Drawing.Size(74, 162);
            this.a4.TabIndex = 252;
            this.a4.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // a3
            // 
            this.a3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.a3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.a3.Font = new System.Drawing.Font("Utsaah", 12F);
            this.a3.ForeColor = System.Drawing.Color.Black;
            this.a3.FormattingEnabled = true;
            this.a3.ItemHeight = 18;
            this.a3.Location = new System.Drawing.Point(285, 243);
            this.a3.Name = "a3";
            this.a3.Size = new System.Drawing.Size(104, 162);
            this.a3.TabIndex = 251;
            this.a3.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // a2
            // 
            this.a2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.a2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.a2.Font = new System.Drawing.Font("Utsaah", 12F);
            this.a2.ForeColor = System.Drawing.Color.Black;
            this.a2.FormattingEnabled = true;
            this.a2.HorizontalScrollbar = true;
            this.a2.ItemHeight = 18;
            this.a2.Location = new System.Drawing.Point(75, 243);
            this.a2.Name = "a2";
            this.a2.Size = new System.Drawing.Size(209, 162);
            this.a2.TabIndex = 250;
            this.a2.SelectedIndexChanged += new System.EventHandler(this.a_SelectedIndexChanged);
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.BackColor = System.Drawing.Color.Transparent;
            this.label125.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label125.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label125.Location = new System.Drawing.Point(6, 3);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(584, 32);
            this.label125.TabIndex = 249;
            this.label125.Text = "CRUD Propina: Cadastrar, Seleccionar, Alterar e Deletar ";
            // 
            // heyechRadioButton2
            // 
            this.heyechRadioButton2.BaseColour = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.heyechRadioButton2.BorderColour = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.heyechRadioButton2.Checked = true;
            this.heyechRadioButton2.CheckedColour = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            this.heyechRadioButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.heyechRadioButton2.FontColour = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.heyechRadioButton2.HighlightColour = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.heyechRadioButton2.Location = new System.Drawing.Point(212, 195);
            this.heyechRadioButton2.Name = "heyechRadioButton2";
            this.heyechRadioButton2.Size = new System.Drawing.Size(100, 22);
            this.heyechRadioButton2.TabIndex = 247;
            this.heyechRadioButton2.Text = "Por Nome";
            // 
            // button20
            // 
            this.button20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button20.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("Rod", 12F);
            this.button20.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button20.Location = new System.Drawing.Point(182, 194);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(21, 23);
            this.button20.TabIndex = 239;
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // tb_Peprop
            // 
            this.tb_Peprop.Font = new System.Drawing.Font("Rod", 12F);
            this.tb_Peprop.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_Peprop.Location = new System.Drawing.Point(5, 194);
            this.tb_Peprop.Name = "tb_Peprop";
            this.tb_Peprop.Size = new System.Drawing.Size(174, 23);
            this.tb_Peprop.TabIndex = 238;
            this.tb_Peprop.TextChanged += new System.EventHandler(this.tb_Peprop_TextChanged);
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label127.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label127.Location = new System.Drawing.Point(9, 169);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(75, 20);
            this.label127.TabIndex = 246;
            this.label127.Text = "*Pesquisar";
            // 
            // tb_IDpropina
            // 
            this.tb_IDpropina.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_IDpropina.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_IDpropina.Location = new System.Drawing.Point(7, 84);
            this.tb_IDpropina.Name = "tb_IDpropina";
            this.tb_IDpropina.Size = new System.Drawing.Size(174, 25);
            this.tb_IDpropina.TabIndex = 233;
            // 
            // tb_Mesprop
            // 
            this.tb_Mesprop.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_Mesprop.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_Mesprop.Location = new System.Drawing.Point(225, 83);
            this.tb_Mesprop.Name = "tb_Mesprop";
            this.tb_Mesprop.Size = new System.Drawing.Size(174, 25);
            this.tb_Mesprop.TabIndex = 234;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label128.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label128.Location = new System.Drawing.Point(215, 62);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(75, 20);
            this.label128.TabIndex = 244;
            this.label128.Text = "*Mês Pago";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label129.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label129.Location = new System.Drawing.Point(3, 58);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(67, 20);
            this.label129.TabIndex = 240;
            this.label129.Text = "*ID Aluno";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label130.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label130.Location = new System.Drawing.Point(646, 56);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(62, 20);
            this.label130.TabIndex = 241;
            this.label130.Text = "*Usuário";
            // 
            // data_prop
            // 
            this.data_prop.CalendarForeColor = System.Drawing.SystemColors.ControlDark;
            this.data_prop.CalendarTitleBackColor = System.Drawing.SystemColors.ControlDark;
            this.data_prop.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDark;
            this.data_prop.CalendarTrailingForeColor = System.Drawing.Color.Silver;
            this.data_prop.CustomFormat = "dd-MM-yyyy";
            this.data_prop.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.data_prop.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data_prop.Location = new System.Drawing.Point(452, 83);
            this.data_prop.Name = "data_prop";
            this.data_prop.Size = new System.Drawing.Size(146, 25);
            this.data_prop.TabIndex = 236;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label131.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label131.Location = new System.Drawing.Point(444, 56);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(119, 20);
            this.label131.TabIndex = 243;
            this.label131.Text = "*Data de Deposito";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label132.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label132.Location = new System.Drawing.Point(876, 56);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(39, 20);
            this.label132.TabIndex = 242;
            this.label132.Text = "*Ano";
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1294, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(35, 518);
            this.panel4.TabIndex = 31;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(35, 518);
            this.panel3.TabIndex = 29;
            // 
            // tpageDEPOSITO
            // 
            this.tpageDEPOSITO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tpageDEPOSITO.Controls.Add(this.panel13);
            this.tpageDEPOSITO.Controls.Add(this.TbDepositoDen);
            this.tpageDEPOSITO.Controls.Add(this.panel7);
            this.tpageDEPOSITO.Controls.Add(this.panel8);
            this.tpageDEPOSITO.Location = new System.Drawing.Point(4, 5);
            this.tpageDEPOSITO.Name = "tpageDEPOSITO";
            this.tpageDEPOSITO.Padding = new System.Windows.Forms.Padding(3);
            this.tpageDEPOSITO.Size = new System.Drawing.Size(1332, 524);
            this.tpageDEPOSITO.TabIndex = 4;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.panel13.Controls.Add(this.button28);
            this.panel13.Controls.Add(this.button31);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel13.Location = new System.Drawing.Point(38, 477);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1256, 44);
            this.panel13.TabIndex = 38;
            // 
            // button28
            // 
            this.button28.AutoSize = true;
            this.button28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button28.FlatAppearance.BorderSize = 0;
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button28.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button28.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button28.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button28.Location = new System.Drawing.Point(1097, 7);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(59, 30);
            this.button28.TabIndex = 63;
            this.button28.Text = " (0_0)";
            this.button28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button28.UseVisualStyleBackColor = false;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button31
            // 
            this.button31.AutoSize = true;
            this.button31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button31.FlatAppearance.BorderSize = 0;
            this.button31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button31.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button31.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button31.Image = global::PPA.Properties.Resources.k4_fw;
            this.button31.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button31.Location = new System.Drawing.Point(16, 7);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(148, 30);
            this.button31.TabIndex = 58;
            this.button31.Text = "      CRUD Deposito";
            this.button31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button31.UseVisualStyleBackColor = false;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // TbDepositoDen
            // 
            this.TbDepositoDen.Controls.Add(this.TbDepositoDen_pub);
            this.TbDepositoDen.Controls.Add(this.TbDepositoDen_CRD);
            this.TbDepositoDen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbDepositoDen.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.TbDepositoDen.ItemSize = new System.Drawing.Size(0, 30);
            this.TbDepositoDen.Location = new System.Drawing.Point(38, 3);
            this.TbDepositoDen.Name = "TbDepositoDen";
            this.TbDepositoDen.SelectedIndex = 0;
            this.TbDepositoDen.Size = new System.Drawing.Size(1256, 518);
            this.TbDepositoDen.TabIndex = 36;
            // 
            // TbDepositoDen_pub
            // 
            this.TbDepositoDen_pub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TbDepositoDen_pub.Controls.Add(this.label71);
            this.TbDepositoDen_pub.Controls.Add(this.pictureBox19);
            this.TbDepositoDen_pub.Controls.Add(this.button19);
            this.TbDepositoDen_pub.Controls.Add(this.label70);
            this.TbDepositoDen_pub.Location = new System.Drawing.Point(4, 34);
            this.TbDepositoDen_pub.Name = "TbDepositoDen_pub";
            this.TbDepositoDen_pub.Padding = new System.Windows.Forms.Padding(3);
            this.TbDepositoDen_pub.Size = new System.Drawing.Size(1248, 480);
            this.TbDepositoDen_pub.TabIndex = 0;
            // 
            // label71
            // 
            this.label71.BackColor = System.Drawing.Color.Transparent;
            this.label71.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.label71.Location = new System.Drawing.Point(9, 215);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(162, 46);
            this.label71.TabIndex = 70;
            this.label71.Text = "Faça o CRUD Depositos!";
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::PPA.Properties.Resources.images_9;
            this.pictureBox19.Location = new System.Drawing.Point(48, 95);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(74, 78);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 67;
            this.pictureBox19.TabStop = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button19.FlatAppearance.BorderSize = 0;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button19.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button19.Location = new System.Drawing.Point(14, 184);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(148, 27);
            this.button19.TabIndex = 66;
            this.button19.Text = "CRUD Deposito";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.Color.Transparent;
            this.label70.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label70.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label70.Location = new System.Drawing.Point(0, 3);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(178, 54);
            this.label70.TabIndex = 28;
            this.label70.Text = "Deposito";
            // 
            // TbDepositoDen_CRD
            // 
            this.TbDepositoDen_CRD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TbDepositoDen_CRD.Controls.Add(this.DepositoDen);
            this.TbDepositoDen_CRD.Location = new System.Drawing.Point(4, 34);
            this.TbDepositoDen_CRD.Name = "TbDepositoDen_CRD";
            this.TbDepositoDen_CRD.Padding = new System.Windows.Forms.Padding(3);
            this.TbDepositoDen_CRD.Size = new System.Drawing.Size(1248, 480);
            this.TbDepositoDen_CRD.TabIndex = 1;
            // 
            // DepositoDen
            // 
            this.DepositoDen.Controls.Add(this.DepositoDen_cadasDep);
            this.DepositoDen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DepositoDen.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.DepositoDen.ItemSize = new System.Drawing.Size(0, 1);
            this.DepositoDen.Location = new System.Drawing.Point(3, 3);
            this.DepositoDen.Name = "DepositoDen";
            this.DepositoDen.SelectedIndex = 0;
            this.DepositoDen.Size = new System.Drawing.Size(1242, 474);
            this.DepositoDen.TabIndex = 0;
            // 
            // DepositoDen_cadasDep
            // 
            this.DepositoDen_cadasDep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.DepositoDen_cadasDep.Controls.Add(this.cb_cla);
            this.DepositoDen_cadasDep.Controls.Add(this.label73);
            this.DepositoDen_cadasDep.Controls.Add(this.sal);
            this.DepositoDen_cadasDep.Controls.Add(this.label72);
            this.DepositoDen_cadasDep.Controls.Add(this.ddd);
            this.DepositoDen_cadasDep.Controls.Add(this.button18);
            this.DepositoDen_cadasDep.Controls.Add(this.tb_d);
            this.DepositoDen_cadasDep.Controls.Add(this.label69);
            this.DepositoDen_cadasDep.Controls.Add(this.tb_bancoDep);
            this.DepositoDen_cadasDep.Controls.Add(this.tb_IDATODEP);
            this.DepositoDen_cadasDep.Controls.Add(this.dd);
            this.DepositoDen_cadasDep.Controls.Add(this.label92);
            this.DepositoDen_cadasDep.Controls.Add(this.d);
            this.DepositoDen_cadasDep.Controls.Add(this.btn_deletDep);
            this.DepositoDen_cadasDep.Controls.Add(this.btn_actDep);
            this.DepositoDen_cadasDep.Controls.Add(this.button46);
            this.DepositoDen_cadasDep.Controls.Add(this.button47);
            this.DepositoDen_cadasDep.Controls.Add(this.dataPesqDep);
            this.DepositoDen_cadasDep.Controls.Add(this.label81);
            this.DepositoDen_cadasDep.Controls.Add(this.label83);
            this.DepositoDen_cadasDep.Controls.Add(this.label84);
            this.DepositoDen_cadasDep.Controls.Add(this.label85);
            this.DepositoDen_cadasDep.Controls.Add(this.label90);
            this.DepositoDen_cadasDep.Controls.Add(this.label91);
            this.DepositoDen_cadasDep.Controls.Add(this.d6);
            this.DepositoDen_cadasDep.Controls.Add(this.d5);
            this.DepositoDen_cadasDep.Controls.Add(this.d4);
            this.DepositoDen_cadasDep.Controls.Add(this.d3);
            this.DepositoDen_cadasDep.Controls.Add(this.d2);
            this.DepositoDen_cadasDep.Controls.Add(this.d1);
            this.DepositoDen_cadasDep.Controls.Add(this.label78);
            this.DepositoDen_cadasDep.Controls.Add(this.label79);
            this.DepositoDen_cadasDep.Controls.Add(this.heyechRadioButton1);
            this.DepositoDen_cadasDep.Controls.Add(this.btn_pesqDep);
            this.DepositoDen_cadasDep.Controls.Add(this.tb_pesqDep);
            this.DepositoDen_cadasDep.Controls.Add(this.label80);
            this.DepositoDen_cadasDep.Controls.Add(this.data_actual);
            this.DepositoDen_cadasDep.Controls.Add(this.tb_IDalunoDep);
            this.DepositoDen_cadasDep.Controls.Add(this.tb_talão);
            this.DepositoDen_cadasDep.Controls.Add(this.label82);
            this.DepositoDen_cadasDep.Controls.Add(this.btn_cadDep);
            this.DepositoDen_cadasDep.Controls.Add(this.label86);
            this.DepositoDen_cadasDep.Controls.Add(this.tb_valorDep);
            this.DepositoDen_cadasDep.Controls.Add(this.label87);
            this.DepositoDen_cadasDep.Controls.Add(this.data_dep);
            this.DepositoDen_cadasDep.Controls.Add(this.label88);
            this.DepositoDen_cadasDep.Controls.Add(this.label89);
            this.DepositoDen_cadasDep.Location = new System.Drawing.Point(4, 5);
            this.DepositoDen_cadasDep.Name = "DepositoDen_cadasDep";
            this.DepositoDen_cadasDep.Padding = new System.Windows.Forms.Padding(3);
            this.DepositoDen_cadasDep.Size = new System.Drawing.Size(1234, 465);
            this.DepositoDen_cadasDep.TabIndex = 0;
            // 
            // cb_cla
            // 
            this.cb_cla.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_cla.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_cla.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.cb_cla.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.cb_cla.FormattingEnabled = true;
            this.cb_cla.Items.AddRange(new object[] {
            "Todos Anos",
            "2016",
            "2015",
            "2014"});
            this.cb_cla.Location = new System.Drawing.Point(895, 222);
            this.cb_cla.Name = "cb_cla";
            this.cb_cla.Size = new System.Drawing.Size(146, 28);
            this.cb_cla.TabIndex = 232;
            this.cb_cla.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label73.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label73.Location = new System.Drawing.Point(717, 225);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(100, 20);
            this.label73.TabIndex = 231;
            this.label73.Text = "*Saldo Total Kz";
            // 
            // sal
            // 
            this.sal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sal.Font = new System.Drawing.Font("Utsaah", 12F);
            this.sal.ForeColor = System.Drawing.Color.Black;
            this.sal.FormattingEnabled = true;
            this.sal.ItemHeight = 18;
            this.sal.Location = new System.Drawing.Point(724, 249);
            this.sal.Name = "sal";
            this.sal.Size = new System.Drawing.Size(93, 162);
            this.sal.TabIndex = 230;
            this.sal.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label72.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label72.Location = new System.Drawing.Point(666, 225);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(39, 20);
            this.label72.TabIndex = 229;
            this.label72.Text = "*Ano";
            // 
            // ddd
            // 
            this.ddd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ddd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ddd.Font = new System.Drawing.Font("Utsaah", 12F);
            this.ddd.ForeColor = System.Drawing.Color.Black;
            this.ddd.FormattingEnabled = true;
            this.ddd.ItemHeight = 18;
            this.ddd.Location = new System.Drawing.Point(662, 249);
            this.ddd.Name = "ddd";
            this.ddd.Size = new System.Drawing.Size(61, 162);
            this.ddd.TabIndex = 228;
            this.ddd.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button18.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button18.Font = new System.Drawing.Font("Rod", 12F);
            this.button18.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button18.Location = new System.Drawing.Point(183, 140);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(21, 27);
            this.button18.TabIndex = 227;
            this.toolTip1.SetToolTip(this.button18, "Checar Se Está Confirmado!");
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // tb_d
            // 
            this.tb_d.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tb_d.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tb_d.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_d.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_d.FormattingEnabled = true;
            this.tb_d.Items.AddRange(new object[] {
            "2016"});
            this.tb_d.Location = new System.Drawing.Point(5, 140);
            this.tb_d.Name = "tb_d";
            this.tb_d.Size = new System.Drawing.Size(174, 28);
            this.tb_d.TabIndex = 226;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label69.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label69.Location = new System.Drawing.Point(-1, 118);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(82, 20);
            this.label69.TabIndex = 225;
            this.label69.Text = "*Ano Leitivo";
            // 
            // tb_bancoDep
            // 
            this.tb_bancoDep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tb_bancoDep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tb_bancoDep.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_bancoDep.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_bancoDep.FormattingEnabled = true;
            this.tb_bancoDep.Items.AddRange(new object[] {
            "BCI",
            "BIC",
            "IBAM"});
            this.tb_bancoDep.Location = new System.Drawing.Point(676, 88);
            this.tb_bancoDep.Name = "tb_bancoDep";
            this.tb_bancoDep.Size = new System.Drawing.Size(174, 28);
            this.tb_bancoDep.TabIndex = 224;
            // 
            // tb_IDATODEP
            // 
            this.tb_IDATODEP.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_IDATODEP.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_IDATODEP.Location = new System.Drawing.Point(223, 121);
            this.tb_IDATODEP.Name = "tb_IDATODEP";
            this.tb_IDATODEP.Size = new System.Drawing.Size(26, 25);
            this.tb_IDATODEP.TabIndex = 223;
            this.tb_IDATODEP.Visible = false;
            // 
            // dd
            // 
            this.dd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dd.Font = new System.Drawing.Font("Utsaah", 12F);
            this.dd.ForeColor = System.Drawing.Color.Gray;
            this.dd.FormattingEnabled = true;
            this.dd.ItemHeight = 18;
            this.dd.Location = new System.Drawing.Point(1155, 19);
            this.dd.Name = "dd";
            this.dd.Size = new System.Drawing.Size(61, 40);
            this.dd.TabIndex = 222;
            this.dd.Visible = false;
            this.dd.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label92.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label92.Location = new System.Drawing.Point(5, 226);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(28, 20);
            this.label92.TabIndex = 221;
            this.label92.Text = "*ID";
            // 
            // d
            // 
            this.d.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.d.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d.ForeColor = System.Drawing.Color.Black;
            this.d.FormattingEnabled = true;
            this.d.ItemHeight = 18;
            this.d.Location = new System.Drawing.Point(11, 249);
            this.d.Name = "d";
            this.d.Size = new System.Drawing.Size(61, 162);
            this.d.TabIndex = 220;
            this.d.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // btn_deletDep
            // 
            this.btn_deletDep.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_deletDep.BackgroundImage = global::PPA.Properties.Resources.save_delete;
            this.btn_deletDep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_deletDep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_deletDep.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_deletDep.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_deletDep.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_deletDep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_deletDep.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_deletDep.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_deletDep.Location = new System.Drawing.Point(1011, 158);
            this.btn_deletDep.Name = "btn_deletDep";
            this.btn_deletDep.Size = new System.Drawing.Size(30, 28);
            this.btn_deletDep.TabIndex = 219;
            this.btn_deletDep.UseVisualStyleBackColor = false;
            this.btn_deletDep.Click += new System.EventHandler(this.btn_deletDep_Click);
            // 
            // btn_actDep
            // 
            this.btn_actDep.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_actDep.BackgroundImage = global::PPA.Properties.Resources.save_41_;
            this.btn_actDep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_actDep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_actDep.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_actDep.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_actDep.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_actDep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_actDep.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_actDep.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_actDep.Location = new System.Drawing.Point(954, 158);
            this.btn_actDep.Name = "btn_actDep";
            this.btn_actDep.Size = new System.Drawing.Size(30, 28);
            this.btn_actDep.TabIndex = 218;
            this.btn_actDep.UseVisualStyleBackColor = false;
            this.btn_actDep.Click += new System.EventHandler(this.btn_actDep_Click);
            // 
            // button46
            // 
            this.button46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button46.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button46.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button46.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button46.Location = new System.Drawing.Point(895, 256);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(146, 33);
            this.button46.TabIndex = 217;
            this.button46.Text = "Todos";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // button47
            // 
            this.button47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button47.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button47.Font = new System.Drawing.Font("Rod", 12F);
            this.button47.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button47.Location = new System.Drawing.Point(1046, 196);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(21, 23);
            this.button47.TabIndex = 216;
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // dataPesqDep
            // 
            this.dataPesqDep.CalendarForeColor = System.Drawing.SystemColors.ControlDark;
            this.dataPesqDep.CalendarTitleBackColor = System.Drawing.SystemColors.ControlDark;
            this.dataPesqDep.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDark;
            this.dataPesqDep.CalendarTrailingForeColor = System.Drawing.Color.Silver;
            this.dataPesqDep.CustomFormat = "dd-MM-yyyy";
            this.dataPesqDep.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.dataPesqDep.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataPesqDep.Location = new System.Drawing.Point(895, 195);
            this.dataPesqDep.Name = "dataPesqDep";
            this.dataPesqDep.Size = new System.Drawing.Size(146, 25);
            this.dataPesqDep.TabIndex = 215;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label81.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label81.Location = new System.Drawing.Point(599, 226);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(66, 20);
            this.label81.TabIndex = 214;
            this.label81.Text = "*DataAct.";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label83.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label83.Location = new System.Drawing.Point(537, 226);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(65, 20);
            this.label83.TabIndex = 213;
            this.label83.Text = "*DataDp.";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label84.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label84.Location = new System.Drawing.Point(482, 226);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(51, 20);
            this.label84.TabIndex = 212;
            this.label84.Text = "*Banco";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label85.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label85.Location = new System.Drawing.Point(381, 226);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(81, 20);
            this.label85.TabIndex = 211;
            this.label85.Text = "*Valor Dep.";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label90.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label90.Location = new System.Drawing.Point(276, 226);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(48, 20);
            this.label90.TabIndex = 210;
            this.label90.Text = "*Talão";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label91.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label91.Location = new System.Drawing.Point(71, 226);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(50, 20);
            this.label91.TabIndex = 209;
            this.label91.Text = "*Aluno";
            // 
            // d6
            // 
            this.d6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.d6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d6.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d6.ForeColor = System.Drawing.Color.Black;
            this.d6.FormattingEnabled = true;
            this.d6.ItemHeight = 18;
            this.d6.Location = new System.Drawing.Point(605, 249);
            this.d6.Name = "d6";
            this.d6.Size = new System.Drawing.Size(56, 162);
            this.d6.TabIndex = 208;
            this.d6.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // d5
            // 
            this.d5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.d5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d5.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d5.ForeColor = System.Drawing.Color.Black;
            this.d5.FormattingEnabled = true;
            this.d5.ItemHeight = 18;
            this.d5.Location = new System.Drawing.Point(548, 249);
            this.d5.Name = "d5";
            this.d5.Size = new System.Drawing.Size(56, 162);
            this.d5.TabIndex = 207;
            this.d5.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // d4
            // 
            this.d4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.d4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d4.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d4.ForeColor = System.Drawing.Color.Black;
            this.d4.FormattingEnabled = true;
            this.d4.ItemHeight = 18;
            this.d4.Location = new System.Drawing.Point(487, 249);
            this.d4.Name = "d4";
            this.d4.Size = new System.Drawing.Size(60, 162);
            this.d4.TabIndex = 206;
            this.d4.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // d3
            // 
            this.d3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.d3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d3.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d3.ForeColor = System.Drawing.Color.Black;
            this.d3.FormattingEnabled = true;
            this.d3.HorizontalScrollbar = true;
            this.d3.ItemHeight = 18;
            this.d3.Location = new System.Drawing.Point(388, 249);
            this.d3.Name = "d3";
            this.d3.Size = new System.Drawing.Size(98, 162);
            this.d3.TabIndex = 205;
            this.d3.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // d2
            // 
            this.d2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.d2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d2.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d2.ForeColor = System.Drawing.Color.Black;
            this.d2.FormattingEnabled = true;
            this.d2.ItemHeight = 18;
            this.d2.Location = new System.Drawing.Point(283, 249);
            this.d2.Name = "d2";
            this.d2.Size = new System.Drawing.Size(104, 162);
            this.d2.TabIndex = 204;
            this.d2.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // d1
            // 
            this.d1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.d1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.d1.Font = new System.Drawing.Font("Utsaah", 12F);
            this.d1.ForeColor = System.Drawing.Color.Black;
            this.d1.FormattingEnabled = true;
            this.d1.HorizontalScrollbar = true;
            this.d1.ItemHeight = 18;
            this.d1.Location = new System.Drawing.Point(73, 249);
            this.d1.Name = "d1";
            this.d1.Size = new System.Drawing.Size(209, 162);
            this.d1.TabIndex = 203;
            this.d1.SelectedIndexChanged += new System.EventHandler(this.d1_SelectedIndexChanged);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.BackColor = System.Drawing.Color.Transparent;
            this.label78.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label78.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label78.Location = new System.Drawing.Point(4, 9);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(589, 32);
            this.label78.TabIndex = 174;
            this.label78.Text = "CRUD Deposito: Cadastrar, Seleccionar, Alterar e Deletar ";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label79.ForeColor = System.Drawing.Color.Gray;
            this.label79.Location = new System.Drawing.Point(724, 199);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(152, 20);
            this.label79.TabIndex = 173;
            this.label79.Text = "*Por Data de Deposito :";
            // 
            // heyechRadioButton1
            // 
            this.heyechRadioButton1.BaseColour = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.heyechRadioButton1.BorderColour = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.heyechRadioButton1.Checked = true;
            this.heyechRadioButton1.CheckedColour = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            this.heyechRadioButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.heyechRadioButton1.FontColour = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.heyechRadioButton1.HighlightColour = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.heyechRadioButton1.Location = new System.Drawing.Point(210, 201);
            this.heyechRadioButton1.Name = "heyechRadioButton1";
            this.heyechRadioButton1.Size = new System.Drawing.Size(100, 22);
            this.heyechRadioButton1.TabIndex = 172;
            this.heyechRadioButton1.Text = "Por Nome";
            // 
            // btn_pesqDep
            // 
            this.btn_pesqDep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_pesqDep.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btn_pesqDep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_pesqDep.Font = new System.Drawing.Font("Rod", 12F);
            this.btn_pesqDep.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_pesqDep.Location = new System.Drawing.Point(180, 200);
            this.btn_pesqDep.Name = "btn_pesqDep";
            this.btn_pesqDep.Size = new System.Drawing.Size(21, 23);
            this.btn_pesqDep.TabIndex = 159;
            this.btn_pesqDep.UseVisualStyleBackColor = true;
            this.btn_pesqDep.Click += new System.EventHandler(this.btn_pesqDep_Click);
            // 
            // tb_pesqDep
            // 
            this.tb_pesqDep.Font = new System.Drawing.Font("Rod", 12F);
            this.tb_pesqDep.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_pesqDep.Location = new System.Drawing.Point(3, 200);
            this.tb_pesqDep.Name = "tb_pesqDep";
            this.tb_pesqDep.Size = new System.Drawing.Size(174, 23);
            this.tb_pesqDep.TabIndex = 158;
            this.tb_pesqDep.TextChanged += new System.EventHandler(this.tb_pesqDep_TextChanged);
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label80.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label80.Location = new System.Drawing.Point(7, 175);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(75, 20);
            this.label80.TabIndex = 171;
            this.label80.Text = "*Pesquisar";
            // 
            // data_actual
            // 
            this.data_actual.CalendarForeColor = System.Drawing.SystemColors.ControlDark;
            this.data_actual.CalendarTitleBackColor = System.Drawing.SystemColors.ControlDark;
            this.data_actual.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDark;
            this.data_actual.CalendarTrailingForeColor = System.Drawing.Color.Silver;
            this.data_actual.CustomFormat = "dd-MM-yyyy";
            this.data_actual.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.data_actual.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data_actual.Location = new System.Drawing.Point(895, 120);
            this.data_actual.Name = "data_actual";
            this.data_actual.Size = new System.Drawing.Size(146, 25);
            this.data_actual.TabIndex = 169;
            this.data_actual.Visible = false;
            // 
            // tb_IDalunoDep
            // 
            this.tb_IDalunoDep.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_IDalunoDep.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_IDalunoDep.Location = new System.Drawing.Point(5, 90);
            this.tb_IDalunoDep.Name = "tb_IDalunoDep";
            this.tb_IDalunoDep.Size = new System.Drawing.Size(174, 25);
            this.tb_IDalunoDep.TabIndex = 147;
            // 
            // tb_talão
            // 
            this.tb_talão.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_talão.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_talão.Location = new System.Drawing.Point(223, 89);
            this.tb_talão.Name = "tb_talão";
            this.tb_talão.Size = new System.Drawing.Size(174, 25);
            this.tb_talão.TabIndex = 148;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label82.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label82.Location = new System.Drawing.Point(213, 68);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(48, 20);
            this.label82.TabIndex = 167;
            this.label82.Text = "*Talão";
            // 
            // btn_cadDep
            // 
            this.btn_cadDep.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_cadDep.BackgroundImage = global::PPA.Properties.Resources.disk_blue_ok;
            this.btn_cadDep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cadDep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_cadDep.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_cadDep.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_cadDep.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btn_cadDep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cadDep.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btn_cadDep.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btn_cadDep.Location = new System.Drawing.Point(895, 158);
            this.btn_cadDep.Name = "btn_cadDep";
            this.btn_cadDep.Size = new System.Drawing.Size(30, 28);
            this.btn_cadDep.TabIndex = 156;
            this.btn_cadDep.UseVisualStyleBackColor = false;
            this.btn_cadDep.Click += new System.EventHandler(this.btn_cadDep_Click);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label86.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label86.Location = new System.Drawing.Point(1, 64);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(67, 20);
            this.label86.TabIndex = 160;
            this.label86.Text = "*ID Aluno";
            // 
            // tb_valorDep
            // 
            this.tb_valorDep.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.tb_valorDep.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_valorDep.Location = new System.Drawing.Point(451, 90);
            this.tb_valorDep.Name = "tb_valorDep";
            this.tb_valorDep.Size = new System.Drawing.Size(174, 25);
            this.tb_valorDep.TabIndex = 149;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label87.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label87.Location = new System.Drawing.Point(445, 63);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(105, 20);
            this.label87.TabIndex = 161;
            this.label87.Text = "*Valor Deposito";
            // 
            // data_dep
            // 
            this.data_dep.CalendarForeColor = System.Drawing.SystemColors.ControlDark;
            this.data_dep.CalendarTitleBackColor = System.Drawing.SystemColors.ControlDark;
            this.data_dep.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDark;
            this.data_dep.CalendarTrailingForeColor = System.Drawing.Color.Silver;
            this.data_dep.CustomFormat = "dd-MM-yyyy";
            this.data_dep.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.data_dep.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data_dep.Location = new System.Drawing.Point(895, 89);
            this.data_dep.Name = "data_dep";
            this.data_dep.Size = new System.Drawing.Size(146, 25);
            this.data_dep.TabIndex = 151;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label88.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label88.Location = new System.Drawing.Point(887, 62);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(119, 20);
            this.label88.TabIndex = 163;
            this.label88.Text = "*Data de Deposito";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label89.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label89.Location = new System.Drawing.Point(670, 63);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(51, 20);
            this.label89.TabIndex = 162;
            this.label89.Text = "*Banco";
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(1294, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(35, 518);
            this.panel7.TabIndex = 37;
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(35, 518);
            this.panel8.TabIndex = 35;
            // 
            // tpageEstatis
            // 
            this.tpageEstatis.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tpageEstatis.Controls.Add(this.panel15);
            this.tpageEstatis.Controls.Add(this.TbEstatisticaDen);
            this.tpageEstatis.Controls.Add(this.panel9);
            this.tpageEstatis.Controls.Add(this.panel10);
            this.tpageEstatis.Location = new System.Drawing.Point(4, 5);
            this.tpageEstatis.Name = "tpageEstatis";
            this.tpageEstatis.Padding = new System.Windows.Forms.Padding(3);
            this.tpageEstatis.Size = new System.Drawing.Size(1332, 524);
            this.tpageEstatis.TabIndex = 5;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.panel15.Controls.Add(this.button40);
            this.panel15.Controls.Add(this.button43);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel15.Location = new System.Drawing.Point(38, 477);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(1256, 44);
            this.panel15.TabIndex = 61;
            // 
            // button40
            // 
            this.button40.AutoSize = true;
            this.button40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button40.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button40.FlatAppearance.BorderSize = 0;
            this.button40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button40.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button40.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button40.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button40.Location = new System.Drawing.Point(1214, 7);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(59, 30);
            this.button40.TabIndex = 63;
            this.button40.Text = " (0_0)";
            this.button40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button40.UseVisualStyleBackColor = false;
            // 
            // button43
            // 
            this.button43.AutoSize = true;
            this.button43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(240)))));
            this.button43.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button43.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button43.FlatAppearance.BorderSize = 0;
            this.button43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button43.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button43.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button43.Image = global::PPA.Properties.Resources.h3_fw;
            this.button43.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button43.Location = new System.Drawing.Point(16, 7);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(148, 30);
            this.button43.TabIndex = 58;
            this.button43.Text = "      Estatística";
            this.button43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button43.UseVisualStyleBackColor = false;
            // 
            // TbEstatisticaDen
            // 
            this.TbEstatisticaDen.Controls.Add(this.TbEstatisticaDen_pub);
            this.TbEstatisticaDen.Controls.Add(this.TbEstatisticaDen_CRUD);
            this.TbEstatisticaDen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbEstatisticaDen.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.TbEstatisticaDen.ItemSize = new System.Drawing.Size(0, 1);
            this.TbEstatisticaDen.Location = new System.Drawing.Point(38, 3);
            this.TbEstatisticaDen.Name = "TbEstatisticaDen";
            this.TbEstatisticaDen.SelectedIndex = 0;
            this.TbEstatisticaDen.Size = new System.Drawing.Size(1256, 518);
            this.TbEstatisticaDen.TabIndex = 39;
            // 
            // TbEstatisticaDen_pub
            // 
            this.TbEstatisticaDen_pub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TbEstatisticaDen_pub.Controls.Add(this.label133);
            this.TbEstatisticaDen_pub.Controls.Add(this.label126);
            this.TbEstatisticaDen_pub.Controls.Add(this.label118);
            this.TbEstatisticaDen_pub.Controls.Add(this.label116);
            this.TbEstatisticaDen_pub.Controls.Add(this.label115);
            this.TbEstatisticaDen_pub.Controls.Add(this.label113);
            this.TbEstatisticaDen_pub.Controls.Add(this.label120);
            this.TbEstatisticaDen_pub.Location = new System.Drawing.Point(4, 5);
            this.TbEstatisticaDen_pub.Name = "TbEstatisticaDen_pub";
            this.TbEstatisticaDen_pub.Padding = new System.Windows.Forms.Padding(3);
            this.TbEstatisticaDen_pub.Size = new System.Drawing.Size(1248, 509);
            this.TbEstatisticaDen_pub.TabIndex = 0;
            // 
            // label133
            // 
            this.label133.BackColor = System.Drawing.Color.Transparent;
            this.label133.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label133.ForeColor = System.Drawing.Color.SteelBlue;
            this.label133.Location = new System.Drawing.Point(940, 183);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(63, 30);
            this.label133.TabIndex = 35;
            this.label133.Text = "54%";
            // 
            // label126
            // 
            this.label126.BackColor = System.Drawing.Color.Transparent;
            this.label126.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 13F);
            this.label126.ForeColor = System.Drawing.Color.SteelBlue;
            this.label126.Location = new System.Drawing.Point(889, 292);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(159, 66);
            this.label126.TabIndex = 34;
            this.label126.Text = "Propinas Actualizada (Por mês)";
            // 
            // label118
            // 
            this.label118.BackColor = System.Drawing.Color.Transparent;
            this.label118.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label118.ForeColor = System.Drawing.Color.SteelBlue;
            this.label118.Location = new System.Drawing.Point(557, 183);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(63, 30);
            this.label118.TabIndex = 33;
            this.label118.Text = "42%";
            // 
            // label116
            // 
            this.label116.BackColor = System.Drawing.Color.Transparent;
            this.label116.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 13F);
            this.label116.ForeColor = System.Drawing.Color.SteelBlue;
            this.label116.Location = new System.Drawing.Point(515, 293);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(146, 49);
            this.label116.TabIndex = 32;
            this.label116.Text = "Depositos Efectados (Por mês)";
            // 
            // label115
            // 
            this.label115.BackColor = System.Drawing.Color.Transparent;
            this.label115.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label115.ForeColor = System.Drawing.Color.SteelBlue;
            this.label115.Location = new System.Drawing.Point(175, 183);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(63, 30);
            this.label115.TabIndex = 31;
            this.label115.Text = "36%";
            // 
            // label113
            // 
            this.label113.BackColor = System.Drawing.Color.Transparent;
            this.label113.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 13F);
            this.label113.ForeColor = System.Drawing.Color.SteelBlue;
            this.label113.Location = new System.Drawing.Point(136, 292);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(140, 49);
            this.label113.TabIndex = 30;
            this.label113.Text = "Alunos Cadastrado (Ano Em Curso)";
            this.label113.Click += new System.EventHandler(this.label113_Click);
            // 
            // label120
            // 
            this.label120.BackColor = System.Drawing.Color.Transparent;
            this.label120.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 20F);
            this.label120.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.label120.Location = new System.Drawing.Point(0, 3);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(178, 54);
            this.label120.TabIndex = 28;
            this.label120.Text = "Estatística";
            // 
            // TbEstatisticaDen_CRUD
            // 
            this.TbEstatisticaDen_CRUD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.TbEstatisticaDen_CRUD.Location = new System.Drawing.Point(4, 5);
            this.TbEstatisticaDen_CRUD.Name = "TbEstatisticaDen_CRUD";
            this.TbEstatisticaDen_CRUD.Padding = new System.Windows.Forms.Padding(3);
            this.TbEstatisticaDen_CRUD.Size = new System.Drawing.Size(1248, 509);
            this.TbEstatisticaDen_CRUD.TabIndex = 3;
            // 
            // panel9
            // 
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(1294, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(35, 518);
            this.panel9.TabIndex = 40;
            // 
            // panel10
            // 
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(3, 3);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(35, 518);
            this.panel10.TabIndex = 38;
            // 
            // lbMini
            // 
            this.lbMini.ActiveLinkColor = System.Drawing.SystemColors.GrayText;
            this.lbMini.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMini.BackColor = System.Drawing.Color.Transparent;
            this.lbMini.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbMini.Font = new System.Drawing.Font("Vrinda", 30F, System.Drawing.FontStyle.Bold);
            this.lbMini.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.lbMini.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lbMini.LinkColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbMini.Location = new System.Drawing.Point(1266, 0);
            this.lbMini.Name = "lbMini";
            this.lbMini.Size = new System.Drawing.Size(41, 33);
            this.lbMini.TabIndex = 24;
            this.lbMini.TabStop = true;
            this.lbMini.Text = "-";
            this.lbMini.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbMini.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbMini_LinkClicked);
            // 
            // panelRodSuperio
            // 
            this.panelRodSuperio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(64)))));
            this.panelRodSuperio.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelRodSuperio.Location = new System.Drawing.Point(0, 0);
            this.panelRodSuperio.Name = "panelRodSuperio";
            this.panelRodSuperio.Size = new System.Drawing.Size(1340, 15);
            this.panelRodSuperio.TabIndex = 13;
            // 
            // lb_fech
            // 
            this.lb_fech.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_fech.AutoSize = true;
            this.lb_fech.DisabledLinkColor = System.Drawing.Color.DimGray;
            this.lb_fech.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_fech.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lb_fech.LinkColor = System.Drawing.Color.Gray;
            this.lb_fech.Location = new System.Drawing.Point(1313, 18);
            this.lb_fech.Name = "lb_fech";
            this.lb_fech.Size = new System.Drawing.Size(21, 20);
            this.lb_fech.TabIndex = 16;
            this.lb_fech.TabStop = true;
            this.lb_fech.Text = "X";
            this.lb_fech.VisitedLinkColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_fech.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lb_fech_LinkClicked);
            // 
            // CbAnProp
            // 
            this.CbAnProp.BackColor = System.Drawing.Color.White;
            this.CbAnProp.Controls.Add(this.lbNomeUsuario);
            this.CbAnProp.Controls.Add(this.label1);
            this.CbAnProp.Controls.Add(this.label3);
            this.CbAnProp.Controls.Add(this.lbNivel);
            this.CbAnProp.Controls.Add(this.panellinhacima);
            this.CbAnProp.Controls.Add(this.pictureBox1);
            this.CbAnProp.Controls.Add(this.lb_fech);
            this.CbAnProp.Controls.Add(this.panelRodSuperio);
            this.CbAnProp.Controls.Add(this.lbMini);
            this.CbAnProp.Controls.Add(this.pictureBox2);
            this.CbAnProp.Dock = System.Windows.Forms.DockStyle.Top;
            this.CbAnProp.Location = new System.Drawing.Point(0, 0);
            this.CbAnProp.Name = "CbAnProp";
            this.CbAnProp.Size = new System.Drawing.Size(1340, 147);
            this.CbAnProp.TabIndex = 34;
            // 
            // lbNomeUsuario
            // 
            this.lbNomeUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbNomeUsuario.BackColor = System.Drawing.Color.Transparent;
            this.lbNomeUsuario.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.lbNomeUsuario.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbNomeUsuario.Location = new System.Drawing.Point(1175, 96);
            this.lbNomeUsuario.Name = "lbNomeUsuario";
            this.lbNomeUsuario.Size = new System.Drawing.Size(155, 20);
            this.lbNomeUsuario.TabIndex = 44;
            this.lbNomeUsuario.Text = "Utilizador Normal";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(1108, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 20);
            this.label3.TabIndex = 43;
            this.label3.Text = "Nivel***:";
            // 
            // panellinhacima
            // 
            this.panellinhacima.BackColor = System.Drawing.Color.Gainsboro;
            this.panellinhacima.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.panellinhacima.Location = new System.Drawing.Point(3, 146);
            this.panellinhacima.Name = "panellinhacima";
            this.panellinhacima.Size = new System.Drawing.Size(1340, 1);
            this.panellinhacima.TabIndex = 38;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PPA.Properties.Resources.jkiu_fw;
            this.pictureBox1.Location = new System.Drawing.Point(58, 86);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(163, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PPA.Properties.Resources._15824271_1216074758470968_68670057_o;
            this.pictureBox2.Location = new System.Drawing.Point(-1, 37);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(168, 58);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 25;
            this.pictureBox2.TabStop = false;
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelMenu.Controls.Add(this.btnDeposit);
            this.panelMenu.Controls.Add(this.btnEstast);
            this.panelMenu.Controls.Add(this.btnPropina);
            this.panelMenu.Controls.Add(this.btnAluno);
            this.panelMenu.Controls.Add(this.btnInicio);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenu.Location = new System.Drawing.Point(0, 147);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(1340, 31);
            this.panelMenu.TabIndex = 35;
            // 
            // btnDeposit
            // 
            this.btnDeposit.AutoSize = true;
            this.btnDeposit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDeposit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeposit.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnDeposit.FlatAppearance.BorderSize = 0;
            this.btnDeposit.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDeposit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnDeposit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeposit.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnDeposit.ForeColor = System.Drawing.Color.Gray;
            this.btnDeposit.Image = global::PPA.Properties.Resources.h76_fw;
            this.btnDeposit.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnDeposit.Location = new System.Drawing.Point(603, 2);
            this.btnDeposit.Name = "btnDeposit";
            this.btnDeposit.Size = new System.Drawing.Size(198, 30);
            this.btnDeposit.TabIndex = 17;
            this.btnDeposit.Text = "        Depositos";
            this.btnDeposit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeposit.UseVisualStyleBackColor = false;
            this.btnDeposit.Click += new System.EventHandler(this.btnDeposit_Click);
            // 
            // btnEstast
            // 
            this.btnEstast.AutoSize = true;
            this.btnEstast.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEstast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnEstast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEstast.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnEstast.FlatAppearance.BorderSize = 0;
            this.btnEstast.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnEstast.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnEstast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstast.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnEstast.ForeColor = System.Drawing.Color.Gray;
            this.btnEstast.Image = global::PPA.Properties.Resources.h3_fw;
            this.btnEstast.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEstast.Location = new System.Drawing.Point(803, 2);
            this.btnEstast.Name = "btnEstast";
            this.btnEstast.Size = new System.Drawing.Size(198, 30);
            this.btnEstast.TabIndex = 3;
            this.btnEstast.Text = "        Estatística";
            this.btnEstast.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstast.UseVisualStyleBackColor = false;
            this.btnEstast.Click += new System.EventHandler(this.btnEstast_Click);
            // 
            // btnPropina
            // 
            this.btnPropina.AutoSize = true;
            this.btnPropina.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPropina.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPropina.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPropina.FlatAppearance.BorderSize = 0;
            this.btnPropina.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnPropina.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnPropina.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPropina.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnPropina.ForeColor = System.Drawing.Color.Gray;
            this.btnPropina.Image = global::PPA.Properties.Resources.h5_fw;
            this.btnPropina.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPropina.Location = new System.Drawing.Point(405, 2);
            this.btnPropina.Name = "btnPropina";
            this.btnPropina.Size = new System.Drawing.Size(198, 30);
            this.btnPropina.TabIndex = 2;
            this.btnPropina.Text = "        Propina";
            this.btnPropina.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPropina.UseVisualStyleBackColor = false;
            this.btnPropina.Click += new System.EventHandler(this.btnPropina_Click);
            // 
            // btnAluno
            // 
            this.btnAluno.AutoSize = true;
            this.btnAluno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAluno.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAluno.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAluno.FlatAppearance.BorderSize = 0;
            this.btnAluno.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAluno.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnAluno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAluno.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnAluno.ForeColor = System.Drawing.Color.Gray;
            this.btnAluno.Image = global::PPA.Properties.Resources.h4_fw;
            this.btnAluno.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAluno.Location = new System.Drawing.Point(207, 2);
            this.btnAluno.Name = "btnAluno";
            this.btnAluno.Size = new System.Drawing.Size(198, 30);
            this.btnAluno.TabIndex = 1;
            this.btnAluno.Text = "        Aluno";
            this.btnAluno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAluno.UseVisualStyleBackColor = false;
            this.btnAluno.Click += new System.EventHandler(this.btnAluno_Click);
            // 
            // btnInicio
            // 
            this.btnInicio.AutoSize = true;
            this.btnInicio.BackColor = System.Drawing.Color.White;
            this.btnInicio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnInicio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInicio.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnInicio.FlatAppearance.BorderSize = 0;
            this.btnInicio.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.btnInicio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInicio.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.btnInicio.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btnInicio.Image = global::PPA.Properties.Resources.h2_fw;
            this.btnInicio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInicio.Location = new System.Drawing.Point(4, 3);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Size = new System.Drawing.Size(198, 30);
            this.btnInicio.TabIndex = 0;
            this.btnInicio.Text = "        Inicio";
            this.btnInicio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInicio.UseVisualStyleBackColor = false;
            this.btnInicio.Click += new System.EventHandler(this.btnInicio_Click);
            // 
            // US_menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.TB_CENTRAL);
            this.Controls.Add(this.painelROdape);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.CbAnProp);
            this.Name = "US_menu";
            this.Size = new System.Drawing.Size(1340, 768);
            this.Load += new System.EventHandler(this.US_menu_Load);
            this.painelROdape.ResumeLayout(false);
            this.painelROdape.PerformLayout();
            this.TB_CENTRAL.ResumeLayout(false);
            this.tpagINICIO.ResumeLayout(false);
            this.panelLAT.ResumeLayout(false);
            this.panelLAT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.tb_estaNOinicio.ResumeLayout(false);
            this.pagehome.ResumeLayout(false);
            this.pagehome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tpageALUNO.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.tabContestadentrodoAluno.ResumeLayout(false);
            this.tpdentroAlu_pub.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.tpdentroAlu_CRUD.ResumeLayout(false);
            this.dentroAlu_CRUD.ResumeLayout(false);
            this.tp_Cadastrar.ResumeLayout(false);
            this.tp_Cadastrar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg)).EndInit();
            this.tp_Confirmar.ResumeLayout(false);
            this.tp_Confirmar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgConf)).EndInit();
            this.tp_PesMat_Conf.ResumeLayout(false);
            this.tp_PesMat_Conf.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCompleto)).EndInit();
            this.tp_Alt_det_Matr.ResumeLayout(false);
            this.tp_Alt_det_Matr.PerformLayout();
            this.tp_Alt_det_Conf.ResumeLayout(false);
            this.tp_Alt_det_Conf.PerformLayout();
            this.tpagePROPRINA.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.TbPropinaDen.ResumeLayout(false);
            this.TbPropinaDen_pub.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            this.TbPropinaDen_crud.ResumeLayout(false);
            this.TbPropinaDen_crud.PerformLayout();
            this.LvalorDesc.ResumeLayout(false);
            this.LvalorDesc.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.TbPropinaDen_propina.ResumeLayout(false);
            this.TbPropinaDen_propina.PerformLayout();
            this.tpageDEPOSITO.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.TbDepositoDen.ResumeLayout(false);
            this.TbDepositoDen_pub.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.TbDepositoDen_CRD.ResumeLayout(false);
            this.DepositoDen.ResumeLayout(false);
            this.DepositoDen_cadasDep.ResumeLayout(false);
            this.DepositoDen_cadasDep.PerformLayout();
            this.tpageEstatis.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.TbEstatisticaDen.ResumeLayout(false);
            this.TbEstatisticaDen_pub.ResumeLayout(false);
            this.CbAnProp.ResumeLayout(false);
            this.CbAnProp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel painelROdape;
        private System.Windows.Forms.TabControl TB_CENTRAL;
        private System.Windows.Forms.TabPage tpagINICIO;
        private System.Windows.Forms.TabPage tpageALUNO;
        private System.Windows.Forms.Panel panelLAT;
        private System.Windows.Forms.Panel panelVertical;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.LinkLabel lbMini;
        private System.Windows.Forms.Panel panelRodSuperio;
        private System.Windows.Forms.LinkLabel lb_fech;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel CbAnProp;
        private System.Windows.Forms.Panel panellinhacima;
        internal System.Windows.Forms.Button btnInicio;
        private System.Windows.Forms.Button btnAluno;
        private System.Windows.Forms.Button btnPropina;
        private System.Windows.Forms.Button btnEstast;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Label label2;
        private Heyech_Application_Development.HeyechTabControl tb_estaNOinicio;
        private System.Windows.Forms.TabPage pagehome;
        private System.Windows.Forms.Label lbNivel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbNomeUsuario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel painelL2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panelL1;
        private System.Windows.Forms.Panel panelL0;
        private System.Windows.Forms.Label lb_data;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private Heyech_Application_Development.HeyechTabControl tabContestadentrodoAluno;
        private System.Windows.Forms.TabPage tpdentroAlu_pub;
        private System.Windows.Forms.TabPage tpdentroAlu_CRUD;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tpagePROPRINA;
        private Heyech_Application_Development.HeyechTabControl TbPropinaDen;
        private System.Windows.Forms.TabPage TbPropinaDen_pub;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TabPage TbPropinaDen_crud;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnDeposit;
        private System.Windows.Forms.TabPage tpageDEPOSITO;
        private Heyech_Application_Development.HeyechTabControl TbDepositoDen;
        private System.Windows.Forms.TabPage TbDepositoDen_pub;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TabPage tpageEstatis;
        private Heyech_Application_Development.HeyechTabControl TbEstatisticaDen;
        private System.Windows.Forms.TabPage TbEstatisticaDen_pub;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.TabPage TbEstatisticaDen_CRUD;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Button button54;
        private Heyech_Application_Development.HeyechTabControl dentroAlu_CRUD;
        private System.Windows.Forms.TabPage tp_Cadastrar;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button btn_cadMarti;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox tb_genero;
        private System.Windows.Forms.TextBox tb_pai;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tb_mae;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tb_nome;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.DateTimePicker da;
        private System.Windows.Forms.TextBox tb_morada;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TabPage tp_Confirmar;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.TextBox tb_bi;
        private Heyech_Application_Development.HeyechRadioButton rb_nomeConf;
        private System.Windows.Forms.Button btn_pesConf;
        private System.Windows.Forms.TextBox tb_pesqConf;
        private System.Windows.Forms.Label lp;
        private System.Windows.Forms.DataGridView dgConf;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.ComboBox tb_status;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.TextBox tb_confMatri;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox tb_anoConf;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox tb_sala;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox tb_classe;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox tb_curso;
        private System.Windows.Forms.ComboBox tb_turno;
        private System.Windows.Forms.TabPage tp_PesMat_Conf;
        private Heyech_Application_Development.HeyechRadioButton heyechRadioButton70;
        private System.Windows.Forms.Button btn_pesqCompleto;
        private System.Windows.Forms.TextBox tb_pesqCompleto;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.DataGridView dgCompleto;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button btnTerminar;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox tb_An;
        private System.Windows.Forms.Button btn_Confir;
        private System.Windows.Forms.Label lpA;
        private System.Windows.Forms.ComboBox tb_AnC;
        private System.Windows.Forms.DateTimePicker dataHoje;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.ComboBox Cb_AnoCompleto;
        private System.Windows.Forms.TabPage tp_Alt_det_Matr;
        private System.Windows.Forms.TabPage tp_Alt_det_Conf;
        private Heyech_Application_Development.HeyechCheckBox chec_verMat;
        private Heyech_Application_Development.HeyechCheckBox chec_verConf;
        private System.Windows.Forms.Label lpMatAno;
        private System.Windows.Forms.ComboBox CbAn;
        private Heyech_Application_Development.HeyechRadioButton rb_nome2;
        private System.Windows.Forms.Button btnPesqCad;
        private System.Windows.Forms.TextBox tb_pesqCad;
        private System.Windows.Forms.Label lpMat;
        private System.Windows.Forms.DataGridView dg;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnAbriralt_del_Conf;
        private System.Windows.Forms.Button btnAbriAlt_del_Matr;
        private System.Windows.Forms.Button btnAbrir_Cad_matr;
        private System.Windows.Forms.Button btnAbrirSele_alu;
        private System.Windows.Forms.Button btnAbrirConf;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox tbAno_alt_del_Mat;
        private System.Windows.Forms.TextBox tbId_alt_del_Mat;
        private System.Windows.Forms.TextBox tbBi_alt_del_Mat;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox tbGenero_alt_del_Mat;
        private System.Windows.Forms.TextBox tbPai_alt_del_Mat;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox tbMae_alt_del_Mat;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox tbNome_alt_del_Mat;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.DateTimePicker da_alt_del_Mat;
        private System.Windows.Forms.TextBox tbMorada_alt_del_Mat;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Button btnPesq_alt_del_Mat;
        private System.Windows.Forms.TextBox tbPesq_alt_del_Mat;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.ComboBox cbAninho_alt_del_Mat;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.ComboBox cbAno_alt_del_Conf;
        private System.Windows.Forms.Button btnPesq__alt_del_Conf;
        private System.Windows.Forms.TextBox tbPesq_alt_del_Conf;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox tbStatus_alt_del_Conf;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.TextBox tbId_alt_del_Conf;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox tbAno_alt_del_Conf;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.ComboBox tbSala_alt_del_Conf;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.ComboBox tbClasse_alt_del_Conf;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.ComboBox tbCurso_alt_del_Conf;
        private System.Windows.Forms.ComboBox tbTurno_alt_del_Conf;
        private System.Windows.Forms.ListBox l9;
        private System.Windows.Forms.ListBox l8;
        private System.Windows.Forms.ListBox l7;
        private System.Windows.Forms.ListBox l6;
        private System.Windows.Forms.ListBox l5;
        private System.Windows.Forms.ListBox l4;
        private System.Windows.Forms.ListBox l3;
        private System.Windows.Forms.ListBox l2;
        private System.Windows.Forms.ListBox l1;
        private System.Windows.Forms.Button btnDeletarMAt;
        private System.Windows.Forms.Button btnAtualizarMAt;
        private System.Windows.Forms.ListBox lb8;
        private System.Windows.Forms.ListBox lb7;
        private System.Windows.Forms.ListBox lb6;
        private System.Windows.Forms.ListBox lb5;
        private System.Windows.Forms.ListBox lb4;
        private System.Windows.Forms.ListBox lb3;
        private System.Windows.Forms.ListBox lb2;
        private System.Windows.Forms.ListBox lb1;
        private System.Windows.Forms.ListBox lb10;
        private System.Windows.Forms.ListBox lb9;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Button tbnActualiza_alt_del_Conf;
        private System.Windows.Forms.Button btnDeletar__alt_del_Conf;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.TabPage TbDepositoDen_CRD;
        private Heyech_Application_Development.HeyechTabControl DepositoDen;
        private System.Windows.Forms.TabPage DepositoDen_cadasDep;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private Heyech_Application_Development.HeyechRadioButton heyechRadioButton1;
        private System.Windows.Forms.Button btn_pesqDep;
        private System.Windows.Forms.TextBox tb_pesqDep;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.DateTimePicker data_actual;
        private System.Windows.Forms.TextBox tb_IDalunoDep;
        private System.Windows.Forms.TextBox tb_talão;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Button btn_cadDep;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox tb_valorDep;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.DateTimePicker data_dep;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.ListBox d6;
        private System.Windows.Forms.ListBox d5;
        private System.Windows.Forms.ListBox d4;
        private System.Windows.Forms.ListBox d3;
        private System.Windows.Forms.ListBox d2;
        private System.Windows.Forms.ListBox d1;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.DateTimePicker dataPesqDep;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button btn_deletDep;
        private System.Windows.Forms.Button btn_actDep;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.ListBox d;
        private System.Windows.Forms.ListBox dd;
        private System.Windows.Forms.TextBox tb_IDATODEP;
        private System.Windows.Forms.ComboBox tb_bancoDep;
        private System.Windows.Forms.ComboBox tb_d;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.ListBox ddd;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.ListBox sal;
        private System.Windows.Forms.ComboBox cb_cla;
        private System.Windows.Forms.Label lb_ferra;
        private System.Windows.Forms.Button btn_ferra;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.ListBox c7;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.ListBox c;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.ListBox c6;
        private System.Windows.Forms.ListBox c5;
        private System.Windows.Forms.ListBox c4;
        private System.Windows.Forms.ListBox c3;
        private System.Windows.Forms.ListBox c2;
        private System.Windows.Forms.ListBox c1;
        private System.Windows.Forms.Button button12;
        private Heyech_Application_Development.HeyechRadioButton prop_nome;
        private System.Windows.Forms.TextBox tb_pesqProp;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Panel LvalorDesc;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label btn_cadPAg;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.ComboBox tb_mesdesc;
        private System.Windows.Forms.ComboBox tb_numMes;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Button btnCadPAg;
        private System.Windows.Forms.DateTimePicker datadoPAG;
        private System.Windows.Forms.Label Classepreco;
        private System.Windows.Forms.ListBox Lmeses;
        private System.Windows.Forms.Button btn_CadImprimir;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.TabPage TbPropinaDen_propina;
        internal System.Windows.Forms.Label Lturno;
        internal System.Windows.Forms.Label Lclasse;
        internal System.Windows.Forms.Label Lcurso;
        internal System.Windows.Forms.Label Lnome;
        internal System.Windows.Forms.Label Lid;
        internal System.Windows.Forms.Label Lsaldo;
        internal System.Windows.Forms.Label Lano;
        internal System.Windows.Forms.Label Lsala;
        internal System.Windows.Forms.Label precott;
        private System.Windows.Forms.ListBox a;
        private System.Windows.Forms.ComboBox tb_Anoprop;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.ListBox a1;
        private System.Windows.Forms.Button btn_delProp;
        private System.Windows.Forms.Button btn_actProp;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.ListBox a6;
        private System.Windows.Forms.ListBox a5;
        private System.Windows.Forms.ListBox a4;
        private System.Windows.Forms.ListBox a3;
        private System.Windows.Forms.ListBox a2;
        private System.Windows.Forms.Label label125;
        private Heyech_Application_Development.HeyechRadioButton heyechRadioButton2;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.TextBox tb_Peprop;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox tb_IDpropina;
        private System.Windows.Forms.TextBox tb_Mesprop;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.DateTimePicker data_prop;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.ComboBox tb_Userprop;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox tb_pagamentoID;
        private System.Windows.Forms.ComboBox cbANOpag;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.TextBox textNOme;
        private System.Windows.Forms.ListBox ax4;
        private System.Windows.Forms.ListBox ax5;
        private System.Windows.Forms.ListBox ax3;
        private System.Windows.Forms.ListBox ax2;
        private System.Windows.Forms.ListBox ax1;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TextBox textClasse;
        private System.Windows.Forms.TextBox textSaldo;
        private System.Windows.Forms.TextBox textTurno;
        private System.Windows.Forms.TextBox textSala;
        private System.Windows.Forms.TextBox textCurso;
        private System.Windows.Forms.Label txtOBSmes;
        private System.Windows.Forms.Label txtdatOBS;
    }
}
