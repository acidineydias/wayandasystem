﻿namespace PPA
{
    partial class frm_pag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_pag));
            this.button1 = new System.Windows.Forms.Button();
            this.uS_Pag1 = new PPA.US_Pag();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 12F);
            this.button1.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.button1.Location = new System.Drawing.Point(317, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 27);
            this.button1.TabIndex = 1;
            this.button1.Text = "Imprimir";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // uS_Pag1
            // 
            this.uS_Pag1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.uS_Pag1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uS_Pag1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uS_Pag1.Location = new System.Drawing.Point(0, 0);
            this.uS_Pag1.Name = "uS_Pag1";
            this.uS_Pag1.Size = new System.Drawing.Size(597, 736);
            this.uS_Pag1.TabIndex = 0;
            // 
            // frm_pag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(597, 736);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.uS_Pag1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_pag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_pag";
            this.Load += new System.EventHandler(this.frm_pag_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private US_Pag uS_Pag1;
        private System.Windows.Forms.Button button1;
    }
}