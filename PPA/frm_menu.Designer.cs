﻿namespace PPA
{
    partial class frm_menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uS_menu1 = new PPA.US_menu();
            this.SuspendLayout();
            // 
            // uS_menu1
            // 
            this.uS_menu1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.uS_menu1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uS_menu1.Location = new System.Drawing.Point(0, 0);
            this.uS_menu1.Name = "uS_menu1";
            this.uS_menu1.Size = new System.Drawing.Size(1292, 768);
            this.uS_menu1.TabIndex = 0;
            this.uS_menu1.Load += new System.EventHandler(this.uS_menu1_Load);
            // 
            // frm_menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1292, 768);
            this.Controls.Add(this.uS_menu1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private US_menu uS_menu1;
    }
}